#!/bin/bash

echo -e "\e[04;36m++++++\e[04;31m Cleaning \e[04;36m++++++\e[0m" &&
make clean --no-print-directory && 
echo -e "\e[04;36m++++++\e[04;31m Compiling \e[04;36m++++++\e[0m" &&
make all --no-print-directory && 
echo -e "\e[04;36m++++++\e[04;31m Generating documentation \e[04;36m++++++\e[0m" &&
make clean-doc && make doxygen-doc &&
echo -e "\e[04;36m++++++\e[04;31m Deploying deb packages to target systems \e[04;36m++++++\e[0m" &&
make clean-deb && make deb --no-print-directory > /dev/null 2>&1 &&
make deb-deploy  

