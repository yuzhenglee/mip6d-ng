/*!
 *
 * \defgroup iapi Internal API
 * \defgroup eapi External API
 * \defgroup internal Internal operations

 * \mainpage mip6d-ng Documentation
 *
 * \section main-intro Introduction
 *
 * Next Generation of Mobile IPv6 for Linux includes several client-based mobile IPv6 solutions, 
 * such as NEMO, MCoA, FMIP, HMIP, etc., and also integrates the paradigm of distributed and dynamic 
 * mobility management to enhance applicability and encourage its usage in future mobile systems. 
 * The new daemon and the corresponding applications will be released under GPLv3 license.
 *
 * The implementation is modular, plug-in based, naturally supports cross-layer information exchange 
 * for optimization purposes and it has well documented APIs for further extensions. The mobile IPv6 
 * extensions will be released as separate modules, which could be loaded dynamically to enable or disable 
 * these features. Future developers easily could re-implement and replace these plug-ins.
 *
 * This project is the part of the CONCERTO project. The research leading to these results 
 * has received funding from the European Union's Seventh Framework Programme ([FP7/2007-2013]) under 
 * grant agreement n° 288502.
 * 
 * \section status Current Status
 *
 * Current version of mip6d-ng supports basic functionalities and integrates features from the following 
 * IETF RFCs:
 *  - RFC 6275 - Mobility Support in IPv6
 *  - RFC 5648 - Multiple Care-of Addresses Registration
 *  - RFC 6088 & 6089 - Flow Bindings in Mobile IPv6 and Network Mobility (NEMO) Basic Support
 *
 * \section about About mip6d-ng
 * 
 * The most important design consideration of mip6d-ng focuses for easy development and extension.
 * It means, that the functionalities has been implemented in separate modules (plug-ins), which
 * could to be replaced with new modules, or could to be extended with new plug-ins. The core binary
 * is responsible only for module loading, and provides powerful communication techniques for modules.
 * 
 * \section main-howto-doc How to use this documentation
 * 
 * This documentation organizes the informations into three sections:
 *  - \ref iapi : Messages and APIs provided (and used) by modules
 *  - \ref eapi : External API for applications (libmip6api)
 *  - \ref internal : Internal operations of modules
 *
 * Developers could find usefull information about module dependencies on the next page: \ref moduledepsgraph.
 *
 * Users could find information about module dependency for configuration on the next page: \ref moduledeps.
 *
 */


