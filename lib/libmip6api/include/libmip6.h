
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup eapi-libapi
 * \{
 */

#ifndef _LIBMIP6_H
#define _LIBMIP6_H

#include <netinet/in.h>
#include <inttypes.h>

struct libmip6h;

struct libmip6h * libmip6_init(struct in6_addr * addr, in_port_t port);
int libmip6_close(struct libmip6h * handle);
uint8_t libmip6_get_seq(struct libmip6h * handle);
uint8_t libmip6_get_last_seq(struct libmip6h * handle);
int libmip6_send(struct libmip6h * handle, char * module, uint16_t cmd, uint8_t seq, unsigned char * buf, unsigned int blen);

/*!
 * \brief Error codes
 */
enum libmip6_error_codes {
	/*! Success */
	LIBMIP6_OK = 0,
	/*! Unknown module */
	LIBMIP6_UNKNOWN_MOD,
	/*! Unknown command */
	LIBMIP6_UNKNOWN_CMD,
	/*! Invalid argument */
	LIBMIP6_INVALID,
	/*! Not enough memory */
	LIBMIP6_NOMEM,
	/*! Connection issue */
	LIBMIP6_CONN,
	/*! Disconnection issue */
	LIBMIP6_DISCONN,
	/*! Send issue */
	LIBMIP6_SEND,
	/*! Receive issue */
	LIBMIP6_RECV,
	/*! Operation timed out */
	LIBMIP6_TOUT,
	/*! Message format issue */
	LIBMIP6_MESSAGE,
	/*! No answer */
	LIBMIP6_NOANSW,
	LIBMIP6_UNKNOWN
};

//data.c

/*!
 * \brief Binding Update List Entry
 */
struct libmip6_bule {
	//copy from modules/data/include/mip6d-ng/data.h
	/*! Destination address */
	struct in6_addr dest;
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! Binding lifetime */
	uint16_t lifetime;
	/*! Remaining binding lifetime */
	uint16_t lifetime_rem;
	/*! Time of sending last Binding Update message */
	int32_t last_bu;
	/*! BULE flags */
	uint32_t flags;
	/*! Interface identifier */
	uint32_t ifindex;
};

int data_get_bul(struct libmip6h * handle, struct libmip6_bule ** bul, unsigned int * entries);
int data_free_bul(struct libmip6h * handle, struct libmip6_bule * bul);

/*!
 * \brief Binding Cache Entry
 */
struct libmip6_bce {
	//copy from modules/data/include/mip6d-ng/data.h
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! Binding lifetime */
	uint16_t lifetime;
	/*! BCE flags */
	uint32_t flags;
};

int data_get_bc(struct libmip6h * handle, struct libmip6_bce ** bc, unsigned int * entries);
int data_free_bc(struct libmip6h * handle, struct libmip6_bce * bc);

//mcoa.c

/*!
 * \brief Binding Update List Entry - MCoA version
 */
struct libmip6_bule_mcoa {
	//copy from modules/mcoa/include/mip6d-ng/mcoa.h
	/*! Destination address */
	struct in6_addr dest;
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! Binding lifetime */
	uint16_t lifetime;
	/*! Remaining binding lifetime */
	uint16_t lifetime_rem;
	/*! Time of sending last Binding Update message */
	int32_t last_bu;
	/*! BULE flags */
	uint32_t flags;
	/*! Interface identifier */
	uint32_t ifindex;
	/*! Binding Identifier */
	uint16_t bid;
	/*! Priority */
	uint8_t prio;
};

int mcoa_get_bul(struct libmip6h * handle, struct libmip6_bule_mcoa ** bul, unsigned int * entries);
int mcoa_free_bul(struct libmip6h * handle, struct libmip6_bule_mcoa * bul);

/*!
 * \brief Binding Cache Entry - MCoA version
 */
struct libmip6_bce_mcoa {
	//copy from modules/mcoa/include/mip6d-ng/mcoa.h
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! Binding lifetime */
	uint16_t lifetime;
	/*! BCE flags */
	uint32_t flags;
	/*! Binding Identifier */
	uint16_t bid;
	/*! Priority */
	uint8_t prio;
};

int mcoa_get_bc(struct libmip6h * handle, struct libmip6_bce_mcoa ** bc, unsigned int * entries);
int mcoa_free_bc(struct libmip6h * handle, struct libmip6_bce_mcoa * bc);

//nemo.c

/*!
 * \brief Binding Update List Entry - NEMO version
 */
struct libmip6_bule_nemo {
	//copy from modules/mcoa/include/mip6d-ng/nemo.h
	/*! Destination address */
	struct in6_addr dest;
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! Binding lifetime */
	uint16_t lifetime;
	/*! Remaining binding lifetime */
	uint16_t lifetime_rem;
	/*! Time of sending last Binding Update message */
	int32_t last_bu;
	/*! BULE flags */
	uint32_t flags;
	/*! Interface identifier */
	uint32_t ifindex;
	/*! Mobile Network Prefix */
	struct in6_addr mnp;
	/*! Mobile Network Prefix Length */
	uint8_t mnp_len;
};

int nemo_get_bul(struct libmip6h * handle, struct libmip6_bule_nemo ** bul, unsigned int * entries);
int nemo_free_bul(struct libmip6h * handle, struct libmip6_bule_nemo * bul);

/*!
 * \brief Binding Cache Entry - NEMO version
 */
struct libmip6_bce_nemo {
	//copy from modules/mcoa/include/mip6d-ng/nemo.h
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! Binding lifetime */
	uint16_t lifetime;
	/*! BCE flags */
	uint32_t flags;
	/*! Mobile Network Prefix */
	struct in6_addr mnp;
	/*! Mobile Network Prefix Length */
	uint8_t mnp_len;
};

int nemo_get_bc(struct libmip6h * handle, struct libmip6_bce_nemo ** bc, unsigned int * entries);
int nemo_free_bc(struct libmip6h * handle, struct libmip6_bce_nemo * bc);

//addr.c

int ctrl_get_haa(struct libmip6h * handle, struct in6_addr * addr);
int ctrl_get_hoa(struct libmip6h * handle, struct in6_addr * addr);

//flows.c

/*! 
 * \brief Flow Binding Authentication key
 */
struct libmip6_flowb_key {
	// copy frm modules/flowb/include/mip6d-ng/flowb.h
	/*! Key */
	unsigned char k[16];
};

int flowb_register(struct libmip6h * handle, struct in6_addr * src, uint8_t splen, uint16_t sport, 
		struct in6_addr * dst, uint8_t dplen, uint16_t dport, uint32_t proto,
		uint16_t bid, uint8_t is_static, struct libmip6_flowb_key * key);

int flowb_update(struct libmip6h * handle, uint16_t bid, uint32_t id);

int flowb_delete(struct libmip6h * handle, unsigned int id);

//log.c

int mip6dng_log(struct libmip6h * handle, char * msg);

#endif

/*! \} */
