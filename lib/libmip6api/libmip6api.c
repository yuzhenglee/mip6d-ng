
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup eapi-libapi
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <libmip6.h>
#include <mip6d-ng/api-cmd-fmt.h>

#include "tcpc.h"
#include "handle.h"
#include "recv.h"

/*!
 * \brief Initialize the libmip6api library
 *
 * It initializes a handle, and connects to mip6d-ng.
 *
 * \param addr Address of mip6d-ng
 * \param port Port of mip6d-ng
 * \return Handle or NULL
 */
struct libmip6h * libmip6_init(struct in6_addr * addr, in_port_t port)
{
    int ret;
	struct libmip6h * handle;

	handle = malloc(sizeof(struct libmip6h));
    if (handle == NULL)
        return NULL;
    
    memset(&handle->tcph, 0, sizeof(struct tcpc_handle));

	pthread_mutex_init(&handle->lock, NULL);
	handle->seq = 0;
	pthread_mutex_init(&handle->recv_mutex, NULL);
	pthread_cond_init(&handle->recv_cond, NULL);
	handle->recv_msg = NULL;
	handle->recv_mlen = 0;

    handle->tcph.is_i4 = 0;
    handle->tcph.is_i6 = 1;
    memcpy(&handle->tcph.i6.remote, addr, sizeof(struct in6_addr));
    handle->tcph.i6.remotep = port;
    
    ret = tcpc_init(&handle->tcph, recv_fn, handle);
    if (ret)
        return NULL;
    
    return handle;
}

/*!
 * \brief Close the libmip6api connection
 *
 * \param handle Handle
 */
int libmip6_close(struct libmip6h * handle)
{
	int ret;

    if (handle == NULL)
        return LIBMIP6_INVALID;
    
    ret = tcpc_close(&handle->tcph);
	if (ret)
		return -LIBMIP6_UNKNOWN;

	return 0;
}

/*!
 * \brief Get the next sequence number for communication
 *
 * \param handle Handle
 * \return Sequence number or zero
 */
uint8_t libmip6_get_seq(struct libmip6h * handle)
{
	uint8_t seq;

	if (handle == NULL)
		return 0;

	pthread_mutex_lock(&handle->lock);
	seq = handle->seq++;
	pthread_mutex_unlock(&handle->lock);

	return seq;
}

/*!
 * \brief Get the last applied sequence number for communication
 *
 * \param handle Handle
 * \return Sequence number or zero
 */
uint8_t libmip6_get_last_seq(struct libmip6h * handle)
{
	uint8_t seq;

	if (handle == NULL)
		return 0;

	pthread_mutex_lock(&handle->lock);
	seq = handle->seq - 1;
	pthread_mutex_unlock(&handle->lock);

	return seq;
}

/*!
 * \brief Send a message (command) to mip6d-ng
 *
 * \param handle Handle 
 * \param module Name of destination module
 * \param cmd Command ID
 * \param seq Message sequence number
 * \param buf Message (command) payload
 * \param blen Message (command) length
 * \return Number of sent bytes or negative
 */
int libmip6_send(struct libmip6h * handle, char * module, uint16_t cmd, uint8_t seq, unsigned char * buf, unsigned int blen)
{
	int ret;
	unsigned char message[blen + sizeof(struct mip6dng_hdr)];
	unsigned int mlen;
	struct mip6dng_hdr * hdr;
	unsigned char * body;

	if (handle == NULL || module == NULL)
		return -LIBMIP6_INVALID;

	hdr = (struct mip6dng_hdr *)message;
	strncpy(hdr->module, module, sizeof(hdr->module));
	hdr->cmd = htons(cmd);
	hdr->seq = seq;
	body = (unsigned char *)(hdr + 1);
	if (buf != NULL)
		memcpy(body, buf, blen);
	mlen = sizeof(struct mip6dng_hdr) + blen;

	pthread_mutex_lock(&handle->recv_mutex);
	handle->recv_mlen = -1;
	pthread_mutex_unlock(&handle->recv_mutex);

	ret = tcpc_send(&handle->tcph, message, mlen);
	if (ret <= 0)
		return -LIBMIP6_SEND;

	return 0;
}

/*! \} */
