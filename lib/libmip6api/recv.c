
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-libapi
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>

#include <libmip6.h>
#include <mip6d-ng/api-cmd-fmt.h>

#include "tcpc.h"
#include "handle.h"
#include "recv.h"

#ifndef DISABLE_TCPC_DEBUG
#define DEBUG(fmt, ...)     fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...)     fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#else
#define DEBUG(fmt, ...)     
#define ERROR(fmt, ...)     
#endif      

/*!
 * \brief receive (parse) function
 *
 * It parses the header of the api command reply, 
 * and notifies the waiting thread about the reception.
 *
 * \param tcp_handle TCP handle
 * \param buf Received message
 * \param blen Length of the received message
 * \param priv libmip6api handle
 */
void recv_fn(struct tcpc_handle * tcp_handle, unsigned char * buf, unsigned int blen, void * priv) 
{
	struct libmip6h * handle = (struct libmip6h *)priv;
	struct mip6dng_reply_hdr * hdr = (struct mip6dng_reply_hdr *)buf;

	if (tcp_handle == NULL || handle == NULL) {
		ERROR("Invalid handle in recv function. Drop.");
		return;
	}

	if (buf == NULL || blen < sizeof(struct mip6dng_reply_hdr)) {
		ERROR("Too short message. Drop.");
		return;
	}

	if (hdr->req.seq != libmip6_get_last_seq(handle)) {
		ERROR("SEQ mismatch. %u sent, %u received", 
				libmip6_get_last_seq(handle), hdr->req.seq);
		return;
	}

	pthread_mutex_lock(&handle->recv_mutex);

	if (handle->recv_msg != NULL)
		free(handle->recv_msg);
	handle->recv_msg = malloc(blen - sizeof(struct mip6dng_reply_hdr));
	if (handle->recv_msg == NULL) {
		ERROR("Out of memory: Unable to allocate recv buffer");
		goto out;
	}
	memcpy(handle->recv_msg, (unsigned char *)(hdr + 1), blen - sizeof(struct mip6dng_reply_hdr));
	handle->recv_mlen = blen - sizeof(struct mip6dng_reply_hdr);
	switch(hdr->status) {
	case MIP6DNG_REPLY_STATUS_OK:
		handle->recv_status = 0;
		break;
	case MIP6DNG_REPLY_STATUS_FORMAT:
		handle->recv_status = -LIBMIP6_MESSAGE;
		break;
	case MIP6DNG_REPLY_STATUS_UNKNOWN_MOD:
		handle->recv_status = -LIBMIP6_UNKNOWN_MOD;
		break;
	case MIP6DNG_REPLY_STATUS_UNKNOWN_CMD:
		handle->recv_status = -LIBMIP6_UNKNOWN_CMD;
		break;
	case MIP6DNG_REPLY_STATUS_INVALID:
		handle->recv_status = -LIBMIP6_MESSAGE;
		break;
	case MIP6DNG_REPLY_STATUS_NOANSW:
		handle->recv_status = -LIBMIP6_NOANSW;
		break;
	case MIP6DNG_REPLY_STATUS_EMPTY:
		DEBUG("EMPTY answer. Clear mlen and msg");
		handle->recv_status = 0;
		handle->recv_mlen = 0;
		if (handle->recv_msg != NULL)
			free(handle->recv_msg);
		handle->recv_msg = NULL;
		break;
	default:
		handle->recv_status = -LIBMIP6_UNKNOWN;
		break;
	}
		
	DEBUG("Received %u bytes -> %u. Status %u -> %u. Send pthread signal...", blen, handle->recv_mlen, hdr->status, handle->recv_status);
	pthread_cond_broadcast(&handle->recv_cond);

out:
	pthread_mutex_unlock(&handle->recv_mutex);
}

/*! 
 * \brief Waiting for reception
 *
 * It waits for the notification of recv_fn
 *
 * \param handle Handle
 * \param timeout Wait timeout
 * \return Zero if OK
 */
int recv_wait(struct libmip6h * handle, struct timespec * timeout)
{
	int ret;
	struct timeval now;
	struct timespec abst;

	if (handle == NULL)
		return -LIBMIP6_INVALID;

	gettimeofday(&now, NULL);
	abst.tv_sec = now.tv_sec + timeout->tv_sec; 
	abst.tv_nsec = now.tv_usec * 1000 + timeout->tv_nsec;
	if (abst.tv_nsec >= 1000000000) { 
		++abst.tv_sec; 
		abst.tv_nsec -= 1000000000;
	} 

	pthread_mutex_lock(&handle->recv_mutex);

	if (handle->recv_mlen != -1)
		return 0;

	if (handle->recv_msg != NULL)
		free(handle->recv_msg);
	handle->recv_msg = NULL;
	handle->recv_mlen = 0;

	ret = pthread_cond_timedwait(&handle->recv_cond, &handle->recv_mutex, &abst);
	if (ret) 
		return -LIBMIP6_TOUT;

	return 0;
}

/*!
 * \brief Cleanup after recv_wait
 *
 * \param handle Handle
 */
int recv_done(struct libmip6h * handle)
{
	if (handle == NULL)
		return -LIBMIP6_INVALID;

	if (handle->recv_msg != NULL)
		free(handle->recv_msg);
	handle->recv_msg = NULL;
	handle->recv_mlen = 0;

	pthread_mutex_unlock(&handle->recv_mutex);

	return 0;
}

/*! \} */
