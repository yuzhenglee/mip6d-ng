
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-libapi
 * \{
 */

#ifndef _RECV_H_
#define _RECV_H_

#include "tcpc.h"

void recv_fn(struct tcpc_handle * tcp_handle, unsigned char * buf, unsigned int blen, void * priv);

int recv_wait(struct libmip6h * handle, struct timespec * timeout);
int recv_done(struct libmip6h * handle);


#endif /* _RECV_H_ */

/*! \} */
