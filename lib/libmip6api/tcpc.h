
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-libapi
 * \{
 */

#ifndef _TCPC_H
#define _TCPC_H

#include <netinet/in.h>
#include <pthread.h>

/*!
 * \brief TCP handler
 */
struct tcpc_handle {
	/*! If non-zero it is an IPv4 compatible TCP connection */
	unsigned char is_i4;
	/*! IPv4 specific stuff */
	struct {
		/*! Remote address */
		struct in_addr remote;
		/*! Remote port */
		in_port_t remotep;
		/*! Socket's file descriptor */
		int fd;
	} i4;
	/*! If non-zero it is an IPv6 compatible TCP connection */
	unsigned char is_i6;
	/*! IPv6 specific stuff */
	struct {
		/*! Remote address */
		struct in6_addr remote;
		/*! Remote port */
		in_port_t remotep;
		/*! Socket's file descriptor */
		int fd;
	} i6;
	/*! Handle lock */
	pthread_mutex_t lock;
};

int tcpc_init(struct tcpc_handle * handle, void (* fn)(struct tcpc_handle * handle, unsigned char *, unsigned int, void *), void * priv);
int tcpc_close(struct tcpc_handle * handle);
int tcpc_send(struct tcpc_handle * handle, unsigned char * buf, unsigned int blen);

#endif

/*! \} */
