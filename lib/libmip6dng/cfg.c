
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-corelib
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/cfg.h>
#include <module.h>

/*!
 * \brief Helper global variable
 * It saves the last processed argument's index
 */
static int last_optind;

/*!
 * \brief Preparing the argument parsing

 * It initializes the r_argv array, and sets argv[0] as the first element of it
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of remaining (unused) command line arguments
 * \param r_argv Remaining (unused) command line arguments
 * \return Zero if OK, otherwise negative
 */
int prepare_get_my_opts(int argc, char * const argv[], int * const r_argc, char *** r_argv)
{
	optind = 1;
	opterr = 0;
	last_optind = 2;

	if (argc == 0 || argv == NULL || r_argc == NULL || r_argv == NULL)
		return -1;

	*r_argv = malloc(sizeof(char *));
	if (*r_argv == NULL) {
		ERROR("Unable to alloc r_argv");
		return -1;
	}
	(*r_argv)[0] = strdup(argv[0]);
	*r_argc = 1;

	return 0;
}

/*!
 * \brief Getting the next argument

 * It is a wrapper for POSIX getopt_long
 * It collects the unused (remaining) arguments to a new array
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param optstring Short options, see POSIX getopt_long
 * \param longopts Long options, see POSIX getopt_long
 * \param longindex Found long option's index, see POSIX getopt_long
 * \param r_argc Number of remaining (unused) command line arguments
 * \param r_argv Remaining (unused) command line arguments
 * \return Fount option character, otherwise -1, see POSIX getopt_long
 */
int get_my_opts(int argc, char * const argv[], const char *optstring,
		const struct option *longopts, int *longindex, int * const r_argc, char *** r_argv)
{
	int opt;
	int s;

	if (argv == NULL || r_argc == NULL || r_argv == NULL)
		return -1;

	opt = getopt_long(argc, argv, optstring, longopts, longindex);

	if (opt != -1 && last_optind + 1 != optind && optind > 2) {
		s = (opt == '?') ? 2 : 3;
		if (*r_argc < 2 || strcmp((*r_argv)[*r_argc - 1], argv[optind - s]) != 0) {
			//DEBUG("- '%s' (%d)", argv[optind - s], *r_argc);

			*r_argv = (char **)realloc(*r_argv, (*r_argc + 1) * sizeof(char *));
			if (*r_argv == NULL) {
				ERROR("Unable to alloc r_argv");
				return -1;
			}
			(*r_argv)[*r_argc] = malloc(strlen(argv[optind-s]) + 1);
			strncpy((*r_argv)[*r_argc], argv[optind-s], strlen(argv[optind-s]) + 1);
			*r_argc += 1;
		}
	}

	if (opt == '?') {
		//DEBUG("? %d '%c' '%s'", optind, (optopt != 0) ? optopt : '?', argv[optind-1]);

		*r_argv = (char **)realloc(*r_argv, (*r_argc + 1) * sizeof(char *));
		if (*r_argv == NULL) {
			ERROR("Unable to alloc r_argv");
			return -1;
		}
		(*r_argv)[*r_argc] = malloc(strlen(argv[optind-1]) + 1);
		strncpy((*r_argv)[*r_argc], argv[optind-1], strlen(argv[optind-1]) + 1);
		*r_argc += 1;
	}

	
	last_optind = optind;
	
	return opt;
}

/*!
 * \brief Releasing the array of remaining arguments
 * \param r_argc Number of command line arguments in r_argv
 * \param r_argv Array of remaining command line arguments
 */
void free_r_args(int r_argc, char ** r_argv) 
{
	int i;

	if (r_argv == NULL)
		return;

	for (i = 0; i < r_argc; i++) {
		if (r_argv[i] == NULL)
			continue;
		free(r_argv[i]);
	}

	free(r_argv);
}

/*!
 * \brief Wrapper function for confuse parsing errors
 * \param cfg Unused
 * \param fmt Format string
 * \param ap Arguments
 */
static void __config_error(cfg_t * cfg, const char * fmt, va_list ap)
{
	char buffer[1024];

	vsnprintf(buffer, sizeof(buffer), fmt, ap);

	ERROR(buffer);
}


/*!
 * \brief Collected config options, from core and all modules
 */
static cfg_opt_t * all_opts = NULL;

/*!
 * \brief Loading configuration

 * It collects configuration options from modules (see module_construct_cfg_opt).
 * Next it reads the configuration file and parsing it
 * Finally, it gets options of core
 * \param path Configuration file path
 * \param main_opts Options of core binary
 * \param main_opts_size Size of core binary options 
 * \return Configuration handler if OK, otherwise NULL
 */
cfg_t * config_load(const char * const path, cfg_opt_t * main_opts, size_t main_opts_size)
{
	cfg_opt_t * opts;
	unsigned char opts_len;
	cfg_t * cfg;
	int ret;

	opts = malloc(main_opts_size);
	if (opts == NULL) {
		ERROR("Unable to allocate main cfg opts");
		exit(EXIT_FAILURE);
	}
	memcpy(opts, main_opts, main_opts_size);
	opts_len = main_opts_size / sizeof(cfg_opt_t);

	module_construct_cfg_opt(&opts, &opts_len);

	DEBUG("New opt len: %u", opts_len);

	cfg = cfg_init(opts, CFGF_NOCASE);
	if (cfg == NULL) {
		ERROR("Unable initialize config parser");
		return NULL;
	}

	(void)cfg_set_error_function(cfg, __config_error);

	ret = cfg_parse(cfg, path);
	if(ret == CFG_FILE_ERROR) {
		ERROR("Unable to read config file: %s", path);
		return NULL;
	} else if(ret == CFG_PARSE_ERROR) {
		ERROR("Unable to parse config file: %s", path);
		return NULL;
	}

	all_opts = opts;
	return cfg;
}

/*!
 * \brief Releasing configuration stuff
 * \param cfg Configuration handler (returned by config_load)
 */
void config_release(cfg_t * cfg)
{
	if (cfg != NULL)
		cfg_free(cfg);

	module_free_cfg_opt(all_opts);
}

/*! \} */

