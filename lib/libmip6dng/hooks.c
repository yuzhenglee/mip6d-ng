
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-corelib
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <limits.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/hooks.h>

/*!
 * \brief RW lock for hooks list

 * Protects hooks_list
 */
static pthread_rwlock_t hooks_mutex = PTHREAD_RWLOCK_INITIALIZER;

/*!
 * \brief Representing an added hook function
 */
struct hook_entry {
	/*! Linked list management */			struct list_head list;
	/*! Sequence number */					unsigned char seq;
	/*! Hook function */					hook_t * hook_fp;
};

/*!
 * \brief List of available hooks
 */
static struct hooks_list {
	/*! Name of hook */						char name[100];
	/*! List of added hook functions */		struct list_head hooks;
	/*! If not null, it could re-used */	unsigned char unused;
} * hooks_list = NULL;

/*!
 * Number of available hooks
 */
static unsigned int hooks_num = 0;

/*!
 * \brief Initializing hooks support
 */
int hooks_init ()
{
    pthread_rwlock_wrlock(&hooks_mutex);

   
	pthread_rwlock_unlock(&hooks_mutex);
    
	return 0;
}

/*!
 * \brief Registering a hook
 * \ingroup iapi-corelib
 * Later modules could add their functions to this hook
 * \param name Name of hook
 * \return ID if OK, otherwise negative
 */
int hooks_register(const char * const name)
{
	unsigned int i;
	int id = -1;
	struct list_head * hooks;

	if (name == NULL)
		return -1;

    pthread_rwlock_wrlock(&hooks_mutex);

	for (i = 0; i < hooks_num; i++) {
		if (hooks_list[i].unused == 1) {
			id = (int)i;
			break;
		}
	}

	if (hooks_num + 1 == UINT_MAX)
		goto out;

	if (id < 0) {
		hooks = malloc(sizeof(struct list_head) * hooks_num);
		if (hooks == NULL) {
			ERROR("Out of memory: Save hooks");
			exit(EXIT_FAILURE);
		}
		for (i = 0; i < hooks_num; i++) {
			INIT_LIST_HEAD(&hooks[i]);
			list_splice(&hooks_list[i].hooks, &hooks[i]);
		}
		hooks_list = realloc(hooks_list, (hooks_num + 1) * sizeof(struct hooks_list));
		if (hooks_list == NULL) {
			ERROR("Out of memory: Reallocating hooks list");
			exit(EXIT_FAILURE);
		}
		for (i = 0; i < hooks_num; i++) {
			INIT_LIST_HEAD(&hooks_list[i].hooks);
			list_splice(&hooks[i], &hooks_list[i].hooks);
		}
		free(hooks);
		id = (int)(hooks_num++);
	}

	strncpy(hooks_list[id].name, name, sizeof(hooks_list[id].name));
	INIT_LIST_HEAD(&hooks_list[id].hooks);
	hooks_list[id].unused = 0;

out:

	pthread_rwlock_unlock(&hooks_mutex);

#ifdef CORE_IC_DEBUG
	DEBUG("Hooks: new entry registered: '%s' ID %d", name, id);
#endif

	return id;
}

/*!
 * \brief Unregistering a hook
 * \ingroup iapi-corelib
 * \param id ID of hook (return value of register function)
 * \return zero if OK, otherwise negative
 */
int hooks_unregister(unsigned int id)
{	
	int retval = 1;
	struct hook_entry * ep;
	struct list_head * pos, * pos2;

    pthread_rwlock_wrlock(&hooks_mutex);

	if (id >= hooks_num)
		goto out;
	
	if (hooks_list[id].unused == 1)
		goto out;

	hooks_list[id].name[0] = '\0';

	list_for_each_safe(pos, pos2, &hooks_list[id].hooks) {
		ep = (struct hook_entry *)list_entry(pos, struct hook_entry, list);
		list_del(pos);
		free(ep);
	}

	hooks_list[id].unused = 1;

	retval = 0;

out:

	pthread_rwlock_unlock(&hooks_mutex);

	return retval;
}

/*!
 * \brief Getting hook id from hook name
 * \ingroup iapi-corelib
 * \param name Hook name
 * \return hook id if OK, otherwise negative
 */
int hooks_get_id(const char * const name)
{
	unsigned int i;
	int id = -1;

    pthread_rwlock_rdlock(&hooks_mutex);

	for (i = 0; i < hooks_num; i++) {
		if (strcmp(hooks_list[i].name, name) == 0) {
			id = (int)i;
			break;
		}
	}

	pthread_rwlock_unlock(&hooks_mutex);

	return id;
}

/*!
 * \brief Getting hook name form hook ID
 * \ingroup iapi-corelib
 * \param id hook id
 * \param name Buffer for the name of the hook
 * \param nlen Size of buffer
 * \return zero if OK, otherwise negative
 */
int hooks_get_name(unsigned int id, char * const name, size_t nlen)
{
	int retval = -1;

	if (name == NULL || nlen == 0)
		return -1;

    pthread_rwlock_rdlock(&hooks_mutex);

	if (id >= hooks_num)
		goto out;

	strncpy(name, hooks_list[id].name, nlen);

	retval = 0;

out:

	pthread_rwlock_unlock(&hooks_mutex);

	return retval;
}

/*!
 * \brief Adding a function to a hook
 * \ingroup iapi-corelib
 * The sequence number will be used for ordering of hook functions
 * Lower value means, earlier processing
 * \param id hook ID
 * \param seq Sequence number
 * \param hook_fp Hook function
 * \return zero if OK, otherwise negative
 */
int hooks_add(unsigned int id, unsigned char seq, hook_t * hook_fp)
{
	int retval = -1;
	struct hook_entry * entry;
	struct hook_entry * ep;
	struct list_head * pos;

	if (hook_fp == NULL)
		return -1;

	entry = malloc(sizeof(struct hook_entry));
	if (entry == NULL)
		return -1;

	entry->hook_fp = hook_fp;
	entry->seq = seq;

	pthread_rwlock_wrlock(&hooks_mutex);

	if (id > hooks_num)
		goto out;

	if (hooks_list[id].unused == 1)
		goto out;

	list_for_each(pos, &hooks_list[id].hooks) {
		ep = (struct hook_entry *)list_entry(pos, struct hook_entry, list);
		if (ep->seq > seq) {
			list_add_tail(&entry->list, pos);
			entry = NULL;
			break;
		}
	}

	if (entry != NULL) {
		list_add_tail(&entry->list, &hooks_list[id].hooks);
		entry = NULL;
	}

#ifdef CORE_IC_DEBUG
	DEBUG("Added 1 entry to hook '%s'", hooks_list[id].name);
#endif

	retval = 0;

out:

	pthread_rwlock_unlock(&hooks_mutex);

	if (entry)
		free(entry);

	return retval;
}

/*!
 * \brief Deleting a function from a hook
 * \ingroup iapi-corelib
 * \param id hook ID
 * \param hook_fp Hook function (used for identification)
 * \return zero if K, otherwise negative
 */
int hooks_del(unsigned int id, hook_t * hook_fp)
{
	int retval = -1;
	struct hook_entry * ep;
	struct list_head * pos, * pos2;

	if (hook_fp == NULL)
		return -1;

	pthread_rwlock_wrlock(&hooks_mutex);

	if (id > hooks_num)
		goto out;

	if (hooks_list[id].unused == 1)
		goto out;

	list_for_each_safe(pos, pos2, &hooks_list[id].hooks) {
		ep = (struct hook_entry *)list_entry(pos, struct hook_entry, list);
		if (ep->hook_fp == hook_fp) {
			list_del(pos);
			free(ep);
		}
	}

#ifdef CORE_IC_DEBUG
	DEBUG("Deleted 1 entry from hook '%s'", hooks_list[id].name);
#endif

out:

	pthread_rwlock_unlock(&hooks_mutex);

	return retval;
}

/*!
 * \brief Execute a hook's all functions
 * \ingroup iapi-corelib
 * It runs hook functions in order of sequence numbers
 * It stops the execution, if any of the hook functions
 * returns non-zero value
 * \param id hook ID
 * \param arg Hook function argument
 * \return zero if OK, otherwise negative
 */
int hooks_run(unsigned int id, void * arg)
{
	int retval = -1;
	int ret = 0;
	struct hook_entry * ep;
	struct list_head * pos;
	
	pthread_rwlock_rdlock(&hooks_mutex);

	if (id > hooks_num)
		goto out;

	if (hooks_list[id].unused == 1)
		goto out;

	list_for_each(pos, &hooks_list[id].hooks) {
		ep = (struct hook_entry *)list_entry(pos, struct hook_entry, list);
		
		/* Unlock hooks while the hook function runs */
		pthread_rwlock_unlock(&hooks_mutex);
		
		ret = ep->hook_fp(arg);
		
		pthread_rwlock_rdlock(&hooks_mutex);
		if (hooks_list[id].unused == 1) {
			DEBUG("The hook was deleted while the hook function was running. Stop execution of other hooks");
			goto out;
		}
#ifdef CORE_IC_DEBUG
		DEBUG("Hook entry in '%s' with %d seq returns: %d", hooks_list[id].name, ep->seq, ret);
#endif
		if (ret)
			break;
	}

	retval = ret;

out:

	pthread_rwlock_unlock(&hooks_mutex);

	return retval;
}

/*! \} */

