

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_HOOKS_H
#define MIP6D_NG_HOOKS_H

/*!
 * \brief Hook function
 *
 * \param arg Hook function argument
 * \return If non-zero it stops further hook execution
 */
typedef int hook_t(void * arg);

/*! \ingroup internal-corelib */
int hooks_init ();
int hooks_register(const char * const name);
int hooks_unregister(unsigned int id);
int hooks_get_id(const char * const name);
int hooks_get_name(unsigned int id, char * const name, size_t nlen);
int hooks_add(unsigned int id, unsigned char seq, hook_t * hook_fp);
int hooks_del(unsigned int id, hook_t * hook_fp);
int hooks_run(unsigned int id, void * arg);

#endif /* MIP6D_NG_HOOKS_H */

/*! \} */

