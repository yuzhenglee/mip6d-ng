

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_MISC_H
#define MIP6D_NG_MISC_H

#include <netinet/in.h>
#include <arpa/inet.h>

#define TIME_SEC_MSEC	1000
#define TIME_SEC_NSEC 	1000000000
#define TIME_MSEC_NSEC	1000000

#define tstomsec(tv) \
	((tv).tv_sec * TIME_SEC_MSEC + (tv).tv_nsec / TIME_MSEC_NSEC)

#define tstodsec(tv) \
	((double)(tv).tv_sec + (double)(tv).tv_nsec / TIME_SEC_NSEC)

#define tsisset(tv)	\
	((tv).tv_sec || (tv).tv_nsec)

#define tsclear(tv)	\
	((tv).tv_sec = (tv).tv_nsec = 0)

#define tscmp(a, b, CMP) \
	(((a).tv_sec == (b).tv_sec) ? \
	((a).tv_nsec CMP (b).tv_nsec) : \
	((a).tv_sec CMP (b).tv_sec))

#define tsequ(a, b) \
	(((a).tv_sec == (b).tv_sec) && ((a).tv_nsec == (b).tv_nsec))

#define tsadd(a, b, result) \
	do { \
		(result).tv_sec = (a).tv_sec + (b).tv_sec; \
		(result).tv_nsec = (a).tv_nsec + (b).tv_nsec; \
		if ((result).tv_nsec >= TIME_SEC_NSEC) { \
			++(result).tv_sec; \
			(result).tv_nsec -= TIME_SEC_NSEC; \
		} \
	} while (0)

#define tssub(a, b, result) \
	do { \
		(result).tv_sec = (a).tv_sec - (b).tv_sec; \
		(result).tv_nsec = (a).tv_nsec - (b).tv_nsec; \
		if ((result).tv_nsec < 0) { \
			--(result).tv_sec; \
			(result).tv_nsec += TIME_SEC_NSEC; \
		} \
	} while (0)

#define tsafter(a, b) \
	tscmp(a,b,<)

#define tsbefore(a, b) \
	tscmp(a,b,>)

#define tscpy(to, from) \
	do { \
		(to).tv_sec = (from).tv_sec; \
		(to).tv_nsec = (from).tv_nsec; \
	} while (0)

#define tsset(tv, sec, nsec) \
	do { \
		(tv).tv_sec = (sec); \
		(tv).tv_nsec = (nsec); \
	} while (0)

#define tssetsec(tv, sec) \
	do { \
		(tv).tv_sec = (sec); \
		(tv).tv_nsec = 0; \
	} while (0)

#define tssetmsec(tv, msec) \
	do { \
		(tv).tv_sec = (msec) / TIME_SEC_MSEC; \
		(tv).tv_nsec = ((msec) % TIME_SEC_MSEC) * TIME_MSEC_NSEC; \
	} while (0)

#define tssetdsec(tv, sec) \
	do { \
		(tv).tv_sec = (long)(sec); \
		(tv).tv_nsec = (long)(((sec)-(tv).tv_sec) * TIME_SEC_NSEC); \
	} while (0)


#define tsinc(tv, sec, nsec) \
	do { \
		(tv).tv_sec += (sec); \
		(tv).tv_nsec += (nsec); \
		if ((tv).tv_nsec >= TIME_SEC_NSEC) { \
			++(tv).tv_sec; \
			(tv).tv_nsec -= TIME_SEC_NSEC; \
		} \
	} while (0)

#define tsincmsec(tv, msec) \
	do { \
		(tv).tv_sec += (msec) / TIME_SEC_MSEC; \
		(tv).tv_nsec += ((msec) % TIME_SEC_MSEC) * TIME_MSEC_NSEC; \
		if ((tv).tv_nsec >= TIME_SEC_NSEC) { \
			++(tv).tv_sec; \
			(tv).tv_nsec -= TIME_SEC_NSEC; \
		} \
	} while (0)

#define tsdec(tv, sec, nsec) \
	do { \
		(tv).tv_sec -= sec; \
		(tv).tv_nsec -= nsec; \
		if ((tv).tv_nsec < 0) { \
			--(tv).tv_sec; \
			(tv).tv_nsec += TIME_SEC_NSEC; \
		} \
	} while (0)

#define tsdecmsec(tv, msec) \
	do { \
		(tv).tv_sec -= (msec) / TIME_SEC_MSEC; \
		(tv).tv_nsec -= ((msec) % TIME_SEC_MSEC) * TIME_MSEC_NSEC; \
		if ((tv).tv_nsec < 0) { \
			--(tv).tv_sec; \
			(tv).tv_nsec += TIME_SEC_NSEC; \
		} \
	} while (0)

#define tsmin(a, b) \
	tsbefore((a), (b)) ? (b) : (a)

#define tsmax(a, b) \
	tsafter((a), (b)) ? (b) : (a)

#define TS_TO_STR(ts) \
		(ts)->tv_sec, \
		(ts)->tv_nsec

#define TS_FMT "%d.%d"

#define IP6ADDR_TO_STR(addr) \
        ntohs((addr)->s6_addr16[0]), \
        ntohs((addr)->s6_addr16[1]), \
        ntohs((addr)->s6_addr16[2]), \
        ntohs((addr)->s6_addr16[3]), \
        ntohs((addr)->s6_addr16[4]), \
        ntohs((addr)->s6_addr16[5]), \
        ntohs((addr)->s6_addr16[6]), \
        ntohs((addr)->s6_addr16[7])

#define IP6ADDR_FMT "%04x:%04x:%04x:%04x:%04x:%04x:%04x:%04x"

static inline int in6_is_addr_routable_unicast(const struct in6_addr *a)
{
	return (int)((!IN6_IS_ADDR_UNSPECIFIED(a) &&
			 !IN6_IS_ADDR_LOOPBACK(a) &&
			 !IN6_IS_ADDR_MULTICAST(a) &&
			 !IN6_IS_ADDR_LINKLOCAL(a)));
}

static inline void ipv6_addr_set(struct in6_addr *addr, 
				 uint32_t w1, uint32_t w2,
				 uint32_t w3, uint32_t w4)
{
	addr->s6_addr32[0] = w1;
	addr->s6_addr32[1] = w2;
	addr->s6_addr32[2] = w3;
	addr->s6_addr32[3] = w4;
}

static inline void ipv6_addr_solict_mult(const struct in6_addr *addr,
					 struct in6_addr *solicited)
{
	ipv6_addr_set(solicited, htonl(0xFF020000), 0, htonl(0x1),
		      htonl(0xFF000000) | addr->s6_addr32[3]);
}

static inline void ipv6_addr_llocal(const struct in6_addr *addr,
				    struct in6_addr *llocal)
{
	ipv6_addr_set(llocal, htonl(0xFE800000), 0,
		      addr->s6_addr32[2], addr->s6_addr32[3]);
}

extern struct in6_addr in6addr_allr;

#define ROUTE_TABLE_MAIN		254

#endif /* MIP6D_NG_MISC_H */

/*! \} */

