

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_TASKS_H
#define MIP6D_NG_TASKS_H

#include <time.h>
#include <mip6d-ng/misc.h>

/*!
 * \brief Task function
 * \param Task argument
 */
typedef void task_t(void * arg);

/*! \ingroup internal-corelib */
int tasks_init();
unsigned long tasks_add(task_t * task_fp, void * arg, struct timespec tm);
int tasks_del(unsigned long id);

/*!
 * \brief Adding task with relative timing
 *
 * Adding the specified rel_tm to the current time, and calculates absolute scheduled time from this.
 * It calls tasks_add.
 * \param task_fp Task function
 * \param arg Argument of task function
 * \param rel_tm Relative scheduled time (from now)
 * \return ID of the registered task, otherwise (unsigned long)0
 */
static inline unsigned long tasks_add_rel(task_t * task_fp, void * arg, struct timespec rel_tm)
{
	struct timespec now;
	struct timespec tm;

	clock_gettime(CLOCK_REALTIME, &now);

	tsadd(now, rel_tm, tm);
		
	return tasks_add(task_fp, arg, tm);
}

#endif /* MIP6D_NG_TASKS_H */

/*! \} */

