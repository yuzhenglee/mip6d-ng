
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_THREADING_H
#define MIP6D_NG_THREADING_H

#include <pthread.h>

/*! \ingroup internal-corelib */
int threading_init();
/*! \ingroup internal-corelib */
pthread_attr_t threading_get_detached_attr();

/*!
 * \brief Starting detached thread
 *
 * It creates a thread with detach state true option
 * \param start_routine Thread function
 * \param arg Thread argument
 * \return Thread ID or (pthread_t)0 in case of error
 */
static inline pthread_t threading_create(void *(*start_routine)(void *), void *arg)
{
	int ret;
	pthread_t thid;
	pthread_attr_t attr = threading_get_detached_attr();

	ret = pthread_create(&thid, &attr, start_routine, arg);
	if (ret)
		return (pthread_t)0;

	pthread_detach(thid);

	return thid;
}

#endif /* MIP6D_NG_THREADING_H */

/*! \} */

