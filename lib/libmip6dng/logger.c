
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-corelib
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <stdarg.h>
#include <errno.h>
# include <syslog.h>

#include <mip6d-ng/logger.h>

#define LOGGER_MODE_SYSLOG_FILE		"syslog"

/*!
 * \brief Logging destination options 
 */
static enum {
	/*! Log to file */
	LOGGER_MODE_FILE,
	/*! Log to syslog */
	LOGGER_MODE_SYSLOG,
	/*! Log to standard output */
	LOGGER_MODE_STDOUT
} logger_mode = LOGGER_MODE_STDOUT;

/*! 
 * \brief String representation of log levels
 */
static char * logger_level_str[] = {
	"ERR",
	"INF",
	"DBG"
};

/*!
 * \brief Syslog level matching to log levels
 */
static unsigned int logger_level_syslog[] = {
	LOG_ERR,
	LOG_INFO,
	LOG_DEBUG
};

/*!
 * \brief Log to this file (path)
 */
static char * logger_file = NULL;
/*!
 * \brief Log level
 */
static enum logger_level logger_level = 1;
/*!
 * \brief Maximum length of log file (before logrotate)
 */
static unsigned int logger_size = 0;

/*!
 * \brief Logger mutex
 */
static pthread_mutex_t logger_mutex;

/*!
 * \brief Log to this (file descriptor)
 */
static FILE * logger_fn = NULL;
/*!
 * \brief In non-zero syslog is opened
 */
static unsigned char syslog_opened = 0;

/*!
 * \brief Last log line
 */
static struct {
	/*! Last line string */
	char line[1024];
	/*! Duplicate line counter */
	int counter;
	/*! Time */
	time_t time;
} last_line;

#define LOGGER_FORMAT 			"[%s] %04d%02d%02d %02d%02d%02d.%03ld |%s-%s| %s"
#define LOGGER_FORMAT_SYSLOG 	"[%s] |%s-%s| %s"
#define LOGGER_FORMAT_CMP_POS	26
#define LOGGER_REPEAT_MAX		20
#define LOGGER_REPEAT_TIME		30


#define MAX_LOG_FILE_LEN 		12
#define MAX_LOG_FUNC_LEN 		20

static void __logger_reopen();

/*!
 * \brief Rotating log file if necessary
 *
 * First it checks the size of the log file
 * If it exceeded the size limit, it will rename log file (oldpath.1) and it will reopen it
 */
static void check_logsize()
{
	int ret;
    struct stat s;
    char bak_path[516];

    stat(logger_file, &s);
    if (((unsigned int)s.st_size < logger_size) || (logger_size == 0))
		return;

	snprintf(bak_path, sizeof(bak_path), "%s.1", logger_file);
	ret = stat(bak_path, &s);
	if (ret < 0 && errno != ENOENT)
		return;
	else if (ret == 0) {
		unlink(bak_path);
	}

	fclose(logger_fn);
	logger_fn = NULL;
	rename(logger_file, bak_path);
	unlink(logger_file);
	__logger_reopen();
}

/*!
 * \brief Log a line to standar output or to a file
 *
 * If logging to a file, it's implementing a 'last message repeated ... times' feature.
 * \param line Log line
 */
static void logger_log_line(char *line)
{
	if (logger_mode == LOGGER_MODE_STDOUT) {
		printf("%s\n", line);
		return;
	}

	pthread_mutex_lock(&logger_mutex);

	if ((strcmp(&line[LOGGER_FORMAT_CMP_POS], &last_line.line[LOGGER_FORMAT_CMP_POS]) != 0) ||
			(last_line.counter >= LOGGER_REPEAT_MAX) ||
			(time(NULL) - last_line.time >= LOGGER_REPEAT_TIME)) {
		if (last_line.counter > 0)
			fprintf(logger_fn, "%s\n\t (last message repeated %d times)\n",
				last_line.line, last_line.counter + 1);

		fprintf(logger_fn, "%s\n", line);
		fflush(logger_fn);

		last_line.counter = 0;
		last_line.time = time(NULL);
		strncpy(last_line.line, line, sizeof(last_line.line));

		check_logsize();
	} else
		last_line.counter++;

	pthread_mutex_unlock(&logger_mutex);
}

/*!
 * \brief Getting thread id as a string
 *
 * Converts pthread id to a string
 * \param buf Working buffer
 * \param buflen Size of working buffer
 * \return Same as buf (but with the result)
 */
static char * logger_get_thread_id(char * const buf, size_t buflen)
{
    unsigned char *p;
    size_t i = 0;
    pthread_t id = pthread_self();

    memset(buf, 0, buflen);

    p = (unsigned char *)&id;
    for (i = 0; i < sizeof(id); i++, p++) 
        snprintf(buf + 2 * i, buflen - 2 * i - 1, "%02x", *p);

    return buf;
}

/*!
 * \brief Logging the specified message (it's adding ending \\n)
 *
 * If the level ignores the message, it will drop it.
 * If mode is syslog, it will simply pass the message to the syslog daemon,
 * Otherwise it constructs the log line with date, time, thread information.
 * If level is ERROR, it will add strerror and errno to the message.
 * It passes it to logger_log_line
 * \param level Log level
 * \param module Name of the module
 * \param msg Format string
 * \param ... (Optional) arguments
 */
void logger(enum logger_level level, const char * const module, const char * const msg, ...)
{
	int saved_errno = errno;
	va_list args;
	char threadid[17];
	char tmp[1024];
	char * tmp_ptr;
	size_t tmp_len;
	char line[4096];
	struct timeval tv;
	struct tm tm;

	if (level > logger_level)
		return;

	if (logger_mode == LOGGER_MODE_SYSLOG && syslog_opened == 1) {
		va_start(args, msg);
		snprintf(tmp, sizeof(tmp), LOGGER_FORMAT_SYSLOG "\n", 
				logger_level_str[level], module,
				logger_get_thread_id(threadid, sizeof(threadid)), msg);
		vsyslog(logger_level_syslog[level], tmp, args);
		va_end(args);
		return;
	}

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &tm);

	snprintf(tmp, sizeof(tmp), LOGGER_FORMAT,
			logger_level_str[level],
			tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour,
			tm.tm_min, tm.tm_sec, tv.tv_usec / 1000, module,
			logger_get_thread_id(threadid, sizeof(threadid)), msg);
	if (level == LERR) {
		tmp_len = strlen(tmp);
		tmp_ptr = tmp + tmp_len;
		snprintf(tmp_ptr, sizeof(tmp) - tmp_len, "\n\t [%d] %s", saved_errno, strerror(saved_errno));
	}

	va_start(args, msg);
	vsnprintf(line, sizeof(line), tmp, args);
	va_end(args);

	logger_log_line(line);
}

/*!
 * \brief It reopens log file or re-initializes syslog
 *
 * It also clears all existing message buffers
 */
static void __logger_reopen()
{
	//fprintf(stderr, "loglevel: %d, logfile: '%s', logsize: %d\n", logger_level, logger_file, logger_size);

	if (logger_fn != NULL) {
		fclose(logger_fn);
		logger_fn = NULL;
	}

	if (syslog_opened == 1) {
		closelog();
		syslog_opened = 0;
	}

	switch (logger_mode) {
	case LOGGER_MODE_FILE:
		logger_fn = fopen(logger_file, "a");
		if (logger_fn == NULL) {
			fprintf(stderr, "Error: opening logfile: %s\n  %d %s\n", logger_file, errno, strerror(errno));
			exit(EXIT_FAILURE);
		}
		setbuf(logger_fn, NULL);

		memset(last_line.line, 0, sizeof(last_line.line));
		last_line.counter = 0;
		last_line.time = time(NULL);
		break;
	case LOGGER_MODE_SYSLOG:
		openlog(PACKAGE_NAME "-" PACKAGE_VERSION, LOG_PID | LOG_NDELAY, LOG_DAEMON); 
		syslog_opened = 1;
		break;
	case LOGGER_MODE_STDOUT:
		break;
	}
}

/*!
 * \brief Reopens log file or re-initializes syslog
 *
 * It's only a wrapper function. It locks the mutex, and calls __logger_reopen
 */
static void logger_reopen()
{
	pthread_mutex_lock(&logger_mutex);
    __logger_reopen();
	pthread_mutex_unlock(&logger_mutex);
}

/*!
 * \brief Initialize logger facility
 *
 * \param logfile If it is an empty string or NULL, logging to standard output.
 *   If it is the 'syslog' string, logging with syslog. Otherwise using the specified file.
 * \param level Log level.
 * \param size Maximum size of the log file. Zero means unlimited
 */
void logger_init(const char * const logfile, enum logger_level level, unsigned int size)
{
	pthread_mutex_init(&logger_mutex, NULL);

	if (logfile == NULL || strlen(logfile) == 0) 
		logger_mode = LOGGER_MODE_STDOUT;
	else if (strcmp(logfile, LOGGER_MODE_SYSLOG_FILE) == 0)
		logger_mode = LOGGER_MODE_SYSLOG;
	else {
		logger_mode = LOGGER_MODE_FILE;
		logger_file = strdup(logfile);
	}

	logger_level = level;
    logger_size = size;

	logger_reopen();
}

/*!
 * \brief Re-initialize (re-set) logger facility
 *
 * \param logfile If it is an empty string or NULL, logging to standard output.
 *   If it is the 'syslog' string, logging with syslog. Otherwise using the specified file.
 * \param level Log level.
 * \param size Maximum size of the log file. Zero means unlimited
 */
void logger_reinit(const char * const logfile, enum logger_level level, unsigned int size)
{
	pthread_mutex_lock(&logger_mutex);
	
	if (logfile == NULL || strlen(logfile) == 0) 
		logger_mode = LOGGER_MODE_STDOUT;
	else if (strcmp(logfile, LOGGER_MODE_SYSLOG_FILE) == 0)
		logger_mode = LOGGER_MODE_SYSLOG;
	else {
		logger_mode = LOGGER_MODE_FILE;
		logger_file = strdup(logfile);
	}

	logger_level = level;
    logger_size = size;

	__logger_reopen();

	pthread_mutex_unlock(&logger_mutex);
}

/*! \} */

