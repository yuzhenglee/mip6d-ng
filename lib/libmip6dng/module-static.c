
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-corelib
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <mip6d-ng/list.h>
#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/cfg.h>
#include <module.h>

/*!
 * \brief Module descriptor struct
 */
struct module {
	/*! Linked list support */						struct list_head list;
	/*! name of the module */						char name[100];
	/*! Requested sequence number of the module */	unsigned int seq;
	/*! Config descriptorgetting */					mip6_module_get_config_desc_t * module_get_config_desc_fp;
	/*! Init function */							mip6_module_init_t * module_init_fp;
};

/*!
 * \brief List of modules
 */
static LIST_HEAD(module_list);

/*!
 * \brief Register module. It is used for non-dynamic module loading case.
 * \param name Name of module
 * \param seq Sequence number of module
 * \param module_get_config_desc_fp Function pointer to module configure descriptor getting
 * \param module_init_fp Function pointer to module initialization
 */
void module_register(char * name, unsigned int seq, mip6_module_get_config_desc_t * module_get_config_desc_fp, mip6_module_init_t * module_init_fp)
{
	struct module * mod;
	struct list_head * pos;
	struct module * dmod;
	int dup;

	mod = malloc(sizeof(struct module));
	if (mod == NULL) {
		ERROR("Unable to allocate module");
		exit(EXIT_FAILURE);
	}

	dup = 0;
	list_for_each(pos, &module_list) {
		dmod = list_entry(pos, struct module, list);
		if (strcmp(dmod->name, name) == 0) {
			dup = 1;
			break;
		}
	}
	if (dup) {
		ERROR("Duplicate module name");
		free(mod);
		return;
	}

	strncpy(mod->name, name, sizeof(mod->name));
	mod->seq = seq;
	mod->module_get_config_desc_fp = module_get_config_desc_fp;
	mod->module_init_fp = module_init_fp;

	list_add_tail(&(mod->list), &module_list);

	INFO("Loading module: %s [%u]", mod->name, mod->seq);
}

/*!
 * \brief Initializing non-dynamically loaded modules
 *
 * It reorder modules, specified by sequence numbers
 * \return Zero if OK
 */
int module_static_init()
{
	struct module * mod;
	struct list_head * pos, * pos2;
	struct module * dmod;
	struct list_head * dpos;
	int moved;

	do {
		moved = 0;
		list_for_each_safe(pos, pos2, &module_list) {
			dpos = pos->next;
			if (dpos == &module_list)
				break;
			mod = list_entry(pos, struct module, list);
			dmod = list_entry(dpos, struct module, list);

			if (dmod->seq < mod->seq) {
				list_move_tail(pos, dpos->next);
				moved = 1;
			}
		}
	} while(moved == 1);
		
	return 0;
}

/*!
 * \brief Array with the loaded cfg_opt_t configure options
 */
static cfg_opt_t ** opts = NULL;

/*!
 * \brief Number of loaded cfg_opt_t configure options
 */
static unsigned char opts_len = 0;

/*!
 * \brief Constructing root cfg_opt_t array, which contains all of the root options
 *
 * It gets the options of core (main_opts parameter), and append the options of the modules
 * NOTE: Do not close core options with CFG_END!. At the end it will append it to it.
 * It loads all of the options, and appending the root options to main_opts.
 * It replacing the referencing section pointers to the transfered sym pointer
 * \param main_opts In: Options of core, Out: Options of core and all modules (with CFG_END, ath the end)
 * \param main_opts_len: In: Number of core options, Out: Number of core and all modules options
 * \return Zero if OK, otherwise negative
 */
int module_construct_cfg_opt(cfg_opt_t ** main_opts, unsigned char * main_opts_len)
{
	struct module * mod;
	struct list_head * pos;
	int i, j;
	cfg_opt_t * opt;
	int scnt;
	int start = 0;

	if (main_opts == NULL || main_opts_len == NULL)
		return -1;

	list_for_each(pos, &module_list) {
		mod = list_entry(pos, struct module, list);

		for (i = 0; ; i++) { 
			opt = mod->module_get_config_desc_fp(i);
			if (opt == NULL)
				break;
	
			opts = (cfg_opt_t **)realloc(opts, (opts_len + 1) * sizeof(cfg_opt_t *));
			if (opts == NULL) {
				ERROR("Unable to alloc sym opts");
				exit(EXIT_FAILURE);
			}

			opts[opts_len++] = opt;
		}

		if (opts != NULL) {
			for (i = scnt = start; i < opts_len; i++) {
				for (j = 0; opts[i][j].type != CFGT_NONE; j++) {
					if (opts[i][j].type == CFGT_SEC) {
						scnt++;
						if (scnt >= opts_len) {
							ERROR("Missing cfg sections from exported symbols!");
							break;
						}
						opts[i][j].subopts = opts[scnt];
					}
					if (opts[i][j].type != CFGT_NONE)
						break;
				}

				if (i == start) {
					for (j = 0; opts[i][j].type != CFGT_NONE; j++);
					if (j > 0) {
						*main_opts = realloc(*main_opts, (*main_opts_len + j) * sizeof(cfg_opt_t));
						if (*main_opts == NULL) {
							ERROR("Unable to alloc main opts");
							exit(EXIT_FAILURE);
						}
						memcpy(&(*main_opts)[*main_opts_len], opts[i], j * sizeof(cfg_opt_t));
						*main_opts_len += j;
					}
				}
			}
			start = opts_len;
		}
	}

	*main_opts = realloc(*main_opts, (*main_opts_len + 1) * sizeof(cfg_opt_t));
	if (*main_opts == NULL) {
		ERROR("Unable to alloc main opts");
		exit(EXIT_FAILURE);
	}
	
	(*main_opts)[*main_opts_len].name = 0;
	(*main_opts)[*main_opts_len].type = CFGT_NONE;
	(*main_opts)[*main_opts_len].nvalues = 0;
	(*main_opts)[*main_opts_len].values = 0;
	(*main_opts)[*main_opts_len].flags = CFGF_NONE;
	(*main_opts)[*main_opts_len].subopts = 0;
	(*main_opts)[*main_opts_len].def.number = 0;
	(*main_opts)[*main_opts_len].def.fpnumber = 0;
	(*main_opts)[*main_opts_len].def.boolean = cfg_false;
	(*main_opts)[*main_opts_len].def.string = 0;
	(*main_opts)[*main_opts_len].def.parsed = 0;
	(*main_opts)[*main_opts_len].func = 0;
	(*main_opts)[*main_opts_len].simple_value = 0;
	(*main_opts)[*main_opts_len].parsecb = 0;
	(*main_opts)[*main_opts_len].validcb = 0;
	(*main_opts)[*main_opts_len].pf = 0;
	(*main_opts)[*main_opts_len].freecb = 0;

	*main_opts_len += 1;

	return 0;
}

/*!
 * \brief It frees the transfered and sym cfg_opt_t configure options
 *
 * \param main_opts Array of the options of core and all modules (constructed by module_construct_cfg_opt)
 */
void module_free_cfg_opt(cfg_opt_t * main_opts)
{
	if (opts != NULL) {
		free(opts);
		opts = NULL;
	}
	opts_len = 0;

	if (main_opts != NULL) {
		free(main_opts);
	}
}

/*!
 * \brief Initializing all of the modules
 *
 * It passes the remaining command line arguments and the configure handler to the modules for initialization
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param cfg Configure handler
 * \return Zero if OK, otherwise (if any module returns nonzero) negative
 */
int module_call_init(int argc, char * argv[], cfg_t * cfg)
{
	int ret;
	struct module * mod;
	struct list_head * pos;
	int i_argc;
	char ** i_argv;
	int o_argc;
	char ** o_argv;

	i_argc = argc;
	i_argv = argv;
	o_argc = 0;
	o_argv = NULL;

	list_for_each(pos, &module_list) {
		mod = list_entry(pos, struct module, list);

		ret = mod->module_init_fp(i_argc, i_argv, &o_argc, &o_argv, cfg);
		if (ret) {
			ERROR("Unable to init module: %s", mod->name);
			return -1;
		}

		free_r_args(i_argc, i_argv);
		i_argc = o_argc;
		i_argv = o_argv;
	}

	return 0;
}

/*! \} */
