
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-corelib
 * \{
 */

#include <config.h>

#include <mip6d-ng/threading.h>

/*!
 * \brief Thread attribute to create detached threads 
 */
static pthread_attr_t threading_detached_attr;

/*!
 * \brief Initializing threading support
 */
int threading_init()
{

	pthread_attr_init(&threading_detached_attr);
	pthread_attr_setdetachstate(&threading_detached_attr, PTHREAD_CREATE_DETACHED);

	return 0;
}

/*!
 * \brief Returns with the pthread_attr configured for detached threads
 */
pthread_attr_t threading_get_detached_attr()
{
	return threading_detached_attr;
}

/*! \} */

