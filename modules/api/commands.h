
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-api
 * \{
 */

#ifndef _COMMANDS_H_
#define _COMMANDS_H_

#include <inttypes.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/api.h>

/*!
 * \brief External API command
 */
struct command {
	/*! Linked list management */
	struct list_head list;
	/*! Module identification imessaging ID */
	unsigned int module;
	/*! Name of the command */
	char name[API_COMMAND_NAME_MAX];
	/*! ID of the command */
	uint16_t command;
	/*! Internal command ID */
	unsigned int imsg_cmd;
};

int commands_register(unsigned int module, struct msg_api_register_cmd * rc);
int commands_get(char * module, uint16_t cmd, unsigned int * module_id);

#endif /* _COMMANDS_H_ */

/*! \} */
