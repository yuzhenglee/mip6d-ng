
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-api
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/api.h>

#include "main.h"
#include "recv.h"
#include "tcp.h"
#include "commands.h"

/*!
 * \brief Configuration options
 *
 * See config sample for details!
 */
cfg_opt_t api_api_opts[] = {
    CFG_STR("listening_address", "", CFGF_NONE),
    CFG_INT("listening_port", 7777, CFGF_NONE),
    CFG_END()
};

/*!
 * \brief Configuration options
 *
 * See config sample for details!
 */
cfg_opt_t api_main_opts[] = {
	CFG_SEC("api", api_api_opts, CFGF_NODEFAULT),
	CFG_END()
};

struct api_opts api_opts;

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the api module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int api_proc_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	int ret;
	struct msg_api_register_cmd * rc;

	switch (message) {
	case MSG_API_REGISTER_CMD:
		rc = (struct msg_api_register_cmd *)arg;
		if (rc == NULL)
			return -1;
		ret = commands_register(sender, rc);
		return ret;
		break;
	};

	return -1;
}

static struct imsg_opts imsg_opts = {
	.event_fp = NULL,
	.message_fp = api_proc_message
};

/*!
 * \brief Cleanup
 * \return Zero
 */
static int api_exit_handler(void * arg)
{
	return 0;
}

/*!
 * \brief Initializing api module
 *
 * Registers exit function to the 'core-exit' hook
 * Register internal messaging.
 * Initializes TCP connection.
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int api_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
    cfg_t * api_cfg;
    char * strv;
    int intv;
	unsigned int core_exit_id;

	DEBUG("Initializing module: api");

	ret = imsg_register(MSG_API, &imsg_opts);
    if (ret < 0)
		return ret;
	api_opts.msg_ids.me = ret;

	memset(&api_opts.tcph, 0, sizeof(struct tcp_handle));
    api_opts.tcph.is_i4 = 0;
    api_opts.tcph.is_i6 = 1;
    memcpy(&api_opts.tcph.i6.local, &in6addr_any, sizeof(struct in6_addr));
    api_opts.tcph.i6.localp = 7777;

    if (cfg == NULL)
		return -1;
	api_cfg = cfg_getsec(cfg, "api");
	if (api_cfg != NULL) {
        strv = cfg_getstr(api_cfg, "listening_address");
        if (strv == NULL || strlen(strv) == 0) {
            DEBUG("API listening address is unspecified: using in6addr_any");
            ret = 1;
        } else
            ret = inet_pton(AF_INET6, strv, &api_opts.tcph.i6.local);
        intv = cfg_getint(api_cfg, "listening_port");
        if (ret != 1 || intv <= 0 || intv > UINT16_MAX) {
            ERROR("Invalid API config: invalid address or port (ret %d, intv %d)", ret, intv);
            return -1;
        }
        api_opts.tcph.i6.localp = intv;
    }

	ret = hooks_register("api-peer-close-hook");
	if (ret < 0) {
		ERROR("Unable to register peer-close-hook");
		return ret;
	}
	api_opts.peer_close_hook_id = ret;
    
    ret = tcp_init(&api_opts.tcph, recv_fn, NULL);
    if (ret) {
        ERROR("Unable to initialize API tcp connection");
        return ret;
    }
    
	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 100-SEQ_API, api_exit_handler);
	}

	return 0;
}

MIP6_MODULE_INIT(" api", SEQ_API, api_module_init, api_main_opts, api_api_opts);

/*! \} */

