/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-api
 * \{
 */

#ifndef MIP6D_NG_API_MAIN_H_
#define MIP6D_NG_API_MAIN_H_

#include <mip6d-ng/api.h>
#include "tcp.h"

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
struct api_opts {
	/*! Messaging IDs */
	struct {
		/*! API module's own ID */
		int me;
	} msg_ids;
	/*! TCP handler */
    struct tcp_handle tcph;
    /*! Hook id to close connection with a specific hook */
	int peer_close_hook_id;
};

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
extern struct api_opts api_opts;

#endif /* MIP6D_NG_MAIN_H_ */

/*! \} */
