
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-api
 * \{
 */

#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/select.h>
#include <fcntl.h>

#include <pthread.h>

#include "tcp.h"

#ifndef DISABLE_TCP_LOGGING
#include <mip6d-ng/logger.h>
#else   
#define DEBUG(fmt, ...)     fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...)     fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#endif      

#ifndef TCP_RECV_BUFFER_SIZE
#define TCP_RECV_BUFFER_SIZE	(5*1024)
#endif

/*!
 * \brief Receiving function
 *
 * Receiving function of a given peer
 *
 * \param arg tcp_peer
 * \return Unused
 */
static void * tcp_recv_thread(void * arg)
{
	int ret;
	struct tcp_peer * peer = (struct tcp_peer *)arg;
	char addr4[INET_ADDRSTRLEN + 1];
	char addr6[INET6_ADDRSTRLEN + 1];
	fd_set fs;
	struct timeval tv;
	unsigned char buf[TCP_RECV_BUFFER_SIZE];
	int blen = sizeof(buf);

	if (peer == NULL || peer->handle == NULL)
		return NULL;
	if (peer->family != AF_INET && peer->family != AF_INET6)
		return NULL;

	if (peer->family == AF_INET) {
		if (inet_ntop(AF_INET, &peer->i4.remote, addr4, sizeof(addr4)) != NULL)
			DEBUG("IPv4/TCP receiving thread started (from %s:%u)", addr4, peer->i4.remotep);
	} else {
		if (inet_ntop(AF_INET6, &peer->i6.remote, addr6, sizeof(addr6)) != NULL) 
			DEBUG("IPv6/TCP receiving thread started (from [%s]:%u)", addr6, peer->i6.remotep);
	}

	for (;;) {
		pthread_mutex_lock(&peer->lock);
		if (peer->fd == -1)
			goto destroy;

		FD_ZERO(&fs);
		FD_SET(peer->fd, &fs);

		tv.tv_sec  = 1;
		tv.tv_usec = 0;
		
		ret = select(peer->fd + 1, &fs, 0, 0, &tv);
		if (ret != 1) 
			goto retry;	
		if (! FD_ISSET(peer->fd, &fs)) 
			goto retry;
	
		blen = sizeof(buf);
		ret = recv(peer->fd, buf, blen, 0);

		if (ret == 0) {
			if (peer->family == AF_INET) 
				DEBUG("IPv4/TCP connection closed from %s:%u", addr4, peer->i4.remotep);
			else
				DEBUG("IPv6/TCP connection closed from [%s]:%u", addr6, peer->i6.remotep);
			goto close_and_destroy;
		}

		if (ret < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK)
				goto retry;

			if (peer->family == AF_INET) 
				ERROR("IPv4/TCP receive error from %s:%u. %s (%d)", addr4, peer->i4.remotep, strerror(errno), ret);
			else
				ERROR("IPv6/TCP receive error from [%s]:%u. %s (%d)", addr6, peer->i6.remotep, strerror(errno), ret);
			goto close_and_destroy;
		}
			
		blen = ret;
		if (peer->family == AF_INET) 
			DEBUG("IPv4/TCP received %d bytes from %s:%u", blen, addr4, peer->i4.remotep);
		else
			DEBUG("IPv6/TCP received %d bytes from [%s]:%u", blen, addr6, peer->i6.remotep);


		pthread_mutex_unlock(&peer->lock);

		if (peer->fn != NULL) 
			peer->fn(peer, buf, blen, peer->priv);

		sleep(1);
		continue;

retry:
		pthread_mutex_unlock(&peer->lock);

		sleep(5);
	}
	
	return NULL;

close_and_destroy:

	if (peer->fn != NULL)
		peer->fn(peer, NULL, 0, peer->priv);
	close(peer->fd);
	peer->fd = -1;

destroy:
			
	pthread_mutex_lock(&peer->handle->lock);
	list_del(&peer->list);
	pthread_mutex_unlock(&peer->handle->lock);
	free(peer);
	
	return NULL;
}

/*!
 * Argument of TCP accept thread
 */
struct tcp_accept_arg {
	/*! TCP handle */
	struct tcp_handle * handle;
	/*! Protocol family: AF_INET or AF_INET6 */
	int family;
	/*! Receiving / parsing callback function */
	void (* fn)(struct tcp_peer *, unsigned char *, unsigned int, void *);
	/*! Extra argument of receiving / parsing callback */
	void * priv;
};

/*!
 * \brief TCP accept thread
 *
 * Accept incoming connections, and creates receiving threads
 * for each peer
 *
 * \param arg struct tcpaccept_arg
 * \return Unused
 */
static void * tcp_accept_thread(void * arg)
{
	int ret;
	struct tcp_accept_arg * aa = (struct tcp_accept_arg *)arg;
	int fd;
	struct sockaddr_in  si4_remote;
	struct sockaddr_in6 si6_remote;
	struct sockaddr * si_remote;
	socklen_t sir_len;
	int remote_fd;
	char addr4[INET_ADDRSTRLEN + 1];
	char addr6[INET6_ADDRSTRLEN + 1];
	struct tcp_peer * peer = NULL;
	pthread_attr_t thattr;
	pthread_t th;

	if (aa == NULL || aa->handle == NULL)
		return NULL;
	if (aa->family != AF_INET && aa->family != AF_INET6)
		return NULL;

	pthread_attr_init(&thattr);
	pthread_attr_setdetachstate(&thattr, PTHREAD_CREATE_DETACHED);

	if (aa->family == AF_INET) {
		if (inet_ntop(AF_INET, &aa->handle->i4.local, addr4, sizeof(addr4)) != NULL)
			DEBUG("IPv4/TCP accept thread started on %s:%u", addr4, aa->handle->i4.localp);
	} else {
		if (inet_ntop(AF_INET6, &aa->handle->i6.local, addr6, sizeof(addr6)) != NULL) 
			DEBUG("IPv6/TCP accept thread started on [%s]:%u", addr6, aa->handle->i6.localp);
	}

	for(;;) {
		peer = NULL;
		remote_fd = -1;

		if (aa->family == AF_INET) {
			memset(&si4_remote, 0, sizeof(si4_remote));
			si_remote = (struct sockaddr *)&si4_remote;
			sir_len = sizeof(si4_remote);
			fd = aa->handle->i4.fd;
		} else {
			memset(&si6_remote, 0, sizeof(si6_remote));
			si_remote = (struct sockaddr *)&si6_remote;
			sir_len = sizeof(si6_remote);
			fd = aa->handle->i6.fd;
		}
		ret = accept(fd, si_remote, &sir_len);

		if (ret < 0) {
			ERROR("%s/TCP accpet error", (aa->family == AF_INET) ? "IPv4" : "IPv6");
			goto error;
		}
		remote_fd = ret;

		if (aa->family == AF_INET) {
			if (inet_ntop(AF_INET, &si4_remote.sin_addr, addr4, sizeof(addr4)) == NULL) {
				ERROR("Invalid accpeted IPv4 address");
				goto error;
			}
			DEBUG("New IPv4/TCP connection from %s:%u", addr4, ntohs(si4_remote.sin_port));
		} else {
			if (inet_ntop(AF_INET6, &si6_remote.sin6_addr, addr6, sizeof(addr6)) == NULL) {
				ERROR("Invalid accpeted IPv6 address");
				goto error;
			}
			DEBUG("New IPv6/TCP connection from [%s]:%u", addr6, ntohs(si6_remote.sin6_port));
		}

		ret = fcntl(remote_fd, F_SETFL, O_NONBLOCK);
		if (ret) {
			ERROR("Unable to set non-blocking IO on TCP socket");
			goto error;
		}

		peer = malloc(sizeof(struct tcp_peer));
		if (peer == NULL) {
			ERROR("Out of memory: Unable to allocate TCP peer");
			goto error;
		}

		peer->fd = remote_fd;
		peer->family = aa->family;
		if (aa->family == AF_INET) {
			memcpy(&peer->i4.remote, &si4_remote.sin_addr, sizeof(struct in_addr));
			peer->i4.remotep = ntohs(si4_remote.sin_port);
		} else {
			memcpy(&peer->i6.remote, &si6_remote.sin6_addr, sizeof(struct in6_addr));
			peer->i6.remotep = ntohs(si6_remote.sin6_port);
		}
		pthread_mutex_init(&peer->lock, NULL);
		peer->fn = aa->fn;
		peer->priv = aa->priv;
		peer->handle = aa->handle;

		ret = pthread_create(&th, &thattr, tcp_recv_thread, peer);
		if (ret) {
			ERROR("Unable to create %s/TCP receive thread", (aa->family == AF_INET) ? "IPv4" : "IPv6");
			goto error;
		}

		peer->thread_id = th;
		pthread_mutex_lock(&aa->handle->lock);
		list_add_tail(&peer->list, &aa->handle->peers);
		pthread_mutex_unlock(&aa->handle->lock);

		continue;

error:
		if (remote_fd >= 0)
			close(remote_fd);
		if (peer != NULL) 
			free(peer);
	}

	return NULL;
}

/*!
 * \brief Initialize TCP connection
 *
 * The TCP connection is specified by the configured handler. The is_i4 and is_i6 flags should to be configured
 * and the corresponding local address, port values should to be given.
 * It starts the accpet thread.
 *
 * \param handle TCP handle
 * \param fn Receiving / parsing callback function
 * \param priv Extra argument for fn
 * \return Zero if OK
 */
int tcp_init(struct tcp_handle * handle, void (* fn)(struct tcp_peer * peer, unsigned char *, unsigned int, void *), void * priv)
{
	int ret;
	int v;
	struct sockaddr_in  si4_local;
	struct sockaddr_in6 si6_local;
	struct tcp_accept_arg * aa = NULL;
	pthread_attr_t thattr;
	pthread_t th;

	if (handle == NULL) 
		return -1;

	INIT_LIST_HEAD(&handle->peers);
	pthread_mutex_init(&handle->lock, NULL);
	handle->i4.fd = -1;
	handle->i6.fd = -1;
		
	pthread_attr_init(&thattr);
	pthread_attr_setdetachstate(&thattr, PTHREAD_CREATE_DETACHED);

	if (handle->is_i4) {
		ret = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (ret < 0) {
			ERROR("Unable to create IPv/TCP socket");
			goto out;
		}
		handle->i4.fd = ret;

		v = 1;
		ret = setsockopt(handle->i4.fd, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(v));
		if (ret) {
			ERROR("Unable to do IPv4/TCP SO_REUSEADDR");
			goto out;
		}
		
		memset(&si4_local, 0, sizeof(si4_local));
		si4_local.sin_family = AF_INET;
		si4_local.sin_port = htons(handle->i4.localp);
		memcpy(&si4_local.sin_addr, &handle->i4.local, sizeof(struct in_addr));

		ret = bind(handle->i4.fd, (struct sockaddr *)&si4_local, sizeof(si4_local));
		if (ret < 0) {
			ERROR("Unable to do IPv4/TCP bind");
			goto out;
		}

		ret = listen(handle->i4.fd, 5);

		aa = malloc(sizeof(struct tcp_accept_arg));
		if (aa == NULL) {
			ERROR("Out of memory: Unable to start IPv4/TCP accept thread");
			goto out;
		}

		aa->handle = handle;
		aa->family = AF_INET;
		aa->fn = fn;
		aa->priv = priv;
		ret = pthread_create(&th, &thattr, tcp_accept_thread, aa);
		if (ret) {
			ERROR("Unable to create IPv4/TCP accept thread");
			goto out;
		}
		aa = NULL;

	}

	if (handle->is_i6) {
		ret = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
		if (ret < 0) {
			ERROR("Unable to create IPv6/TCP socket");
			goto out;
		}
		handle->i6.fd = ret;

		v = 1;
		ret = setsockopt(handle->i6.fd, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(v));
		if (ret) {
			ERROR("Unable to do IPv6/TCP SO_REUSEADDR");
			goto out;
		}
		
		memset(&si6_local, 0, sizeof(si6_local));
		si6_local.sin6_family = AF_INET6;
		si6_local.sin6_port = htons(handle->i6.localp);
		memcpy(&si6_local.sin6_addr, &handle->i6.local, sizeof(struct in6_addr));

		ret = bind(handle->i6.fd, (struct sockaddr *)&si6_local, sizeof(si6_local));
		if (ret < 0) {
			ERROR("Unable to do IPv6/TCP bind");
			goto out;
		}

		ret = listen(handle->i6.fd, 5);

		aa = malloc(sizeof(struct tcp_accept_arg));
		if (aa == NULL) {
			ERROR("Out of memory: Unable to start IPv6/TCP accept thread");
			goto out;
		}

		aa->handle = handle;
		aa->family = AF_INET6;
		aa->fn = fn;
		aa->priv = priv;
		ret = pthread_create(&th, &thattr, tcp_accept_thread, aa);
		if (ret) {
			ERROR("Unable to create IPv6/TCP accept thread");
			goto out;
		}
		aa = NULL;	
	}
		
	pthread_attr_destroy(&thattr);

	return 0;

out:
	if (handle->i4.fd >= 0) {
		close(handle->i4.fd);
		handle->i4.fd = -1;
	}
	if (handle->i6.fd >= 0) {
		close(handle->i6.fd);
		handle->i6.fd = -1;
	}

	return -1;
}

/*!
 * \brief Close a TCP connection
 *
 * Close the peer and main file descriptors
 *
 * \param handle TCP handle
 * \return Zero if OK
 */
int tcp_close(struct tcp_handle * handle)
{
	struct list_head * pos, * pos2;
	struct tcp_peer * peer;

	if (handle == NULL)
		return -1;

	pthread_mutex_lock(&handle->lock);

	list_for_each_safe(pos, pos2, &handle->peers) {
		peer = list_entry(pos, struct tcp_peer, list);
		pthread_mutex_lock(&peer->lock);
		close(peer->fd);
		peer->fd = -1;
		pthread_mutex_unlock(&peer->lock);
	}

	if (handle->i4.fd >= 0) {
		close(handle->i4.fd);
		handle->i4.fd = -1;
	}
	if (handle->i6.fd >= 0) {
		close(handle->i6.fd);
		handle->i6.fd = -1;
	}
	
	pthread_mutex_unlock(&handle->lock);

	return 0;
}

/*!
 * \brief TCP sending function
 *
 * \param peer TCP peer information
 * \param buf Message to send
 * \param blen Message length
 * \return Sent bytes or negative
 */
static int __tcp_send(struct tcp_peer * peer, unsigned char * buf, unsigned int blen)
{
	int ret = -1;
	char addr4[INET_ADDRSTRLEN + 1];
	char addr6[INET6_ADDRSTRLEN + 1];
	unsigned char * buf_ptr = buf;
	unsigned int blen_rem = blen;

	if (peer == NULL || peer->handle == NULL)
		return -1;
	if (peer->family != AF_INET && peer->family != AF_INET6)
		return -1;
	if (buf == NULL || blen == 0)
		return -1;

	pthread_mutex_lock(&peer->lock);
	
	if (peer->family == AF_INET) {
		inet_ntop(AF_INET, &peer->i4.remote, addr4, sizeof(addr4));
	} else {
		inet_ntop(AF_INET6, &peer->i6.remote, addr6, sizeof(addr6));
	}

	if (peer->fd < 0)
		goto out;

	for (;;) {
		ret = send(peer->fd, buf_ptr, blen_rem, 0);
		
		if (ret == 0) {
			if (peer->family == AF_INET) 
				DEBUG("IPv4/TCP connection closed from %s:%u", addr4, peer->i4.remotep);
			else
				DEBUG("IPv6/TCP connection closed from [%s]:%u", addr6, peer->i6.remotep);
			goto close;
		}

		if (ret < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK)
				goto out;

			if (peer->family == AF_INET) 
				ERROR("IPv4/TCP send error from %s:%u. %s (%d)", addr4, peer->i4.remotep, strerror(errno), ret);
			else
				ERROR("IPv6/TCP send error from [%s]:%u. %s (%d)", addr6, peer->i6.remotep, strerror(errno), ret);
			goto close;
		}

		if (peer->family == AF_INET) {
			DEBUG("IPv4/TCP sent %d bytes to %s:%u", ret, addr4, peer->i4.remotep);
		} else {
			DEBUG("IPv6/TCP sent %d bytes to [%s]:%u", ret, addr6, peer->i6.remotep);
		}

		buf_ptr += ret;
		blen_rem -= ret;
		if (blen_rem == 0)
			break;
	}

out:
	pthread_mutex_unlock(&peer->lock);

	return blen - blen_rem;

close:
		
	close(peer->fd);
	peer->fd = -1;
	
	pthread_mutex_unlock(&peer->lock);

	return -1;
}

/*!
 * \brief TCP sending function
 *
 * \param peer TCP peer information
 * \param buf Message to send
 * \param blen Message length
 * \return Sent bytes or negative
 */
int tcp_send(struct tcp_peer * peer, unsigned char * buf, unsigned int blen)
{
	return __tcp_send(peer, buf, blen);
}

/*!
 * \brief Send to all peers
 *
 * It calls the __tcp_send function for all the connected peers
 *
 * \param handle TCP handle
 * \param buf Message to send
 * \param blen Message length
 * \return Number of peers which are received the message or negative
 */
int tcp_send_all(struct tcp_handle * handle, unsigned char * buf, unsigned int blen)
{
	int ret;
	int cnt = 0;
	struct list_head * pos, *pos2;
	struct tcp_peer * peer;

	if (handle == NULL)
		return -1;
	if (buf == NULL || blen == 0)
		return -1;

	pthread_mutex_lock(&handle->lock);
	
	list_for_each_safe(pos, pos2, &handle->peers) {
		peer = list_entry(pos, struct tcp_peer, list);

		ret = __tcp_send(peer, buf, blen);
		if (ret == blen)
			cnt++;
	}

	pthread_mutex_unlock(&handle->lock);
	
	return cnt;
}

/*! \} */
