
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-control
 * \{
 */

#ifndef MIP6D_NG_HA_H_
#define MIP6D_NG_HA_H_

#include <mip6d-ng/data.h>
#include <mip6d-ng/core-comm.h>

int ctrl_ha_init(cfg_t * cfg);
int ctrl_ha_clean();

int ctrl_ha_init_bce(struct bce * bce);
int ctrl_ha_bce_update(struct bce * bce);


#endif /* MIP6D_NG_HA_H_ */

/*! \} */
