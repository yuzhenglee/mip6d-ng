
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-control
 * \{
 */

#ifndef MIP6D_NG_CONTROLL_H
#define MIP6D_NG_CONTROLL_H

/*! 
 * \brief The name of the internal messaging reference point
 */
#define MSG_CTRL			"ctrl"

/*!
 * \brief Module load order number of control module
 */
#define SEQ_CTRL			20

/*!
 * \brief Available data API message commands
 */
enum control_messages {
	/*!
	 * \brief Get node type
	 *
	 * Argument: none (it could to be NULL)
	 *
	 * Reply argument: enum ctrl_node_type
	 */
	MSG_CTRL_GET_NODE_TYPE,
	/*!
	 * \brief Get Home Address (on Mobile Node)
	 *
	 * Argument: none (it could to be NULL)
	 *
	 * Reply argument: struct in6_addr
	 */
	MSG_CTRL_GET_HOA,
	/*!
	 * \brief Request sending Binding Update now
	 *
	 * Argument: struct bule
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_CTRL_RESEND_BU_NOW,
	/*!
	 * \brief Get Home Address (on Mobile Node), used by API module
	 *
	 * Argument: none (it could to be NULL)
	 *
	 * Reply argument: struct in6_addr
	 */
	MSG_CTRL_API_GET_HOA,
	/*!
	 * \brief Get Home Agent Address
	 *
	 * Argument: none (it could to be NULL)
	 *
	 * Reply argument: struct in6_addr
	 */
	MSG_CTRL_GET_HAA,
	/*!
	 * \brief Get Home Agent Address, used by API module
	 *
	 * Argument: none (it could to be NULL)
	 *
	 * Reply argument: struct in6_addr
	 */
	MSG_CTRL_API_GET_HAA,
	MSG_CTRL_NONE
};

/*!
 * \brief Node type
 */
enum ctrl_node_type {
	/*! Home Agent */
	CTRL_NODE_TYPE_HA,
	/*! Mobile Node */
	CTRL_NODE_TYPE_MN,
	/*! Correspondent Node */
	CTRL_NODE_TYPE_CN,
	CTRL_NODE_TYPE_NONE,
};

/*!
 * \brief Available data API commands by the api module 
 * \ingroup eapi-control
 */
enum ctrl_api_commands {
	/*! Get Home Address (on Mobile Node) */
	API_CTRL_GET_HOA,
	/*! Get Home Agent Address */
	API_CTRL_GET_HAA,
	API_CTRL_NONE
};

#endif /* MIP6D_NG_CONTROLL_H */

/*! \} */
