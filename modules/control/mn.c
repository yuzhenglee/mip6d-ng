
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-control
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>
#include <pthread.h>

#include <net/if.h>
#include <arpa/inet.h>
#include <linux/if_addr.h>
#include <linux/rtnetlink.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/tasks.h>
#include <mip6d-ng/missing.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/md.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/control.h>

#include "main.h"
#include "mn.h"

/*!
 * \brief Home Address configuration (DAD) pool
 */
static LIST_HEAD(hoa_pool);

/*!
 * \brief Home Address configuration (DAD) pool lock
 */
static pthread_mutex_t hoa_pool_lock = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief Home Address configuration (DAD) pool item
 */
struct hoa_pool_e {
	/*! Linked list management */
	struct list_head list;
	/*! Destination address of BULE */
	struct in6_addr dest;
	/*! Interface index */
	int ifindex;
};

/*!
 * Default Binding Update resend (re-try) interval in seconds
 */
#define CTRL_MN_DEFAULT_RESEND_IVAL		1.5

 /*!
  * Maximum Binding Update resend (re-try) interval in seconds
  */
#define CTRL_MN_MAX_RESEND_IVAL			32

/*!
 * \brief Initialize Mobile Node
 *
 * Read configuration file, and created XFRM rule to
 * receive BA (type 2 routing header decapsulation)
 * 
 * \param cfg Confuse cfg
 * \return Zero if OK, otherwise negative
 */
int ctrl_mn_init(cfg_t * cfg)
{
	int ret;
	int len;
	char * hoa;
	char * ha;
	int lifetime;
	struct msg_env_rt2 rt2;
	struct msg_env_ip6ip6_tunnel i6t;

	DEBUG("Initializing node: MN");
	ctrl_node_type = CTRL_NODE_TYPE_MN;

	hoa = cfg_getstr(cfg, "home-address");
	len = strlen(hoa);
	if (hoa == NULL || len == 0) {
		ERROR("Missing home-address option");
		return -1;
	}
	ret = inet_pton(AF_INET6, hoa, &ctrl_node_opts.mn.hoa);
	if (ret != 1) {
		ERROR("Invalid home-address option: '%s'", hoa);
		return -1;
	}
	DEBUG("Home-address: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ctrl_node_opts.mn.hoa));

	ha = cfg_getstr(cfg, "ha-address");
	len = strlen(ha);
	if (ha == NULL || len == 0) {
		ERROR("Missing ha-address option");
		return -1;
	}
	ret = inet_pton(AF_INET6, ha, &ctrl_node_opts.mn.haa);
	if (ret != 1) {
		ERROR("Inavalid ha-address option: '%s'", ha);
		return -1;
	}
	DEBUG("HA address: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ctrl_node_opts.mn.haa));

	lifetime = cfg_getint(cfg, "lifetime");
	ctrl_node_opts.mn.lifetime = lifetime;

	memset(&rt2, 0, sizeof(rt2));
	memcpy(&rt2.src, &ctrl_node_opts.mn.haa, sizeof(rt2.src));
	rt2.splen = 128;
	memcpy(&rt2.dst, &ctrl_node_opts.mn.hoa, sizeof(rt2.dst));
	rt2.dplen = 128;
	rt2.proto = IPPROTO_MH;
	rt2.type = 6;
	rt2.dir = XFRM_POLICY_IN;
	rt2.prio = 5;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env, MSG_ENV_RT2,
			&rt2, sizeof(rt2),NULL, 0);
	if (ret) {
		ERROR("Unable to create RT2 opt XFRM policy: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ctrl_node_opts.mn.hoa));
		return -1;
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.daddr, &ctrl_node_opts.mn.hoa, sizeof(i6t.daddr));
	i6t.dplen = 128;
	i6t.prio = 10;
	i6t.dir = XFRM_POLICY_IN;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.ccom, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, &ctrl_node_opts.mn.hoa, sizeof(i6t.saddr));
	i6t.splen = 128;
	i6t.prio = 15;
	i6t.dir = XFRM_POLICY_OUT;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.ccom, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	return 0;
}

/*!
 * \brief Cleanup MN functionalities
 *
 * Request deleting of BUL entries from data module
 * and deletes the XFRM rules created by the ctrl_mn_init
 * function
 * \return Zero if OK
 */
int ctrl_mn_clean()
{
	int ret;
	struct msg_env_rt2 rt2;
	struct msg_env_ip6ip6_tunnel i6t;

	DEBUG("Cleaning node: MN");

	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.data, MSG_DATA_CLEAR_BUL, NULL, 0, NULL, 0);
	if (ret < 0) {
		ERROR("Unable to clear all BUL entries");
		return -1;
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.daddr, &ctrl_node_opts.mn.hoa, sizeof(i6t.daddr));
	i6t.dplen = 128;
	i6t.prio = 10;
	i6t.dir = XFRM_POLICY_IN;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.ccom, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	memset(&rt2, 0, sizeof(rt2));
	memcpy(&rt2.src, &ctrl_node_opts.mn.haa, sizeof(rt2.src));
	rt2.splen = 128;
	memcpy(&rt2.dst, &ctrl_node_opts.mn.hoa, sizeof(rt2.dst));
	rt2.dplen = 128;
	rt2.proto = IPPROTO_MH;
	rt2.type = 6;
	rt2.dir = XFRM_POLICY_IN;
	rt2.prio = 5;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env, MSG_ENV_RT2_DEL,
			&rt2, sizeof(rt2),NULL, 0);
	if (ret) {
		ERROR("Unable to create RT2 opt XFRM policy: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ctrl_node_opts.mn.hoa));
		return -1;
	}

	return 0;
}

/*!
 * \brief Creating environmental stuff for signaling packets
 *
 * It adds the Home Address to the interface. It saves the address and the interface in the
 * HoA DAD pool, and wait for successful address configuration.
 *
 * While the Home Address Destination Option XFRM rule creation was moved to the core-comm module
 * (before sending BU message), this function does not do anything else.
 *
 * \param hoa Home Address
 * \param ifindex Interface index
 * \param bule_dest Destination address in the BULE (typically the Home Agent Address)
 * \return Zero if OK
 */
static int ctrl_mn_bule_env_create(struct in6_addr * hoa, unsigned int ifindex, struct in6_addr * bule_dest)
{
	int ret;
	struct msg_env_addr a;
	struct list_head * pos, * pos2;
	struct hoa_pool_e * hpe, * hpe2;

	DEBUG("MN: BULE environment create: ADDR");

	hpe = malloc(sizeof(struct hoa_pool_e));
	if (hpe == NULL) {
		ERROR("Out of memory: Unable to alloc hoa_pool_e");
		return -1;
	}
	hpe->ifindex = ifindex;
	memcpy(&hpe->dest, bule_dest, sizeof(struct in6_addr));
	pthread_mutex_lock(&hoa_pool_lock);
	list_for_each_safe(pos, pos2, &hoa_pool) {
		hpe2 = list_entry(pos, struct hoa_pool_e, list);
		if (hpe2->ifindex == ifindex) {
			list_del(pos);
			free(hpe2);
		}
	}
	list_add_tail(&hpe->list, &hoa_pool);
	pthread_mutex_unlock(&hoa_pool_lock);

	memset(&a, 0, sizeof(a));
	memcpy(&a.addr, hoa, sizeof(a.addr));
	a.plen = 128;
	a.ifindex = ifindex;
	a.flags = IFA_F_HOMEADDRESS;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env, MSG_ENV_ADDR,
			&a, sizeof(a), NULL, 0);
	if (ret) {
		ERROR("Unable to configure address " IP6ADDR_FMT " on interface: %d",
				IP6ADDR_TO_STR(hoa), ifindex);
		return -1;
	}

	return 0;
}

/*!
 * \brief Cleaning environmental stuff for signaling packets
 *
 * It deletes the Home Address from the given interface.
 *
 *  While the Home Address Destination Option XFRM rule creation was moved to the core-comm module
 * (before sending BU message), this function does not do anything else.
 *
 * \param hoa Home Address
 * \param ifindex Interface index
 * \return Zero if OK
 */
static int ctrl_mn_bule_env_clean(struct in6_addr * hoa, unsigned int ifindex)
{
	int ret;
	struct msg_env_addr a;

	DEBUG("MN: BULE environment clean: ADDR");

	memset(&a, 0, sizeof(a));
	memcpy(&a.addr, hoa, sizeof(a.addr));
	a.plen = 128;
	a.ifindex = ifindex;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env, MSG_ENV_ADDR_DEL,
			&a, sizeof(a), NULL, 0);
	if (ret) {
		ERROR("Unable to delete address " IP6ADDR_FMT " on interface: %d",
				IP6ADDR_TO_STR(hoa), ifindex);
		return -1;
	}

	return 0;
}

/*!
 * \brief Creating environmental stuff for data packets
 *
 * It creates the outgoing and incoming tunnel's XFRM representation.
 *
 * \param local Local address (home address)
 * \param remote Destination address (typically the home agent address)
 * \param coa Care-of Address
 * \param mark MARK value if exists (otherwise zero)
 * \return Zero if OK
 */
static int ctrl_mn_data_env_create(struct in6_addr * local, struct in6_addr * remote, struct in6_addr * coa, unsigned int mark)
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;
	
	DEBUG("MN: BULE data create: IN-OUT TUNNELS");

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, local, sizeof(i6t.saddr));
	i6t.splen = 128;
	if (mark > 0)
		i6t.mark = mark;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, coa, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, remote, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_OUT;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.ccom, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
	i6t.splen = 128;
	memcpy(&i6t.daddr, coa, sizeof(i6t.daddr));
	i6t.dplen = 128;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, remote, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, coa, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_IN;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.ccom, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	return 0;
}

/*!
 * \brief Cleaning environmental stuff for data packets
 *
 * It destroys the outgoing and incoming tunnel's XFRM representation.
 *
 * \param local Local address (home address)
 * \param remote Destination address (typically the home agent address)
 * \param coa Care-of Address
 * \param mark MARK value if exists (otherwise zero)
 * \return Zero if OK
 */
static int ctrl_mn_data_env_clean(struct in6_addr * local, struct in6_addr * remote, struct in6_addr * coa, unsigned int mark)
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;

	DEBUG("MN: BULE data clean: IN-OUT TUNNELS");

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, local, sizeof(i6t.saddr));
	i6t.splen = 128;
	if (mark > 0)
		i6t.mark = mark;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, coa, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, remote, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_OUT;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.ccom, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to delete IP6IP6 XFRM tunnel");
		return -1;
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
	i6t.splen = 128;
	memcpy(&i6t.daddr, coa, sizeof(i6t.daddr));
	i6t.dplen = 128;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, remote, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, coa, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_IN;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.ccom, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to delete IP6IP6 XFRM tunnel");
		return -1;
	}
	
	return 0;
}

static void ctrl_mn_bu_resend(void * arg);

/*!
 * \brief Starting scheduled BU resend task
 *
 * It destroys teh existing (waiting) resend task, if exists, and create a new one
 *
 * \param bule BULE
 * \param lock Is the BULE locked?
 * \param sec Schedule: seconds
 * \param nsec Schedule: nanoseconds
 * \return Zero if OK
 */
static int ctrl_mn_bu_resend_start(struct bule * bule, unsigned char lock, unsigned long sec, unsigned long nsec)
{
	int retval = 0;
	unsigned long id;
	struct timespec task_ts;

	if (bule == NULL)
		return -1;

	if (lock)
		bule_lock(bule);
	
	if (bule->resend_task != 0) {
		tasks_del(bule->resend_task);
		bule->resend_task = 0;
		/* Get refcnt here, got in bu_resend_start */
		refcnt_put(&bule->refcnt);
	}

	task_ts.tv_sec = sec;
	task_ts.tv_nsec = nsec;
	DEBUG("BU resend task: +%u sec %u nsec", task_ts.tv_sec, task_ts.tv_nsec);
	/* Get refcnt here, put in bu_resend */
	refcnt_get(&bule->refcnt);
	id = tasks_add_rel(ctrl_mn_bu_resend, bule, task_ts);
	bule->resend_task = id;
	if (id == 0) {
		ERROR("Unable to start BU resend task");
		retval = -1;
	} else
		bule->flags |= BULE_F_RESEND;

	if (lock)
		bule_unlock(bule);

	return retval;
}

/*!
 * \brief Cancel a scheduled and waiting BU resend task
 *
 * \param bule BULE
 * \param lock Is the BULE locked?
 */
static void ctrl_mn_bu_resend_cancel(struct bule * bule, unsigned char lock)
{

	if (bule == NULL)
		return;

	if (lock)
		bule_lock(bule);

	if (bule->resend_task != 0) {
		DEBUG("BU resend task cancelled");
		tasks_del(bule->resend_task);
		bule->resend_task = 0;
		/* Get refcnt here, got in bu_resend_start */
		refcnt_put(&bule->refcnt);
	} else
		DEBUG("BU resend task: not running, unable to cancel");

	if (lock)
		bule_unlock(bule);
}

/*!
 * \brief Argument of the ctrl_mn_bule_del_find function
 */
struct ctrl_mn_bule_del_find {
	/*! Destination address to delete */
	struct in6_addr to_del_dest;
	/*! Home Address to delete */
	struct in6_addr to_del_hoa;
	/*! Interface index to delete */
	int to_del_ifindex;
	/*! Care-of address which could to be used to send Lifetime=0-BU */
	struct in6_addr send_coa;
	/*! Default route which could to be used to send Lifetime=0-BU */
	struct in6_addr send_default_route;
	/*! Interface which could to be used to send Lifetime=0-BU */
	int send_ifindex;
	/*! Configuration found to send the Lifetime=0-BU */
	unsigned char send_found;
};

/*!
 * \brief BUL iteration function to find a BULE which cound send the Lifetime=0-BU of another
 *
 * We need to find a CoA, ifindex, default route triple which successfully could deliver the
 * Lifetime=0-BU message of a BULE which is under deletion
 *
 * \param bule BULE to examine
 * \param arg struct ctrl_mn_bule_del_find
 * \return Zero to continue, -1 to stop iteration
 */
static int ctrl_mn_bule_del_find(struct bule * bule, void * arg)
{
	int ret = 0;
	struct ctrl_mn_bule_del_find * df = (struct ctrl_mn_bule_del_find *)arg;

	bule_lock(bule);
	if (memcmp(&bule->dest, &df->to_del_dest, sizeof(struct in6_addr)) == 0 &&
		memcmp(&bule->hoa,  &df->to_del_hoa,  sizeof(struct in6_addr)) == 0 &&
		bule->ifindex != df->to_del_ifindex) {
		memcpy(&df->send_coa, &bule->coa, sizeof(struct in6_addr));
		memcpy(&df->send_default_route, &bule->default_route, sizeof(struct in6_addr));
		df->send_ifindex = bule->ifindex;
		df->send_found = 1;
		ret = -1;
	}
	bule_unlock(bule);

	return ret;
}

/*!
 * \brief Send Lifetime=0-BU
 *
 * It temporary changes the CoA, ifindex, and default route fields of the BULE, and
 * request sending BULE from the core-comm module.
 *
 * \param bule BULE
 * \param coa CoA to send
 * \param default_route Default route to send
 * \param ifindex Interface to send
 * \return Zero if OK
 */
static int ctrl_mn_send_bu_from_another_interface(struct bule * bule, struct in6_addr * coa, struct in6_addr * default_route, int ifindex)
{
	int ret;

	if (bule == NULL || coa == NULL || default_route == NULL || ifindex == 0)
		return -1;

	bule_lock(bule);
	memcpy(&bule->sending_coa, coa, sizeof(struct in6_addr));
	memcpy(&bule->sending_default_route, default_route, sizeof(struct in6_addr));
	bule->sending_ifindex = ifindex;
	DEBUG("Modifying BULE before sending lifetime=0-BU message. New CoA: " IP6ADDR_FMT " New DefRt: " IP6ADDR_FMT " Ifindex: %d",
			IP6ADDR_TO_STR(&bule->coa), IP6ADDR_TO_STR(&bule->default_route), bule->ifindex);
	bule_unlock(bule);

	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.ccom, MSG_CCOM_SEND_BU, bule, sizeof(*bule), NULL, 0);
	if (ret) {
		ERROR("Unable to send BU (lifetime 0) message to the HA");
	}

	bule_lock(bule);
	memcpy(&bule->sending_coa, &bule->coa, sizeof(struct in6_addr));
	memcpy(&bule->sending_default_route, &bule->default_route, sizeof(struct in6_addr));
	bule->sending_ifindex = bule->ifindex;
	bule_unlock(bule);

	return ret;
}

/*!
 * \brief Use a newly initialized BULE to send a pending Lifetime=0-BU message
 *
 * \param bule BULE with pending Lifetime=0-BU message
 * \param arg New BULE
 * \return Zero
 */
static int ctrl_mn_send_lifetime_0_bus(struct bule * bule, void * arg)
{
	struct bule * new_bule = (struct bule *)arg;

	if (bule == NULL || new_bule == NULL)
		return 0;

	//DEBUG("Checking BULE @ %d to find Lifetime=0-BU task. Flags: %08X (%08X)", bule->ifindex, bule->flags, (BULE_F_TO_DELETE | BULE_F_SEND_LIFET_0));

	if ((bule->flags & (BULE_F_TO_DELETE | BULE_F_SEND_LIFET_0)) == (BULE_F_TO_DELETE | BULE_F_SEND_LIFET_0)) {
		bule_lock(bule);
		if (bule->lifet0_send_task != 0) {
			tasks_del(bule->lifet0_send_task);
			bule->lifet0_send_task = 0;
			refcnt_put(&bule->refcnt);
		}
		bule_unlock(bule);

		ctrl_mn_send_bu_from_another_interface(bule, &new_bule->coa, &new_bule->default_route, new_bule->ifindex);
	}

	return 0;
}

/*!
 * \brief Lifetim=0-BU task
 *
 * If this function is called, it canclels the sending of Lifetime=0-BU
 * and releases the BULE. It will cause the deletion of it.
 *
 * \param arg BULE
 */
static void ctrl_mn_lifetime_0_bu_send_task(void * arg)
{
	struct bule * bule = (struct bule *)arg;

	if (bule == NULL)
		return;

	refcnt_put(&bule->refcnt);
}

/*!
 * \brief Clean and release a BULE
 *
 * It tries to find another BULE which may be able to deliver its Lifetime=0-BU message. 
 * If no applicant-able BULE found, a scheduled task will be created to wait a new one.
 * This task will wait until the end of the BULE lifetime to release it.
 *
 * Next this function cleans the environmental stuff, and if it was inited the data stuff.
 *
 * \param arg BULE
 * \return Zero if OK
 */
static int ctrl_mn_bule_del(void * arg)
{
	int ret;
	int retval = 0;
	struct bule * bule = (struct bule *)arg;
	struct ctrl_mn_bule_del_find df;
	struct msg_data_iterate_bul buli;
	struct timespec taskts;
	unsigned long taskid;

	if (bule == NULL)
		return -1;
	
	DEBUG("MN: BULE cleanup @ %d...", bule->ifindex);

	memset(&df, 0, sizeof(df));
	bule_lock(bule);
	memcpy(&df.to_del_dest, &bule->dest, sizeof(struct in6_addr));
	memcpy(&df.to_del_hoa,  &bule->hoa,  sizeof(struct in6_addr));
	df.to_del_ifindex = bule->ifindex;
	bule_unlock(bule);

	memset(&buli, 0, sizeof(buli));
	buli.fp = ctrl_mn_bule_del_find;
	buli.arg = &df;

	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.data, MSG_DATA_ITERATE_BUL,
			&buli, sizeof(buli), NULL, 0);

	bule_lock(bule);
	bule->lifetime = 0;
	bule_unlock(bule);

	if (df.send_found != 0) {
		retval = ctrl_mn_send_bu_from_another_interface(bule, &df.send_coa, &df.send_default_route, df.send_ifindex);
	} else {
		bule_lock(bule);
		taskts.tv_sec = bule->lifetime_rem;
		taskts.tv_nsec = 0;
		bule->flags |= BULE_F_SEND_LIFET_0;
		bule_unlock(bule);
		DEBUG("Lifetime=0-BU send task: +%u sec %u nsec", taskts.tv_sec, taskts.tv_nsec);
		/* Get refcnt here, put in bu_resend */
		refcnt_get(&bule->refcnt);
		taskid = tasks_add_rel(ctrl_mn_lifetime_0_bu_send_task, bule, taskts);
		if (taskid == 0)
			refcnt_put(&bule->refcnt);
		else {
			bule_lock(bule);
			bule->lifet0_send_task = taskid;
			bule_unlock(bule);
		}
	}

	bule_lock(bule);

	ret = ctrl_mn_bule_env_clean(&bule->hoa, bule->ifindex);
	if (ret)
		retval = -1;

	if (bule->flags & BULE_F_DATA_INITED) {
		ret = ctrl_mn_data_env_clean(&bule->hoa, &bule->dest, &bule->coa, bule->mark);
		
		if (ret)
			retval = -1;
		
		bule->flags &= ~(BULE_F_DATA_INITED);
	}

	ctrl_mn_bu_resend_cancel(bule, 0);

	bule_unlock(bule);
	
	DEBUG("MN: HOME BULE cleanup @ %d finished with code %d", bule->ifindex, retval);

	refcnt_put(&bule->refcnt);

	/* Non-zero return value stops the execution of other hooks */
	return 0;
}

/*!
 * \brief Initialize a new BULE
 *
 * Creates the environmental stuff, and configure a few fields of BULE (i.e. lifetime)
 *
 * \param bule BULE
 * \return Zero if OK
 */
int ctrl_mn_init_bule(struct bule * bule)
{
	int ret;

	bule_lock(bule);

	DEBUG("MN: Processing HOME BULE init @ %d...", bule->ifindex);

	/* Get 1 refct for the delete hook */
	refcnt_get(&bule->refcnt);
	ret = hooks_add(bule->del_hook_id, 1, ctrl_mn_bule_del);

	bule->lifetime = (ctrl_node_opts.mn.lifetime >> 2);
	bule->lifetime_rem = 0;
	bule->resend_interval = CTRL_MN_DEFAULT_RESEND_IVAL;

	ret = ctrl_mn_bule_env_create(&bule->hoa, bule->ifindex, &bule->dest);
	if (ret)
		return ret;
			
	bule_unlock(bule);

	return 0;
}

/*!
 * \brief Update BULE
 *
 * If the BULE has changed, it updates the corresponding data stuff.
 * If it was inited cleans, and configures the new one.
 *
 * \param bule BULE
 * \return Zero if OK
 */
int ctrl_mn_update_bule(struct bule * bule)
{
	int ret;
	struct msg_env_addr_get_reply addrs;
	int i, found = 1;
	struct msg_env_addr a;

	if (bule->flags & BULE_F_DATA_INITED) {
		ret = ctrl_mn_data_env_clean(&bule->hoa, &bule->dest, &bule->prev_coa, bule->mark);
		if (ret) {
			ERROR("Unable to clean previous DATA environment of this Binding");
		}
		bule->flags &= ~(BULE_F_DATA_INITED);
	}

	DEBUG("MN: UPDATE MN");

	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
				MSG_ENV_ADDR_GET, &bule->ifindex, sizeof(bule->ifindex),
				&addrs, sizeof(addrs));
	if (ret) {
		ERROR("Unable to get addresses of interface: %d", bule->ifindex);
	} else {
		found = 0;
		for (i = 0; i < MSG_ENV_ADDR_GET_NUM && addrs.last[i] == 0; i++) {
			if (addrs.scopes[i] != RT_SCOPE_UNIVERSE)
				continue;
			if (memcmp(&ctrl_node_opts.mn.hoa, &addrs.addrs[i], sizeof(ctrl_node_opts.mn.hoa)) == 0)
				found = 1;
		}
	}
	
	if (found == 0) {
		INFO("MN: UPDATE MN: Missing HoA from interface %d", bule->ifindex);

		memset(&a, 0, sizeof(a));
		memcpy(&a.addr, &ctrl_node_opts.mn.hoa, sizeof(a.addr));
		a.plen = 128;
		a.ifindex = bule->ifindex;
		a.flags = IFA_F_HOMEADDRESS;
		ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env, MSG_ENV_ADDR,
				&a, sizeof(a), NULL, 0);
		if (ret) {
			ERROR("Unable to configure address " IP6ADDR_FMT " on interface: %d",
					IP6ADDR_TO_STR(&a.addr), a.ifindex);
		}
	}

	return ctrl_mn_resend_bu_now(bule, 0);
}

/*!
 * \brief Send a Binding Update message
 *
 * It asks the core-comm module to send a Binding Update message.
 * It starts a new resend task to re-try or re-confirm the binding.
 *
 * \param bule BULE
 * \param resend_sec Re-try interval in seconds
 * \param reason String in the log: why re-sending BU?
 * \return Zero if OK
 */
static int ctrl_mn_send_bu(struct bule * bule, double resend_sec, char * reason)
{
	int ret;
	unsigned long sec, nsec;

	INFO("Sending BU to HA: %s", reason);

	bule_lock(bule);
	sec = (int)resend_sec;
	nsec = (resend_sec - sec) * 1000000000;
	ret = ctrl_mn_bu_resend_start(bule, 0, sec, nsec);
	bule->flags &= ~(BULE_F_BINDING_DONE);
	bule_unlock(bule);

	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.ccom, MSG_CCOM_SEND_BU, bule, sizeof(*bule), NULL, 0);
	if (ret) {
		ERROR("Unable to send BU message to the HA");
		return -1;
	}

	return 0;
}

/*!
 * \brief Resend Binding Update
 *
 * It calculates the interval to the next BU sending. If it will be a re-try,
 * it increases the interval as it is specified by the RFC. If it is a simple
 * re-sending due to expiration, it's using the lifetime value.
 *
 * \param arg BULE
 */
static void ctrl_mn_bu_resend(void * arg)
{
	struct bule * bule = (struct bule *)arg;
	unsigned char sending_mode;

	bule_lock(bule);

	if (bule->resend_task == 0) {
		bule_unlock(bule);
		return;
	}
	bule->resend_task = 0;

	if ((bule->flags & BULE_F_BINDING_DONE) == 0) 
		sending_mode = 1;
	else
		sending_mode = 2;

	bule_unlock(bule);

	if (sending_mode == 1) {
		ctrl_mn_send_bu(bule, bule->resend_interval, "Resend (no answer)");
		bule->resend_interval *= 2;
		if (bule->resend_interval > CTRL_MN_MAX_RESEND_IVAL)
			bule->resend_interval = CTRL_MN_DEFAULT_RESEND_IVAL;
	} else {
		ctrl_mn_send_bu(bule, bule->lifetime_rem * 4 * 0.6, "Resend (lifetime expired)");
	}
	
	/* Put refcnt here, got in bu_resend_start */
	refcnt_put(&bule->refcnt);
}

/*!
 * \brief Send Binding Update now
 *
 * It cancels the scheduled and waiting BU resend task,
 * and creates a new one, to send the BU message now.
 *
 * \param bule BULE
 * \param bule_lock Is the BUKLE locked?
 * \return Zero if OK
 */
int ctrl_mn_resend_bu_now(struct bule * bule, unsigned char bule_lock)
{
	if (bule == NULL)
		return -1;

	if (bule_lock)
		bule_lock(bule);

	ctrl_mn_bu_resend_cancel(bule, 0);
	ctrl_mn_bu_resend_start(bule, 0, 0, 5000);

	if (bule_lock)
		bule_unlock(bule);

	return -1;
}

/*!
 * \brief Processing new address event
 *
 * It try to find a corresponding entry in the HoA DAD pool. If an entry found,
 * it finds the corresponding BULE and sends the initial BU message.
 * Finally it provides the newly configured BULE to send pending
 * Lifetime=0-Bu messages
 *
 * \param ifindex Interface index
 * \param addr Newly configured address
 */
void ctrl_mn_newaddr_event(int ifindex, struct in6_addr * addr)
{
	struct list_head * pos, *pos2;
	struct hoa_pool_e * hpe;
	struct msg_data_find_bule fbule;
	int ret;
	struct bule * bule = NULL;
	struct msg_data_iterate_bul buli;

	if (memcmp(addr, &ctrl_node_opts.mn.hoa, sizeof(struct in6_addr)) != 0)
		return;

	pthread_mutex_lock(&hoa_pool_lock);
	list_for_each_safe(pos, pos2, &hoa_pool) {
		hpe = list_entry(pos, struct hoa_pool_e, list);
		if (hpe->ifindex == ifindex) {
			list_del(pos);
			fbule.ifindex = ifindex;
			memcpy(&fbule.dest, &hpe->dest, sizeof(struct in6_addr));
			memcpy(&fbule.hoa, &ctrl_node_opts.mn.hoa, sizeof(struct in6_addr));
			memset(&fbule.coa, 0, sizeof(struct in6_addr));
			free(hpe);

			ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.data, MSG_DATA_FIND_BULE, &fbule, sizeof(fbule), &bule, sizeof(bule));
			if (ret || bule == NULL) {
				ERROR("Unable to find corresponding BULE for HOA configure event");
			} else {
				DEBUG("HOA configured successfully @ %d", ifindex);
			}
			
			break;
		}
	}
	pthread_mutex_unlock(&hoa_pool_lock);

	if (bule != NULL) {
		ctrl_mn_send_bu(bule, 2, "New HOME BULE");
		refcnt_put(&bule->refcnt);
	}
	
	memset(&buli, 0, sizeof(buli));
	buli.fp = ctrl_mn_send_lifetime_0_bus;
	buli.arg = bule;
	buli.include_to_delete = 1;

	IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.data, MSG_DATA_ITERATE_BUL,
			&buli, sizeof(buli), NULL, 0);
}

/*!
 * \brief Receiving Binding Acknowledge message
 *
 * It chacks the BA message and creates the data environmental stuff.
 * It re-schedule the resend task to the lifetime value.
 *
 * \param ba struct evt_ccom_ba
 * \return Zero if OK
 */
int ctrl_mn_ba_recv(struct evt_ccom_ba * ba)
{
	int ret;

	if (ba == NULL || ba->bule == NULL)
		return -1;

	if (ba->ba.ip6mhba_status != 0) {
		INFO("BA STATUS not zero. Try again...");
		refcnt_put(&ba->bule->refcnt);
		return 0;
	}

	bule_lock(ba->bule);

	ctrl_mn_bu_resend_cancel(ba->bule, 0);

	ba->bule->lifetime_rem = ntohs(ba->ba.ip6mhba_lifetime);
	ba->bule->flags |= BULE_F_BINDING_DONE;
	ba->bule->resend_interval = CTRL_MN_DEFAULT_RESEND_IVAL;

	if ((ba->bule->flags & BULE_F_DATA_INITED) == 0) {
		ret = ctrl_mn_data_env_create(&ba->bule->hoa, &ba->bule->dest, &ba->bule->coa, ba->bule->mark);
		if (ret) {
			ERROR("Unable to set up DATA environment of this Binding");
			goto err;
		}

		ba->bule->flags |= BULE_F_DATA_INITED;
	}
	ret = ctrl_mn_bu_resend_start(ba->bule, 0, ba->bule->lifetime_rem * 4 * 0.6, 0);
	if (ret) {
		ERROR("Unable to start BU resend task");
		goto err;
	}
	ba->bule->flags |= BULE_F_RESEND;

	bule_unlock(ba->bule);
	refcnt_put(&ba->bule->refcnt);

	return 0;

err:
	bule_unlock(ba->bule);
	refcnt_put(&ba->bule->refcnt);
	ctrl_mn_send_bu(ba->bule, ba->bule->resend_interval, "BA processing error");

	bule_lock(ba->bule);
	refcnt_get(&ba->bule->refcnt);
	ba->bule->resend_interval *= 2;
	if (ba->bule->resend_interval > CTRL_MN_MAX_RESEND_IVAL)
		ba->bule->resend_interval = CTRL_MN_DEFAULT_RESEND_IVAL;
	bule_unlock(ba->bule);
	refcnt_put(&ba->bule->refcnt);

	return -1;
}

/*! \} */
