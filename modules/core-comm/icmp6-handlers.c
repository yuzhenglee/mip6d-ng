
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/list.h>

#include <mip6d-ng/core-comm.h>

#include "icmp6-io.h"
#include "icmp6-handlers.h"

/*!
 * \brief ICMPv6 Message handler
 */
struct icmp6_handler {
	/*! Linked list management */
	struct list_head list;
	/*! ICMPv6 type */
	uint8_t type;
	/*! Processing callback function */
	void (* recv)(const struct icmp6_hdr * hdr, ssize_t len, const struct icmp6_pkt_info_bundle * info);
};

/*!
 * \brief List of ICMPv6 handlers 
 */
static LIST_HEAD(icmp6_handlers);

/*! 
 * \brief Register ICMPv6 Message handler 
 *
 * \param type ICMPv6 type
 * \param recv Parsing callback function
 * \return Zero if OK
 */
int icmp6_handler_register(uint8_t type, void (* recv)(const struct icmp6_hdr *, ssize_t, const struct icmp6_pkt_info_bundle *))
{
	int ret;
	struct icmp6_handler * handler;

	handler = malloc(sizeof(struct icmp6_handler));
	if (handler == NULL) {
		ERROR("Unable to allocate icmp6 handler");
		return -1;
	}

	ret = icmp6_add_to_filters(type);
	if (ret) {
		free(handler);
		return ret;
	}

	memset(handler, 0, sizeof(struct icmp6_handler));

	handler->type = type;
	handler->recv = recv;

	list_add_tail(&handler->list, &icmp6_handlers);

	return 0;
}

/*!
 * \brief Find corresponding ICMPv6 handler and call it
 *
 * It finds the corresponding ICMPv6 handler for the message, and calls it
 *
 * \param ih ICMPv6 message
 * \param len Length of the message
 * \param info Packet info
 */
void icmp6_handlers_call(const struct icmp6_hdr * ih, ssize_t len, const struct icmp6_pkt_info_bundle * info)
{
	struct list_head * pos;
	struct icmp6_handler * handler;
	int type = ih->icmp6_type;

	list_for_each(pos, &icmp6_handlers) {
		handler = list_entry(pos, struct icmp6_handler, list);

		if (type == handler->type) {
			if (handler->recv != NULL)
				handler->recv(ih, len, info);
		}
	}
}

/*! \} */

