
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <sys/socket.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/threading.h>

#include <mip6d-ng/core-comm.h>

#include "main.h"
#include "icmp6-io.h"
#include "icmp6-handlers.h"

/*!
 * \brief Maximum length of incoming ICMPv6 packet 
 */
#define MAX_ICMP6_PKT_LEN	1540

/*!
 * \brief Maximum length of control field in incoming messages
 */
#define CMSG_BUF_LEN 		(CMSG_SPACE(sizeof(struct in6_pktinfo)) + CMSG_SPACE(sizeof(int)))

/*!
 * \brief Locking ICMPv6 socket
 */
static pthread_mutex_t icmp6_socket_mutex = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief ICMPv6 socket 
 */
static int icmp6_socket = -1;

/*!
 * \brief Raw ICMPv6 socket 
 */
static int icmp6_raw_socket = -1;

/*!
 * \brief ICMPv6 type filter
 */
static struct icmp6_filter icmp6_filter;

/*!
 * \brief Dump message
 *
 * It works if ICMP6_CORE_DEBUG is enabled
 *
 * \param msg Message
 */
static void icmp6_msg_dump(struct msghdr * msg)
{
#ifdef ICMP6_CORE_DEBUG
	ccom_msg_dump(msg);
#endif
}

/*!
 * \brief Basic receiving function for ICMPv6 messages
 *
 * It receives the message from the ICMPv6 socket, and
 * gets and parses a few basic information regarding to it.
 *
 * \param msg Received message
 * \param msglen Maximum length of the received message
 * \param src_addr Source address
 * \param pkt_info Packet information
 * \param hoplimit Hop counter (output)
 * \return Length of the message or negative
 */
static ssize_t icmp6_recv(unsigned char * msg, size_t msglen,
		struct sockaddr_in6 * src_addr, struct in6_pktinfo * pkt_info,
		int * hoplimit)
{
	int ret;
	struct msghdr mhdr;
	struct cmsghdr * cmsg;
	struct iovec iov;
	static unsigned char chdr[CMSG_BUF_LEN];
	ssize_t len;

	iov.iov_len = msglen;
	iov.iov_base = msg;

	mhdr.msg_name = (void *)src_addr;
	mhdr.msg_namelen = sizeof(struct sockaddr_in6);
	mhdr.msg_iov = &iov;
	mhdr.msg_iovlen = 1;
	mhdr.msg_control = (void *)&chdr;
	mhdr.msg_controllen = CMSG_BUF_LEN;

	ret = recvmsg(icmp6_socket, &mhdr, 0);
	if (ret < 0) {
		ERROR("mh: Receiving error: %d", ret);
		return ret;
	}
	len = ret;

	for (cmsg = CMSG_FIRSTHDR(&mhdr); cmsg != NULL; cmsg = CMSG_NXTHDR(&mhdr, cmsg)) {
		if (cmsg->cmsg_level != IPPROTO_IPV6)
			continue;

		switch (cmsg->cmsg_type) {
		case IPV6_PKTINFO:
			memcpy(pkt_info, CMSG_DATA(cmsg), sizeof(*pkt_info));
			break;
		case IPV6_HOPLIMIT:
			memcpy(hoplimit, CMSG_DATA(cmsg), sizeof(*hoplimit));
			break;
		}
	}

	return len;
}

/*!
 * \brief Listening ICMPv6 messages
 *
 * Endless loop for receiving and checking ICMPv6 messages.
 * It calls the parsing function, if any has been registered 
 * for the given type
 *
 * \param arg Unused
 * \return Unused
 */
static void * icmp6_listen(void * arg)
{
	int ret;
	uint8_t msg[MAX_ICMP6_PKT_LEN];
	struct in6_pktinfo pktinfo;
	struct sockaddr_in6 addr;
	int hoplimit;
	struct icmp6_pkt_info_bundle info;
	size_t len;
	struct icmp6_hdr * ih;

	while (1) {
#ifdef HAVE_PTHREAD_SETCANCELSTATE
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
#endif

		ret = icmp6_recv(msg, sizeof(msg), &addr, &pktinfo, &hoplimit);
		if (ret < 0) {
			ERROR("icmp6: Listening error. Exit.");
			exit(EXIT_FAILURE);
		}
		len = ret;

		if (len < 0 || len < sizeof(struct icmp6_hdr))
			continue;

		memset(&info, 0, sizeof(info));

		memcpy(&info.src, &addr.sin6_addr, sizeof(info.src));
		memcpy(&info.dst, &pktinfo.ipi6_addr, sizeof(info.dst));
		info.ifindex = pktinfo.ipi6_ifindex;
		info.hoplimit = hoplimit;

#ifdef HAVE_PTHREAD_SETCANCELSTATE
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
#endif

		ih = (struct icmp6_hdr *)msg;
		icmp6_handlers_call(ih, len, &info);
	}

	return NULL;
}

/*!
 * \brief Initialize ICMPv6 I/O
 *
 * It opens and configures the ICMPv6 sockets, and creates the listening thread.
 * It initializes the ICMPv6 receiving filter.
 *
 * \return Zero if OK
 */
int icmp6_init()
{
	int ret;
	int val;
	pthread_t th;

	ret = socket(AF_INET6, SOCK_RAW, IPPROTO_RAW);
	if (ret < 0) {
		ERROR("icmp6: Unable to open ICMPV6 raw socket");
		return ret;
	}
	icmp6_raw_socket = ret;

	val = 1;
	ret = setsockopt(icmp6_raw_socket, IPPROTO_IPV6, IP_HDRINCL, &val, sizeof(val));
	if (ret < 0) {
		ERROR("icmp6: Unable to set sockopt: IP HDR include");
		return ret;
	}

	ret = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
	if (ret < 0) {
		ERROR("icmp6: Unable to open ICMPV6 socket");
		return ret;
	}
	icmp6_socket = ret;

	ret = setsockopt(icmp6_socket, IPPROTO_IPV6, IPV6_RECVPKTINFO, &val, sizeof(val));
	if (ret < 0) {
		ERROR("icmp6: Unable to set sockopt: receive packet info");
		return -1;
	}

	val = 2;
	ret = setsockopt(icmp6_socket, IPPROTO_RAW, IPV6_CHECKSUM, &val, sizeof(val));
	if (ret < 0) {
		ERROR("icmp6: Unable to set sockopt: IPv6 checksum offset");
		return  -1;
	}

	val = 255;
	ret = setsockopt(icmp6_socket, IPPROTO_IPV6, IPV6_UNICAST_HOPS, &val, sizeof(val));
	if (ret < 0) {
		ERROR("icmp6: Unable to set sockopt: IPv6 unicast hops");
		return  -1;
	}

	val = 255;
	ret = setsockopt(icmp6_socket, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &val, sizeof(val));
	if (ret < 0) {
		ERROR("icmp6: Unable to set sockopt: IPv6 multicast hops");
		return  -1;
	}

	val = 1;
	ret = setsockopt(icmp6_socket, IPPROTO_IPV6, IPV6_RECVHOPLIMIT, &val, sizeof(val));
	if (ret < 0) {
		ERROR("icmp6: Unable to set sockopt: IPv6 receive hoplimit hops");
		return  -1;
	}

	ICMP6_FILTER_SETBLOCKALL(&icmp6_filter);
	ICMP6_FILTER_SETPASS(ICMP6_DST_UNREACH, &icmp6_filter);

	ret = setsockopt(icmp6_socket, IPPROTO_ICMPV6, ICMP6_FILTER, &icmp6_filter, sizeof(icmp6_filter));
	if (ret < 0) {
		ERROR("icmp6: Unable to set sockopt: IPCMv6 set filters");
		return  -1;
	}

	th = threading_create(icmp6_listen, NULL);

	return (int)(th == (pthread_t)0);
}

/*!
 * \brief Add ICMPv6 type to the receiving filter
 *
 * \param type ICMPv6 type
 * \return Zero if OK
 */
int icmp6_add_to_filters(uint8_t type)
{
	int ret;

	ICMP6_FILTER_SETPASS(type, &icmp6_filter);

	ret = setsockopt(icmp6_socket, IPPROTO_ICMPV6, ICMP6_FILTER, &icmp6_filter, sizeof(icmp6_filter));
	if (ret < 0) {
		ERROR("icmp6: Unable to set sockopt: IPCMv6 set filters");
		return  -1;
	}

	return 0;
}

/*!
 * \brief Sending ICMPv6 message
 *
 * \param info Message info
 * \param iov Message iov
 * \param iov_count Number of pieces in message iov
 * \return Number of sent bytes or negative
 */
int icmp6_send(const struct icmp6_pkt_info_bundle * info, struct iovec * iov, int iov_count)
{
	struct sockaddr_in6 daddr;
	struct in6_pktinfo pinfo;
	struct msghdr msg;
	void * cmsgbuf;
	struct cmsghdr *cmsg;
	int cmsglen;
	int ret = 0;
	int on;
	int hops;

	hops = (info->hoplimit == 0) ? 64 : info->hoplimit;

	memset(&daddr, 0, sizeof(struct sockaddr_in6));
	daddr.sin6_family = AF_INET6;
	memcpy(&daddr.sin6_addr, &info->dst, sizeof(daddr.sin6_addr));
	daddr.sin6_port = htons(IPPROTO_ICMPV6);

	memset(&pinfo, 0, sizeof(pinfo));
	memcpy(&pinfo.ipi6_addr, &info->src, sizeof(pinfo.ipi6_addr));
	pinfo.ipi6_ifindex = info->ifindex;

	cmsglen = CMSG_SPACE(sizeof(pinfo));
	cmsgbuf = malloc(cmsglen);
	if (cmsgbuf == NULL) {
		ERROR("icmp6: Unable to allocate send CMSG");
		return -1;
	}
	memset(cmsgbuf, 0, cmsglen);
	memset(&msg, 0, sizeof(msg));

	msg.msg_control = cmsgbuf;
	msg.msg_controllen = cmsglen;
	msg.msg_iov = iov;
	msg.msg_iovlen = iov_count;
	msg.msg_name = (void *)&daddr;
	msg.msg_namelen = sizeof(daddr);

	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_len = CMSG_LEN(sizeof(pinfo));
	cmsg->cmsg_level = IPPROTO_IPV6;
	cmsg->cmsg_type = IPV6_PKTINFO;
	memcpy(CMSG_DATA(cmsg), &pinfo, sizeof(pinfo));

	icmp6_msg_dump(&msg);

	pthread_mutex_lock(&icmp6_socket_mutex);
	on = 1;
	setsockopt(icmp6_socket, IPPROTO_IPV6, IPV6_PKTINFO, &on, sizeof(int));
	setsockopt(icmp6_socket, IPPROTO_IPV6, IPV6_UNICAST_HOPS, &hops, sizeof(hops));
	setsockopt(icmp6_socket, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &hops, sizeof(hops));
	ret = sendmsg(icmp6_socket, &msg, 0);
	if (ret < 0)
		ERROR("mh: Send error: %d", ret);
	pthread_mutex_unlock(&icmp6_socket_mutex);

	free(msg.msg_control);

	return ret;
}

/*!
 * \brief Linearize ICMPv6 message
 *
 * Create byte-stream form iov oieces
 *
 * \param dst Output byte stream
 * \param dstlen Length of output buffer
 * \param iov Message iov
 * \param iovlen Number of message pieces
 * \param written Number of written bytes into the output buffer
 * \return Zero if OK
 */
static int icmp6_iov_linearize(uint8_t *dst, int dstlen, struct iovec *iov, size_t iovlen, int *written) 
{
	unsigned int i = 0, l;
	unsigned int rem = dstlen;
	uint8_t *data = dst;
	struct iovec cur;

	if (iov == NULL || iovlen == 0)
		return 0;

	cur = iov[i];

	while ((i < iovlen) && ((l = cur.iov_len) <= rem)) {
		if (cur.iov_base == NULL)
			return -1;

		memcpy(data, cur.iov_base, l);
		cur = iov[++i];
		rem  -= l;
		data += l;
	}

	if (i != iovlen) {
		ERROR("icmp6: Not enough space to linearize iov (%u != %u | %u <= %u)\n", i, iovlen, l, rem);
		return -1;
	}

	*written = dstlen - rem;

	return 0;
}

/*!
 * \brief Calculate ICMPv6 checksum
 *
 * \param src Source address
 * \param dst Destination address
 * \param data ICMPv6 message
 * \param datalen Length of message
 * \param nh Next header type
 * \return Checksum
 */
static uint16_t icmp6_cksum(const struct in6_addr * src, const struct in6_addr * dst, const void * data, socklen_t datalen, uint8_t nh)
{
	struct _phdr {
		struct in6_addr src;
		struct in6_addr dst;
		uint32_t plen;
		uint8_t reserved[3];
		uint8_t nxt;
	} phdr;

	register unsigned long sum = 0;
	socklen_t count;
	uint16_t *addr;
	int i;

	/* Prepare pseudo header for csum */
	memset(&phdr, 0, sizeof(phdr));
	phdr.src = *src;
	phdr.dst = *dst;
	phdr.plen = htonl(datalen);
	phdr.nxt = nh;

	/* caller must make sure datalen is even */
	addr = (uint16_t *)&phdr;
	for (i = 0; i < 20; i++)
		sum += *addr++;

	count = datalen;
	addr = (uint16_t *)data;

     while (count > 1) {
		sum += *(addr++);
		count -= 2;
	}

	while (sum >> 16)
		sum = (sum & 0xffff) + (sum >> 16);

	return (uint16_t)~sum;
}

/*!
 * \brief Sending ICMPv6 message
 *
 * Using RAW socket instead of ICMP socket. Do ICMPv6-related stuff by mip6d-ng.
 * 
 * \param info Message info
 * \param msg Message
 * \param mlen Message length
 * \param optv Message options' iov
 * \param optvlen Number of pieces in message options' iov
 * \return Number of sent bytes or negative
 */
int icmp6_raw_send(const struct icmp6_pkt_info_bundle * info, char * msg, size_t mlen, struct iovec *optv, size_t optvlen)
{
	int ret;
	struct {
		struct ip6_hdr ip;
		struct icmp6_hdr icmp;
		uint8_t data[1500];
	} frame;
	uint8_t *data = (uint8_t *)(&frame.icmp);
	int datalen = 0;
	int remlen = sizeof(frame) - sizeof(struct ip6_hdr);
	int written = 0;

	struct msghdr msgh;
	struct in6_pktinfo pinfo;
	void * cmsgbuf;
	struct cmsghdr *cmsg;
	int cmsglen;
	struct sockaddr_in6 dst;
	struct iovec iov;

	if (msg == NULL || 
		mlen < sizeof(struct icmp6_hdr) ||
		mlen > (sizeof(frame) - sizeof(struct ip6_hdr))) {
		ERROR("icmp6: Invalid raw socket to send.");
		return -1;
	}

	memset(&frame, 0, sizeof(frame));
	memset(&dst, 0, sizeof(dst));
	memset(&pinfo, 0, sizeof(pinfo));

	dst.sin6_family = AF_INET6;
	memcpy(&dst.sin6_addr, &info->dst, sizeof(dst.sin6_addr));

	pinfo.ipi6_ifindex = info->ifindex;

	cmsglen = CMSG_SPACE(sizeof(pinfo));
	cmsgbuf = malloc(cmsglen);
	if (cmsgbuf == NULL) {
		ERROR("icmp6: Unable to allocate send CMSG");
		return -1;
	}
	memset(cmsgbuf, 0, cmsglen);
	memset(&msgh, 0, sizeof(msgh));

	/* Copy ICMPv6 header and update various length values */
	memcpy(data, msg, mlen);
	data += mlen;
	datalen += mlen;
	remlen -= mlen;

	/* Prepare for csum: write trailing options by linearizing iov */
	ret = icmp6_iov_linearize(data, remlen, optv, optvlen, &written);
	if (ret || (written & 0x1)) { /* Ensure length is even for csum() */
		ERROR("icmp6: Invalid raw message iov.");
		free(cmsgbuf);
		return -1;
	}
	datalen += written;
	remlen -= written;

	/* Fill in the IPv6 header */
	frame.ip.ip6_vfc = 0x60;
	frame.ip.ip6_plen = htons(datalen);
	frame.ip.ip6_nxt = IPPROTO_ICMPV6;
	frame.ip.ip6_hlim = 255;
	memcpy(&frame.ip.ip6_dst, &info->dst, sizeof(frame.ip.ip6_dst));

	frame.icmp.icmp6_cksum = icmp6_cksum(&in6addr_any, &info->dst, &frame.icmp, datalen, IPPROTO_ICMPV6);

	iov.iov_base = &frame;
	iov.iov_len = sizeof(frame.ip) + datalen;

	msgh.msg_control = cmsgbuf;
	msgh.msg_controllen = cmsglen;
	msgh.msg_iov = &iov;
	msgh.msg_iovlen = 1;
	msgh.msg_name = (void *)&dst;
	msgh.msg_namelen = sizeof(dst);

	cmsg = CMSG_FIRSTHDR(&msgh);
	cmsg->cmsg_len = CMSG_LEN(sizeof(pinfo));
	cmsg->cmsg_level = IPPROTO_IPV6;
	cmsg->cmsg_type = IPV6_PKTINFO;
	memcpy(CMSG_DATA(cmsg), &pinfo, sizeof(pinfo));

	icmp6_msg_dump(&msgh);

	pthread_mutex_lock(&icmp6_socket_mutex);
	ret = sendmsg(icmp6_raw_socket, &msgh, 0);
	if (ret < 0)
		ERROR("mh: Send error: %d", ret);
	pthread_mutex_unlock(&icmp6_socket_mutex);

	free(msgh.msg_control);

	return ret;
}

/*! \} */


