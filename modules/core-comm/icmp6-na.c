
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <sys/socket.h>


#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/environment.h>

#include <mip6d-ng/core-comm.h>

#include "icmp6-io.h"
#include "icmp6-handlers.h"
#include "icmp6-na.h"
#include "main.h"

/*!
 * \brief Dump ICMP6 NA message
 *
 * Enabled only if ICMP6_CORE_DEBUG is enabled
 *
 * \param ina Neighbor Advertisement message
 */
static void icmp6_na_dump(struct icmp6_na * ina)
{
#ifdef ICMP6_CORE_DEBUG

	DEBUG("ICMPv6 NA DUMP");
	DEBUG("NA: \n"
		  "\t\t timestamp: " TS_FMT "\n"
		  "\t\t from: " IP6ADDR_FMT " on iface %d\n"
		  "\t\t target link addr: " HWA_FMT " (%d)\n"
		  "\t\t hoplimit %d flags %08X\n",
		  TS_TO_STR(&ina->timestamp),
		  IP6ADDR_TO_STR(&ina->src_addr), ina->ifindex,
		  HWA_TO_STR(ina->hwa), ina->hwalen,
		  ina->hoplimit, ina->na_flags);
#endif
}

/*!
 * \brief Send Neighbor Advertisement message
 *
 * \param info Packet info
 * \param target Destination
 * \param flags Flags
 * \return Number of sent bytes or negative
 */
int icmp6_na_send(struct icmp6_pkt_info_bundle * info, const struct in6_addr *target, uint32_t flags)
{
	struct nd_neighbor_advert na;
	uint8_t *hdr = (uint8_t *)&na;
	int hdrlen = sizeof(na);

	struct iovec iov[2];
	int ret;
	unsigned int ifindex = info->ifindex;
	uint8_t l2addr[ETH_ALEN + 1];
	int len, iovlen = 0;
	struct nd_opt_hdr *opt = NULL;
	int hlen = sizeof(struct nd_opt_hdr);

	memset(hdr, 0, hdrlen);
	na.nd_na_hdr.icmp6_type = ND_NEIGHBOR_ADVERT;
	na.nd_na_hdr.icmp6_code = 0;
	memcpy(&na.nd_na_target, target, sizeof(struct in6_addr));
	na.nd_na_flags_reserved = flags;

	len = ETH_ALEN;
	ret = IMSG_MSG_ARGS(ccom_opts.msg_ids.me, ccom_opts.msg_ids.env, MSG_ENV_GET_L2ADDR,
			&ifindex, sizeof(ifindex), l2addr, sizeof(l2addr));
	if (ret == 0) {
		opt = malloc(len + hlen);
		if (opt == NULL)
			return -1;
		
		opt->nd_opt_type = ND_OPT_TARGET_LINKADDR;
		opt->nd_opt_len = (len + hlen) >> 3;
		memcpy(opt + 1, l2addr, len);
		iov[0].iov_base = opt;
		iov[0].iov_len = len + hlen;
		iovlen++;
	}

	ret = icmp6_raw_send(info, (char *)hdr, hdrlen, iov, iovlen);

	if (opt != NULL)
		free(opt);

	return ret;
}

/*!
 * \brief Parse Neighbor Advertisement message
 *
 * Currently this has no effect, only dumps the parsed message
 * \param hdr ICMPv6 message
 * \param len Length of message
 * \param info Packet info
 */
static void icmp6_na_recv(const struct icmp6_hdr * hdr, ssize_t len, const struct icmp6_pkt_info_bundle * info)
{
	struct icmp6_na * ina = NULL;

	struct nd_neighbor_advert * na = (struct nd_neighbor_advert *)hdr;
	int optlen = len - sizeof(struct nd_neighbor_advert);
	uint8_t *opt;
	uint16_t olen;
	int err;

	/* validity checks */
	if (info->hoplimit < 255) {
		INFO("icmp6: NA hoplimit < 255");
		return;
	}
	if (hdr->icmp6_code != 0) {
		INFO("icmp6: NA code is not zero");
		return;
	}
	if (len < sizeof(struct nd_neighbor_advert)) {
		INFO("icmp6: Invalid NA length: %d", len);
		return;
	}
	if (IN6_IS_ADDR_MULTICAST(&na->nd_na_target)) {
		INFO("icmp6: NA target is multicast");
		return;
	}
	if (na->nd_na_flags_reserved & ND_NA_FLAG_SOLICITED && IN6_IS_ADDR_MULTICAST(&info->dst)) {
		INFO("icmp6: NA is solicited, but destination is multicast");
		return;
	}

	opt = (uint8_t *)(na + 1);

	ina = malloc(sizeof(struct icmp6_na));
	if (ina == NULL) {
		ERROR("icmp6: Unable to allocate NA");
		return;
	}
	memset(ina, 0, sizeof(struct icmp6_na));

	clock_gettime(CLOCK_REALTIME, &ina->timestamp);
	memcpy(&ina->src_addr, &info->src, sizeof(ina->src_addr));
	ina->ifindex = info->ifindex;
	ina->hoplimit = info->hoplimit;
	ina->na_flags = na->nd_na_flags_reserved;

	err = 0;
	while (optlen > 1 && err == 0) {
		olen = opt[1] << 3;
		if (olen > (unsigned int)optlen || olen == 0) {
			INFO("icmp6: Invalid NA opt length.");
			err = 1;
			break;
		}

		switch (opt[0]) {
		case ND_OPT_TARGET_LINKADDR:
			/*! TODO: This method is valid only for ARPHRD_ETHER, ARPHRD_IEEE802, ARPHRD_IEEE802_TR, ARPHRD_IEEE80211, ARPHRD_FDDI */
			if (olen - 2 != 6) {
				INFO("icmp6: Invalid NA LINKADDR length.");
				err = 1;
				break;
			}
			memcpy(ina->hwa, &opt[2], 6);
			ina->hwalen = 6;
			break;
		}

		optlen -= olen;
		opt += olen;
	}

	if (err)
		goto out;

	icmp6_na_dump(ina);

out:
	if (ina) {
		free(ina);
		ina = NULL;
	}
}

/*!
 * \brief Register Router Advertisement to ICMPv6 handlers
 */
int icmp6_na_register()
{
	return icmp6_handler_register(ND_NEIGHBOR_ADVERT, icmp6_na_recv);
}

/*! \} */


