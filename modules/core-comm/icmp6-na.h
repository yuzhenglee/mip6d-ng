
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#ifndef MIP6D_NG_ICMP6_NA_H_
#define MIP6D_NG_ICMP6_NA_H_

#include <netinet/in.h>
#include <sys/time.h>

#define L2ADDR_MAX_SIZE     6

#define HWA_FMT		"%02x:%02x:%02x:%02x:%02x:%02x"

#define HWA_TO_STR(hwa) \
		(hwa)[0], (hwa)[1], (hwa)[2], (hwa)[3], (hwa)[4], (hwa)[5]

int icmp6_na_send(struct icmp6_pkt_info_bundle * info, const struct in6_addr *target, uint32_t flags);

/*!
 * \brief Neighbor Advertisement 
 */
struct icmp6_na {
	/*! Time stamp */
	struct timespec timestamp;
	/*! Source address */
	struct in6_addr src_addr;
	/*! Interface index */
	int ifindex;
	/*! Hop counter */
	uint8_t hoplimit;
	/*! Flags */
	uint8_t na_flags;
	/*! L2 hardware address */
	uint8_t hwa[L2ADDR_MAX_SIZE];
	/*! L2 hardware address length */
	uint8_t hwalen;
};

int icmp6_na_register();

#endif /* MIP6D_NG_ICMP6_RS_H_ */

/*! \} */
