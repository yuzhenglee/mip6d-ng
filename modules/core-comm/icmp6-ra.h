
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#ifndef MIP6D_NG_ICMP6_RA_H_
#define MIP6D_NG_ICMP6_RA_H_

#include <netinet/in.h>
#include <sys/time.h>

#include <mip6d-ng/list.h>

#define L2ADDR_MAX_SIZE     6

#define HWA_FMT		"%02x:%02x:%02x:%02x:%02x:%02x"

#define HWA_TO_STR(hwa) \
		(hwa)[0], (hwa)[1], (hwa)[2], (hwa)[3], (hwa)[4], (hwa)[5]

/*!
 * \brief Router Advertisement 
 */
struct icmp6_ra {
	/*! Time stamp */
	struct timespec timestamp;
	/*! Source Address */
	struct in6_addr src_addr;
	/*! Interface index */
	int ifindex;
	/*! Hop counter */
	uint8_t hoplimit;
	/*! RA flags */
	uint8_t ra_flags;
	/*! Router lifetime */
	struct timespec rtr_lifetime;
	/*! Reachable */
	struct timespec reachable;
	/*! Retransmit timeout */
	struct timespec retransmit;
	/*! L2 hardware address */
	uint8_t hwa[L2ADDR_MAX_SIZE];
	/*! L2 hardware address length */
	uint8_t hwalen;
	/*! List of advertised prefixes */
	struct list_head prefixes;
	/*! Number of advertised prefixes */
	int prefix_cnt;
	/*! MTU */
	uint32_t mtu;
	/*! Advertisement interval */
	struct timespec adv_ival;
};

/*!
 * \brief Router Advertisement prefix 
 */
struct icmp6_ra_prefix {
	/*! Linked list management */
	struct list_head list;
	/*! Prefix valid lifetime */
	struct timespec valid_lifetime;
	/*! Prefix preferred lifetime */
	struct timespec preferred_lifetime;
	/*! Prefix */
	struct in6_addr prefix;
	/*! Prefix length */
	uint8_t prefix_len;
};

int icmp6_ra_register();

#endif /* MIP6D_NG_ICMP6_RA_H_ */

/*! \} */
