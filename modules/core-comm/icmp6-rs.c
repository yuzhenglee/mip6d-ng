
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <sys/socket.h>


#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/core-comm.h>

#include "icmp6-io.h"
#include "icmp6-rs.h"

/*!
 * \brief Send router solicitation 
 * \param info Packet info
 * \return Number of sent bytes of negative
 */
int icmp6_rs_send(struct icmp6_pkt_info_bundle * info)
{
	struct nd_router_solicit rs;
	uint8_t *hdr = (uint8_t *)&rs;
	int hdrlen = sizeof(rs);

	memset(hdr, 0, hdrlen);
	rs.nd_rs_type = ND_ROUTER_SOLICIT;

	return icmp6_raw_send(info, (char *)hdr, hdrlen, NULL, 0);
}

/*! \} */
