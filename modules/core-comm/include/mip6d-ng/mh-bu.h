
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-core-comm
 * \{
 */

#ifndef MIP6D_NG_CCOM_MH_BU_H_
#define MIP6D_NG_CCOM_MH_BU_H_

#include <mip6d-ng/mh.h>
#include <mip6d-ng/core-comm.h>

/*!
 * \brief Binding Update MH message
 *
 * Followed by optional Mobility Options
 */
struct ip6_mh_bu {
	/*! Mobility header */
	struct ip6_mh	ip6mhbu_hdr;
	/*! Sequence Number */
	uint16_t		ip6mhbu_seqno;
	/*! Flags */
	uint16_t		ip6mhbu_flags;
	/*! Lifetime in unit of 4 sec */
	uint16_t		ip6mhbu_lifetime;
} __attribute__ ((packed));

/*!
 *  Binding Update
 */
#define IP6_MH_TYPE_BU		5

/*!
 * BU flag: Acknowledge
 */
#define MH_BU_F_A			0x8000

/*!
 * BU flag: Home Registration
 */
#define MH_BU_F_H			0x4000

/*!
 * BU flag: Link-Local Address Compatibility
 */
#define MH_BU_F_L			0x2000

/*!
 * BU flag: Key Management Mobility Capability
 */
#define MH_BU_F_K			0x1000

#endif /* MIP6D_NG_CCOM_MH_BU_H_ */

/*! \} */
