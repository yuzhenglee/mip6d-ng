
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-core-comm
 * \{
 */

#ifndef MIP6D_NG_CCOM_MH_OPT_H_
#define MIP6D_NG_CCOM_MH_OPT_H_

#include <mip6d-ng/mh.h>

/*!
 * \brief Mobility Option header
 *
 * Followed by variable length Option Data in bytes
 */
struct ip6_mh_opt {
	/*! Option Type */
	uint8_t		ip6mhopt_type;
	/*! Option Length */
	uint8_t		ip6mhopt_len;
} __attribute__ ((packed));

int mh_opt_register(uint8_t type, uint8_t align_n, uint8_t align, int (* fn)(uint8_t, struct ip6_mh_opt *, size_t, void *));
int mh_opt_recv(uint8_t mh_type, unsigned char * opts, size_t optslen, void * priv);

#endif /* MIP6D_NG_CCOM_MH_OPT_H_ */

/*! \} */
