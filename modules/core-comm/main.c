
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/mh-bu.h>
#include "mh.h"
#include "mh-io.h"
#include "mh-bu.h"
#include "mh-ba.h"
#include "mho-altcoa.h"

#include "icmp6-io.h"
#include "icmp6-ra.h"
#include "icmp6-rs.h"
#include "icmp6-na.h"
#include "icmp6-ns.h"
#include <mip6d-ng/const.h>

#include <mip6d-ng/data.h>
#include <mip6d-ng/bce.h>
#include <mip6d-ng/environment.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/core-comm.h>

#include "main.h"

struct ccom_opts ccom_opts;

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the core-comm module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int ccom_proc_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	int retval = -1;
	int ret;

	struct msg_ccom_reg_mh_opt * reg_mh_opt;
	struct bule * bule;
	struct msg_ccom_send_ba * ba;
	struct msg_ccom_send_na * na;
	struct msg_ccom_send_rs * rs;
	struct iovec * iov;
	int iov_count, iov_num;
	struct send_hook_bu_param buhookp;
	struct send_hook_ba_param bahookp;
	struct mh_pkt_info_bundle info;
	struct icmp6_pkt_info_bundle infoi6;

	switch (message) {
	case MSG_CCOM_REG_MH_OPT:
		if (arg == NULL)
			break;
		reg_mh_opt = (struct msg_ccom_reg_mh_opt *)arg;

		retval = mh_opt_register(reg_mh_opt->type, reg_mh_opt->align_n, reg_mh_opt->align, reg_mh_opt->fn);
		break;
	case MSG_CCOM_SEND_BU:
		if (arg == NULL)
			break;
		bule = (struct bule *)arg;

		bule_lock(bule);
		memset(&info, 0, sizeof(info));
		memcpy(&info.haoa, &bule->sending_coa,  sizeof(struct in6_addr));
		memcpy(&info.src,  &bule->hoa,  sizeof(struct in6_addr));
		memcpy(&info.dst,  &bule->dest, sizeof(struct in6_addr));
		info.ifindex = bule->sending_ifindex;
		memcpy(&info.drtr, &bule->sending_default_route, sizeof(struct in6_addr));

		DEBUG("Creating BU message...");
		ret = mh_bu_create(&info, &iov, &iov_count, MH_BU_F_A | MH_BU_F_H | 0x0083, bule->lifetime);
		if (ret <= 0) {
			bule_unlock(bule);
			ERROR("Unable to create BU message");
			break;
		}
		iov_num = ret;
		buhookp.iov = iov;
		buhookp.iov_count = iov_count;
		buhookp.iov_len = iov_num;
		buhookp.bule = bule;
		
		bule_unlock(bule);
		
		ret = hooks_run(ccom_opts.create_bu_hook_id, &buhookp);
		if (ret) {
			ERROR("Error during running BU hooks");
			break;
		}
		DEBUG("Creating BU message...OK");

		DEBUG("Sending BU message...");
		ret = mh_send(&info, iov, buhookp.iov_count);
		if (ret > 0)
			retval = 0;
		DEBUG("Sending BU message...%s %d", (ret < 0) ? "FAILED" : "OK", ret);

		mh_free(iov, iov_num);

		break;
	case MSG_CCOM_SEND_BA:
		if (arg == NULL)
			break;
		ba = (struct msg_ccom_send_ba *)arg;
		if (ba->bce == NULL)
			break;

		memset(&info, 0, sizeof(info));
		memcpy(&info.rt2a, &ba->bce->src_addr, sizeof(struct in6_addr));
		memcpy(&info.src,  &ba->src, sizeof(struct in6_addr));
		memcpy(&info.dst,  &ba->bce->hoa, sizeof(struct in6_addr));
		info.ifindex = ba->ifindex;

		DEBUG("Creating BA message...");
		ret = mh_ba_create(&info, &iov, &iov_count, ba->status, ba->flags, ba->seq, ba->bce->lifetime);
		if (ret <= 0) {
			ERROR("Unable to create BA message");
			break;
		}
		iov_num = ret;
		bahookp.iov = iov;
		bahookp.iov_count = iov_count;
		bahookp.iov_len = iov_num;
		bahookp.bce = ba->bce;
		ret = hooks_run(ccom_opts.create_ba_hook_id, &bahookp);
		if (ret) {
			ERROR("Error during running BA hooks");
			break;
		}
		DEBUG("Creating BA message...OK");

		DEBUG("Sending BA message...");
		ret = mh_send(&info, iov, bahookp.iov_count);
		if (ret > 0)
			retval = 0;
		DEBUG("Sending BA message...%s %d", (ret < 0) ? "FAILED" : "OK", ret);

		mh_free(iov, iov_num);

		break;
	case MSG_CCOM_SEND_NA:
		if (arg == NULL)
			break;
		na = (struct msg_ccom_send_na *)arg;

		memset(&infoi6, 0, sizeof(infoi6));
		memcpy(&infoi6.src, &na->src, sizeof(struct in6_addr));
		memcpy(&infoi6.dst, &na->dst, sizeof(struct in6_addr));
		infoi6.ifindex = na->ifindex;

		retval = icmp6_na_send(&infoi6, &na->target, na->flags);
		if (retval > 0)
			retval = 0;
		break;
	case MSG_CCOM_SEND_RS:
		if (arg == NULL)
			break;
		rs = (struct msg_ccom_send_rs *)arg;

		memset(&infoi6, 0, sizeof(infoi6));
		memcpy(&infoi6.src, &rs->src, sizeof(struct in6_addr));
		memcpy(&infoi6.dst, &rs->dst, sizeof(struct in6_addr));
		infoi6.ifindex = rs->ifindex;

		retval = icmp6_rs_send(&infoi6);
		if (retval > 0)
			retval = 0;
		break;
	}

	return retval;
}

static struct imsg_opts imsg_opts = {
	.event_fp = NULL,
	.message_fp = ccom_proc_message,
	.event_max = EVT_CCOM_NONE
};

/*!
 * \brief Cleanup
 * 
 * Currently it is empty.
 * \return Zero
 */
static int ccom_exit_handler(void * arg)
{
	return 0;
}

/*! 
 * \brief Handler for the core_all_inited hook.
 *
 * It connects to the DATA module, which is initialized later than the core-comm module
 * \param arg Unused
 * \return Zero if OK
 */
static int ccom_init2(void * arg)
{
	int ret;

	ret = imsg_get_id(MSG_DATA);
	if (ret < 0) {
		ERROR("Unable to find module: data");
		return -1;
	}
	ccom_opts.msg_ids.data = ret;

	return 0;
}

/*!
 * \brief Initializing core-comm module
 *
 * Registers exit function to the 'core-exit' hook.
 * Register internal messaging.
 * Creates hooks for message processing stages.
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int core_comm_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	unsigned int core_exit_id;

	DEBUG("Initializing module: core-comm");

	ret = imsg_register(MSG_CCOM, &imsg_opts);
	if (ret < 0)
		return ret;
	ccom_opts.msg_ids.me = ret;

	ret = imsg_get_id(MSG_ENV);
	if (ret < 0) {
		ERROR("Unable to find module: environment");
		return -1;
	}
	ccom_opts.msg_ids.env = ret;

	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 100-SEQ_CCOM, ccom_exit_handler);
	}

	ret = hooks_get_id("core-all-inited");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 1, ccom_init2);
	}

	ret = hooks_register("create-mh-bu");
	if (ret < 0) {
		ERROR("Unable to cretae cretae-mh-bu hook");
		return -1;
	}
	ccom_opts.create_bu_hook_id = ret;

	ret = hooks_register("create-mh-ba");
	if (ret < 0) {
		ERROR("Unable to cretae cretae-mh-ba hook");
		return -1;
	}
	ccom_opts.create_ba_hook_id = ret;

	ret = hooks_register("parse-mh-bu-flags");
	if (ret < 0) {
		ERROR("Unable to cretae parse-mh-bu-flags");
		return -1;
	}
	ccom_opts.parse_bu_flags_hook_id = ret;

	ret = hooks_register("parse-mh-ba-flags");
	if (ret < 0) {
		ERROR("Unable to cretae parse-mh-ba-flags");
		return -1;
	}
	ccom_opts.parse_ba_flags_hook_id = ret;

	icmp6_init();
	mh_init();

	icmp6_ra_register();
	icmp6_na_register();

	mh_bu_register();
	mh_ba_register();

	mho_altcoa_init();

	return 0;
}

MIP6_MODULE_INIT(MSG_CCOM, SEQ_CCOM, core_comm_module_init);

/*!
 * \brief Main dump function
 *
 * Dump message iov and all corresponding stuff, i.e.
 * source and destination, contents of cmsg part etc.
 * \param msg Message
 */
void ccom_msg_dump(struct msghdr * msg)
{
	struct sockaddr_in6 * daddr;
	struct cmsghdr * cmsg;
	struct in6_pktinfo * pinfo;
	void *rthp;
	struct in6_addr *seg = NULL;
	int i, j;
	struct iovec * iov;
	char octet[4];
	char * buf;

	DEBUG("MSGHDR DUMP");
	daddr = (struct sockaddr_in6 *)msg->msg_name;
	DEBUG("msghdr: \n"
		  "\t\t name [" IP6ADDR_FMT "]:%d (name len %d)\n"
		  "\t\t flags %08X\n"
		  "\t\t control len %d iovec count %d",
		  IP6ADDR_TO_STR(&daddr->sin6_addr), ntohs(daddr->sin6_port), msg->msg_namelen,
		  msg->msg_flags,
		  msg->msg_controllen, msg->msg_iovlen);

	for (cmsg = CMSG_FIRSTHDR(msg); cmsg != NULL; cmsg = CMSG_NXTHDR(msg,cmsg)) {
		if (cmsg->cmsg_level == IPPROTO_IPV6 && cmsg->cmsg_type == IPV6_PKTINFO) {
			pinfo = (struct in6_pktinfo *) CMSG_DATA(cmsg);
			DEBUG("cmsg -> ip6_pktinfo: \n"
				  "\t\t addr " IP6ADDR_FMT " ifindex %d",
				  IP6ADDR_TO_STR(&pinfo->ipi6_addr), pinfo->ipi6_ifindex);
		 } else if (cmsg->cmsg_level == IPPROTO_IPV6 && cmsg->cmsg_type == IPV6_RTHDR) {
			rthp = CMSG_DATA(cmsg);
			if (inet6_rth_gettype(rthp) == IPV6_RTHDR_TYPE_2) {
				seg = mip6d_ng_inet6_rth_getaddr(rthp, 0);
				if (seg != NULL)
					DEBUG("cmsg -> rthdr type 2: \n"
						"\t\t addr " IP6ADDR_FMT, IP6ADDR_TO_STR(seg));
			}
		 }
	 }

	for (i = 0; i < msg->msg_iovlen; i++) {
		iov = &msg->msg_iov[i];
		buf = malloc(iov->iov_len * 3 + 1);
		if (buf != NULL) {
			memset(buf, 0, iov->iov_len * 3);
			for (j = 0; j < iov->iov_len && j < 40; j++) {
				snprintf(octet, sizeof(octet), "%02x ", ((uint8_t *)iov->iov_base)[j]);
				strncat(buf, octet, iov->iov_len * 3);
			}
			if (j < iov->iov_len) {
				snprintf(octet, sizeof(octet), "...");
				strncat(buf, octet, iov->iov_len * 3);
			}
			DEBUG("iov #%d (len %d):\n"
				  "\t\t %s",
				  i, iov->iov_len, buf);
			free(buf);
		}
	}
}

/*! \} */

