
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#ifndef MIP6D_NG_MAIN_H_
#define MIP6D_NG_MAIN_H_

void ccom_msg_dump(struct msghdr * msg);

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
struct ccom_opts {
	/*! Messaging IDs */
	struct {
		/*! Core-comm module's own ID */
		int me;
		/*! Messaging ID of data module */
		int data;
		/*! Messaging ID of environment module */
		int env;
	} msg_ids;
	/*! Hook id of create_bu hook */
	int create_bu_hook_id;
	/*! Hook id of create_ba hook */
	int create_ba_hook_id;
	/*! Hook id of parse_bu_flags hook */
	int parse_bu_flags_hook_id;
	/*! Hook id of parse_bu_flags hook */
	int parse_ba_flags_hook_id;
};

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
extern struct ccom_opts ccom_opts;

#endif /* MIP6D_NG_MAIN_H_ */

/*! \} */
