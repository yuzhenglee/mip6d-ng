
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/hooks.h>

#include <mip6d-ng/data.h>
#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-ba.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/imessaging.h>

#include "mh-handlers.h"
#include "mh.h"
#include "mh-ba.h"
#include "main.h"

/*!
 * \brief Maximum number of BA message pieces
 */
#define MH_BA_IOVEC_NUM		5

/*!
 * \brief Create Binding Acknowledge message
 *
 * \param info Packet info
 * \param iov Message iov (output)
 * \param iov_count Number of message pieces (output)
 * \param status BA status
 * \param flags BA flags
 * \param seq Sequence number
 * \param lifetime Binding lifetime
 * \return Number of allocated message pieces, or negative
 */
int mh_ba_create(struct mh_pkt_info_bundle * info, struct iovec ** iov, int * iov_count, uint8_t status, uint8_t flags, uint16_t seq, uint16_t lifetime)
{
	int iov_idx = 0;
	struct ip6_mh_ba * ba;

	*iov = malloc(sizeof(struct iovec) * MH_BA_IOVEC_NUM);
	if (*iov == NULL) {
		ERROR("Unable to allocate BA iov.");
		return -1;
	}
	memset(*iov, 0, sizeof(struct iovec) * MH_BA_IOVEC_NUM);
	ba = (struct ip6_mh_ba *)mh_create(&((*iov)[iov_idx++]), IP6_MH_TYPE_BA, sizeof(struct ip6_mh_ba));
	if (ba == NULL) {
		ERROR("Unable to allocate and create BA message");
		goto out;
	}

	ba->ip6mhba_status = status;
	ba->ip6mhba_flags = flags;
	ba->ip6mhba_seq = seq;
	ba->ip6mhba_lifetime = htons(lifetime);

	*iov_count = iov_idx;

	return MH_BA_IOVEC_NUM;

out:

	mh_free(*iov, MH_BA_IOVEC_NUM);
	*iov = NULL;

	return -1;
}

/*!
 * \brief Helper function to copy evt_ccom_ba struct
 *
 * This helper struct uses memcpy to copy the struct
 * and increases the reference count of the BULE
 * in it
 *
 * \param dest Destination
 * \param src Source
 * \param n Length
 * \return Destination
 */
static void * eba_copy(void * dest, const void * src, size_t n)
{
	struct evt_ccom_ba * eba = (struct evt_ccom_ba *)src;
	struct refcnt * refcnt;

	if (src == NULL || n == 0)
		return NULL;
	
	dest = memcpy(dest, src, n); 
	
	if (dest != NULL) {
		if (eba->bule != NULL) {
			refcnt = (struct refcnt *)eba->bule;
			refcnt_get(refcnt);
		}
	}

	return dest;

}

/*!
 * \brief Parsing Binding Acknowledge message
 *
 * It finds the corresponding BULE to the BA message.
 * It runs the parse_ba_flags hook, to parse more flags, and calls
 * the MH option parsing stuff. Finally it sends a EVT_CCOM_BA event, which
 * argument will be struct evt_ccom_ba. It contains the BULE and the contents
 * of the BA.
 *
 * \param mh Mobility Header message
 * \param len Size of MH message
 * \param info Packet info
 */
static void mh_ba_recv(const struct ip6_mh * mh, ssize_t len, const struct mh_pkt_info_bundle * info)
{
	int ret;
	int olen;
	struct ip6_mh_ba * ba;
	struct evt_ccom_ba * eba;
	struct msg_data_find_bule fbule;
	struct parse_flags_ba_param baf;
	struct bule * bule = NULL;

	if (mh->ip6mh_hdrlen < 1) {
		ERROR("Invalid BA message");
		return;
	}
	ba = (struct ip6_mh_ba *)mh;
	olen = mh->ip6mh_hdrlen * 8 + 8 - sizeof(struct ip6_mh_ba);

	fbule.ifindex = 0;
	memcpy(&fbule.dest, &info->src, sizeof(struct in6_addr));
	memcpy(&fbule.hoa, &info->dst, sizeof(struct in6_addr));
	memcpy(&fbule.coa, &info->rt2a, sizeof(struct in6_addr));
	ret = IMSG_MSG_ARGS(ccom_opts.msg_ids.me, ccom_opts.msg_ids.data, MSG_DATA_FIND_BULE, &fbule, sizeof(fbule), &bule, sizeof(bule));
	if (ret || bule == NULL) {
		ERROR("Unable to find corresponding BULE for BA");
		return;
	}

	baf.flags = ba->ip6mhba_flags;
	baf.bule = bule;
	hooks_run(ccom_opts.parse_ba_flags_hook_id, &baf);
	mh_opt_recv(IP6_MH_TYPE_BA, (unsigned char *)(ba + 1), olen, bule);

	INFO("BA: STATUS %u SEQ %u LIFETIME %u OptsLen %d", ba->ip6mhba_status, ba->ip6mhba_seq, ntohs(ba->ip6mhba_lifetime), olen);

	eba = malloc(SIZEOF_EVT_CCOM_BA(olen));
	if (eba == NULL) {
		ERROR("Unable to allocate event BA buffer");
		return;
	}
	memset(eba, 0, SIZEOF_EVT_CCOM_BA(olen));

	memcpy(&eba->ba, ba, sizeof(struct ip6_mh_ba));
	eba->bule = bule;
	eba->mh_opts_len = olen;
	if (olen > 0)
		memcpy(eba->mh_opts, ba + 1, olen);

	ret = imsg_send_event(ccom_opts.msg_ids.me, EVT_CCOM_BA, eba, sizeof(struct evt_ccom_ba), 1, eba_copy);

	free(eba);

	refcnt_put(&bule->refcnt);
}

/*!
 * \brief Register Binding Update to MH handlers
 */
int mh_ba_register()
{
	return mh_handler_register(IP6_MH_TYPE_BA, mh_ba_recv);
}

/*! \} */

