
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/list.h>

#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/mh.h>

#include "mh-handlers.h"
#include "mh-io.h"

/*!
 * \brief Mobility Header Message handler
 */
struct mh_handler {
	/*! Linked list management */
	struct list_head list;
	/*! MH type */
	uint8_t type;
	/*! Processing callback function */
	void (* recv)(const struct ip6_mh * mh, ssize_t len, const struct mh_pkt_info_bundle * info);
};

/*!
 * \brief List of MH handler 
 */
static LIST_HEAD(mh_handlers);

/*! 
 * \brief Register Mobility Header Message handler 
 *
 * \param type MH type
 * \param recv Parsing callback function
 * \return Zero if OK
 */
int mh_handler_register(uint8_t type, void (* recv)(const struct ip6_mh * mh, ssize_t len, const struct mh_pkt_info_bundle * info))
{
	struct mh_handler * handler;

	handler = malloc(sizeof(struct mh_handler));
	if (handler == NULL) {
		ERROR("Unable to allocate mh handler");
		return -1;
	}

	memset(handler, 0, sizeof(struct mh_handler));

	handler->type = type;
	handler->recv = recv;

	list_add_tail(&handler->list, &mh_handlers);

	return 0;
}

/*!
 * \brief Find corresponding MH handler and call it
 *
 * It finds the corresponding MH handler for the message, and calls it
 *
 * \param mh Mobility Header message
 * \param len Length of the message
 * \param info Packet info
 * \return 1 if handler found, zero if not
 */
int mh_handlers_call(const struct ip6_mh * mh, ssize_t len, const struct mh_pkt_info_bundle * info)
{
	struct list_head * pos;
	struct mh_handler * handler;
	int type = mh->ip6mh_type;
	int known = 0;

	list_for_each(pos, &mh_handlers) {
		handler = list_entry(pos, struct mh_handler, list);

		if (type == handler->type) {
			if (handler->recv != NULL)
				handler->recv(mh, len, info);
			
			known = 1;
		}
	}

	return known;
}

/*! \} */

