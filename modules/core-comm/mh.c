
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-bu.h>
#include <mip6d-ng/mho-altcoa.h>

#include "mh.h"

/*!
 * \brief Create Mobility Header Message
 *
 * It initializes the Mobility Header fields
 *
 * \param iov Message iov
 * \param mh_type MH type
 * \param mh_type_len Length of MH 
 * \return Allocated and initialized MH or NULL
 */
struct ip6_mh * mh_create(struct iovec * iov, uint8_t mh_type, uint8_t mh_type_len)
{
	struct ip6_mh *mh;

	iov->iov_base = malloc(mh_type_len);
	if (iov->iov_base == NULL)
		return NULL;

	iov->iov_len = mh_type_len;
	memset(iov->iov_base, 0, mh_type_len);

	mh = (struct ip6_mh *)iov->iov_base;
	mh->ip6mh_proto = IPPROTO_NONE;
	mh->ip6mh_hdrlen = 0; 		/* calculated after padding */
	mh->ip6mh_type = mh_type;
	mh->ip6mh_reserved = 0;
	mh->ip6mh_cksum = 0; 		/* kernel does this for us */

	return mh;
}

/*!
 * \brief Free Mobility Header message and corrsponding options
 * \param iov Iovec array with message pieces
 * \param iov_count Number of pieces in iov array
 * \return Zero if OK
 */
int mh_free(struct iovec * iov, int iov_count)
{
	int i;

	if (iov == NULL)
		return -1;

	for (i = 0; i < iov_count; i++) {
		if (iov[i].iov_base != NULL) {
			free(iov[i].iov_base);
		}
	}

	free(iov);

	return 0;
}

/*! \} */
