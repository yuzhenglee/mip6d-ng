
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-data
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/tasks.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/imessaging.h>

#include <mip6d-ng/data.h>
#include <mip6d-ng/bce.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/core-comm.h>

#include "bce.h"
#include "main.h"

/*!
 * \brief Binding Cache
 */
static LIST_HEAD(data_bc);

/*!
 * \brief Binding Cache lock
 */
static pthread_mutex_t data_bc_mutex = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief Hook to help finding the matching BCE
 * (matching for ext options)
 */
static unsigned long data_bc_ext_match_hook_id = 0;

/*!
 * \brief Hook to help copying extensions of BCE 
 * in case of BCE update
 */
static unsigned long data_bc_ext_update_hook_id = 0;

/*!
 * \brief Initialize Binding Cache support
 *
 * It creates two hooks: data-bc-ext-match to help
 *  matching BC entries by modules and data-bc-ext-update
 *  to help to the modules to update their BCE stuff
 * \return Zero if OK, otherwise negative
 */
int bce_init()
{
	int ret;

	ret = hooks_register("data-bc-ext-match");
	if (ret < 0) {
		ERROR("Unable to register data-bc-ext-match hook");
		return -1;
	}
	data_bc_ext_match_hook_id = ret;

	ret = hooks_register("data-bc-ext-update");
	if (ret < 0) {
		ERROR("Unable to register data-bc-ext-update hook");
		return -1;
	}
	data_bc_ext_update_hook_id = ret;

	return 0;
}

/*!
 * \brief Clenup Binding Cache support
 *
 * It does nothing
 * \return Zero
 */
int bce_clean()
{
	return 0;
}

/*!
 * \brief Delete and free Binding Cache entry
 *
 * It will be called by the reference counter module automatically,
 * if the reference count will be zero.
 * It frees the bce and all of the extensions.
 * \param arg struct bce to delete
 */
static void data_proc_bce_to_delete(void * arg)
{
	struct bce * bce = arg;
	struct list_head * epos, * epos2;
	struct bce_ext * ext;

	if (bce == NULL)
		return;

	hooks_run(bce->del_hook_id, bce);
	hooks_unregister(bce->del_hook_id);

	list_for_each_safe(epos, epos2, &bce->ext) {
		ext = list_entry(epos, struct bce_ext, list);
		list_del(epos);
		free(ext);
	}

	INFO("Delete and free BCE... @ " IP6ADDR_FMT " @ " IP6ADDR_FMT, IP6ADDR_TO_STR(&bce->hoa), IP6ADDR_TO_STR(&bce->coa));

	free(bce);
}

/*!
 * \brief Initialize new Binding Cache entry
 *
 * It sets everything to zero, initializes the
 * reference counting and sets the id. Additionally it 
 * initializes the mutex, and the list of extensions.
 * It does NOt add the BCE to the Binding cache!
 * \param bce Newly allocated Binding Cache entry
 * \return Zero if OK, otherwise negative 
 */
int data_bce_init_new(struct bce * bce)
{
	char refcnt_name[20];

	if (bce == NULL)
		return -1;

	memset(bce, 0, sizeof(struct bce));
	pthread_mutex_init(&bce->_lock, NULL);
	INIT_LIST_HEAD(&bce->ext);
	bce->id = (unsigned long)bce;
	snprintf(refcnt_name, sizeof(refcnt_name), "BCE %lu", bce->id);  
	refcnt_init((struct refcnt *)&bce->refcnt, data_proc_bce_to_delete, bce, refcnt_name);
	bce->del_hook_id = -1;

	return 0;
}

/*!
 * \brief Processing Binding Cache delete
 *
 * It sets the BCE_F_TO_DELETE_FLAG, and updates the hoa_cnt of the bce.
 * Next it calls the delete hook, so the modules will be able to release the bce.
 * Finally, if the entry was included to the Binding Cache, it will be
 * removed from the list. The initially got reference count will be put.
 * \param bce Binding Cache Entry
 * \param bc_lock If non-zero the Binding Cache List is locked
 * \return Zero if OK, otherwise negative
 */
static int data_proc_bce_del(struct bce * bce, unsigned char bc_lock)
{
	int mnum = 0;
	struct list_head * pos;
	struct bce * b;
	int m;
	int ld = 0;

	if (bce == NULL)
		return -1;

	bce_lock(bce);
	bce->flags |= BCE_F_TO_DELETE;
	bce_unlock(bce);

	if (bc_lock)
		pthread_mutex_lock(&data_bc_mutex);
	list_for_each(pos, &data_bc) {
		b = list_entry(pos, struct bce, list);
		bce_lock(b);
		m = memcmp(&bce->hoa, &b->hoa, sizeof(struct in6_addr));
		bce_unlock(b);
		if (m == 0) 
			mnum++;
	}
	if (bc_lock)
		pthread_mutex_unlock(&data_bc_mutex);
	
	bce_lock(bce);
	bce->hoa_cnt = mnum;
	DEBUG("Modify HoA Cnt of BCE before deleting to %d", bce->hoa_cnt);
	bce_unlock(bce);

	hooks_run(bce->del_hook_id, bce);
	hooks_unregister(bce->del_hook_id);

	bce_lock(bce);
	bce->flags |= BCE_F_TO_DELETE;
	ld = (bce->flags & BCE_F_INC);
	bce_unlock(bce);
	if (ld) {
		if (bc_lock)
			pthread_mutex_lock(&data_bc_mutex);
		list_del(&bce->list);
		if (bc_lock)
			pthread_mutex_unlock(&data_bc_mutex);
	}

	/* Put the initial refcnt 1 */
	refcnt_put(&bce->refcnt);

	return 0;
}

/*!
 * \brief Binding Cache expiration task
 *
 * It calls data_proc_bce_del to delete the entry
 * \param arg struct bce 
 */
static void data_bce_del_task(void * arg)
{
	struct bce * bce = (struct bce *)arg;

	bce_lock(bce);

	if (bce->delete_task == 0) {
		bce_unlock(bce);
		return;
	}
	bce->delete_task = 0;
	
	DEBUG("Delete BCE (from timed delete task)");
	
	bce_unlock(bce);
	
	data_proc_bce_del(bce, 1);

	refcnt_put(&bce->refcnt);
}

/*!
 * \brief Adding Binding Cache entry to the Binding cache
 *
 * It registers the delete hook, and adds the entry to the Binding cache list.
 * It starts the expiration task, and finally sends an EVT_DATA_NEW_BCE event
 * \param bce New Binding Cache entry
 * \param bc_lock If non-zero the Binding cache is locked
 * \return Zero if OK, otherwise negative
 */
static int data_proc_bce_add(struct bce * bce, unsigned char bc_lock)
{
	int ret;
	char hook_name[100];
	struct timespec task_ts;
	unsigned long id;

	if (bce == NULL)
		return -1;

	bce_lock(bce);

	snprintf(hook_name, sizeof(hook_name), "del-bce-%lu", bce->id);
	ret = hooks_register(hook_name);
	if (ret < 0) {
		ERROR("Unable to register BCE del hook: '%s'", hook_name);
		goto err;
	}
	bce->del_hook_id = ret;

	if (bc_lock)
		pthread_mutex_lock(&data_bc_mutex);
	list_add_tail(&bce->list, &data_bc);
	if (bc_lock)
		pthread_mutex_unlock(&data_bc_mutex);
	
	bce->flags |= BCE_F_INC;

	task_ts.tv_sec = bce->lifetime * 4,
	task_ts.tv_nsec = 0;
	refcnt_get(&bce->refcnt);
	id = tasks_add_rel(data_bce_del_task, bce, task_ts);
	bce->delete_task = id;
	if (id == 0) {
		ERROR("Unable to start BCE delete task");
		goto err;
	}

	INFO("New BCE. HoA " IP6ADDR_FMT " @ CoA " IP6ADDR_FMT " flags %08X", IP6ADDR_TO_STR(&bce->hoa), IP6ADDR_TO_STR(&bce->coa), bce->flags);

	imsg_send_event(data_opts.msg_ids.me, EVT_DATA_NEW_BCE, bce, sizeof(*bce), 0, refcnt_copy);
		
	bce_unlock(bce);

	return 0;

err:
		
	bce_unlock(bce);

	return -1;
}

/*!
 * \brief Update Binding Cache entry
 *
 * It restarts the expiration task, and updates the following fields of BCE:
 *  - flags
 *  - coa (and prev_coa, if modified)
 * It class the data-bc-ext-update hook to let for the modules
 * to update their own fields. Finally it send a EVT_DATA_CHANGE_BCE event
 * \param bce In-cache BCE to update
 * \param new_bce Not-in-cache BCE to get new values from it
 * \return Tero if OK, otherwise negative
 */
static int data_proc_bce_update(struct bce * bce, struct bce * new_bce)
{
	struct timespec task_ts;
	unsigned long id;
	struct bce_ext_update eu;
	
	if (bce == NULL || new_bce == NULL)
		return -1;

	bce_lock(bce);

	if (bce->delete_task != 0) {
		tasks_del(bce->delete_task);
		bce->delete_task = 0;
		refcnt_put(&bce->refcnt);
	}

	if (new_bce->lifetime == 0) {
		DEBUG("BCE Lifetime is zero. Delete.");
		bce_unlock(bce);
		/* data_bc_mutext is locked in the caller data_proc_bce function */
		data_proc_bce_del(bce, 0);
		return 0;
	}
		
	bce->lifetime = new_bce->lifetime;
	bce->seq = new_bce->seq;
	if (new_bce->flags & BCE_F_A)
		bce->flags |= BCE_F_A;
	else
		bce->flags &= ~(BCE_F_A);
	if (new_bce->flags & BCE_F_H)
		bce->flags |= BCE_F_H;
	else
		bce->flags &= ~(BCE_F_H);

	DEBUG("Update BCE. HoA " IP6ADDR_FMT " flags %08X", IP6ADDR_TO_STR(&bce->hoa), bce->flags);

	task_ts.tv_sec = bce->lifetime * 4,
	task_ts.tv_nsec = 0;
	refcnt_get(&bce->refcnt);
	id = tasks_add_rel(data_bce_del_task, bce, task_ts);
	bce->delete_task = id;
	if (id == 0) {
		ERROR("Unable to start BCE delete task");
	}

	if (memcmp(&bce->coa, &new_bce->coa, sizeof(struct in6_addr)) != 0) {
		memcpy(&bce->prev_coa, &bce->coa, sizeof(struct in6_addr));
		memcpy(&bce->coa, &new_bce->coa, sizeof(struct in6_addr));

		DEBUG("BCE update: CoA change detected: CoA " IP6ADDR_FMT, IP6ADDR_TO_STR(&bce->coa));
	} else {
		memset(&bce->prev_coa, 0, sizeof(struct in6_addr));
	}

	memcpy(&bce->src_addr, &new_bce->src_addr, sizeof(struct in6_addr));

	eu.bce = bce;
	eu.new_bce = new_bce;
	hooks_run(data_bc_ext_update_hook_id, &eu);

	imsg_send_event(data_opts.msg_ids.me, EVT_DATA_CHANGE_BCE, bce, sizeof(*bce), 0, refcnt_copy);

	bce_unlock(bce);

	return 0;
}

/*!
 * Processing new BCE stuff
 *
 * First it finds all of the corresponding entries, which Home Address 
 * is equal with the new one. Next it calls the data-bc-ext-match hook to 
 * let the modules to match based on their own circumstances. If lifetime is zero
 * in the new BCE, the corresponding BCE will be deleted, otherwise add or update will 
 * be processed. 
 * \param bce New BCE
 * \return Zero if OK, otherwise negative
 */
int data_proc_bce(struct bce * bce)
{
	int ret;
	struct list_head * pos, *pos2;
	struct bce * b;
	int m;
	struct bce_matches {
		struct bce * bce;
		unsigned char match;
	} * matches = NULL;
	int mnum = 0;
	int matches_num = 0;
	int i;
	struct bce_ext_match em;

	if (bce == NULL)
		return -1;

	pthread_mutex_lock(&data_bc_mutex);

	list_for_each_safe(pos, pos2, &data_bc) {
		b = list_entry(pos, struct bce, list);

		bce_lock(b);
		m = memcmp(&bce->hoa, &b->hoa, sizeof(struct in6_addr));
		bce_unlock(b);
		if (m == 0) {
			matches = realloc(matches, (mnum + 1) * sizeof(struct bce_matches));
			if (matches == NULL) {
				ERROR("Out of memory: Unable to allocate BCE matches array");
				pthread_mutex_unlock(&data_bc_mutex);
				return -1;
			}
			matches[mnum].bce = b;
			matches[mnum].match = 1;
			mnum++;
			matches_num++;
		}
	}

	DEBUG("HA: %d matching BC entries...", matches_num);

	for (i = 0; i < mnum; i++) {
		if (! matches[i].match)
			continue;
		bce_lock(matches[i].bce);
		matches[i].bce->hoa_cnt = mnum;
		bce_unlock(matches[i].bce);
		em.bce = matches[i].bce;
		em.new_bce = bce;
		ret = hooks_run(data_bc_ext_match_hook_id, &em);
		if (ret) {
			matches[i].match = 0;
			matches_num--;
		}
	}
	
	DEBUG("HA: %d matching BC entries after ext_match", matches_num);

	if (matches_num == 0) {
		if (bce->lifetime == 0) {
			INFO("Lifetime is zero, no matching BCE. Drop.");
			data_proc_bce_del(bce, 0);
			ret = 0;
		} else {
			/* TODO: New module for MN filtering, or applying restrictions */
			bce_lock(bce);
			bce->hoa_cnt = mnum;
			bce_unlock(bce);
			ret = data_proc_bce_add(bce, 0);
		}
	} else if (matches_num == 1) {
		ret = -1;
		for (i = 0; i < mnum; i++) {
			if (! matches[i].match)
				continue;
			/* TODO: New module for MN filtering, or applying restrictions */
			ret = data_proc_bce_update(matches[i].bce, bce);
			data_proc_bce_del(bce, 0);
			break;
		}
	} else {
		ERROR("Multiple matching BC entries. Do nothing.");
		data_proc_bce_del(bce, 0);
		ret = -1;
	}

	if (matches != NULL)
		free(matches);

	pthread_mutex_unlock(&data_bc_mutex);

	return ret;
}

/*!
 * \brief Delete all of the Binding Cache entries
 * \return Zero if OK
 */
int bce_clear_all()
{
	int ret = 0;
	struct list_head * pos, *pos2;
	struct bce * bce;

	pthread_mutex_lock(&data_bc_mutex);
	list_for_each_safe(pos, pos2, &data_bc) {
		bce = list_entry(pos, struct bce, list);
		ret |= data_proc_bce_del(bce, 0);
	}
	pthread_mutex_unlock(&data_bc_mutex);

	return ret;
}

/*!
 * \brief API function to get Binding Cache entries
 * \param reply_msg Reply message to construct
 * \param mlen Maximum length of reply_msg
 * \return Length of reply_message if OK, otherwise negative
 */
int bce_api_get(unsigned char * reply_msg, unsigned int mlen)
{
	struct data_api_get_bc * bc = NULL, * bc_new = NULL;
	unsigned int bnum = 0;

	struct list_head * pos;
	struct bce * b;

	pthread_mutex_lock(&data_bc_mutex);
	list_for_each(pos, &data_bc) {
		b = list_entry(pos, struct bce, list);

		bc_new = realloc(bc, (bnum + 1) * sizeof(struct data_api_get_bc));
		if (bc_new == NULL) {
			ERROR("Out of memory: Unable to alloc get_bc command answer");
			free(bc);
			goto out;
		}
		bc = bc_new;

		memcpy(&bc[bnum].hoa, &b->hoa, sizeof(struct in6_addr));
		memcpy(&bc[bnum].coa, &b->coa, sizeof(struct in6_addr));
		bc[bnum].lifetime = htons(b->lifetime);
		bc[bnum].flags = htonl(b->flags);
		bnum++;
	}

out:

	pthread_mutex_unlock(&data_bc_mutex);

	if (bnum == 0)
		return 0;

	if (reply_msg != NULL && mlen > 0) {
		if (mlen > bnum * sizeof(struct data_api_get_bc))
			mlen = bnum * sizeof(struct data_api_get_bc);
		memcpy(reply_msg, (unsigned char *)bc, mlen);

		return bnum * sizeof(struct data_api_get_bc);
	}
	
	return -1;
}

/*!
 * \brief Iterate Binding cache entries
 *
 * The given callback function will be called for all of them.
 * If The callback function returns negative, the iteration
 * will be stopped.
 * \param fp Callback
 * \param arg Extra argument for callback
 * \return Return value of the last callback function
 */
int bce_iterate(int (*fp)(struct bce *, void *), void * arg)
{
	int ret = 0;
	struct list_head * pos;
	struct bce * b;

	if (fp == NULL)
		return -1;

	pthread_mutex_lock(&data_bc_mutex);
	list_for_each(pos, &data_bc) {
		b = list_entry(pos, struct bce, list);

		refcnt_get(&b->refcnt);
		bce_lock(b);
		if ((b->flags & BCE_F_TO_DELETE) == BCE_F_TO_DELETE) {
			bce_unlock(b);
			refcnt_put(&b->refcnt);
			continue;
		}
		bce_unlock(b);
		ret = fp(b, arg);
		refcnt_put(&b->refcnt);
		if (ret < 0)
			break;
	}

	pthread_mutex_unlock(&data_bc_mutex);

	return ret;
}

/*! \} */
