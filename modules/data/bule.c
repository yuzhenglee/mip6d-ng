
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
 * \addtogroup internal-data
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/imessaging.h>

#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/core-comm.h>

#include "bule.h"
#include "main.h"

/*!
 * \brief Binding Update List
 */
static LIST_HEAD(data_bul);

/*!
 * \brief Binding Update List lock
 */
static pthread_mutex_t data_bul_mutex = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief Delete and free Binding Update List entry
 *
 * It will be called by the reference counter module automatically,
 * if the reference count will be zero.
 * It frees the bule and all of the extensions.
 * \param arg struct bule to delete
 */
static void * __data_proc_bule_do_delete(void * arg)
{
	struct bule * bule = arg;
	struct list_head * epos, * epos2;
	struct bule_ext * ext;

	if (bule == NULL)
		return NULL;

	bule_lock(bule);
	pthread_mutex_lock(&data_bul_mutex);
	list_del(&bule->list);
	pthread_mutex_unlock(&data_bul_mutex);
	bule_unlock(bule);

	list_for_each_safe(epos, epos2, &bule->ext) {
		ext = list_entry(epos, struct bule_ext, list);
		list_del(epos);
		free(ext);
	}
	
	INFO("Delete and free BULE... @ %u " IP6ADDR_FMT " dest " IP6ADDR_FMT, 
			bule->ifindex, IP6ADDR_TO_STR(&bule->coa), IP6ADDR_TO_STR(&bule->dest));

	free(bule);

	return NULL;
}

/*!
 * \brief Delete and free Binding Update List entry
 *
 * It will be called by the reference counter module automatically,
 * if the reference count will be zero.
 * It frees the bule and all of the extensions.
 * It executes the delete operation in  a new thread.
 * \param arg struct bule to delete
 */
static void data_proc_bule_do_delete(void * arg)
{
	threading_create(__data_proc_bule_do_delete, arg);
}

/*!
 * \brief Create new Binding Update List entry and add it to the Binding Update List
 *
 * It gets the Home Address from the control modules
 * It checks duplicate entries, based on the next values: ifindex, coa
 * It allocates a new entry and initializes the next stuff:
 *  - Set everything to zero
 *  - Initialize mutex
 *  - Initialize list of extensions
 *  - Initialize reference counter and id
 *  - Based on function arguments: ifindex, coa, default_route, destination
 *  - Create delete hook
 * Finally it adds it to the Binding Update List and calls the EVT_DATA_INIT_BULE event
 * \param ifindex Interface index
 * \param coa Care-of Address
 * \param dst Destination (i.e. Home Agent address)
 * \param drtr Default route
 * \return Zero if OK, otherwise negative
 */
int data_proc_bule_add(unsigned int ifindex, struct in6_addr * coa, struct in6_addr * dst, struct in6_addr * drtr)
{
	int ret;
	int i;
	struct in6_addr hoa;
	struct list_head * pos;
	char hook_name[100];
	struct bule * bule;
	char refcnt_name[20];

	ret = IMSG_MSG_ARGS(data_opts.msg_ids.me, data_opts.msg_ids.ctrl, MSG_CTRL_GET_HOA, NULL, 0, &hoa, sizeof(hoa));
	if (ret) {
		ERROR("Unable to get HOA from ctrl");
		return -1;
	}

	if (coa == NULL || dst == NULL || drtr == NULL)
		return -1;


	ret = 0;
	i = 0;
	pthread_mutex_lock(&data_bul_mutex);
	list_for_each(pos, &data_bul) {
		bule = list_entry(pos, struct bule, list);
		DEBUG("New BULE. Dump existing BUL: %d. HoA " IP6ADDR_FMT " dst " IP6ADDR_FMT " CoA " IP6ADDR_FMT " via " IP6ADDR_FMT " @ %u", ++i,
				IP6ADDR_TO_STR(&bule->hoa), IP6ADDR_TO_STR(&bule->dest),
				IP6ADDR_TO_STR(&bule->coa), IP6ADDR_TO_STR(&bule->default_route), bule->ifindex);
		if (bule->ifindex == ifindex && memcmp(&bule->coa, coa, sizeof(struct in6_addr)) == 0) {
			ret = 1;;
		}
	}
	pthread_mutex_unlock(&data_bul_mutex);
	if (ret) {
		ERROR("BULE exists");
		return -1;
	}

	bule = malloc(sizeof(struct bule));
	if (bule == NULL) {
		ERROR("Out of memory: Unable to alloc new BULE");
		goto out;
	}
	memset(bule, 0, sizeof(struct bule));
	pthread_mutex_init(&bule->_lock, NULL);
	INIT_LIST_HEAD(&bule->ext);
	snprintf(refcnt_name, sizeof(refcnt_name), "BULE %d", ifindex);
	refcnt_init((struct refcnt *)&bule->refcnt, data_proc_bule_do_delete, bule, refcnt_name);
	bule->id = (unsigned long)bule;
	bule->del_hook_id = -1;

	bule->ifindex = ifindex;
	bule->sending_ifindex = ifindex;
	memcpy(&bule->coa, coa, sizeof(bule->coa));
	memcpy(&bule->sending_coa, coa, sizeof(bule->sending_coa));
	memcpy(&bule->dest, dst, sizeof(bule->dest));
	memcpy(&bule->hoa, &hoa, sizeof(bule->hoa));
	memcpy(&bule->default_route, drtr, sizeof(bule->default_route));
	memcpy(&bule->sending_default_route, drtr, sizeof(bule->sending_default_route));

	pthread_mutex_lock(&data_bul_mutex);
	list_add_tail(&bule->list, &data_bul);
	pthread_mutex_unlock(&data_bul_mutex);

	snprintf(hook_name, sizeof(hook_name), "del-bule-%lu", bule->id);
	ret = hooks_register(hook_name);
	if (ret < 0) {
		ERROR("Unable to register BULE del hook: '%s'", hook_name);
		goto out;
	}
	bule->del_hook_id = ret;

	INFO("New BULE. HoA " IP6ADDR_FMT " dst " IP6ADDR_FMT " CoA " IP6ADDR_FMT " via " IP6ADDR_FMT " @ %u", 
			IP6ADDR_TO_STR(&bule->hoa), IP6ADDR_TO_STR(&bule->dest),
			IP6ADDR_TO_STR(&bule->coa), IP6ADDR_TO_STR(&bule->default_route), bule->ifindex);

	imsg_send_event(data_opts.msg_ids.me, EVT_DATA_INIT_BULE, bule, sizeof(*bule), 0, refcnt_copy);

	return 0;

out:
	if (bule)
		free(bule);

	return -1;
}

/*!
 * \brief Update Binding Update List Entry
 *
 * It updates the coa and default_route fields, and sets the previous values into the prev_coa and fprv_default_route fields
 * Finally it sends a EVT_DATA_CHANGE_BULE event
 * \param bule Binding Update List Entry
 * \param coa Care-of Address
 * \param dst Destination (i.e. Home Agent address)
 * \param drtr Default route
 * \return Zero if OK, otherwise negative
 */
int data_proc_bule_update(struct bule * bule, struct in6_addr * coa, struct in6_addr * dst, struct in6_addr * drtr)
{
	if (bule == NULL || coa == NULL || dst == NULL || drtr == NULL)
		return -1;

	if (memcmp(&bule->coa, coa, sizeof(struct in6_addr)) == 0 && memcmp(&bule->dest, dst, sizeof(struct in6_addr)) == 0 && 
			memcmp(&bule->default_route, drtr, sizeof(struct in6_addr)) == 0) {
		DEBUG("BULE update: No change detected.");
		return 0;
	}

	memcpy(&bule->prev_coa, &bule->coa, sizeof(bule->prev_coa));
	memcpy(&bule->prev_default_route, &bule->default_route, sizeof(bule->prev_default_route));

	memcpy(&bule->coa, coa, sizeof(bule->coa));
	memcpy(&bule->sending_coa, coa, sizeof(bule->sending_coa));
	memcpy(&bule->default_route, drtr, sizeof(bule->default_route));
	memcpy(&bule->sending_default_route, drtr, sizeof(bule->sending_default_route));
	
	bule->flags &= ~(BULE_F_INITED);

	DEBUG("BULE update: Change detected.");

	imsg_send_event(data_opts.msg_ids.me, EVT_DATA_CHANGE_BULE, bule, sizeof(*bule), 0, refcnt_copy);
		
	return 0;
}

/*!
 * \brief Delete Binding Update List entry
 *
 * It puts the initial reference count, and runs the delete hook.
 * Finally it removes the delete hook
 * \param bule Binding Update List Entry
 * \param bul_lock If non-zero the Binding Update List is locked
 * \return Zero if OK, otherwise negative
 */
int data_proc_bule_del(struct bule * bule, unsigned char bul_lock)
{
	struct list_head * pos;
	struct bule * b;
	int i;

	if (bule == NULL)
		return -1;
	
	if ((bule->flags & BULE_F_TO_DELETE) == BULE_F_TO_DELETE)
		return 0;

	i = 0;
	pthread_mutex_lock(&data_bul_mutex);
	list_for_each(pos, &data_bul) {
		b = list_entry(pos, struct bule, list);
		DEBUG("Del BULE. Dump existing BUL: %d. HoA " IP6ADDR_FMT " dst " IP6ADDR_FMT " CoA " IP6ADDR_FMT " via " IP6ADDR_FMT " @ %u", ++i,
				IP6ADDR_TO_STR(&b->hoa), IP6ADDR_TO_STR(&b->dest),
				IP6ADDR_TO_STR(&b->coa), IP6ADDR_TO_STR(&b->default_route), b->ifindex);
	}
	pthread_mutex_unlock(&data_bul_mutex);

	bule_lock(bule);
	bule->flags |= BULE_F_TO_DELETE;
	bule_unlock(bule);

	hooks_run(bule->del_hook_id, bule);
	hooks_unregister(bule->del_hook_id);
	
	/* Second put: put the initial refcnt 1 */
	refcnt_put(&bule->refcnt);

	return 0;
}

/*!
 * \brief Find a Binding Update List entry
 *
 * If ifindex argument is non-zero, it must match with the given one.
 * If coa argument is not NULL, it must match with the given one.
 * The destination and the home address must match with the given one.
 * Reference count will be held on the returned bule.
 * \param ifindex Interface index or zero
 * \param coa Care-of Address or NULL
 * \param dest Destination (i.e. Home Agent address)
 * \param hoa Home Address
 * \return BULE with held reference count) or NULL
 */
struct bule * data_bule_find(unsigned int ifindex, struct in6_addr * coa, struct in6_addr * dest, struct in6_addr * hoa)
{
	struct list_head * pos;
	struct bule * b;
	struct in6_addr _coa;

	if (dest == NULL || hoa == NULL)
		return NULL;

	memset(&_coa, 0, sizeof(struct in6_addr));
	if (coa != NULL) {
		if (memcmp(&_coa, coa, sizeof(struct in6_addr)) == 0)
			coa = NULL;
		else
			memcpy(&_coa, coa, sizeof(struct in6_addr));
	}

	pthread_mutex_lock(&data_bul_mutex);
	list_for_each(pos, &data_bul) {
		b = list_entry(pos, struct bule, list);

		refcnt_get(&b->refcnt);
		bule_lock(b);
		if ((b->flags & BULE_F_TO_DELETE) == BULE_F_TO_DELETE) {
			bule_unlock(b);
			refcnt_put(&b->refcnt);
			continue;
		}
		if ((b->ifindex == ifindex || ifindex == 0) && memcmp(&b->dest, dest, sizeof(struct in6_addr)) == 0 &&
				memcmp(&b->hoa, hoa, sizeof(struct in6_addr)) == 0 && 
				(memcmp(&b->coa, &_coa, sizeof(struct in6_addr)) == 0 || coa == NULL)) {
			bule_unlock(b);
			pthread_mutex_unlock(&data_bul_mutex);
			return b;
		}
		bule_unlock(b);
		refcnt_put(&b->refcnt);
	}
	pthread_mutex_unlock(&data_bul_mutex);

	return NULL;
}

struct bule * data_bule_find_d(unsigned int ifindex, struct in6_addr * coa, struct in6_addr * dest, struct in6_addr * hoa)
{
	struct list_head * pos;
	struct bule * b;
	struct in6_addr _coa;

	if (dest == NULL || hoa == NULL)
		return NULL;

	memset(&_coa, 0, sizeof(struct in6_addr));
	if (coa != NULL) {
		if (memcmp(&_coa, coa, sizeof(struct in6_addr)) == 0)
			coa = NULL;
		else
			memcpy(&_coa, coa, sizeof(struct in6_addr));
	}

	pthread_mutex_lock(&data_bul_mutex);
	list_for_each(pos, &data_bul) {
		b = list_entry(pos, struct bule, list);

		refcnt_get(&b->refcnt);
		bule_lock(b);
		if ((b->ifindex == ifindex || ifindex == 0) && memcmp(&b->dest, dest, sizeof(struct in6_addr)) == 0 &&
				memcmp(&b->hoa, hoa, sizeof(struct in6_addr)) == 0 && 
				(memcmp(&b->coa, &_coa, sizeof(struct in6_addr)) == 0 || coa == NULL)) {
			bule_unlock(b);
			pthread_mutex_unlock(&data_bul_mutex);
			return b;
		}
		bule_unlock(b);
		refcnt_put(&b->refcnt);
	}
	pthread_mutex_unlock(&data_bul_mutex);

	return NULL;
}

/*!
 * \brief Delete all of the Binding Update List entries
 * \return Zero if OK
 */
int bule_clear_all()
{
	int ret = 0;
	struct list_head * pos, *pos2;
	struct bule * bule;

	pthread_mutex_lock(&data_bul_mutex);
	list_for_each_safe(pos, pos2, &data_bul) {
		bule = list_entry(pos, struct bule, list);
		ret |= data_proc_bule_del(bule, 0);
	}
	pthread_mutex_unlock(&data_bul_mutex);

	return ret;
}

/*!
 * \brief API function to get Binding Update List entries
 * \param reply_msg Reply message to construct
 * \param mlen Maximum length of reply_msg
 * \return Length of reply_message if OK, otherwise negative
 */
int bule_api_get(unsigned char * reply_msg, unsigned int mlen)
{
	struct data_api_get_bul * bul = NULL, * bul_new = NULL;
	unsigned int bnum = 0;

	struct list_head * pos;
	struct bule * b;

	pthread_mutex_lock(&data_bul_mutex);
	list_for_each(pos, &data_bul) {
		b = list_entry(pos, struct bule, list);

		bul_new = realloc(bul, (bnum + 1) * sizeof(struct data_api_get_bul));
		if (bul_new == NULL) {
			ERROR("Out of memory: Unable to alloc get_bul command answer");
			free(bul);
			goto out;
		}
		bul = bul_new;

		memcpy(&bul[bnum].dest, &b->dest, sizeof(struct in6_addr));
		memcpy(&bul[bnum].hoa, &b->hoa, sizeof(struct in6_addr));
		memcpy(&bul[bnum].coa, &b->coa, sizeof(struct in6_addr));
		bul[bnum].lifetime = htons(b->lifetime);
		bul[bnum].lifetime_rem = htons(b->lifetime_rem);
		bul[bnum].last_bu = htonl((int32_t)b->last_bu);
		bul[bnum].flags = htonl(b->flags);
		bul[bnum].ifindex = htonl((uint32_t)b->ifindex);
		bnum++;
	}

out:

	pthread_mutex_unlock(&data_bul_mutex);

	if (bnum == 0)
		return 0;

	if (reply_msg != NULL && mlen > 0) {
		if (mlen > bnum * sizeof(struct data_api_get_bul))
			mlen = bnum * sizeof(struct data_api_get_bul);
		memcpy(reply_msg, (unsigned char *)bul, mlen);

		return bnum * sizeof(struct data_api_get_bul);
	}
	
	return -1;
}

/*!
 * \brief Iterate Binding Update List entries
 *
 * The given callback function will be called for all of them.
 * If The callback function returns negative, the iteration
 * will be stopped.
 * \param fp Callback
 * \param arg Extra argument for callback
 * \param include_to_delete If non-zero dO not skip the BUL entries with BULE_F_TO_DELETE flags
 * \return Return value of the last callback function
 */
int bule_iterate(int (*fp)(struct bule *, void *), void * arg, unsigned char include_to_delete)
{
	int ret = 0;
	struct list_head * pos;
	struct bule * b;

	if (fp == NULL)
		return -1;

	pthread_mutex_lock(&data_bul_mutex);
	list_for_each(pos, &data_bul) {
		b = list_entry(pos, struct bule, list);

		refcnt_get(&b->refcnt);
		bule_lock(b);
		if (include_to_delete == 0 && ((b->flags & BULE_F_TO_DELETE) == BULE_F_TO_DELETE)) {
			bule_unlock(b);
			refcnt_put(&b->refcnt);
			continue;
		}
		bule_unlock(b);
		ret = fp(b, arg);
		refcnt_put(&b->refcnt);
		if (ret < 0)
			break;
	}

	pthread_mutex_unlock(&data_bul_mutex);

	return ret;
}

/*! \} */
