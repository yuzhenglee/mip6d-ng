
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
 * \addtogroup iapi-data
 * \{
 */

#ifndef MIP6D_NG_DATA_BCE_H_
#define MIP6D_NG_DATA_BCE_H_

#include <config.h>
#include <inttypes.h>
#include <pthread.h>
#include <netinet/in.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/refcnt.h>
#include <mip6d-ng/logger.h>

/*! BCE flag: H */
#define BCE_F_H			0x0001
/*! BCE flag: A */
#define BCE_F_A			0x0002
/*! BCE flag: L */
#define BCE_F_L			0x0004
/*! BCE flag: In cache (not only to notify about BU) */
#define BCE_F_INC		0x0008	
/*! BCE flag: deleting is in progress */
#define BCE_F_TO_DELETE	0x8000

/*!
 * \brief Binding Cache Entry
 */
struct bce {
	/*! BCE reference count */
	struct refcnt refcnt;
	/*! Linked list management */
	struct list_head list;
	/*! BCE locking */
	pthread_mutex_t _lock;
	/*! The home address of the mobile node for which this is the Binding Cache entry */
	struct in6_addr hoa;
	/*! The care-of address for the mobile node */
	struct in6_addr coa;
	/*! A lifetime value, indicating the remaining lifetime for this Binding Cache entry */
	uint16_t lifetime;
	/*! Additional state flags */
	uint32_t flags;
	/*! The maximum value of the Sequence Number field received in previous Binding Updates for this home address */
	uint16_t seq;
	/*! Usage information for this Binding Cache entry */
	void * usage;

	/*! Previous CoA (used for update) */
	struct in6_addr prev_coa;
	/*! Source address of BU */
	struct in6_addr src_addr;
	/*! (Netfilter) mark */
	unsigned int mark;
	/*! HoA count: number of BCE entries with the same HoA, updated before deletion or modification */
	unsigned char hoa_cnt;
	/*! Identifier */
	unsigned long id;
	/*! Delete hook ID */
	int del_hook_id;
	/*! Scheduled expiration task */
	unsigned long delete_task;

	/*! Linked list for BCE extensions */
	struct list_head ext;
};

#ifdef LOCK_UNLOCK_DEBUG
#define bce_lock(bce)		do { \
		DEBUG("Locking BCE %lu at %s:%d...", bce->id, __FUNCTION__, __LINE__); \
		pthread_mutex_lock(&bce->_lock); \
	} while(0)

#define bce_unlock(bce)	do { \
		pthread_mutex_unlock(&bce->_lock); \
		DEBUG("Unlocked BCE %lu at %s:%d", bce->id, __FUNCTION__, __LINE__); \
	} while(0)
#else /* LOCK_UNLOCK_DEBUG */
#define bce_lock(bce)		do { \
		pthread_mutex_lock(&bce->_lock); \
	} while(0)

#define bce_unlock(bce)	do { \
		pthread_mutex_unlock(&bce->_lock); \
	} while(0)
#endif /* LOCK_UNLOCK_DEBUG */

#define BCE_EXT_NAME_LEN		20

/*!
 * \brief BCE extension header
 *
 * It should be the first field of an extension
 */
struct bce_ext {
	/*! Linked list for BCE extension */
	struct list_head list;
	/*! Name of the extension */
	char name[BCE_EXT_NAME_LEN];
};

/*!
 * \brief BCE matching
 * 
 * Used to matching BCE by other modules
 */
struct bce_ext_match {
	/*! In cache BCE to compare */
	struct bce * bce;
	/*! New BCE to compare */
	struct bce * new_bce;
};

/*!
 * \brief BCE updating
 *
 * Used to update BCE by other modules
 */
struct bce_ext_update {
	/*! In cache BCE to update */
	struct bce * bce;
	/*! New BCE to get new values */
	struct bce * new_bce;
};

#endif /* MIP6D_NG_DATA_BCE_H_ */

/*! \} */

