
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-data
 * \{
 */

#ifndef MIP6D_NG_DATA_H
#define MIP6D_NG_DATA_H

#include <mip6d-ng/bule.h>
#include <mip6d-ng/bce.h>

/*! 
 * \brief The name of the internal messaging reference point
 */
#define MSG_DATA			"data"

/*!
 * \brief Module load order number of data module
 */
#define SEQ_DATA			40

/*!
 * \brief Available data API message commands
 */
enum data_messages {
	/*!
	 * \brief Find BULE
	 *
	 * Argument: struct msg_data_find_bule 
	 *
	 * Reply argument: struct bule
	 */
	MSG_DATA_FIND_BULE,
	/*!
	 * \brief Get the Binding Update list, used by API module
	 *
	 * Argument: struct imsg_arg_reply_len
	 *
	 * Reply argument: unsigned char *
	 */
	MSG_DATA_API_GET_BUL,
	/*!
	 * \brief Iterate BUL entries
	 *
	 * Argument: msg_data_iterate_bul
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_DATA_ITERATE_BUL,
	/*!
	 * \brief Clear all of the BUL entries
	 *
	 * Argument: none (it could to be NULL)
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_DATA_CLEAR_BUL,
	/*!
	 * \brief Initialize a new Binding Cache entry
	 *
	 * Argument: struct bce
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_DATA_INIT_NEW_BCE,
	/*!
	 * \brief Get the Binding Cache, used by API module
	 *
	 * Argument: struct imsg_arg_reply_len
	 *
	 * Reply argument: unsigned char *
	 */
	MSG_DATA_API_GET_BC,
	/*!
	 * \brief Iterate BC entries
	 *
	 * Argument: msg_data_iterate_bc
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_DATA_ITERATE_BC,
	/*!
	 * \brief Clear all of the BC entries
	 *
	 * Argument: none (it could to be NULL)
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_DATA_CLEAR_BC,
	MSG_DATA_NONE
};

/*!
 * \brief Find BUL entry
 *
 * \see data_bule_find
 */
struct msg_data_find_bule {
	/*! Interface index or zero */
	unsigned int ifindex;
	/*! Destination (i.e. Home Agent address) */
	struct in6_addr dest;
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address or set to 00000000... */
	struct in6_addr coa;
};

/*! 
 * \brief Iterate Binding Update List entries
 *
 * \see bule_iterate
 */
struct msg_data_iterate_bul {
	/*! Callback */
	int (* fp)(struct bule *, void *);
	/*! Extra argument for callback */
	void * arg;
	/*! If non-zero dO not skip the BUL entries with BULE_F_TO_DELETE flags */
	unsigned char include_to_delete;
};

/*!
 * \brief Iterate Binding cache entries
 *
 * \see bce_iterate
 */
struct msg_data_iterate_bc {
	/*! Callback */
	int (* fp)(struct bce *, void *);
	/*! Extra argument for callback */
	void * arg;
};

/*!
 * \brief Available data API events
 */
enum data_events {
	/*!
	 * \brief Init new BULE
	 *
	 * Argument: struct bule
	 */
	EVT_DATA_INIT_BULE,
	/*!
	 * \brief Change (update) BULE
	 *
	 * Argument: struct bule
	 */
	EVT_DATA_CHANGE_BULE,
	/*!
	 * \brief Adding new BCE to the Binding Cache
	 *
	 * Argument: struct bce
	 */
	EVT_DATA_NEW_BCE,
	/*!
	 * \brief Change (update) BCE
	 *
	 * Argument: struct bce
	 */
	EVT_DATA_CHANGE_BCE,
	EVT_DATA_NONE
};

/*!
 * \brief Available data API commands by the api module 
 * \ingroup eapi-data
 */
enum data_api_commands {
	/*! Getting Binding Update List entries */
	API_DATA_GET_BUL,
	/*! Getting Binding Cache entries */
	API_DATA_GET_BC,
	API_DATA_NONE
};

/*!
 * \brief API commands by the api module: get BUL
 * \ingroup eapi-data
 */
struct data_api_get_bul {
	/*! Destination (i.e. Home Agent address) */
	struct in6_addr dest;
	/*! Home Address */
	struct in6_addr hoa;
	/*! Cate-of Address */
	struct in6_addr coa;
	/*! Lifetime */
	uint16_t lifetime;
	/*! Remaining lifetime */
	uint16_t lifetime_rem;
	/*! Receiving time of the last BU */
	int32_t last_bu;
	/*! Flags */
	uint32_t flags;
	/*! Interface index */
	uint32_t ifindex;
} __attribute__((packed));

/*!
 * \brief API commands by the api module: get BC
 * \ingroup eapi-data
 */
struct data_api_get_bc {
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! Lifetime */
	uint16_t lifetime;
	/*! Flags */
	uint32_t flags;
} __attribute__((packed));

#endif /* MIP6D_NG_DATA_H */

/*! \} */
