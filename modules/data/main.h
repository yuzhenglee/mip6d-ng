
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
 * \addtogroup internal-data
 * \{
 */

#ifndef MIP6D_NG_DATA_MAIN_H_
#define MIP6D_NG_DATA_MAIN_H_

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
struct data_opts {
	/*! Messaging IDs */
	struct {
		/*! Data module's own ID */
		int me;
		/*! Messaging ID of control module */
		int ctrl;
		/*! Messaging ID of md-simple module */
		int mds;
		/*! Messaging ID of core-comm module */
		int ccom;
		/*! Messaging ID of api module */
		int api;
	} msg_ids;
};

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
extern struct data_opts data_opts;

#endif /* MIP6D_NG_DATA_MAIN_H_ */

/*! \} */
