
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <net/if.h>
#include <arpa/inet.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>

#include "main.h"
#include "addr.h"

int env_test_addr(struct test_options * test_options, int cmd)
{
	int ret;
	unsigned int i;
	struct msg_env_addr a;
	
	if (test_options->node == ENV_TEST_NODE_MR) {
		for (i = 0; i < test_options->num; i++) {
			memset(&a, 0, sizeof(a));
			memcpy(&a.addr, &test_options->hoa, sizeof(a.addr));
			a.plen = 128;
			a.ifindex = test_options->ifindicies[i];
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
					&a, sizeof(a), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do address: " IP6ADDR_FMT " on interface %d",
						IP6ADDR_TO_STR(&test_options->hoa), test_options->ifindicies[i]);
				return -1;
			}
			DEBUG("ENV TEST: Home address " IP6ADDR_FMT "/128 on %d", 
					IP6ADDR_TO_STR(&test_options->hoa), test_options->ifindicies[i]);
		}
	} else {
		memset(&a, 0, sizeof(a));
		memcpy(&a.addr, &test_options->ha, sizeof(a.addr));
		a.plen = 128;
		a.ifindex = test_options->ha_ifindex;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
				&a, sizeof(a), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do address: " IP6ADDR_FMT " on interface %d",
					IP6ADDR_TO_STR(&test_options->ha), test_options->ha_ifindex);
			return -1;
		}
		DEBUG("ENV TEST: Home agent address " IP6ADDR_FMT "/128 on %d", 
				IP6ADDR_TO_STR(&test_options->ha), test_options->ha_ifindex);
	}

	return 0;
}

