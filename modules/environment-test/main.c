
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <confuse.h>
#include <getopt.h>

#include <net/if.h>
#include <arpa/inet.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/cfg.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>

#include "main.h"
#include "addr.h"
#include "route.h"
#include "rule.h"
#include "nf.h"
#include "xfrm.h"
#include "mh.h"

cfg_opt_t env_test[] = {
	CFG_STR("node", "", CFGF_NONE),
	CFG_STR("home-address", "", CFGF_NONE),
	CFG_STR("home-agent-address", "", CFGF_NONE),
	CFG_STR_LIST("care-of-addresses", "", CFGF_NONE),
	CFG_INT_LIST("bids", "", CFGF_NONE),
	CFG_STR_LIST("interfaces", "", CFGF_NONE),
	CFG_STR_LIST("gateways", "", CFGF_NONE),
	CFG_STR("ha-interface", "", CFGF_NONE),
	CFG_STR("auth_name", "", CFGF_NONE),
	CFG_STR("auth_key", "", CFGF_NONE),
	CFG_STR("enc_name", "", CFGF_NONE),
	CFG_STR("enc_key", "", CFGF_NONE),
	CFG_INT("spi", 0, CFGF_NONE),
	CFG_INT("reqid", 0, CFGF_NONE),
	CFG_BOOL("flush", cfg_false, CFGF_NONE),
	CFG_END()
};

cfg_opt_t envT_opts[] = {
	CFG_SEC("environment-test", env_test, CFGF_NONE),
	CFG_END()
};

static struct test_options test_options;

static void processing_event(unsigned int sender, unsigned int event, void * arg, unsigned char need_to_free)
{

}

static int processing_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	return -1;
}

static struct imsg_opts imsg_opts = {
	.event_fp = processing_event,
	.message_fp = processing_message
};

static int exit_hook(void * arg)
{
	int ret;
	
	if (test_options.mode == ENV_TEST_MODE_IPSEC)  {
		ret = env_test_mh_esp(&test_options, MSG_ENV_ESP_TRANSPORT_DEL, MSG_ENV_ESP_TRANSPORT_SA_DEL);
		if (ret)
			return ret;
	}
	ret = env_test_mh(&test_options, MSG_ENV_HAO_DEL, MSG_ENV_RT2_DEL);
	if (ret)
		return ret;
	if (test_options.mode == ENV_TEST_MODE_IP6IP6)
		ret = env_test_xfrm_ip6ip6(&test_options, MSG_ENV_IP6IP6_TUNNEL_DEL, 0);
	else
		ret = env_test_xfrm_esp(&test_options, MSG_ENV_ESP_TUNNEL_DEL, MSG_ENV_ESP_TUNNEL_SA_DEL, 0);
	if (ret)
		return ret;
	ret = env_test_nf(&test_options, MSG_ENV_NF_TO_MIP6D_NG_DEL, MSG_ENV_NF_MARK_AND_ACC_DEL, 0);
	if (ret)
		return ret;
	ret = env_test_rule_and_route(&test_options, MSG_ENV_RULE_DEL, MSG_ENV_ROUTE_DEL);
	if (ret)
		return ret;
	ret = env_test_route(&test_options, MSG_ENV_ROUTE_DEL);
	if (ret)
		return ret;
	ret = env_test_addr(&test_options, MSG_ENV_ADDR_DEL);

	return ret;
}

static int env_test_do()
{
	int ret;
	
	/* Adding Home Address or Home Agent Adress */
	ret = env_test_addr(&test_options, MSG_ENV_ADDR);
	if (ret)
		return ret;

	/* Adding (default) routes */
	ret = env_test_route(&test_options, MSG_ENV_ROUTE_APPEND);
	if (ret)
		return ret;

	/* Adding rules and routes for them */
	ret = env_test_rule_and_route(&test_options, MSG_ENV_RULE, MSG_ENV_ROUTE);
	if (ret)
		return ret;

	/* Adding ip6tables rules */
	ret = env_test_nf(&test_options, MSG_ENV_NF_TO_MIP6D_NG, MSG_ENV_NF_MARK_AND_ACC, 1);
	if (ret)
		return ret;

	/* Adding XFRM rules */
	if (test_options.mode == ENV_TEST_MODE_IP6IP6) 
		ret = env_test_xfrm_ip6ip6(&test_options, MSG_ENV_IP6IP6_TUNNEL, 1);
	else
		ret = env_test_xfrm_esp(&test_options, MSG_ENV_ESP_TUNNEL, MSG_ENV_ESP_TUNNEL_SA, 1);
	if (ret)
		return ret;

	/* Adding XFRM rules for Binding */
	ret = env_test_mh(&test_options, MSG_ENV_HAO, MSG_ENV_RT2);
	if (ret)
		return ret;

	if (test_options.mode == ENV_TEST_MODE_IPSEC) 
		ret = env_test_mh_esp(&test_options, MSG_ENV_ESP_TRANSPORT, MSG_ENV_ESP_TRANSPORT_SA);

	return ret;
}

int envt_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	cfg_t * opts;
	char * str, * str2, * str3, * str4;
	unsigned int num1, num2, num3, num4;
	unsigned int i, j;
	long int ival;
	cfg_bool_t bval;
	char ifname[IF_NAMESIZE];
	
	DEBUG("Initing environment test module");

	memset(&test_options, 0, sizeof(test_options));

	ret = imsg_register("test", &imsg_opts);
	if (ret < 0) {
		ERROR("Unable to register imsg");
		return -1;
	}
	test_options.imid = (unsigned int)ret;

	ret = imsg_get_id(MSG_ENV);
	if (ret < 0) {
		ERROR("Unable to get imsg id of environment module");
		return -1;
	}
	test_options.envid = (unsigned int)ret;

	opts = cfg_getsec(cfg, "environment-test");
	if (opts == NULL) {
		ERROR("Missing environment-test configuration");
		return -1;
	}

	str = cfg_getstr(opts, "node");
	if (str == NULL) {
		ERROR("Missing node option");
		return -1;
	}
	if (strcmp(str, "MR") == 0) 
		test_options.node = ENV_TEST_NODE_MR;
	else if (strcmp(str, "HA") == 0) 
		test_options.node = ENV_TEST_NODE_HA;
	else {
		ERROR("Invlaid node option: '%s' (MR or HA)", str);
		return -1;
	}

	str =cfg_getstr(opts, "home-address");
	if (str == NULL) {
		ERROR("Missing home-address option");
		return -1;
	}
	ret = inet_pton(AF_INET6, str, &test_options.hoa);
	if (ret != 1) {
		ERROR("Inavalid home-address option: '%s'", str);
		return -1;
	}

	str =cfg_getstr(opts, "home-agent-address");
	if (str == NULL) {
		ERROR("Missing home-agent-address option");
		return -1;
	}
	ret = inet_pton(AF_INET6, str, &test_options.ha);
	if (ret != 1) {
		ERROR("Inavalid home-agent-address option: '%s'", str);
		return -1;
	}

	num1 = cfg_size(opts, "care-of-addresses");
	if (num1 == 0) {
		ERROR("Missing care-of-addresses option");
		return -1;
	}
	num2 = cfg_size(opts, "bids");
	if (num2 == 0) {
		ERROR("Missing bids option");
		return -1;
	}
	num3 = cfg_size(opts, "interfaces");
	if (num3 == 0) {
		ERROR("Missing interfaces option");
		return -1;
	}
	num4 = cfg_size(opts, "gateways");
	if (num4 == 0) {
		ERROR("Missing gateways option");
		return -1;
	}
	if (num2 < num1)
		num1 = num2;
	if (num3 < num1)
		num1 = num3;
	if (num4 < num1)
		num1 = num4;
	if (test_options.coas != NULL)
		free(test_options.coas);
	test_options.coas = malloc(num1 * sizeof(struct in6_addr));
	if (test_options.coas == NULL) {
		ERROR("Unable to allocate coa buffer");
		exit(EXIT_FAILURE);
	}
	if (test_options.bids != NULL)
		free(test_options.bids);
	test_options.bids = malloc(num1 * sizeof(unsigned int));
	if (test_options.bids == NULL) {
		ERROR("Unable to allocate bid buffer");
		exit(EXIT_FAILURE);
	}
	if (test_options.ifindicies != NULL)
		free(test_options.ifindicies);
	test_options.ifindicies = malloc(num1 * sizeof(unsigned int));
	if (test_options.ifindicies == NULL) {
		ERROR("Unable to allocate ifindex buffer");
		exit(EXIT_FAILURE);
	}
	if (test_options.gws != NULL)
		free(test_options.gws);
	test_options.gws = malloc(num1 * sizeof(struct in6_addr));
	if (test_options.gws == NULL) {
		ERROR("Unable to allocate gw buffer");
		exit(EXIT_FAILURE);
	}
	for (i = j = 0; i < num1; i++) {
		str = cfg_getnstr(opts, "care-of-addresses", i);
		ival = cfg_getnint(opts, "bids", i);
		str2 = cfg_getnstr(opts, "interfaces", i);
		str3 = cfg_getnstr(opts, "gateways", i);
		if (str != NULL) {
			ret = inet_pton(AF_INET6, str, &(test_options.coas[j]));
			if (ret == 1) {
				ret = IMSG_MSG_ARGS(test_options.imid, test_options.envid, 
						MSG_ENV_GET_IFINDEX, str2, strlen(str2) + 1,
						&(test_options.ifindicies[j]), sizeof(test_options.ifindicies[j]));
				if (ret == 0) {
					ret = inet_pton(AF_INET6, str3, &(test_options.gws[j]));
					if (ret == 1) {
						test_options.bids[j] = (unsigned int)ival;
						j++;
					}
				}
			}
		}
	}
	test_options.num = j;

	if (test_options.node == ENV_TEST_NODE_HA) {
		str = cfg_getstr(opts, "ha-interface");
		if (str == NULL) {
			ERROR("Missing ha-interface option");
			return -1;
		}
		ret = IMSG_MSG_ARGS(test_options.imid, test_options.envid, 
					MSG_ENV_GET_IFINDEX, str, strlen(str) + 1,
					&(test_options.ha_ifindex), sizeof(test_options.ha_ifindex));
		if (ret != 0) {
			ERROR("Invalid ha-interface option: '%s'", str);
			return -1;
		}
	}

	str = cfg_getstr(opts, "auth_name");
	str2 = cfg_getstr(opts, "auth_key");
	str3 = cfg_getstr(opts, "enc_name");
	str4 = cfg_getstr(opts, "enc_key");
	if (str != NULL && strlen(str) > 0 && str2 != NULL && strlen(str2) > 0 &&
		str3 != NULL && strlen(str3) > 0 && str4 != NULL && strlen(str4) > 0) {
		if (test_options.auth_name != NULL)
			free(test_options.auth_name);
		test_options.auth_name = strdup(str);
		if (test_options.auth_name == NULL) {
			ERROR("Unable to clone auth_name string");
			exit(EXIT_FAILURE);
		}

		if (test_options.auth_key != NULL)
			free(test_options.auth_key);
		test_options.auth_key = strdup(str2);
		if (test_options.auth_key == NULL) {
			ERROR("Unable to clone auth_key string");
			exit(EXIT_FAILURE);
		}

		if (test_options.enc_name != NULL)
			free(test_options.enc_name);
		test_options.enc_name = strdup(str3);
		if (test_options.enc_name == NULL) {
			ERROR("Unable to clone enc_name string");
			exit(EXIT_FAILURE);
		}

		if (test_options.enc_key != NULL)
			free(test_options.enc_key);
		test_options.enc_key = strdup(str4);
		if (test_options.enc_key == NULL) {
			ERROR("Unable to clone enc_key string");
			exit(EXIT_FAILURE);
		}

		ival = cfg_getint(opts, "spi");
		test_options.spi = (unsigned int)ival;
		ival = cfg_getint(opts, "reqid");
		test_options.reqid = (unsigned int)ival;
		test_options.mode = ENV_TEST_MODE_IPSEC;
	} else
		test_options.mode = ENV_TEST_MODE_IP6IP6;

	bval = cfg_getbool(opts, "flush");
	if ((int)bval == 1) 
		test_options.flush = 1;
	else
		test_options.flush = 0;

	DEBUG("Node: %s", (test_options.node == ENV_TEST_NODE_MR) ? "MR" : "HA");
	DEBUG("HOA: " IP6ADDR_FMT " HA: " IP6ADDR_FMT, 
			IP6ADDR_TO_STR(&test_options.hoa), IP6ADDR_TO_STR(&test_options.ha));
	for (i = 0; i < test_options.num; i++) {
		ret = IMSG_MSG_ARGS(test_options.imid, test_options.envid, 
				MSG_ENV_GET_IFNAME, (void *)(test_options.ifindicies[i]), sizeof(test_options.ifindicies[i]), 
				ifname, sizeof(ifname));
		if (ret != 0) {
			ERROR("Invalid intreface / ifindex: %d", test_options.ifindicies[i]);
			return -1;
		}
		DEBUG("COA: " IP6ADDR_FMT " BID: %u IF: %s %d via " IP6ADDR_FMT,
				IP6ADDR_TO_STR(&(test_options.coas[i])), test_options.bids[i],
				ifname, test_options.ifindicies[i],
				IP6ADDR_TO_STR(&(test_options.gws[i])));
	}
	if (test_options.node == ENV_TEST_NODE_HA) {
		ret = IMSG_MSG_ARGS(test_options.imid, test_options.envid, 
				MSG_ENV_GET_IFNAME, (void *)(test_options.ha_ifindex), sizeof(test_options.ha_ifindex), 
				ifname, sizeof(ifname));
		if (ret != 0) {
			ERROR("Invalid intreface / ifindex: %d", test_options.ha_ifindex);
			return -1;
		}
		DEBUG("HA IF: %s %d", ifname, test_options.ha_ifindex);
	}
	DEBUG("Mode: %s", (test_options.mode == ENV_TEST_MODE_IP6IP6) ? "IP6IP6" : "IPSEC");
	if (test_options.mode == ENV_TEST_MODE_IPSEC) {
		DEBUG("SPI: %u reqid: %u AUTH: '%s' '%s' ENC: '%s' '%s'", 
				test_options.spi, test_options.reqid, test_options.auth_name, test_options.auth_key,
				test_options.enc_name, test_options.enc_key);
	}
	DEBUG("FLUSH: %s", (test_options.flush) ? "yes" : "no");

	ret = hooks_get_id("core-exit");
	if (ret < 0) {
		ERROR("Unable to register for core-exit hook");
		return -1;
	}
	hooks_add((unsigned int)ret, 98, exit_hook);

	return env_test_do();
}

MIP6_MODULE_INIT("envT", 1, envt_module_init, envT_opts, env_test);

