
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MIP6D_NG_ENVT_MAIN_H
#define MIP6D_NG_ENVT_MAIN_H

#define ENV_TEST_NODE_MR			1
#define ENV_TEST_NODE_HA			2

#define ENV_TEST_MODE_IP6IP6		1
#define ENV_TEST_MODE_IPSEC			2

struct test_options {
       unsigned int imid;
       unsigned int envid;
       unsigned int node;
       struct in6_addr hoa;
       struct in6_addr ha;
       struct in6_addr * coas;
       unsigned int * bids;
       unsigned int * ifindicies;
       struct in6_addr * gws;
       unsigned int num;
       unsigned int ha_ifindex;
       unsigned int mode;
       char * auth_name;
       char * auth_key;
       char * enc_name;
       char * enc_key;
       unsigned int spi;
       unsigned int reqid;
       int flush;
};

#endif /* MIP6D_NG_ENVT_MAIN_H */
