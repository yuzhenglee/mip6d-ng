
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <net/if.h>
#include <arpa/inet.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/missing.h>

#include <mip6d-ng/environment.h>

#include "main.h"
#include "mh.h"

int env_test_mh(struct test_options * test_options, int hao_cmd, int rt2_cmd)
{
	int ret;
	struct msg_env_hao hao;
	struct msg_env_rt2 rt2;

	if (test_options->node == ENV_TEST_NODE_MR) {
		memset(&hao, 0, sizeof(hao));
		memcpy(&hao.src, &test_options->hoa, sizeof(hao.src));
		hao.splen = 128;
		memcpy(&hao.dst, &test_options->ha, sizeof(hao.dst));
		hao.dplen = 128;
		memcpy(&hao.coa, &test_options->coas[0], sizeof(hao.coa));
		hao.proto = IPPROTO_MH;
		hao.type = 5;
		hao.dir = XFRM_POLICY_OUT;
		hao.prio = 5;	
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, hao_cmd, 
				&hao, sizeof(hao), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do HAO xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " OUT coa " IP6ADDR_FMT, 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[0]));
			return -1;
		}
		DEBUG("ENV TEST: HAO xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " OUT coa " IP6ADDR_FMT, 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[0]));

		memset(&rt2, 0, sizeof(rt2));
		memcpy(&rt2.src, &test_options->ha, sizeof(rt2.src));
		rt2.splen = 128;
		memcpy(&rt2.dst, &test_options->hoa, sizeof(rt2.dst));
		rt2.dplen = 128;
		memcpy(&rt2.coa, &test_options->coas[0], sizeof(rt2.coa));
		hao.proto = IPPROTO_MH;
		hao.type = 6;
		rt2.dir = XFRM_POLICY_IN;
		rt2.prio = 15;	
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, rt2_cmd, 
				&rt2, sizeof(rt2),NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do RT2 xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN coa " IP6ADDR_FMT, 
					IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]));
			return -1;
		}
		DEBUG("ENV TEST: RT2 xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN coa " IP6ADDR_FMT, 
					IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]));
	} else {
		memset(&hao, 0, sizeof(hao));
		memcpy(&hao.src, &test_options->hoa, sizeof(hao.src));
		hao.splen = 128;
		memcpy(&hao.dst, &test_options->ha, sizeof(hao.dst));
		hao.dplen = 128;
		memcpy(&hao.coa, &test_options->coas[0], sizeof(hao.coa));
		hao.proto = IPPROTO_MH;
		hao.type = 5;
		hao.dir = XFRM_POLICY_IN;
		hao.prio = 5;	
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, hao_cmd, 
				&hao, sizeof(hao), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do HAO xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN coa " IP6ADDR_FMT, 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[0]));
			return -1;
		}
		DEBUG("ENV TEST: HAO xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN coa " IP6ADDR_FMT, 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[0]));

		memset(&rt2, 0, sizeof(rt2));
		memcpy(&rt2.src, &test_options->ha, sizeof(rt2.src));
		rt2.splen = 128;
		memcpy(&rt2.dst, &test_options->hoa, sizeof(rt2.dst));
		rt2.dplen = 128;
		memcpy(&rt2.coa, &test_options->coas[0], sizeof(rt2.coa));
		hao.proto = IPPROTO_MH;
		hao.type = 6;
		rt2.dir = XFRM_POLICY_OUT;
		rt2.prio = 15;	
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, rt2_cmd, 
				&rt2, sizeof(rt2),NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do RT2 xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " OUT coa " IP6ADDR_FMT, 
					IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]));
			return -1;
		}
		DEBUG("ENV TEST: RT2 xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " OUT coa " IP6ADDR_FMT, 
					IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]));
	}
	
	return 0;
}

int env_test_mh_esp(struct test_options * test_options, int pol_cmd, int sa_cmd)
{
	int ret;
	struct msg_env_esp_transport ep;
	struct msg_env_esp_transport_sa eps;

	if (test_options->node == ENV_TEST_NODE_MR) {
		memset(&ep, 0, sizeof(ep));
		memcpy(&ep.saddr, &test_options->hoa, sizeof(ep.saddr));
		ep.splen = 128;
		memcpy(&ep.daddr, &test_options->ha, sizeof(ep.daddr));
		ep.dplen = 128;
		ep.proto = IPPROTO_MH;
		ep.type = 5;
		ep.prio = 5;
		memcpy(&ep.tmpl_src, &test_options->hoa, sizeof(ep.tmpl_src));
		memcpy(&ep.tmpl_dst, &test_options->ha, sizeof(ep.tmpl_dst));
		ep.dir = XFRM_POLICY_OUT;
		ep.tmpl_spi = 100;
		ep.tmpl_reqid = test_options->reqid;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
				&ep, sizeof(ep), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP transport xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " OUT", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha));
			return -1;
		}
		DEBUG("ENV TEST: ESP transport xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " OUT", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha));

		memset(&eps, 0, sizeof(eps));
		memcpy(&eps.saddr, &test_options->hoa, sizeof(eps.saddr));
		memcpy(&eps.daddr, &test_options->ha, sizeof(eps.daddr));
		eps.spi = 100;
		eps.reqid = test_options->reqid;
		strncpy(eps.auth_name, test_options->auth_name, sizeof(eps.auth_name));
		strncpy(eps.auth_key, test_options->auth_key, sizeof(eps.auth_key));
		strncpy(eps.enc_name, test_options->enc_name, sizeof(eps.enc_name));
		strncpy(eps.enc_key, test_options->enc_key, sizeof(eps.enc_key));

		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, sa_cmd, 
				&eps, sizeof(eps), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP transport xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha),
					100, test_options->reqid);
			return -1;
		}
		DEBUG("ENV TEST: ESP transform xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha),
					100, test_options->reqid);

		memset(&ep, 0, sizeof(ep));
		memcpy(&ep.saddr, &test_options->ha, sizeof(ep.saddr));
		ep.splen = 128;
		memcpy(&ep.daddr, &test_options->hoa, sizeof(ep.daddr));
		ep.dplen = 128;
		ep.proto = IPPROTO_MH;
		ep.type = 6;
		ep.prio = 5;
		memcpy(&ep.tmpl_src, &test_options->ha, sizeof(ep.tmpl_src));
		memcpy(&ep.tmpl_dst, &test_options->hoa, sizeof(ep.tmpl_dst));
		ep.dir = XFRM_POLICY_IN;
		ep.tmpl_spi = 101;
		ep.tmpl_reqid = test_options->reqid;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
				&ep, sizeof(ep), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP transport xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha));
			return -1;
		}
		DEBUG("ENV TEST: ESP transport xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha));

		memset(&eps, 0, sizeof(eps));
		memcpy(&eps.saddr, &test_options->ha, sizeof(eps.saddr));
		memcpy(&eps.daddr, &test_options->hoa, sizeof(eps.daddr));
		eps.spi = 101;
		eps.reqid = test_options->reqid;
		strncpy(eps.auth_name, test_options->auth_name, sizeof(eps.auth_name));
		strncpy(eps.auth_key, test_options->auth_key, sizeof(eps.auth_key));
		strncpy(eps.enc_name, test_options->enc_name, sizeof(eps.enc_name));
		strncpy(eps.enc_key, test_options->enc_key, sizeof(eps.enc_key));

		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, sa_cmd, 
				&eps, sizeof(eps), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP transport xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha),
					101, test_options->reqid);
			return -1;
		}
		DEBUG("ENV TEST: ESP transform xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha),
					101, test_options->reqid);
	} else {
		memset(&ep, 0, sizeof(ep));
		memcpy(&ep.saddr, &test_options->hoa, sizeof(ep.saddr));
		ep.splen = 128;
		memcpy(&ep.daddr, &test_options->ha, sizeof(ep.daddr));
		ep.dplen = 128;
		ep.proto = IPPROTO_MH;
		ep.type = 5;
		ep.prio = 5;
		memcpy(&ep.tmpl_src, &test_options->hoa, sizeof(ep.tmpl_src));
		memcpy(&ep.tmpl_dst, &test_options->ha, sizeof(ep.tmpl_dst));
		ep.dir = XFRM_POLICY_IN;
		ep.tmpl_spi = 100;
		ep.tmpl_reqid = test_options->reqid;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
				&ep, sizeof(ep), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP transport xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha));
			return -1;
		}
		DEBUG("ENV TEST: ESP transport xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha));

		memset(&eps, 0, sizeof(eps));
		memcpy(&eps.saddr, &test_options->hoa, sizeof(eps.saddr));
		memcpy(&eps.daddr, &test_options->ha, sizeof(eps.daddr));
		eps.spi = 100;
		eps.reqid = test_options->reqid;
		strncpy(eps.auth_name, test_options->auth_name, sizeof(eps.auth_name));
		strncpy(eps.auth_key, test_options->auth_key, sizeof(eps.auth_key));
		strncpy(eps.enc_name, test_options->enc_name, sizeof(eps.enc_name));
		strncpy(eps.enc_key, test_options->enc_key, sizeof(eps.enc_key));

		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, sa_cmd, 
				&eps, sizeof(eps), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP transport xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha),
					100, test_options->reqid);
			return -1;
		}
		DEBUG("ENV TEST: ESP transform xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha),
					100, test_options->reqid);

		memset(&ep, 0, sizeof(ep));
		memcpy(&ep.saddr, &test_options->ha, sizeof(ep.saddr));
		ep.splen = 128;
		memcpy(&ep.daddr, &test_options->hoa, sizeof(ep.daddr));
		ep.dplen = 128;
		ep.proto = IPPROTO_MH;
		ep.type = 6;
		ep.prio = 5;
		memcpy(&ep.tmpl_src, &test_options->ha, sizeof(ep.tmpl_src));
		memcpy(&ep.tmpl_dst, &test_options->hoa, sizeof(ep.tmpl_dst));
		ep.dir = XFRM_POLICY_OUT;
		ep.tmpl_spi = 101;
		ep.tmpl_reqid = test_options->reqid;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
				&ep, sizeof(ep), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP transport xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " OUT", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha));
			return -1;
		}
		DEBUG("ENV TEST: ESP transport xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " OUT", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha));

		memset(&eps, 0, sizeof(eps));
		memcpy(&eps.saddr, &test_options->ha, sizeof(eps.saddr));
		memcpy(&eps.daddr, &test_options->hoa, sizeof(eps.daddr));
		eps.spi = 101;
		eps.reqid = test_options->reqid;
		strncpy(eps.auth_name, test_options->auth_name, sizeof(eps.auth_name));
		strncpy(eps.auth_key, test_options->auth_key, sizeof(eps.auth_key));
		strncpy(eps.enc_name, test_options->enc_name, sizeof(eps.enc_name));
		strncpy(eps.enc_key, test_options->enc_key, sizeof(eps.enc_key));

		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, sa_cmd, 
				&eps, sizeof(eps), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP transport xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha),
					101, test_options->reqid);
			return -1;
		}
		DEBUG("ENV TEST: ESP transform xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha),
					101, test_options->reqid);
	}

	return 0;
}
