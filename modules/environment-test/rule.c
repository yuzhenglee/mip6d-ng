
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <net/if.h>
#include <arpa/inet.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/misc.h>

#include <linux/rtnetlink.h>

#include <mip6d-ng/environment.h>

#include "main.h"
#include "rule.h"

int env_test_rule_and_route(struct test_options * test_options, int rule_cmd, int route_cmd)
{
	int ret;
	unsigned int i;
	struct msg_env_rule ru;
	struct msg_env_route r;

	if (test_options->node == ENV_TEST_NODE_MR) {
		for (i = 0; i < test_options->num; i++) {
			memset(&ru, 0, sizeof(ru));
			memcpy(&ru.src, &test_options->coas[i], sizeof(ru.src));
			ru.splen = 128;
			memcpy(&ru.dst, &test_options->ha, sizeof(ru.dst));
			ru.dplen = 128;
			ru.table = test_options->bids[i];
			ru.entries |= MSG_ENV_RULE_SRC;
			ru.action = RTN_UNICAST;
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, rule_cmd,
					&ru, sizeof(ru), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do rule: from " IP6ADDR_FMT " to " IP6ADDR_FMT " lookup %d",
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha), 
						test_options->bids[i]);
				return -1;
			}
			DEBUG("ENV TEST: Rule from " IP6ADDR_FMT " to " IP6ADDR_FMT " lookup %d",
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha), 
						test_options->bids[i]);

			memset(&r, 0, sizeof(r));
			memcpy(&r.dst, &test_options->ha, sizeof(r.dst));
			r.dplen = 128;
			memcpy(&r.gw, &test_options->gws[i], sizeof(r.gw));
			r.entries |= MSG_ENV_ROUTE_GW;
			r.table = test_options->bids[i];
			r.oiface = test_options->ifindicies[i];
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, route_cmd, 
					&r, sizeof(r), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do route: default via " IP6ADDR_FMT " dev %d table %d",
						IP6ADDR_TO_STR(&test_options->gws[i]), test_options->ifindicies[i],
						test_options->bids[i]);
				return -1;
			}
			DEBUG("ENV TEST: Default route via " IP6ADDR_FMT " dev %d", 
					IP6ADDR_TO_STR(&test_options->gws[i]), test_options->ifindicies[i],
					test_options->bids[i]);
		}
	}

	return 0;
}

