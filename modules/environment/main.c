
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-environment
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <net/if.h>
#include <netinet/in.h>
#include <linux/xfrm.h>
#include <sys/ioctl.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/missing.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>

#include "xfrm.h"
#include "rtnl.h"
#ifdef FEATURE_IPTABLES
#include "iptables.h"
#endif
#include "main.h"

#ifdef ENVIRONMENT_DEBUG
#define ENV_DEBUG(...)			DEBUG(__VA_ARGS__)
#else
#define ENV_DEBUG(...)
#endif

struct env_opts env_opts;

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the environment module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int env_proc_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	int retval = -1;

	struct msg_env_hao * hao;
	struct msg_env_rt2 * rt2;
	struct msg_env_ip6ip6_tunnel * i6t;
	struct msg_env_esp_tunnel * et;
	struct msg_env_esp_tunnel_sa * es;
	struct msg_env_esp_transport * ep;
	struct msg_env_esp_transport_sa * eps;
	struct msg_env_addr * a;
	struct msg_env_addr_get_reply * gar;
	struct msg_env_route * r;
	struct msg_env_route_get * gr;
	struct msg_env_route_get_reply * grr;
	struct msg_env_rule * ru;
	struct msg_env_neigh * n;
	struct msg_env_neigh_proxy * np;
	struct msg_env_get_link_reply * glr;
	struct msg_env_nf_to_mip6d_ng * nf2;
	struct msg_env_nf_jump * nfj;
	struct msg_env_nf_chain * nfc;
	struct msg_env_nf_mark_and_acc * nfm;
	struct msg_env_nf_flush * nff;
	struct msg_env_misc_proc_sys * ps;
	char * str;
	unsigned int unum;
	int sock;
	struct ifreq ifr;
	char ifname[IF_NAMESIZE];
	char chainname[35];
	FILE * psfp;
	char pspath[100 + IF_NAMESIZE];

	int used;

	struct xfrm_selector sel;
	struct xfrm_user_tmpl tmpl;
	struct xfrm_mark mark;
	struct in6_addr * src;
	struct in6_addr * dst;
	struct in6_addr * gw;

	struct in6_addr empty_addr;
	uint8_t flags;

	switch(message) {
	case MSG_ENV_HAO:
		hao = (struct msg_env_hao *)arg;
		if (hao == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&hao->dst, hao->dplen,
						  &hao->src, hao->splen,
						  hao->proto, hao->type, 0, hao->ifindex,
						  &sel);

		xfrm_set_tmpl_dstopt(&hao->dst, &hao->src, hao->ifindex, &tmpl);

		retval = xfrm_mip_policy_add(&sel, 
				hao->update, hao->dir, XFRM_POLICY_ALLOW, hao->prio,
				&tmpl, 1);

		flags = 0;
		memset(&empty_addr, 0, sizeof(empty_addr));
		if (memcmp(&hao->dst, &empty_addr, sizeof(struct in6_addr)) == 0 && hao->dplen == 0)
			flags = XFRM_STATE_WILDRECV;
		retval |= xfrm_state_add_ro(NULL, &hao->src, &hao->dst, IPPROTO_DSTOPTS, hao->ifindex, &hao->coa, hao->update, flags);
		break;
	case MSG_ENV_HAO_DEL:
		hao = (struct msg_env_hao *)arg;
		if (hao == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&hao->dst, hao->dplen,
						  &hao->src, hao->splen,
						  hao->proto, hao->type, 0, hao->ifindex,
						  &sel);

		retval = xfrm_mip_policy_del(&sel, hao->dir);

		retval |= xfrm_state_del(&hao->src, &hao->dst, IPPROTO_DSTOPTS, hao->ifindex);
		break;
	case MSG_ENV_RT2:
		rt2 = (struct msg_env_rt2 *)arg;
		if (rt2 == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&rt2->dst, rt2->dplen,
						  &rt2->src, rt2->splen,
						  rt2->proto, rt2->type, 0, rt2->ifindex,
						  &sel);

		xfrm_set_tmpl_rh(rt2->ifindex, &tmpl);

		retval = xfrm_mip_policy_add(&sel, 
				rt2->update, rt2->dir, XFRM_POLICY_ALLOW, rt2->prio,
				&tmpl, 1);
		flags = 0;
		memset(&empty_addr, 0, sizeof(empty_addr));
		if (memcmp(&rt2->dst, &empty_addr, sizeof(struct in6_addr)) == 0 && rt2->dplen == 0)
			flags = XFRM_STATE_WILDRECV;
		if (memcmp(&rt2->coa, &empty_addr, sizeof(struct in6_addr)) == 0)
			flags = XFRM_STATE_WILDRECV;
		retval |= xfrm_state_add_ro(NULL, &rt2->src, &rt2->dst, IPPROTO_ROUTING, rt2->ifindex, &rt2->coa, rt2->update, flags);
		break;
	case MSG_ENV_RT2_DEL:
		rt2 = (struct msg_env_rt2 *)arg;
		if (rt2 == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&rt2->dst, rt2->dplen,
						  &rt2->src, rt2->splen,
						  rt2->proto, rt2->type, 0, rt2->ifindex,
						  &sel);
		
		retval = xfrm_mip_policy_del(&sel, rt2->dir);

		retval |= xfrm_state_del(&rt2->src, &rt2->dst, IPPROTO_ROUTING, rt2->ifindex);
		break;
	case MSG_ENV_IP6IP6_TUNNEL:
		i6t = (struct msg_env_ip6ip6_tunnel *)arg;
		if (i6t == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&i6t->daddr, i6t->dplen,
						  &i6t->saddr, i6t->splen,
						  i6t->proto, i6t->type, i6t->code, i6t->ifindex,
						  &sel);
		xfrm_set_tmpl_ipsec(&i6t->tmpl_dst, &i6t->tmpl_src,
							IPPROTO_IPV6, 1, 0, 0,
							&tmpl);
		if (i6t->mark > 0)
			xfrm_set_mark(i6t->mark, 0xffffffff, &mark);

		retval = xfrm_ipsec_policy_add(&sel, 
				i6t->update, i6t->dir, XFRM_POLICY_ALLOW, i6t->prio,
				&tmpl, 1, (i6t->mark > 0) ? &mark : NULL);

		if (i6t->skip_state == 0) {
			retval |= xfrm_state_add_tunnel(NULL, &i6t->tmpl_src, &i6t->tmpl_dst, IPPROTO_IPV6, NULL, NULL, NULL, NULL, 0, 0, i6t->update, 0);
		}
		break;
	case MSG_ENV_IP6IP6_TUNNEL_DEL:
		i6t = (struct msg_env_ip6ip6_tunnel *)arg;
		if (i6t == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&i6t->daddr, i6t->dplen,
						  &i6t->saddr, i6t->splen,
						  i6t->proto, i6t->type, i6t->code, i6t->ifindex,
						  &sel);
		
		if (i6t->mark > 0)
			xfrm_set_mark(i6t->mark, 0xffffffff, &mark);

		retval = xfrm_ipsec_policy_del(&sel, i6t->dir, (i6t->mark > 0) ? &mark : NULL);

		if (i6t->skip_state == 0) {
			retval |= xfrm_state_del(&i6t->tmpl_src, &i6t->tmpl_dst, IPPROTO_IPV6, 0);
		}
		break;
	case MSG_ENV_ESP_TUNNEL:
		et = (struct msg_env_esp_tunnel *)arg;
		if (et == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&et->daddr, et->dplen,
						  &et->saddr, et->splen,
						  et->proto, et->type, et->code, et->ifindex,
						  &sel);
		xfrm_set_tmpl_ipsec(&et->tmpl_dst, &et->tmpl_src,
							IPPROTO_ESP, 1, et->tmpl_spi, et->tmpl_reqid,
							&tmpl);
		if (et->mark > 0)
			xfrm_set_mark(et->mark, 0xffffffff, &mark);

		retval = xfrm_ipsec_policy_add(&sel, 
				et->update, et->dir, XFRM_POLICY_ALLOW, et->prio,
				&tmpl, 1, (et->mark > 0) ? &mark : NULL);
		break;
	case MSG_ENV_ESP_TUNNEL_DEL:
		et = (struct msg_env_esp_tunnel *)arg;
		if (et == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&et->daddr, et->dplen,
						  &et->saddr, et->splen,
						  et->proto, et->type, et->code, et->ifindex,
						  &sel);
	
		if (et->mark > 0)
			xfrm_set_mark(et->mark, 0xffffffff, &mark);

		retval = xfrm_ipsec_policy_del(&sel, et->dir, (et->mark > 0) ? &mark : NULL);
		break;
	case MSG_ENV_ESP_TRANSPORT:
		ep = (struct msg_env_esp_transport *)arg;
		if (ep == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&ep->daddr, ep->dplen,
						  &ep->saddr, ep->splen,
						  ep->proto, ep->type, ep->code, ep->ifindex,
						  &sel);
		xfrm_set_tmpl_ipsec(&ep->tmpl_dst, &ep->tmpl_src,
							IPPROTO_ESP, 0, ep->tmpl_spi, ep->tmpl_reqid,
							&tmpl);
		if (ep->mark > 0)
			xfrm_set_mark(ep->mark, 0xffffffff, &mark);

		retval = xfrm_ipsec_policy_add(&sel, 
				ep->update, ep->dir, XFRM_POLICY_ALLOW, ep->prio,
				&tmpl, 1, (ep->mark > 0) ? &mark : NULL);
		break;
	case MSG_ENV_ESP_TRANSPORT_DEL:
		ep = (struct msg_env_esp_transport *)arg;
		if (ep == NULL) {
			retval = -1;
			break;
		}
		
		xfrm_set_selector(&ep->daddr, ep->dplen,
						  &ep->saddr, ep->splen,
						  ep->proto, ep->type, ep->code, ep->ifindex,
						  &sel);
	
		if (ep->mark > 0)
			xfrm_set_mark(ep->mark, 0xffffffff, &mark);

		retval = xfrm_ipsec_policy_del(&sel, ep->dir, (ep->mark > 0) ? &mark : NULL);
		break;
	case MSG_ENV_ESP_TUNNEL_SA:
		es = (struct msg_env_esp_tunnel_sa *)arg;
		if (es == NULL) {
			retval = -1;
			break;
		}
		
		retval = xfrm_state_add_tunnel(NULL, &es->saddr, &es->daddr, IPPROTO_ESP,
				es->auth_name, es->auth_key, es->enc_name, es->enc_key, 
				es->spi, es->reqid, es->update, 0);
		break;
	case MSG_ENV_ESP_TUNNEL_SA_DEL:
		es = (struct msg_env_esp_tunnel_sa *)arg;
		if (es == NULL) {
			retval = -1;
			break;
		}
		
		retval = xfrm_state_del(&es->saddr, &es->daddr, IPPROTO_ESP, es->spi);
		break;
	case MSG_ENV_ESP_TRANSPORT_SA:
		eps = (struct msg_env_esp_transport_sa *)arg;
		if (eps == NULL) {
			retval = -1;
			break;
		}
		
		retval = xfrm_state_add_transport(NULL, &eps->saddr, &eps->daddr, IPPROTO_ESP,
				eps->auth_name, eps->auth_key, eps->enc_name, eps->enc_key, 
				eps->spi, eps->reqid, eps->update, 0);
		break;
	case MSG_ENV_ESP_TRANSPORT_SA_DEL:
		eps = (struct msg_env_esp_transport_sa *)arg;
		if (eps == NULL) {
			retval = -1;
			break;
		}
		
		retval = xfrm_state_del(&eps->saddr, &eps->daddr, IPPROTO_ESP, eps->spi);
		break;
	case MSG_ENV_POLICY_FLUSH:
		retval = xfrm_policy_flush(XFRM_POLICY_TYPE_MAIN);
		ENV_DEBUG("XFRM Policy Info: FLUSH (%d)", retval);
		break;
	case MSG_ENV_POLICY_FLUSH_SUB:
		retval = xfrm_policy_flush(XFRM_POLICY_TYPE_SUB);
		ENV_DEBUG("XFRM Policy Info: FLUSH (%d)", retval);
		break;
	case MSG_ENV_SA_FLUSH:
		retval = xfrm_state_flush();
		ENV_DEBUG("XFRM SA Info: FLUSH (%d)", retval);
		break;
	case MSG_ENV_ADDR:
		a = (struct msg_env_addr *)arg;
		if (a == NULL) {
			retval = -1;
			break;
		}

		if (a->scope == 0)
			a->scope = RT_SCOPE_UNIVERSE;

		used = rtnl_addr_isused(&a->addr, a->plen, a->scope, a->ifindex);
		if (used != 1) {
			retval = rtnl_addr_add(&a->addr, a->plen, a->flags, 
					a->scope, a->ifindex, 0, 0);
		} else
			retval = 0;
		ENV_DEBUG("Address INFO: " IP6ADDR_FMT "/%d ifindex %d flags %02X scope %u (%d %d)",
				IP6ADDR_TO_STR(&a->addr), a->plen, a->ifindex,
				a->flags, a->scope, used, retval);
		break;
	case MSG_ENV_ADDR_DEL:
		a = (struct msg_env_addr *)arg;
		if (a == NULL) {
			retval = -1;
			break;
		}

		retval = rtnl_addr_del(&a->addr, a->plen, a->ifindex);
		ENV_DEBUG("Address INFO: DELETE " IP6ADDR_FMT "/%d ifindex %d (%d)",
				IP6ADDR_TO_STR(&a->addr), a->plen, a->ifindex, retval);
		break;
	case MSG_ENV_ADDR_GET:
		if (arg == NULL) {
			retval = -1;
			break;
		}
		unum = *((unsigned int *)arg);
		if (reply_arg == NULL) {
			retval = -1;
			break;
		}
		gar = (struct msg_env_addr_get_reply *)reply_arg;

		retval = rtnl_addr_get(unum, gar);
		ENV_DEBUG("Address INFO: GET '%u' (%d)", unum, retval);
		break;
	case MSG_ENV_ADDR_FLUSH_HOME_ADDR:
		retval = rtnl_addr_flush_home_addr();
		ENV_DEBUG("Address INFO: FLUSH home addr (%d)", retval);
		break;
	case MSG_ENV_ROUTE:
		r = (struct msg_env_route *)arg;
		if (r == NULL) {
			retval = -1;
			break;
		}

		if ((r->entries & MSG_ENV_ROUTE_SRC) == MSG_ENV_ROUTE_SRC)
			src = &r->src;
		else
			src = NULL;
		dst = &r->dst;
		if ((r->entries & MSG_ENV_ROUTE_GW) == MSG_ENV_ROUTE_GW)
			gw = &r->gw;
		else
			gw = NULL;
		if ((r->entries & MSG_ENV_ROUTE_METRIC) != MSG_ENV_ROUTE_METRIC)
			r->metric = 0;

		used = rtnl_route_isused(r->oiface, r->table, r->metric,
				src, r->splen, dst, r->dplen, gw);
		if (used != 1) {
			retval = rtnl_route_add(r->oiface, r->table, RTPROT_BOOT,
					r->flags, r->metric,
					src, r->splen, dst, r->dplen,
					gw);
		} else
			retval = 0;
		ENV_DEBUG("Route INFO: " IP6ADDR_FMT "/%d " IP6ADDR_FMT "/%d via " IP6ADDR_FMT " metric %d flags %02X dev %d (%d %d)",
				IP6ADDR_TO_STR(&r->src), r->splen, IP6ADDR_TO_STR(&r->dst), r->dplen,
				IP6ADDR_TO_STR(&r->gw), r->metric, r->flags, r->oiface, used, retval);
		break;
	case MSG_ENV_ROUTE_APPEND:
		r = (struct msg_env_route *)arg;
		if (r == NULL) {
			retval = -1;
			break;
		}

		if ((r->entries & MSG_ENV_ROUTE_SRC) == MSG_ENV_ROUTE_SRC)
			src = &r->src;
		else
			src = NULL;
		dst = &r->dst;
		if ((r->entries & MSG_ENV_ROUTE_GW) == MSG_ENV_ROUTE_GW)
			gw = &r->gw;
		else
			gw = NULL;
		if ((r->entries & MSG_ENV_ROUTE_METRIC) != MSG_ENV_ROUTE_METRIC)
			r->metric = 0;

		used = rtnl_route_isused(r->oiface, r->table, r->metric,
				src, r->splen, dst, r->dplen, gw);
		if (used != 1) {
			retval = rtnl_route_append(r->oiface, r->table, RTPROT_BOOT,
					r->flags, r->metric,
					src, r->splen, dst, r->dplen,
					gw);
		} else
			retval = 0;

		ENV_DEBUG("Route INFO: APPEND " IP6ADDR_FMT "/%d " IP6ADDR_FMT "/%d via " IP6ADDR_FMT " metric %d flags %02X dev %d (%d %d)",
				IP6ADDR_TO_STR(&r->src), r->splen, IP6ADDR_TO_STR(&r->dst), r->dplen,
				IP6ADDR_TO_STR(&r->gw), r->metric, r->flags, r->oiface, used, retval);
		break;
	case MSG_ENV_ROUTE_DEL:
		r = (struct msg_env_route *)arg;
		if (r == NULL) {
			retval = -1;
			break;
		}

		if ((r->entries & MSG_ENV_ROUTE_SRC) == MSG_ENV_ROUTE_SRC)
			src = &r->src;
		else
			src = NULL;
		dst = &r->dst;
		if ((r->entries & MSG_ENV_ROUTE_GW) == MSG_ENV_ROUTE_GW)
			gw = &r->gw;
		else
			gw = NULL;
		if ((r->entries & MSG_ENV_ROUTE_METRIC) != MSG_ENV_ROUTE_METRIC)
			r->metric = 0;

		retval = rtnl_route_del(r->oiface, r->table, r->metric,
				src, r->splen, dst, r->dplen,
				gw);
		ENV_DEBUG("Route INFO: DELETE " IP6ADDR_FMT "/%d " IP6ADDR_FMT "/%d via " IP6ADDR_FMT " metric %d dev %d (%d)",
				IP6ADDR_TO_STR(&r->src), r->splen, IP6ADDR_TO_STR(&r->dst), r->dplen,
				IP6ADDR_TO_STR(&r->gw), r->metric, r->oiface, retval);
		break;
	case MSG_ENV_ROUTE_GET:
		if (arg == NULL) {
			retval = -1;
			break;
		}
		gr = (struct msg_env_route_get *)arg;
		if (reply_arg == NULL) {
			retval = -1;
			break;
		}
		grr = (struct msg_env_route_get_reply *)reply_arg;

		retval = rtnl_route_get(gr->oiface, gr->table, &gr->dst, gr->dplen, grr);
		ENV_DEBUG("Route INFO: GET '%u' (%d)", gr->oiface, retval);
		break;
	case MSG_ENV_RULE:
		ru = (struct msg_env_rule *)arg;
		if (ru == NULL) {
			retval = -1;
			break;
		}

		if ((ru->entries & MSG_ENV_RULE_SRC) == MSG_ENV_RULE_SRC)
			src = &ru->src;
		else
			src = NULL;
		dst = &ru->dst;
		if ((ru->entries & MSG_ENV_RULE_PRIO) != MSG_ENV_RULE_PRIO)
			ru->priority = 0;
		if ((ru->entries & MSG_ENV_RULE_FWMARK) != MSG_ENV_RULE_FWMARK)
			ru->fwmark = 0;
		if ((ru->entries & MSG_ENV_RULE_IF) != MSG_ENV_RULE_IF)
			ru->iface = 0;

		used = rtnl_rule_isused(ru->iface, ru->table, ru->priority,
				ru->fwmark, ru->action,
				src, ru->splen, dst, ru->dplen);
		
		if (used != 1) {
			retval = rtnl_rule_add(ru->iface, ru->table, ru->priority,
					ru->fwmark, ru->action,
					src, ru->splen, dst, ru->dplen,
					ru->flags);
		} else
			retval = 0;

		ENV_DEBUG("Rule INFO: " IP6ADDR_FMT "/%d " IP6ADDR_FMT "/%d iface %d lookup %u fwmark %u flags %02X (%d %d)",
				IP6ADDR_TO_STR(&ru->src), ru->splen, IP6ADDR_TO_STR(&ru->dst), ru->dplen,
				ru->iface, ru->table, ru->fwmark, ru->flags, used, retval);
		break;
	case MSG_ENV_RULE_DEL:
		ru = (struct msg_env_rule *)arg;
		if (ru == NULL) {
			retval = -1;
			break;
		}

		if ((ru->entries & MSG_ENV_RULE_SRC) == MSG_ENV_RULE_SRC)
			src = &ru->src;
		else
			src = NULL;
		dst = &ru->dst;
		if ((ru->entries & MSG_ENV_RULE_PRIO) != MSG_ENV_RULE_PRIO)
			ru->priority = 0;
		if ((ru->entries & MSG_ENV_RULE_FWMARK) != MSG_ENV_RULE_FWMARK)
			ru->fwmark = 0;
		if ((ru->entries & MSG_ENV_RULE_IF) != MSG_ENV_RULE_IF)
			ru->iface = 0;

		retval = rtnl_rule_del(ru->iface, ru->table, ru->priority,
				ru->fwmark, ru->action,
				src, ru->splen, dst, ru->dplen,
				ru->flags);
		ENV_DEBUG("Rule INFO: DELETE " IP6ADDR_FMT "/%d " IP6ADDR_FMT "/%d iface %d lookup %u fwmark %u flags %02X (%d)",
				IP6ADDR_TO_STR(&ru->src), ru->splen, IP6ADDR_TO_STR(&ru->dst), ru->dplen,
				ru->iface, ru->table, ru->fwmark, ru->flags, retval);
		break;
	case MSG_ENV_NEIGH:
		n = (struct msg_env_neigh *)arg;
		if (n == NULL) {
			retval = -1;
			break;
		}

		retval = rtnl_neigh_add(n->ifindex, n->state, n->flags, &n->dst, n->hwa, ETH_ALEN, n->override);

		ENV_DEBUG("Neigh INFO: ADD " IP6ADDR_FMT " iface %d state %u flags %02X (%d)",
				IP6ADDR_TO_STR(&n->dst), n->ifindex, n->state, n->flags, retval);
		break;
	case MSG_ENV_NEIGH_DEL:
		n = (struct msg_env_neigh *)arg;
		if (n == NULL) {
			retval = -1;
			break;
		}

		retval = rtnl_neigh_del(n->ifindex, &n->dst);

		ENV_DEBUG("Neigh INFO: DEL " IP6ADDR_FMT " iface %d (%d)",
				IP6ADDR_TO_STR(&n->dst), n->ifindex, retval);
		break;
	case MSG_ENV_NEIGH_PROXY:
		np = (struct msg_env_neigh_proxy *)arg;
		if (np == NULL) {
			retval = -1;
			break;
		}

		retval = rtnl_pneigh_add(np->ifindex, np->flags, &np->dst);

		ENV_DEBUG("Proxy Neigh INFO: ADD " IP6ADDR_FMT " iface %d flags %02X (%d)",
				IP6ADDR_TO_STR(&np->dst), np->ifindex, np->flags, retval);
		break;
	case MSG_ENV_NEIGH_PROXY_DEL:
		np = (struct msg_env_neigh_proxy *)arg;
		if (np == NULL) {
			retval = -1;
			break;
		}

		retval = rtnl_pneigh_del(np->ifindex, &np->dst);

		ENV_DEBUG("Proxy Neigh INFO: DEL " IP6ADDR_FMT " iface %d (%d)",
				IP6ADDR_TO_STR(&np->dst), np->ifindex, retval);
		break;
	case MSG_ENV_GET_IFINDEX:
		str = (char *)arg;
		if (str == NULL) {
			retval = -1;
			break;
		}

		unum = if_nametoindex(str);
		if (unum == 0)
			retval = 1;
		else {
			retval = 0;
			if (reply_arg != NULL)
				*((unsigned int *)reply_arg) = unum;
		}
		ENV_DEBUG("Interface INFO: '%s' -> %d (%d)", str, unum, retval);
		break;
	case MSG_ENV_GET_IFNAME:
		if (arg == NULL) {
			retval = -1;
			break;
		}
		unum = *((unsigned int *)arg);

		str = if_indextoname(unum, ifname);
		if (str == NULL)
			retval = -1;
		else {
			retval = 0;
			if (reply_arg != NULL)
				strcpy((char *)reply_arg, ifname);
		}
		ENV_DEBUG("Interface INFO: '%s' <- %d (%d)", ifname, unum, retval);
		break;
	case MSG_ENV_GET_L2ADDR:
		if (arg == NULL) {
			retval = -1;
			break;
		}
		unum = *((unsigned int *)arg);

		retval = -1;
		sock = socket(PF_PACKET, SOCK_DGRAM, 0);
		if (sock > 0) {
			memset(&ifr, 0, sizeof(ifr));
			if (if_indextoname(unum, ifr.ifr_name) != NULL) {
				retval = ioctl(sock, SIOCGIFHWADDR, &ifr);
			}
		}

		if (retval == 0) {
			if (reply_arg != NULL)
				memcpy((char *)reply_arg, ifr.ifr_hwaddr.sa_data, ETH_ALEN);
		}

		ENV_DEBUG("L2ADDR INFO: '%d' -> %02X:%02X:%02X:%02X:%02X:%02X (%d)", unum, 
				(uint8_t)ifr.ifr_hwaddr.sa_data[0], (uint8_t)ifr.ifr_hwaddr.sa_data[1],
				(uint8_t)ifr.ifr_hwaddr.sa_data[2], (uint8_t)ifr.ifr_hwaddr.sa_data[3],
				(uint8_t)ifr.ifr_hwaddr.sa_data[4], (uint8_t)ifr.ifr_hwaddr.sa_data[5], retval);

		break;
	case MSG_ENV_GET_LINK:
		if (arg == NULL) {
			retval = -1;
			break;
		}
		unum = *((unsigned int *)arg);
		if (reply_arg == NULL) {
			retval = -1;
			break;
		}
		glr = (struct msg_env_get_link_reply *)reply_arg;

		retval = rtnl_link_get(unum, &glr->type, &glr->flags);
		ENV_DEBUG("Interface INFO: '%u' type %02X flags %08X (%d)", unum, glr->type, glr->flags, retval);
		break;
	case MSG_ENV_NF_TO_MIP6D_NG:
		nf2 = (struct msg_env_nf_to_mip6d_ng *)arg;
		if (nf2 == NULL) {
			retval = -1;
			break;
		}

#ifdef FEATURE_IPTABLES
		retval = ip6t_jump_rule("mangle", nf2->chain, &nf2->src, nf2->src_plen, nf2->proto, &nf2->dst, nf2->dst_plen, "MIP6D_NG");

		ENV_DEBUG("NF Info: %s src " IP6ADDR_FMT " dst " IP6ADDR_FMT " jump %s (%d)",
				nf2->chain, IP6ADDR_TO_STR(&nf2->src), IP6ADDR_TO_STR(&nf2->dst), "MIP6D_NG", retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_NF_TO_MIP6D_NG_DEL:
		nf2 = (struct msg_env_nf_to_mip6d_ng *)arg;
		if (nf2 == NULL) {
			retval = -1;
			break;
		}

#ifdef FEATURE_IPTABLES
		retval = ip6t_jump_rule_del("mangle", nf2->chain, &nf2->src, nf2->src_plen, nf2->proto, &nf2->dst, nf2->dst_plen, "MIP6D_NG");

		ENV_DEBUG("NF Info: DELETE %s src " IP6ADDR_FMT " dst " IP6ADDR_FMT " jump %s (%d)",
				nf2->chain, IP6ADDR_TO_STR(&nf2->src), IP6ADDR_TO_STR(&nf2->dst), "MIP6D_NG", retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_NF_JUMP:
		nfj = (struct msg_env_nf_jump *)arg;
		if (nfj == NULL) {
			retval = -1;
			break;
		}

#ifdef FEATURE_IPTABLES
		if (strlen(nfj->chain) == 0) 
			strncpy(nfj->chain, "MIP6D_NG", sizeof(nfj->chain));

		retval = ip6t_jump_rule("mangle", nfj->chain, &nfj->src, nfj->src_plen, nfj->proto, &nfj->dst, nfj->dst_plen, nfj->target);

		ENV_DEBUG("NF Info: %s src " IP6ADDR_FMT " dst " IP6ADDR_FMT " jump %s (%d)",
				nfj->chain, IP6ADDR_TO_STR(&nfj->src), IP6ADDR_TO_STR(&nfj->dst), nfj->target, retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_NF_JUMP_DEL:
		nfj = (struct msg_env_nf_jump *)arg;
		if (nfj == NULL) {
			retval = -1;
			break;
		}

#ifdef FEATURE_IPTABLES
		if (strlen(nfj->chain) == 0) 
			strncpy(nfj->chain, "MIP6D_NG", sizeof(nfj->chain));

		retval = ip6t_jump_rule_del("mangle", nfj->chain, &nfj->src, nfj->src_plen, nfj->proto, &nfj->dst, nfj->dst_plen, nfj->target);

		ENV_DEBUG("NF Info: DELETE %s src " IP6ADDR_FMT " dst " IP6ADDR_FMT " jump %s (%d)",
				nfj->chain, IP6ADDR_TO_STR(&nfj->src), IP6ADDR_TO_STR(&nfj->dst), nfj->target, retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_NF_CHAIN:
		nfc = (struct msg_env_nf_chain *)arg;
		if (nfc == NULL) {
			retval = -1;
			break;
		}

#ifdef FEATURE_IPTABLES
		retval = ip6t_create_chain("mangle", nfc->name, 0);
		
		ENV_DEBUG("NF Info: mark chain %s (%d)", nfc->name, retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_NF_CHAIN_DEL:
		nfc = (struct msg_env_nf_chain *)arg;
		if (nfc == NULL) {
			retval = -1;
			break;
		}

#ifdef FEATURE_IPTABLES
		retval = ip6t_remove_chain("mangle", nfc->name, 1);
		
		ENV_DEBUG("NF Info: mark chain %s DELETE (%d)", nfc->name, retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_NF_MARK_AND_ACC:
		nfm = (struct msg_env_nf_mark_and_acc *)arg;
		if (nfm == NULL) {
			retval = -1;
			break;
		}

#ifdef FEATURE_IPTABLES
		snprintf(chainname, sizeof(chainname), "M6_NG_%s", nfm->name);

		retval = ip6t_create_chain("mangle", chainname, 0);
		if (retval) {
			ENV_DEBUG("NF Info: mark chain %s (%d)", chainname, retval);
			break;
		}
		
		retval = ip6t_mark_rule("mangle", chainname, NULL, NULL, nfm->mark);
		if (retval) {
			ENV_DEBUG("NF Info: %s mark %u (%d)", chainname, nfm->mark, retval);
			break;
		}
		
		retval = ip6t_jump_rule("mangle", chainname, NULL, 0, 0, NULL, 0, "ACCEPT");

		ENV_DEBUG("NF Info: %s mark and accept %u (%d)", chainname, nfm->mark, retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_NF_MARK_AND_ACC_DEL:
		nfm = (struct msg_env_nf_mark_and_acc *)arg;
		if (nfm == NULL) {
			retval = -1;
			break;
		}

#ifdef FEATURE_IPTABLES
		snprintf(chainname, sizeof(chainname), "M6_NG_%s", nfm->name);

		retval = ip6t_jump_rule_del("mangle", chainname, NULL, 0, 0, NULL, 0, "ACCEPT");
		if (retval) {
			ENV_DEBUG("NF Info: DELETE mark chain %s (%d)", chainname, retval);
			break;
		}
		
		retval = ip6t_mark_rule_del("mangle", chainname, NULL, NULL, nfm->mark);
		if (retval) {
			ENV_DEBUG("NF Info: DELETE %s mark %u (%d)", chainname, nfm->mark, retval);
			break;
		}
		
		retval = ip6t_remove_chain("mangle", chainname, 1);
		
		ENV_DEBUG("NF Info: DELETE %s mark and accept %u (%d)", chainname, nfm->mark, retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_NF_FLUSH:
		nff = (struct msg_env_nf_flush *)arg;
		if (nff == NULL) {
			retval = -1;
			break;
		}

#ifdef FEATURE_IPTABLES
		if (nff->name_builtin)
			snprintf(chainname, sizeof(chainname), "%s", nff->name);
		else
			snprintf(chainname, sizeof(chainname), "M6_NG_%s", nff->name);

		retval = ip6t_flush_chain("mangle", chainname, 0);
		
		ENV_DEBUG("NF Info: FLUSH %s (%d)", chainname, retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_NF_FLUSH_ALL:
#ifdef FEATURE_IPTABLES
		retval = ip6t_flush_all("mangle");
		
		ENV_DEBUG("NF Info: FLUSH ALL (%d)", retval);
#else
		retval = 0;
#endif
		break;
	case MSG_ENV_MISC_PROC_SYS:
		ps = (struct msg_env_misc_proc_sys *)arg;
		if (ps == NULL) {
			retval = 1;
			break;
		}

		str = if_indextoname(ps->ifindex, ifname);
		if (str != NULL) {
			snprintf(pspath, sizeof(pspath), ps->path, ifname);
			psfp = fopen(pspath, "w");
			if (!psfp) {
				retval = -1;
				break;
			}
			retval = fprintf(psfp, "%d", ps->value);
			if (retval > 0)
				retval = 0;
			fclose(psfp);

		} else 
			retval = -1;
			
		ENV_DEBUG("PROC SYS Info: %s %d (%d)", pspath, ps->value, retval);
		break;
	case MSG_ENV_MISC_PROC_SYS_GET:
		ps = (struct msg_env_misc_proc_sys *)arg;
		if (ps == NULL) {
			retval = 1;
			break;
		}

		str = if_indextoname(ps->ifindex, ifname);
		if (str != NULL) {
			snprintf(pspath, sizeof(pspath), ps->path, ifname);
			psfp = fopen(pspath, "r");
			if (!psfp) {
				retval = -1;
				break;
			}
			retval = fscanf(psfp, "%10d", &ps->value);
			if (retval > 0)
				retval = 0;
			fclose(psfp);

		} else 
			retval = -1;
			
		ENV_DEBUG("PROC SYS GET Info: %s %d (%d)", pspath, ps->value, retval);
		break;
	}

	return retval;
}

static struct imsg_opts imsg_opts = {
	.event_fp = NULL,
	.message_fp = env_proc_message,
	.event_max = EVT_ENV_NONE
};

/*!
 * \brief Cleanup
 *
 * It clears the XFRM states and policies
 * and deletes the MIP6d_NG chain
 * \param arg Unused
 * \return Zero
 */
static int env_exit_handler(void * arg)
{
	xfrm_policy_clear();
	xfrm_state_clear();

#ifdef FEATURE_IPTABLES
	ip6t_remove_chain("mangle", "MIP6D_NG", 1);
#endif
	
	DEBUG("Module: environment is clean.");

	return 0;
}

/*!
 * \brief Initializing environment module
 *
 * Registers exit function to the 'core-exit' hook
 * Register internal messaging
 * It creates the MIP6D_NG ip6tables chain into the mangle table
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int environment_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	unsigned int core_exit_id;

	DEBUG("Initializing module: environment");

	ret = imsg_register(MSG_ENV, &imsg_opts);
	if (ret < 0)
		return ret;
	env_opts.msg_ids.me = ret;

	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 100-SEQ_ENV, env_exit_handler);
	}

	ret = rtnl_start_listen();
	if (ret) {
		ERROR("Unable to start environment event listener");
		return -1;
	}

#ifdef FEATURE_IPTABLES
	ret = ip6t_create_chain("mangle", "MIP6D_NG", 0);
	if (ret)
		return -1;
#endif

	return 0;
}

MIP6_MODULE_INIT(MSG_ENV, SEQ_ENV, environment_module_init);

/*! \} */

