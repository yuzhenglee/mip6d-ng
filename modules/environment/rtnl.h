
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-environment
 * \{
 */

#ifndef MIP6D_NG_ENV_RTNL_H
#define MIP6D_NG_ENV_RTNL_H

#include <libnetlink.h>

/*!
 * \brief Opens a RTNetlink socket
 *
 * Protocol will be NETLINK_ROUTE
 * It's a wrapper for rtnl_open_byproto
 * \param rth RTNetlink handler
 * \param subscriptions Subscriptions
 * \return Zero if OK
 */
static inline int rtnl_route_open(struct rtnl_handle *rth, unsigned int subscriptions)
{
	return rtnl_open_byproto(rth, subscriptions, NETLINK_ROUTE);
}

/*!
 * \brief Opens a RTNetlink socket
 *
 * Protocol will be NETLINK_XFRM
 * It's a wrapper for rtnl_open_byproto
 * \param rth RTNetlink handler
 * \param subscriptions Subscriptions
 * \return Zero if OK
 */
static inline int rtnl_xfrm_open(struct rtnl_handle *rth, unsigned int subscriptions)
{
	return rtnl_open_byproto(rth, subscriptions, NETLINK_XFRM);
}

int rtnl_do(int proto, struct nlmsghdr *sn, struct nlmsghdr *rn);

/*!
 * \brief RTNetlink do function for NETLINK_ROUTE protocol
 *
 * Wrapper for rtnl_do (rtnl_talk).
 * It opens an rtnetlink socket, and passes the message to the kernel
 * Finaly it closes the socket
 * \param sn Message
 * \param rn Reply message, it could be NULL
 * \return Zero if OK
 */
static inline int rtnl_route_do(struct nlmsghdr *sn, struct nlmsghdr *rn)
{
	return rtnl_do(NETLINK_ROUTE, sn, rn);
}

/*!
 * \brief RTNetlink do function for NETLINK_XFRM protocol
 *
 * Wrapper for rtnl_do (rtnl_talk).
 * It opens an rtnetlink socket, and passes the message to the kernel
 * Finaly it closes the socket
 * \param sn Message
 * \param rn Reply message, it could be NULL
 * \return Zero if OK
 */
static inline int rtnl_xfrm_do(struct nlmsghdr *sn, struct nlmsghdr *rn)
{
	return rtnl_do(NETLINK_XFRM, sn, rn);
}

int rtnl_addr_do(const struct in6_addr *addr, int plen, unsigned int ifindex, void *arg,
	    int (*do_callback)(struct ifaddrmsg *ifa, struct rtattr *rta_tb[], void *arg));

int rtnl_addr_add(const struct in6_addr *addr, uint8_t plen, 
	     uint8_t flags, uint8_t scope, unsigned int ifindex, 
	     uint32_t prefered, uint32_t valid);

int rtnl_addr_del(const struct in6_addr *addr, uint8_t plen, unsigned int ifindex);

int rtnl_route_add(unsigned int oif, uint8_t table, uint8_t proto,
	      unsigned flags, uint32_t metric,
	      const struct in6_addr *src, int src_plen,
	      const struct in6_addr *dst, int dst_plen, 
	      const struct in6_addr *gateway);

int rtnl_route_append(unsigned int oif, uint8_t table, uint8_t proto,
	      unsigned flags, uint32_t metric,
	      const struct in6_addr *src, int src_plen,
	      const struct in6_addr *dst, int dst_plen, 
	      const struct in6_addr *gateway);

int rtnl_route_del(unsigned int oif, uint8_t table, uint32_t metric,
	      const struct in6_addr *src, int src_plen,
	      const struct in6_addr *dst, int dst_plen, 
	      const struct in6_addr *gateway);

int rtnl_rule_add(unsigned int iface, uint8_t table,
	     uint32_t priority, uint32_t fwmark, uint8_t action,
	     const struct in6_addr *src, int src_plen,
	     const struct in6_addr *dst, int dst_plen, unsigned int flags);

int rtnl_rule_del(unsigned int iface, uint8_t table,
	     uint32_t priority, uint32_t fwmark, uint8_t action,
	     const struct in6_addr *src, int src_plen,
	     const struct in6_addr *dst, int dst_plen, unsigned int flags);

int rtnl_neigh_add(int ifindex, uint16_t state, uint8_t flags,
		struct in6_addr *dst, uint8_t *hwa, int hwalen,
		int override);

int rtnl_neigh_del(int ifindex, struct in6_addr *dst);

int rtnl_pneigh_add(int ifindex, uint8_t flags, struct in6_addr *dst);

int rtnl_pneigh_del(int ifindex, struct in6_addr *dst);

int rtnl_iterate(int proto, int type, rtnl_filter_t func, void *extarg);

int rtnl_link_get(unsigned int iface, unsigned short * type, unsigned int * flags);

int rtnl_addr_isused(struct in6_addr * addr, uint8_t plen, uint8_t scope, unsigned int iface);

int rtnl_addr_flush_home_addr();

int rtnl_addr_get(unsigned int iface, struct msg_env_addr_get_reply * reply);

int rtnl_route_isused(unsigned int oif, uint8_t table, uint32_t metric,
	      struct in6_addr *src, int src_plen,
	      struct in6_addr *dst, int dst_plen, 
	      struct in6_addr *gateway);

int rtnl_route_get(unsigned int oif, uint8_t table,
	      struct in6_addr *dst, int dst_plen,
	      struct msg_env_route_get_reply * reply);

int rtnl_rule_isused(unsigned int iface, uint8_t table,
	     uint32_t priority, uint32_t fwmark, uint8_t action,
	     struct in6_addr *src, int src_plen,
	     struct in6_addr *dst, int dst_plen);

int rtnl_start_listen();

#endif /* MIP6D_NG_ENV_RTNL_H */

/*! \} */
