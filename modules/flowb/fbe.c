
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flowb
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/bce.h>
#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/mcoa-bule.h>
#include <mip6d-ng/mcoa-bce.h>
#include <mip6d-ng/fbe.h>
#include <mip6d-ng/flowb.h>
#include <mip6d-ng/core-comm.h>

#include "main.h"
#include "fbe.h"
#include "flows.h"
#include "mho-flowi.h"
#include "mho-flows.h"

/*!
 * \brief Flow Binding Entries List
 */
static LIST_HEAD(fbel);
/*!
 * \brief Lock of Flow Binding Entries List
 */
static pthread_mutex_t fbel_lock = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief Add a Flow Binding Entry to the list
 *
 * It initializes the mutex, and the following fields:
 *  - status: new
 *  - active: inactive
 *  - activate and inactivate callback: see flows_activate and flows_inacctivate
 *
 * \param fbe Flow Binding Entry
 * \return Zero if OK
 */
int fbe_add(struct fbe * fbe) 
{
	if (fbe == NULL)
		return -1;

	INFO("Adding %sflow: ID %d from [" IP6ADDR_FMT "/%u]:%u to [" IP6ADDR_FMT "/%u]:%u proto %u BID %u",
			(fbe->is_static) ? "static " : "", fbe->fid,
			IP6ADDR_TO_STR(&fbe->ts.src), fbe->ts.splen, fbe->ts.sport1,
			IP6ADDR_TO_STR(&fbe->ts.dst), fbe->ts.dplen, fbe->ts.dport1, fbe->ts.proto1, fbe->bid);

	pthread_mutex_init(&fbe->_lock, NULL);
	fbe->status = FBE_STATUS_NEW;
	fbe->active = FBE_IS_INACTIVE;
	fbe->activate = flows_activate;
	fbe->inactivate = flows_inactivate;

	pthread_mutex_lock(&fbel_lock);
	list_add_tail(&fbe->list, &fbel);
	pthread_mutex_unlock(&fbel_lock);
	
	return 0;
}

/*!
 * \brief Delete a Flow Binding Entry form the list
 *
 * The first matching condition is the flow identifier (fid) value.
 * If it is zero (in the arguments' list), the second matching
 * condition will be checked: bid value.
 * If force is zero, and the entry is static, it won't be deleted!
 * Finally, if force is zero and the key argument and the key of the entry
 * are mismatch, it won't be deleted!
 * Before deleting it inactivates it.
 *
 * \param fid Flow Identifier
 * \param bid Binding Identifier
 * \param key Authentication
 * \param force Force, see the description
 * \return Zero if OK
 */
int fbe_del(uint16_t bid, uint16_t fid, struct flowb_key * key, int force)
{
	int retval = -1;
	struct list_head * pos, * pos2;
	struct fbe * fbe;
	struct flowb_key k;

	if (key == NULL)
		memset(&k, 0, sizeof(struct flowb_key));
	else
		memcpy(&k, key, sizeof(struct flowb_key));

	if (fid == 0 && bid == 0)
		return -1;

	INFO("Delete FBE(s) with FID %u / BID %u (key: %s, force: %i)", fid, bid, (key == NULL) ? "no" : "yes", force);

	pthread_mutex_lock(&fbel_lock);

	list_for_each_safe(pos, pos2, &fbel) {
		fbe = list_entry(pos, struct fbe, list);

		fbe_lock(fbe);

		if (fid > 0) {
			if (fbe->fid != fid) 
				goto cont;
		} else {
			if (fbe->bid != bid)
				goto cont;
		}

		if (fbe->is_static == 1 && force == 0) {
			goto out;
		}

		if (force == 0 && memcmp(&fbe->key, &k, sizeof(struct flowb_key)) != 0) {
			ERROR("Invalid key.");
			goto out;
		}
		
		retval = fbe->inactivate(fbe);
		list_del(pos);
		free(fbe);

		continue;

cont:

		fbe_unlock(fbe);

		continue;

out:
		
		fbe_unlock(fbe);

		break;
	}

	pthread_mutex_unlock(&fbel_lock);

	return retval;
}

/*! 
 * \brief Extend the Binding Update message
 *
 * It adds Flow Binding Identification sub option with the details of the 
 * Flow Binding entries with status NEW. The FID of the others will be listed
 * in the Flow Binding Summary Mobility Option.
 * 
 * \param param struct send_hook_bu_param 
 * \return Zero if OK
 */
int fbe_extend_bu(void * param)
{
	int ret;
	struct send_hook_bu_param * hookp = (struct send_hook_bu_param *)param;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	struct fbe * fbe;
	uint16_t bid = 0;
	uint16_t * fids = NULL, * fids_new = NULL;
	int num = 0;

	if (hookp == NULL || hookp->iov == NULL || hookp->iov_count+1 >= hookp->iov_len || hookp->bule == NULL)
		return -1;

	refcnt_get(&hookp->bule->refcnt);
	bule_lock(hookp->bule);

	list_for_each(pos, &hookp->bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}
	bule_unlock(hookp->bule);
	refcnt_put(&hookp->bule->refcnt);

	if (bid == 0) {
		ERROR("Unable to find BID extension in BULE");
		return 0;
	}

	DEBUG("Extend BU with flowb options: bid %u....", bid);

	pthread_mutex_lock(&fbel_lock);

	list_for_each(pos, &fbel) {
		fbe = list_entry(pos, struct fbe, list);

		fbe_lock(fbe);

		if (fbe->bid != bid)
			goto cont;
		
		if (fbe->status == FBE_STATUS_OK) {
			fids_new = realloc(fids, (num + 1) * sizeof(uint16_t));
			if (fids_new == NULL) {
				ERROR("Out of memory: Unable to allocate fids");
				fbe_unlock(fbe);
				free(fids);
				goto out;
			}
			fids = fids_new;
			fids[num++] = fbe->fid;
		} else if (fbe->status == FBE_STATUS_NEW || fbe->status == FBE_STATUS_SENT) {
			mho_flowi_create(&(hookp->iov[hookp->iov_count++]), fbe);
			fbe->status = FBE_STATUS_SENT;
		}

cont:
		fbe_unlock(fbe);
	}

	ret = mho_flows_create(&(hookp->iov[hookp->iov_count++]), fids, num);
	if (fids != NULL)
		free(fids);
	if (ret) {
		ERROR("Unable to add flow summary option");
		goto out;
	}

out:
	pthread_mutex_unlock(&fbel_lock);
	
	return 0;
}

/*! 
 * \brief Extend the Binding Acknowledge message
 *
 * The FID of the Flow Binding entries will be listed
 * in the Flow Binding Summary Mobility Option.
 * 
 * \param param struct send_hook_ba_param 
 * \return Zero if OK
 */
int fbe_extend_ba(void * param)
{
	int ret;
	struct send_hook_ba_param * hookp = (struct send_hook_ba_param *)param;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	struct fbe * fbe;
	uint16_t bid = 0;
	uint16_t * fids = NULL, * fids_new = NULL;
	int num = 0;

	if (hookp == NULL || hookp->iov == NULL || hookp->iov_count+1 >= hookp->iov_len || hookp->bce == NULL) 
		return -1;

	refcnt_get(&hookp->bce->refcnt);
	bce_lock(hookp->bce);
	list_for_each(pos, &hookp->bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}
	bce_unlock(hookp->bce);
	refcnt_put(&hookp->bce->refcnt);

	if (bid == 0) {
		ERROR("Unable to find BID extension in BCE");
		return -1;
	}

	DEBUG("Extend BA with flowb options: bid %u....", bid);

	pthread_mutex_lock(&fbel_lock);

	list_for_each(pos, &fbel) {
		fbe = list_entry(pos, struct fbe, list);

		fbe_lock(fbe);

		if (fbe->bid != bid)
			goto cont;
	
		fids_new = realloc(fids, (num + 1) * sizeof(uint16_t));
		if (fids_new == NULL) {
			ERROR("Out of memory: Unable to allocate fids");
			fbe_unlock(fbe);
			free(fids);
			goto out;
		}
		fids = fids_new;
		fids[num++] = fbe->fid;

cont:
		fbe_unlock(fbe);
	}

	ret = mho_flows_create(&(hookp->iov[hookp->iov_count++]), fids, num);
	if (fids != NULL)
		free(fids);
	if (ret) {
		ERROR("Unable to add flow summary option");
		goto out;
	}

out:
	pthread_mutex_unlock(&fbel_lock);

	return 0;
}

/*!
 * \brief Check the Flow Binding Entries List
 *
 * It check all of the FBEs. If the bid parameter is non-zero, and the 
 * hoa parameter is not NULL, it check only the entries which bid and home address
 * are matching.
 * If it founds and entry which is not listed in the fids parameter, it deletes it.
 * If the do_del parameter zero, the entries which should to be deleted,
 * will be only inactivated.
 *
 * \param bid BID
 * \param hoa Home Address
 * \param fids Flow Identifiers
 * \param fids_num Number of flow identifiers
 * \param do_del If non-zero delete not only inactivates.
 * \return Zero if OK
 */
int fbe_check_all(uint16_t bid, struct in6_addr * hoa, uint16_t * fids, int fids_num, unsigned char do_del)
{
	struct list_head * pos, * pos2;
	struct fbe * fbe;
	int i;
	unsigned char found;

	if (fids == NULL)
		fids_num = 0;

	pthread_mutex_lock(&fbel_lock);

	list_for_each_safe(pos, pos2, &fbel) {
		fbe = list_entry(pos, struct fbe, list);

		fbe_lock(fbe);

		if (bid != 0 && hoa != NULL && (fbe->bid != bid || memcmp(&fbe->hoa, hoa, sizeof(struct in6_addr)) != 0)) 
			goto cont;

		found = 0;
		for (i = 0; i < fids_num; i++) {
			if (fbe->fid == fids[i]) {
				found = 1;
				break;
			}
		}

		DEBUG("FID %u -> found %u (active: %02X status %02X)", fbe->fid, found, fbe->active, fbe->status);

		if (found == 1) {
			fbe->status = FBE_STATUS_OK;
			if (fbe->active != FBE_IS_ACTIVE)
				fbe->activate(fbe);
			goto cont;
		} else {
			fbe->inactivate(fbe);
			if (do_del) {
				list_del(pos);
				free(fbe);
				continue;
			}
		}

cont:

		fbe_unlock(fbe);
	}

	pthread_mutex_unlock(&fbel_lock);

	return 0;
}

/*!
 * \brief Find Flow Binding Entry
 *
 * Find FBE based on Flow Identifier value.
 * If hoa is not NULL, it should to be matched with the home address
 * of the FBE.
 *
 * \param fid Flow Identifier
 * \param hoa Home Address
 * \return Flow Binding Entry or NULL
 */
struct fbe * fbe_find_by_fid(uint16_t fid, struct in6_addr * hoa)
{
	struct list_head * pos, * pos2;
	struct fbe * fbe = NULL;
	int found = 0;

	pthread_mutex_lock(&fbel_lock);

	list_for_each_safe(pos, pos2, &fbel) {
		fbe = list_entry(pos, struct fbe, list);

		fbe_lock(fbe);

		if (hoa != NULL && memcmp(hoa, &fbe->hoa, sizeof(struct in6_addr)) != 0)
			goto cont;

		if (fbe->fid == fid) 
			found = 1;

cont:
		fbe_unlock(fbe);

		if (found)
			break;
	}

	pthread_mutex_unlock(&fbel_lock);

	if (found)
		return fbe;
	else
		return NULL;
}

/*!
 * \brief Find all Flow Binding entries which BID value matching with the given one
 *
 * If hoa is not NULL, it should to be matched with the home address
 * of the FBE.
 *
 * \param bid BID
 * \param hoa Home Address
 * \param cb Callback, it will be called for all of the found entries
 * \param arg Extra argument for cb
 */
void fbe_find_all_by_bid(uint16_t bid, struct in6_addr * hoa, void (* cb)(struct fbe *, void *), void * arg)
{
	struct list_head * pos, * pos2;
	struct fbe * fbe = NULL;

	if (hoa == NULL || cb == NULL)
		return;

	pthread_mutex_lock(&fbel_lock);

	list_for_each_safe(pos, pos2, &fbel) {
		fbe = list_entry(pos, struct fbe, list);

		fbe_lock(fbe);

		if (memcmp(hoa, &fbe->hoa, sizeof(struct in6_addr)) != 0) 
			goto cont;
		

		if (fbe->bid != bid) 
			goto cont;
		
		fbe_unlock(fbe);

		cb(fbe, arg);

		continue;

cont:
		fbe_unlock(fbe);
	}

	pthread_mutex_unlock(&fbel_lock);
}

/*! \} */
