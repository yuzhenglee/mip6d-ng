
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flowb
 * \{
 */

#ifndef _FLOWB_H_
#define _FLOWB_H_

#include <mip6d-ng/flowb.h>
#include <mip6d-ng/fbe.h>

int flowb_register(int id, struct flowb_api_register * areg, struct flowb_api_register_reply * areg_rep);
int flowb_update(int id, struct flowb_api_update * aup);
int flowb_delete(int id, struct flowb_api_del * adel);
int flowb_peer_close(void * arg);

int flowb_proc_bu(struct bce * bce);
int flowb_proc_bce_update(void * arg);
int flowb_proc_ba(struct bule * bule);

int flowb_init_bce(struct bce * bce);
int flowb_init_bule(struct bule * bule);

#endif /* _FLOWB_H_ */

/*! \} */
