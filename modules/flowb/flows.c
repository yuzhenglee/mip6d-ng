
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flowb
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/flowb.h>
#include <mip6d-ng/fbe.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/mcoa.h>

#include "main.h"
#include "flows.h"
#include "fbe.h"

/*!
 * \brief Activate a flow
 *
 * It creates the netfilter rules to activate the flow. It will redirect the traffic,
 * identified by the traffic selector part to the specified interface, identified 
 * by the BID value.
 * \param fbe Flow Binding Entry
 * \return Zero if OK
 */
int flows_activate(struct fbe * fbe)
{
	int ret;
	enum ctrl_node_type node_type;
	struct msg_env_nf_jump nfj;

	if (fbe == NULL)
		return -1;

	INFO("Activate %sflow: ID %d from [" IP6ADDR_FMT "/%u]:%u to [" IP6ADDR_FMT "/%u]:%u proto %u BID %u",
			(fbe->is_static) ? "static " : "", fbe->fid,
			IP6ADDR_TO_STR(&fbe->ts.src), fbe->ts.splen, fbe->ts.sport1,
			IP6ADDR_TO_STR(&fbe->ts.dst), fbe->ts.dplen, fbe->ts.dport1, fbe->ts.proto1, fbe->bid);

	ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.ctrl,
			MSG_CTRL_GET_NODE_TYPE, NULL, 0, &node_type, sizeof(node_type));
	if (ret || node_type == CTRL_NODE_TYPE_NONE) {
		ERROR("Unknown node type");
		return -1;
	}

	if (fbe->bid > 0) {
		memset(&nfj, 0, sizeof(nfj));
		snprintf(nfj.chain, sizeof(nfj.chain), "MIP6D_NG_FB");
		memcpy(&nfj.src, &fbe->ts.src, sizeof(struct in6_addr));
		nfj.src_plen = fbe->ts.splen;
		memcpy(&nfj.dst, &fbe->ts.dst, sizeof(struct in6_addr));
		nfj.dst_plen = fbe->ts.dplen;
		nfj.proto = fbe->ts.proto1;
		if (node_type == CTRL_NODE_TYPE_MN)
			snprintf(nfj.target, sizeof(nfj.target), "M6_NG_BID%u", fbe->bid);
		else
			snprintf(nfj.target, sizeof(nfj.target), "M6_NG_HOA%04x_BID%u", ntohs(fbe->hoa.s6_addr16[7]), fbe->bid);

		ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.env, MSG_ENV_NF_JUMP,
				&nfj, sizeof(nfj), NULL, 0);
		if (ret) {
			ERROR("Unable to create flow rule");
			return -1;
		}
	}

	fbe->active = FBE_IS_ACTIVE;

	return 0;
}

/*!
 * \brief Inactivate a flow
 *
 * It deletes the netfilter rules created by the flows_activate function.
 * \param fbe Flow Binding Entry
 * \return Zero if OK
 */
int flows_inactivate(struct fbe * fbe)
{
	int ret = 0;
	enum ctrl_node_type node_type;
	struct msg_env_nf_jump nfj;

	if (fbe == NULL)
		return -1;

	if (fbe->active == FBE_IS_INACTIVE)
		return 0;

	INFO("Inactivate flow: ID %d from [" IP6ADDR_FMT "/%u]:%u to [" IP6ADDR_FMT "/%u]:%u proto %u BID %u",
			fbe->fid, IP6ADDR_TO_STR(&fbe->ts.src), fbe->ts.splen, fbe->ts.sport1,
			IP6ADDR_TO_STR(&fbe->ts.dst), fbe->ts.dplen, fbe->ts.dport1, fbe->ts.proto1, fbe->bid);

	ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.ctrl,
			MSG_CTRL_GET_NODE_TYPE, NULL, 0, &node_type, sizeof(node_type));
	if (ret || node_type == CTRL_NODE_TYPE_NONE) {
		ERROR("Unknown node type");
		return -1;
	}

	if (fbe->bid > 0) {
		memset(&nfj, 0, sizeof(nfj));
		snprintf(nfj.chain, sizeof(nfj.chain), "MIP6D_NG_FB");
		memcpy(&nfj.src, &fbe->ts.src, sizeof(struct in6_addr));
		nfj.src_plen = fbe->ts.splen;
		memcpy(&nfj.dst, &fbe->ts.dst, sizeof(struct in6_addr));
		nfj.dst_plen = fbe->ts.dplen;
		nfj.proto = fbe->ts.proto1;
		if (node_type == CTRL_NODE_TYPE_MN)
			snprintf(nfj.target, sizeof(nfj.target), "M6_NG_BID%u", fbe->bid);
		else
			snprintf(nfj.target, sizeof(nfj.target), "M6_NG_HOA%04x_BID%u", ntohs(fbe->hoa.s6_addr16[7]), fbe->bid);

		ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
				&nfj, sizeof(nfj), NULL, 0);
		if (ret) {
			ERROR("Unable to delete flow rule");
		}
	}

	fbe->active = FBE_IS_INACTIVE;

	return ret;
}

/*! \} */
