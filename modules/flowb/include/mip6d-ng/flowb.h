
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-flowb
 * \{
 */

#ifndef MIP6D_NG_FLOWB_H
#define MIP6D_NG_FLOWB_H

#include <inttypes.h>

/*! 
 * \brief The name of the internal messaging reference point
 */
#define MSG_FLOWB		"flob"

/*!
 * \brief Module load order number of control module
 */
#define SEQ_FLOWB		51

/*!
 * \brief Available flowb API message commands
 */
enum flowb_messages {
	/*!
	 * \brief Register flow via the API module
	 *
	 * Argument: struct flowb_api_register
	 *
	 * Reply argument: struct flowb_api_register_reply
	 */
	MSG_FLOWB_API_REGISTER,
	/*!
	 * \brief Update flow parameters via the API module
	 *
	 * Argument: struct flowb_api_update
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_FLOWB_API_UPDATE,
	/*!
	 * \brief Delete flow parameters via the API module
	 *
	 * Argument: struct flowb_api_del
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_FLOWB_API_DELETE,
	MSG_FLOWB_NONE
};

/*!
 * \brief Available data API commands by the flowb module 
 * \ingroup eapi-flowb
 */
enum flowb_api_commands {
	/*!
	 * \brief Register flow via the API module
	 *
	 * Argument: struct flowb_api_register
	 * 
	 * Reply argument: struct flowb_api_register_reply
	 */
	API_FLOWB_REGISTER,
	/*!
	 * \brief Update flow parameters via the API module
	 *
	 * Argument: struct flowb_api_update
	 */
	API_FLOWB_UPDATE,
	/*!
	 * \brief Delete flow parameters via the API module
	 *
	 * Argument: struct flowb_api_del
	 */
	API_FLOWB_DELETE,
	API_FLOWB_NONE
};

/*!
 * \brief Authorization key 
 */
struct flowb_key {
	/*! Key */
	unsigned char k[16];
};

/*!
 * \brief Register a flow via the API module
 */
struct flowb_api_register {
	/*! Source address */
	struct in6_addr src;
	/*! Source prefix length */
	uint8_t splen;
	/*! Source port */
	uint16_t sport;
	/*! Destination address */
	struct in6_addr dst;
	/*! Destination prefix length */
	uint8_t dplen;
	/*! Destination port */
	uint16_t dport;
	/*! Protocol identifier, i.e. IPPROTO_ICMPV6 */
	uint32_t proto;
	/*! Requested BID (interface) */
	uint16_t bid;
	/*! If non-zero, the flow setup won't be deleted upon disconnection of the application */
	uint8_t is_static;
	/*! Authorization key */
	struct flowb_key key;
} __attribute__((packed));

/*!
 * \brief Register a flow via the API module: reply
 */
struct flowb_api_register_reply {
	/*! Identifier, it could be used to update / delete */
	uint32_t id;
} __attribute__((packed));

/*! 
 * \brief Update flow parameters via the API module
 */
struct flowb_api_update {
	/*! Requested BID (interface) */
	uint16_t bid;
	/*! Identifier from the reply of the register command */
	uint32_t id;
} __attribute__((packed));

/*!
 * \brief Delete flow parameters via the API module
 */
struct flowb_api_del {
	/*! Identifier */
	uint32_t id;
} __attribute__((packed));

#endif /* MIP6D_NG_FLOWB_H */

/*! \} */
