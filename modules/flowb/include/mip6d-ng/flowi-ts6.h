
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-flowb
 * \{
 */

#ifndef MIP6D_NG_FLOWI_TS6_H_
#define MIP6D_NG_FLOWI_TS6_H_

/*!
 * \brief Flow Identifier Traffic Selector
 * 
 * This option describes the selector format used by mip6d-ng,
 * and does not compatible with all of the standard combinations
 */
struct flowi_ts6 {
	/*! Flags */
	uint16_t		ts6_flags;
	/*! Reserved */
	uint16_t		ts6_reserved;
	/*! Source Address: Correspondent Node */
	unsigned char	ts6_source[16];
	/*! Destination Address: Home Address */
	unsigned char	ts6_dest[16];
	/*! Source Port */
	uint16_t		ts6_source_port;
	/*! Destination Port */
	uint16_t		ts6_dest_port;
	/*! Next header */
	uint8_t			ts6_nh;
	/*! Padding: 8n */
	uint8_t			ts6_padding[3];
} __attribute__ ((packed));

/*!
 * \brief Traffic Selector format descriptor: IPv6
 */
#define TS6_FORMAT_IPV6		0x02

#define TS6_FLAGS_A		0x8000
#define TS6_FLAGS_B		0x4000
#define TS6_FLAGS_C		0x2000
#define TS6_FLAGS_D		0x1000
#define TS6_FLAGS_E		0x0800
#define TS6_FLAGS_F		0x0400
#define TS6_FLAGS_G		0x0200
#define TS6_FLAGS_H		0x0100
#define TS6_FLAGS_I		0x0080
#define TS6_FLAGS_J		0x0040
#define TS6_FLAGS_K		0x0020
#define TS6_FLAGS_L		0x0010
#define TS6_FLAGS_M		0x0008
#define TS6_FLAGS_N		0x0004
#define TS6_FLAGS_O		0x0002
#define TS6_FLAGS_P		0x0001

/*!
 * \brief Flow Identifier Traffic Selector flags
 * 
 * This option describes the selector format used by mip6d-ng,
 * and does not compatible with all of the standard combinations
 */
#define FLOWI_TS6_FLAGS		(TS6_FLAGS_A | TS6_FLAGS_C | TS6_FLAGS_I | TS6_FLAGS_K | TS6_FLAGS_O)

#endif /* MIP6D_NG_FLOWI_TS6_H_ */

/*! \} */
