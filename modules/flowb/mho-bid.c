
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flowb
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/mcoa-bule.h>
#include <mip6d-ng/mcoa-bce.h>
#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/mho-bid.h>
#include <mip6d-ng/mho-bid-fb.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/bce.h>

#include "mho-bid.h"
#include "main.h"


/*!
 * \brief Hook function to modify the Binding Identifier Mobility Option
 *
 * It adds the BID-PRI value to the flags field. The BID-PRI value
 * equals to the preference value of the interface.
 * \param arg Iovec
 * \return Zero if OK
 */
int mho_bid_modify(void * arg) 
{
	int ret;
	struct iovec * iov = (struct iovec *)arg;
	struct ip6_mh_opt_bid_fb * opt;
	uint16_t bid;
	
	if (iov == NULL)
		return -1;

	opt = (struct ip6_mh_opt_bid_fb *)iov->iov_base;

	bid = ntohs(opt->ip6mob_bid);
	ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.mcoa, 
			MSG_MCOA_GET_PREFERENCE, &bid, sizeof(bid), NULL, 0);
	if (ret > 0) {
		DEBUG("Add BID-PRI option to MHO-BID: %u - %u", opt->ip6mob_bid, ret);
		BID_FB_SET_PRI(opt, ret);
	}
	
	return 0;
}

/*!
 * \brief Hook function to parse the Binding Identifier Mobility Option
 *
 * It gets the preference value of the interface from the BID-PRI part
 * of the flags field.
 * \param arg Iovec
 * \return Zero if OK
 */
int mho_bid_parse(void * arg)
{
	int prio;
	struct hook_bid_parse_arg * hooka = (struct hook_bid_parse_arg *)arg;
	struct ip6_mh_opt_bid_fb * opt;

	if (hooka == NULL || hooka->opt == NULL || hooka->bce == NULL || hooka->ext_bid == NULL)
		return -1;

	opt = (struct ip6_mh_opt_bid_fb *)hooka->opt;

	prio = BID_FB_GET_PRI(opt);
	hooka->ext_bid->preference = prio;

	INFO("Set MCoA %u@" IP6ADDR_FMT " preference to %u", hooka->ext_bid->bid, IP6ADDR_TO_STR(&hooka->bce->hoa), hooka->ext_bid->preference); 

	return 0;
}

/*! \} */
