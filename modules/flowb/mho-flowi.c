
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flowb
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/mho-flowi.h>
#include <mip6d-ng/flowi-ts6.h>
#include <mip6d-ng/fbe.h>
#include <mip6d-ng/flowb-bce.h>

#include "mho-flowi.h"
#include "flows.h"
#include "fbe.h"
#include "main.h"

/*!
 * \brief Create Flow Binding Identification Mobility Header Option
 *
 * It adds FlowI option to the MH with the given parameters. It could be used 
 * for registration and update.
 * \param iov Message iov
 * \param fbe Flow Binding Entry
 * \return Zero if OK
 */
int mho_flowi_create(struct iovec * iov, struct fbe * fbe)
{
	struct ip6_mh_opt_flowi * opti;
	struct ip6fli_bid * optb;
	struct ip6fli_ts * optts;
	struct flowi_ts6 * ts;
	size_t optlen = sizeof(struct ip6_mh_opt_flowi) + sizeof(struct ip6fli_bid) + 
		sizeof(struct ip6fli_ts) + sizeof(struct flowi_ts6);

	if (fbe == NULL)
		return -1;

	iov->iov_base = malloc(optlen);
	iov->iov_len = optlen;

	if (iov->iov_base == NULL)
		return -1;

	opti = (struct ip6_mh_opt_flowi *)iov->iov_base;

	opti->ip6mof_type = IP6_MHOPT_FLOWI;
	opti->ip6mof_len = optlen - 2;
	opti->ip6mof_fid = htons(fbe->fid);
	opti->ip6mof_fpri = htons(fbe->prio);
	opti->ip6mof_reserved = 0;
	opti->ip6mof_status = 0;

	optb = (struct ip6fli_bid *)(opti + 1);
	optb->ip6flib_type = IP6_MHOPT_FLOWI_SUBOPT_BID;
	optb->ip6flib_len = 2;
	optb->ip6flib_bid = htons(fbe->bid);

	optts = (struct ip6fli_ts *)(optb + 1);
	optts->ip6flits_type = IP6_MHOPT_FLOWI_SUBOPT_TS;
	optts->ip6flits_len = sizeof(struct ip6fli_ts) + sizeof(struct flowi_ts6) - 2;
	optts->ip6flits_format = TS6_FORMAT_IPV6;
	optts->ip6flits_reserved = 0;

	ts = (struct flowi_ts6 *)(optts + 1);
	ts->ts6_flags = FLOWI_TS6_FLAGS;
	ts->ts6_reserved = 0;
	/* Source is destination */
	memcpy(&ts->ts6_source, &fbe->ts.dst, sizeof(ts->ts6_source));
	ts->ts6_source_port = htons(fbe->ts.dport1);
	/* Destination is source */
	memcpy(&ts->ts6_dest, &fbe->ts.src, sizeof(ts->ts6_dest));
	ts->ts6_dest_port = htons(fbe->ts.sport1);
	ts->ts6_nh = fbe->ts.proto1;
	memset(ts->ts6_padding, 0, sizeof(ts->ts6_padding));

	return 0;
}

/*!
 * \brief Parsing Flow Binding Identification Mobility Header Option
 *
 * It could parse Binding Update or Binding Acknowledge option. The private argument
 * should to be a Binding Cache entry or a Binding Update List entry. Currently it does 
 * not have any effect in case of receiving BA, now the priv argument could to be NULL in this case. 
 * It tries to find an existing FBE, based on the Flow Identification value. If found one, it processes as update, 
 * if not as registration. In case of update the Traffic Selector option won't be parsed.
 * It adds extension to the BULE or to the BCE with a the newly created FBE or the update details.
 * This method ensures the processing of registration / update after parsing all of the Mobility Options.
 * 
 * \param mh_type MH type, it should to be BU
 * \param opt MH option, it should to be struct ip6_mh_opt_altcoa
 * \param optslen MH option len
 * \param priv Extra argument, struct bce
 * \return Zero if OK
 */
static int mho_flowi_recv(uint8_t mh_type, struct ip6_mh_opt * opt, size_t optslen, void * priv)
{
	struct fbe * fbe;
	struct ip6_mh_opt_flowi * flowi = (struct ip6_mh_opt_flowi *)opt;
	struct bce * bce = (struct bce *)priv;
	int len;
	int update;
	uint16_t new_bid = 0;
	struct ip6fli_opt * o;
	struct ip6fli_bid * optb;
	struct ip6fli_ts * optts;
	struct flowi_ts6 * ts;
	struct bce_ext_fbe_new * ext_fbe_new = NULL;
	struct bce_ext_fbe_update * ext_fbe_update = NULL;

	if (flowi == NULL || optslen < sizeof(struct ip6_mh_opt_flowi) || bce == NULL)
		return -1;

	fbe = fbe_find_by_fid(ntohs(flowi->ip6mof_fid), &bce->hoa);

	if (fbe == NULL) {
		update = 0;
		fbe = malloc(sizeof(struct fbe));
		if (fbe == NULL) {
			ERROR("Out of memory: Unable to allocate new fbe for flowi");
			return -1;
		}
		memset(fbe, 0, sizeof(struct fbe));
	} else {
		update = 1;
	}

	INFO("Found Flow Identification option: fid %u prio %u", ntohs(flowi->ip6mof_fid), ntohs(flowi->ip6mof_fpri));

	if (! update) {
		fbe->fid = ntohs(flowi->ip6mof_fid);
		fbe->status = FBE_STATUS_NEW;
		fbe->prio = ntohs(flowi->ip6mof_fpri);
		memcpy(&fbe->hoa, &bce->hoa, sizeof(struct in6_addr));
	}

	len = optslen - sizeof(struct ip6_mh_opt_flowi);
	o = (struct ip6fli_opt *)(flowi + 1);
	while (len > 0 && o != NULL) {
		switch(o->ip6fliopt_type) {
		case IP6_MHOPT_FLOWI_SUBOPT_BID:
			optb = (struct ip6fli_bid *)o;
			new_bid = ntohs(optb->ip6flib_bid);
			if (! update)
				fbe->bid = new_bid;
			INFO("  Found BID sub-option: %u", new_bid);
			break;
		case IP6_MHOPT_FLOWI_SUBOPT_TS:
			if (update)
				break;
			optts = (struct ip6fli_ts *)o;
			if (optts->ip6flits_format != TS6_FORMAT_IPV6)
				break;
			ts = (struct flowi_ts6 *)(optts + 1);
			if (ts->ts6_flags != FLOWI_TS6_FLAGS) {
				ERROR("Unknown Flow Identification Traffic Selector format / flags");
				break;
			}
			memcpy(&fbe->ts.src, &ts->ts6_source, sizeof(ts->ts6_source));
			fbe->ts.splen = 128;
			fbe->ts.sport1 = ntohs(ts->ts6_source_port);
			memcpy(&fbe->ts.dst, &ts->ts6_dest, sizeof(ts->ts6_dest));
			fbe->ts.dplen = 128;
			fbe->ts.dport1 = ntohs(ts->ts6_dest_port);
			fbe->ts.proto1 = ts->ts6_nh;
			INFO("  Found Traffic Selector sub-option");
			break;
		}
		len -= (o->ip6fliopt_len + 2);
		o = (struct ip6fli_opt *)( ((unsigned char *)o) + o->ip6fliopt_len + 2 );
	}

	if (! update) {
		ext_fbe_new = malloc(sizeof(struct bce_ext_fbe_new));
		if (ext_fbe_new == NULL) {
			ERROR("Out of memory: Unable to alloc new BULE ext FBE NEW");
			free(fbe);
			return -1;
		}
		memset(ext_fbe_new, 0, sizeof(struct bce_ext_fbe_new));

		strncpy(ext_fbe_new->ext.name, "fbe_new", sizeof(ext_fbe_new->ext.name));
		ext_fbe_new->fbe = fbe;

		list_add_tail(&ext_fbe_new->ext.list, &bce->ext);
	} else if (new_bid != 0) {
		ext_fbe_update = malloc(sizeof(struct bce_ext_fbe_update));
		if (ext_fbe_update == NULL) {
			ERROR("Out of memory: Unable to alloc new BULE ext FBE UPDATE");
			return -1;
		}
		memset(ext_fbe_update, 0, sizeof(struct bce_ext_fbe_update));

		strncpy(ext_fbe_update->ext.name, "fbe_up", sizeof(ext_fbe_update->ext.name));
		ext_fbe_update->fid = fbe->fid;
		ext_fbe_update->bid = new_bid;

		list_add_tail(&ext_fbe_update->ext.list, &bce->ext);
	}

	return 0;
}

/*!
 * \brief Register Flow Binding Identification MH Option
 * \return Zero if OK
 */
int mho_flowi_init()
{
	struct msg_ccom_reg_mh_opt opt;

	opt.type = IP6_MHOPT_FLOWI;
	opt.align_n = 2;
	opt.align = 0;		/* 2n */
	opt.fn = mho_flowi_recv;

	return IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.ccom, MSG_CCOM_REG_MH_OPT, &opt, sizeof(opt), NULL, 0);
}

/*! \} */
