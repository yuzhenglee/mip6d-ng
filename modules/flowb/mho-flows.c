
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flowb
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-bu.h>
#include <mip6d-ng/mh-ba.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/mho-flows.h>
#include <mip6d-ng/fbe.h>
#include <mip6d-ng/flowb-bule.h>
#include <mip6d-ng/flowb-bce.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/bce.h>
#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/mcoa-bule.h>
#include <mip6d-ng/mcoa-bce.h>

#include "mho-flows.h"
#include "flows.h"
#include "fbe.h"
#include "main.h"

/*!
 * \brief Create Flow Binding Summary Mobility Header Option
 *
 * It adds FlowS option to the MH with the given flow identifiers
 * \param iov Message iov
 * \param fids Flow Identifiers
 * \param fids_num Number of flow identifiers
 * \return Zero if OK
 */
int mho_flows_create(struct iovec * iov, uint16_t * fids, int fids_num)
{
	struct ip6_mh_opt_flows * opt;
	struct ip6_mh_opt * o;
	size_t optlen = sizeof(struct ip6_mh_opt_flows) + ((fids_num - 1) * sizeof(uint16_t));
	int i;
	uint16_t * fid_p;

	if (fids == NULL || fids_num <= 0) {
		optlen = sizeof(struct ip6_mh_opt);
		
		iov->iov_base = malloc(optlen);
		iov->iov_len = optlen;

		if (iov->iov_base == NULL)
			return -1;

		o = (struct ip6_mh_opt *)iov->iov_base;

		o->ip6mhopt_type = IP6_MHOPT_FLOWS;
		o->ip6mhopt_len = 0;

		return 0;
	}

	iov->iov_base = malloc(optlen);
	iov->iov_len = optlen;

	if (iov->iov_base == NULL)
		return -1;

	opt = (struct ip6_mh_opt_flows *)iov->iov_base;

	opt->ip6mos_type = IP6_MHOPT_FLOWS;
	opt->ip6mos_len = optlen - 2;
	opt->ip6mos_fid = fids[0];

	fid_p = &opt->ip6mos_fid;

	for (i = 1, fid_p++; i < fids_num; i++, fid_p++)
		*fid_p = fids[i];

	return 0;
}

/*!
 * \brief Parsing Flow Binding Summary Mobility Header Option
 *
 * It could parse Binding Update or Binding Acknowledge option. The private argument
 * should to be a Binding Cache entry or a Binding Update List entry. 
 * It adds extension to the BULE or to the BCE with the list of the Flow Identifiers.
 * This method ensures the processing of fids after parsing all of the Mobility Options.
 * 
 * \param mh_type MH type, it should to be BU
 * \param opt MH option, it should to be struct ip6_mh_opt_altcoa
 * \param optslen MH option len
 * \param priv Extra argument, struct bule or struct bce
 * \return Zero if OK
 */
static int mho_flows_recv(uint8_t mh_type, struct ip6_mh_opt * opt, size_t optslen, void * priv)
{
	struct ip6_mh_opt_flows * flows = (struct ip6_mh_opt_flows *)opt;
	uint16_t * fids;
	uint16_t * fid_p = NULL;
	struct bce * bce = (struct bce *)priv;
	struct bule * bule = (struct bule *)priv;
	int num = 0;
	int i;
	struct list_head * pos, * pos2;
	struct bule_ext * bule_ext;
	struct bce_ext * bce_ext;
	struct bule_ext_fbe * bule_ext_fbe = NULL;
	struct bce_ext_fbe * bce_ext_fbe = NULL;

	if (flows == NULL || optslen < sizeof(struct ip6_mh_opt_flows) || priv == NULL)
		return -1;

	num = flows->ip6mos_len / 2;
	INFO("Found Flow Summary option: %d fids", num);
	fid_p = &flows->ip6mos_fid;

	if (num == 0)
		return 0;

	fids = malloc(num * sizeof(uint16_t));
	if (fids ==  NULL) {
		ERROR("Out of memory: unable to alloc fids array");
		return -1;
	}

	for (i = 0; i < num; i++) {
		fids[i] = *fid_p;
		fid_p++;
	}

	if (mh_type == IP6_MH_TYPE_BU) {
		list_for_each_safe(pos, pos2, &bce->ext) {
			bce_ext = list_entry(pos, struct bce_ext, list);
			if (strcmp(bce_ext->name, "fbe") == 0) {
				bce_ext_fbe = (struct bce_ext_fbe *)bce_ext;
				if (bce_ext_fbe->fids != NULL)
					free(bce_ext_fbe->fids);
				list_del(pos);
				free(bce_ext_fbe);
			}
		}

		bce_ext_fbe = malloc(sizeof(struct bce_ext_fbe));
		if (bce_ext_fbe == NULL) {
			ERROR("Out of memory: Unable to alloc new BULE ext FBE");
			return -1;
		}
		memset(bce_ext_fbe, 0, sizeof(struct bce_ext_fbe));

		strncpy(bce_ext_fbe->ext.name, "fbe", sizeof(bce_ext_fbe->ext.name));
		bce_ext_fbe->fids = fids;
		bce_ext_fbe->num = num;

		list_add_tail(&bce_ext_fbe->ext.list, &bce->ext);
	} else if (mh_type == IP6_MH_TYPE_BA) {
		list_for_each_safe(pos, pos2, &bule->ext) {
			bule_ext = list_entry(pos, struct bule_ext, list);
			if (strcmp(bule_ext->name, "fbe") == 0) {
				bule_ext_fbe = (struct bule_ext_fbe *)bule_ext;
				if (bule_ext_fbe->fids != NULL)
					free(bule_ext_fbe->fids);
				list_del(pos);
				free(bule_ext_fbe);
			}
		}

		bule_ext_fbe = malloc(sizeof(struct bule_ext_fbe));
		if (bule_ext_fbe == NULL) {
			ERROR("Out of memory: Unable to alloc new BULE ext FBE");
			return -1;
		}
		memset(bule_ext_fbe, 0, sizeof(struct bule_ext_fbe));

		strncpy(bule_ext_fbe->ext.name, "fbe", sizeof(bule_ext_fbe->ext.name));
		bule_ext_fbe->fids = fids;
		bule_ext_fbe->num = num;

		list_add_tail(&bule_ext_fbe->ext.list, &bule->ext);
	} else {
		free(fids);
	}

	return 0;
}

/*!
 * \brief Register Flow Binding Summary MH Option
 * \return Zero if OK
 */
int mho_flows_init()
{
	struct msg_ccom_reg_mh_opt opt;

	opt.type = IP6_MHOPT_FLOWS;
	opt.align_n = 2;
	opt.align = 0;		/* 2n */
	opt.fn = mho_flows_recv;

	return IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.ccom, MSG_CCOM_REG_MH_OPT, &opt, sizeof(opt), NULL, 0);
}

/*! \} */
