
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flusher
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/environment.h>

#include <mip6d-ng/flusher.h>

#include "main.h"

static struct imsg_opts imsg_opts = {
	.event_fp = NULL,
	.message_fp = NULL,
};

/*!
 * \brief Initializing flusher module
 *
 * Flush XFRM policies and states
 * Flush ip6tables mangle table
 * Delete all home addresses 
 * 
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int flus_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	int me, env;

	DEBUG("Initializing module: flusher");

	ret = imsg_register(MSG_FLUS, &imsg_opts);
    if (ret < 0)
		return ret;
	me = ret;

	ret = imsg_get_id(MSG_ENV);
	if (ret < 0) {
		ERROR("Unable to find module: environment");
		return -1;
	}
	env = ret;

	ret = IMSG_MSG_ARGS(me, env, MSG_ENV_POLICY_FLUSH, NULL, 0, NULL, 0);
	if (ret) {
		ERROR("Unable to flush XFRM policies");
	}
	ret = IMSG_MSG_ARGS(me, env, MSG_ENV_POLICY_FLUSH_SUB, NULL, 0, NULL, 0);
	if (ret) {
		ERROR("Unable to flush XFRM sub policies");
	}
	ret = IMSG_MSG_ARGS(me, env, MSG_ENV_SA_FLUSH, NULL, 0, NULL, 0);
	if (ret) {
		ERROR("Unable to flush XFRM states");
	}
	ret = IMSG_MSG_ARGS(me, env, MSG_ENV_NF_FLUSH_ALL, NULL, 0, NULL, 0);
	if (ret) {
		ERROR("Unable to flush NF mangle table");
	}
	ret = IMSG_MSG_ARGS(me, env, MSG_ENV_ADDR_FLUSH_HOME_ADDR, NULL, 0, NULL, 0);
	if (ret) {
		ERROR("Unable to flush IPv6 home addresses");
	}

	return 0;
}

MIP6_MODULE_INIT(MSG_FLUS, SEQ_FLUS, flus_module_init);

/*! \} */

