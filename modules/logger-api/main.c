
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-logger-api
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/api.h>

#include <mip6d-ng/logger-api.h>

#include "main.h"

struct logger_api_opts logger_api_opts;

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the logger-api module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int logger_api_proc_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	int retval = -1;
	char * msg;

	switch (message) {
	case MSG_LOGA_LOG:
		msg = (char *)((unsigned char *)arg + sizeof(int));
		if (msg == NULL)
			break;
		DEBUG(msg);
		retval = 0;
		break;
	}

	return retval;
}

static struct imsg_opts imsg_opts = {
	.message_fp = logger_api_proc_message,
	.event_fp = NULL,
};

/*!
 * \brief Initializing logger-API module
 *
 * Initialize API command
 * 
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int logger_api_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	struct msg_api_register_cmd api_rc;

	DEBUG("Initializing module: logger-API");

	ret = imsg_register(MSG_LOGA, &imsg_opts);
    if (ret < 0)
		return ret;
	logger_api_opts.msg_ids.me = ret;

	ret = imsg_get_id(MSG_API);
	if (ret < 0) {
		ERROR("Unable to find module: api");
		return -1;
	}
	logger_api_opts.msg_ids.api = ret;

	memset(&api_rc, 0, sizeof(api_rc));
	api_rc.command = API_LOGA_LOG;
	snprintf(api_rc.command_name, sizeof(api_rc.command_name), "LOGA_LOG");
	api_rc.imsg_cmd = MSG_LOGA_LOG;
	ret = IMSG_MSG_ARGS(logger_api_opts.msg_ids.me, logger_api_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
	if (ret) {
		ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
		return -1;
	}

	return 0;
}

MIP6_MODULE_INIT(MSG_LOGA, SEQ_LOGA, logger_api_module_init);

/*! \} */

