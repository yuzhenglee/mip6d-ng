/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-logger-api
 * \{
 */

#ifndef MIP6D_NG_LOGGER_API_MAIN_H_
#define MIP6D_NG_LOGGER_API_MAIN_H_

#include <mip6d-ng/logger-api.h>

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
struct logger_api_opts {
	/*! Messaging IDs */
	struct {
		/*! Logger-API module's own ID */
		int me;
		/*! Messaging ID of api module */
		int api;
	} msg_ids;
};

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
extern struct logger_api_opts logger_api_opts;

#endif /* MIP6D_NG_LOGGER_API_MAIN_H_ */

/*! \} */

