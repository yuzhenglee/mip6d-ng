
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-mcoa
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/missing.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/md.h>
#include <mip6d-ng/environment.h>
#include <mip6d-ng/control.h>

#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/mcoa-bule.h>
#include <mip6d-ng/mcoa-bce.h>

#include "main.h"
#include "bid.h"
#include "default.h"

/*!
 * \brief BID of current default interface
 */
static int current_default_bid = 0;

/*!
 * \brief Lock of current_default_bid
 */
static pthread_mutex_t default_bid_lock = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief Default BID on Home Agent, one default per MN
 */
static LIST_HEAD(current_default_ha_bids);

/*!
 * \brief Lock of current_default_ha_bids
 */
static pthread_mutex_t default_ha_bids_lock = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief If non-zero cleanup is in progress, do not select new default interface 
 */
static unsigned char cleanup_in_progress = 0;

/*!
 * \brief Create default MARK rule on the MN
 *
 * \param mif Interface
 * \param arg Unused
 * \return Zero if OK
 */
int default_create_marks_mn(struct mcoa_interface * mif, void * arg)
{
	int ret;
	struct msg_env_nf_mark_and_acc nfm;

	if (mif == NULL)
		return 0;

	memset(&nfm, 0, sizeof(nfm));
	snprintf(nfm.name, sizeof(nfm.name), "BID%u", mif->bid);
	nfm.mark = mif->bid;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_MARK_AND_ACC,
			&nfm, sizeof(nfm), NULL, 0);
	if (ret) {
		ERROR("Unable to create ip6tables mark rule for BID %u", mif->bid);
	}

	return 0;
}

/*!
 * \brief Clean what has been created by default_create_marks_mn
 *
 * \param mif Interface
 * \param arg Unused
 * \return Zero if OK
 */
int default_clean_marks_mn(struct mcoa_interface * mif, void * arg)
{
	int ret;
	struct msg_env_nf_mark_and_acc nfm;

	if (mif == NULL)
		return 0;

	memset(&nfm, 0, sizeof(nfm));
	snprintf(nfm.name, sizeof(nfm.name), "BID%u", mif->bid);
	nfm.mark = mif->bid;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_MARK_AND_ACC_DEL,
			&nfm, sizeof(nfm), NULL, 0);
	if (ret) {
		ERROR("Unable to delete ip6tables mark rule for BID %u", mif->bid);
	}

	return 0;
}

/*!
 * \brief Initialize default selection engine on the MN
 *
 * It subscribes for a few events, and creates the initial
 * netfilter rules
 * \return Zero if OK
 */
int default_init_mn() 
{
	int ret;
	struct in6_addr hoa;
	struct in6_addr haa;

	struct msg_env_nf_to_mip6d_ng nf;
	struct msg_env_nf_chain nfc;
	struct msg_env_nf_jump nfj;

	cleanup_in_progress = 0;

	ret = imsg_event_subscribe(mcoa_opts.msg_ids.ccom, EVT_CCOM_BA, mcoa_opts.msg_ids.me);
	if (ret) {
		ERROR("Unable to register for events");
		return ret;
	}

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.ctrl, MSG_CTRL_GET_HOA,
			NULL, 0, &hoa, sizeof(hoa));
	if (ret) {
		ERROR("Unable to get HOA from control module");
		return -1;
	}

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.ctrl, MSG_CTRL_GET_HAA,
			NULL, 0, &haa, sizeof(haa));
	if (ret) {
		ERROR("Unable to get HAA from control module");
		return -1;
	}

	memset(&nf, 0, sizeof(nf));
	strncpy(nf.chain, "OUTPUT", sizeof(nf.chain));
	memcpy(&nf.src, &hoa, sizeof(struct in6_addr));
	nf.src_plen = 128;
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG,
			&nf, sizeof(nf), NULL, 0);
	if (ret) {
		ERROR("Unable to create ip6tables rule from OUTPUT to mip6d-ng");
		return -1;
	}

	memset(&nfc, 0, sizeof(nfc));
	strncpy(nfc.name, "MIP6D_NG_FB", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_CHAIN,
			&nfc, sizeof(nfc), NULL, 0);
	if (ret) {
		ERROR("Unable to create ip6tables chain MIP6D_NG_FB");
		return -1;
	}

	memset(&nfj, 0, sizeof(nfj));
	strncpy(nfj.target, "MIP6D_NG_FB", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP,
			&nfj, sizeof(nfj), NULL, 0);
	if (ret) {
		ERROR("Unable to create ip6tables rule: to MIP6D_NG_FB");
		return -1;
	}

	memset(&nfj, 0, sizeof(nfj));
	memcpy(&nfj.dst, &haa, sizeof(struct in6_addr));
	nfj.dst_plen = 128;
	nfj.proto = IPPROTO_MH;
	strncpy(nfj.target, "ACCEPT", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP,
			&nfj, sizeof(nfj), NULL, 0);
	if (ret) {
		ERROR("Unable to create ip6tables rule: to ACCEPT MH");
		return -1;
	}

	mcoa_iterate_interfaces(default_create_marks_mn, NULL);

	return 0;
}

/*!
 * \brief Clean default selection engine on the MN
 *
 * It unsubscribes from a few events, and deletes the initial
 * netfilter rules
 * \return Zero if OK
 */
int default_clean_mn() 
{
	int ret;
	struct in6_addr hoa;
	struct in6_addr haa;

	struct msg_env_nf_to_mip6d_ng nf;
	struct msg_env_nf_chain nfc;
	struct msg_env_nf_jump nfj;

	cleanup_in_progress = 1;

	imsg_event_unsubscribe(mcoa_opts.msg_ids.ccom, EVT_CCOM_BA, mcoa_opts.msg_ids.me);

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.ctrl, MSG_CTRL_GET_HOA,
			NULL, 0, &hoa, sizeof(hoa));
	if (ret) {
		ERROR("Unable to get HOA from control module");
		return -1;
	}

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.ctrl, MSG_CTRL_GET_HAA,
			NULL, 0, &haa, sizeof(haa));
	if (ret) {
		ERROR("Unable to get HAA from control module");
		return -1;
	}

	pthread_mutex_lock(&default_bid_lock);

	if (current_default_bid > 0) {
		memset(&nfj, 0, sizeof(nfj));
		snprintf(nfj.target, sizeof(nfj.target), "M6_NG_BID%u", current_default_bid);
		ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
				&nfj, sizeof(nfj), NULL, 0);
		if (ret == 0)
			current_default_bid = 0;
	}

	pthread_mutex_unlock(&default_bid_lock);

	memset(&nf, 0, sizeof(nf));
	strncpy(nf.chain, "OUTPUT", sizeof(nf.chain));
	memcpy(&nf.src, &hoa, sizeof(struct in6_addr));
	nf.src_plen = 128;
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG_DEL,
			&nf, sizeof(nf), NULL, 0);
	if (ret) {
		ERROR("Unable to delete ip6tables rule from OUTPUT to mip6d-ng");
		return -1;
	}

	memset(&nfj, 0, sizeof(nfj));
	strncpy(nfj.target, "MIP6D_NG_FB", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
			&nfj, sizeof(nfj), NULL, 0);
	if (ret) {
		ERROR("Unable to delete ip6tables rule: to MIP6D_NG_FB");
		return -1;
	}

	memset(&nfj, 0, sizeof(nfj));
	memcpy(&nfj.dst, &haa, sizeof(struct in6_addr));
	nfj.dst_plen = 128;
	nfj.proto = IPPROTO_MH;
	strncpy(nfj.target, "ACCEPT", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
			&nfj, sizeof(nfj), NULL, 0);
	if (ret) {
		ERROR("Unable to delete ip6tables rule: to ACCEPT MH");
		return -1;
	}

	mcoa_iterate_interfaces(default_clean_marks_mn, NULL);

	memset(&nfc, 0, sizeof(nfc));
	strncpy(nfc.name, "MIP6D_NG_FB", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_CHAIN_DEL,
			&nfc, sizeof(nfc), NULL, 0);
	if (ret) {
		ERROR("Unable to delete ip6tables chain MIP6D_NG_FB");
		return -1;
	}

	return 0;
}

/*!
 * \brief Update the default interface on the MN
 *
 * It gets the best interface by mcoa_interface_get_best, and
 * updates the environment regarding to this.
 * \return Zero if OK
 */
static int default_update_mn()
{
	struct mcoa_interface * mif;
	struct msg_env_nf_jump nfj;
	int ret = 1;

	memset(&nfj, 0, sizeof(nfj));
	
	mif = mcoa_interface_get_best();
	
	pthread_mutex_lock(&default_bid_lock);

	if (mif == NULL) {
		if (current_default_bid > 0) {
			snprintf(nfj.target, sizeof(nfj.target), "M6_NG_BID%u", current_default_bid);
			ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
					&nfj, sizeof(nfj), NULL, 0);
			if (ret == 0)
				current_default_bid = 0;
		}
	} else {
		if (current_default_bid != mif->bid) {
			if (current_default_bid > 0) {
				snprintf(nfj.target, sizeof(nfj.target), "M6_NG_BID%u", current_default_bid);
				ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
						&nfj, sizeof(nfj), NULL, 0);
				if (ret == 0)
					current_default_bid = 0;
			}
			if (! cleanup_in_progress) {
				snprintf(nfj.target, sizeof(nfj.target), "M6_NG_BID%u", mif->bid);
				ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP,
						&nfj, sizeof(nfj), NULL, 0);
				if (ret == 0)
					current_default_bid = mif->bid;
			}
		}
	}

	if (ret == 0) 
		DEBUG("MCoA: New default interface with BID %u (zero means deletion)", current_default_bid);
	else if (ret == 1)
		ret = 0;
	
	pthread_mutex_unlock(&default_bid_lock);

	return ret;
}

/*!
 * \brief Update the default interfaces by a received BA
 *
 * If the BA reports successful binding, it signs the corresponding 
 * interface as alive, and calls the default_update_mn function
 * to update the environment regarding to the new conditions.
 * \param ba Binding Acknowledge details
 * \return Zero if OK
 */
int default_update_by_ba(struct evt_ccom_ba * ba)
{
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;

	if (ba == NULL || ba->bule == NULL)
		return -1;
	
	list_for_each(pos, &ba->bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BULE");
		return -1;
	}

	mcoa_interface_alive(bid, 1);

	return default_update_mn();
}

/*!
 * \brief Update the default interfaces by a change on a BULE
 *
 * BULE change means, uninitialized state. The interface will be again alive
 * if the BA will be received for it. It signs the corresponding 
 * interface as not-alive, and calls the default_update_mn function
 * to update the environment regarding to the new conditions.
 * \param bule BULE
 * \return Zero if OK
 */		
int default_update_by_bule_change(struct bule * bule)
{
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;

	if (bule == NULL)
		return -1;

	list_for_each(pos, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BULE");
		return -1;
	}

	mcoa_interface_alive(bid, 0);

	return default_update_mn();
}

/*!
 * \brief Update the default interfaces by a deletion of a BULE
 *
 * It signs the corresponding interface as not-alive, and calls 
 * the default_update_mn function to update the environment regarding 
 * to the new conditions.
 * \param arg BULE
 * \return Zero if OK
 */	
int default_update_by_bule_delete(void * arg)
{
	struct bule * bule = (struct bule *)arg;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;

	if (bule == NULL)
		return -1;

	list_for_each(pos, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BULE");
		return -1;
	}

	mcoa_interface_alive(bid, 0);

	refcnt_put(&bule->refcnt);

	return default_update_mn();
}

/*!
 * \brief Initialize default selection engine on the HA
 *
 * It subscribes for a few events, and creates the initial
 * netfilter rules
 * \return Zero if OK
 */
int default_init_ha() 
{
	int ret;
	struct in6_addr addr;

	struct msg_env_nf_chain nfc;
	struct msg_env_nf_jump nfj;

	cleanup_in_progress = 0;

	ret = imsg_event_subscribe(mcoa_opts.msg_ids.data, EVT_DATA_NEW_BCE, mcoa_opts.msg_ids.me);
	if (ret) {
		ERROR("Unable to register for events");
		return ret;
	}

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.ctrl, MSG_CTRL_GET_HAA,
			NULL, 0, &addr, sizeof(addr));
	if (ret) {
		ERROR("Unable to get address from control module");
		return -1;
	}

	memset(&nfc, 0, sizeof(nfc));
	strncpy(nfc.name, "MIP6D_NG_FB", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_CHAIN,
			&nfc, sizeof(nfc), NULL, 0);
	if (ret) {
		ERROR("Unable to create ip6tables chain MIP6D_NG_FB");
		return -1;
	}

	memset(&nfj, 0, sizeof(nfj));
	strncpy(nfj.target, "MIP6D_NG_FB", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP,
			&nfj, sizeof(nfj), NULL, 0);
	if (ret) {
		ERROR("Unable to create ip6tables rule: to MIP6D_NG_FB");
		return -1;
	}

	memset(&nfj, 0, sizeof(nfj));
	memcpy(&nfj.src, &addr, sizeof(struct in6_addr));
	nfj.src_plen = 128;
	nfj.proto = IPPROTO_MH;
	strncpy(nfj.target, "ACCEPT", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP,
			&nfj, sizeof(nfj), NULL, 0);
	if (ret) {
		ERROR("Unable to create ip6tables rule: to ACCEPT MH");
		return -1;
	}

	return 0;
}

/*!
 * \brief Clean default selection engine on the HA
 *
 * It unsubscribes from a few events, and deletes the initial
 * netfilter rules
 * \return Zero if OK
 */
int default_clean_ha() 
{
	int ret;
	struct in6_addr addr;

	struct msg_env_nf_chain nfc;
	struct msg_env_nf_jump nfj;

	cleanup_in_progress = 1;

	imsg_event_unsubscribe(mcoa_opts.msg_ids.data, EVT_DATA_NEW_BCE, mcoa_opts.msg_ids.me);

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.ctrl, MSG_CTRL_GET_HAA,
			NULL, 0, &addr, sizeof(addr));
	if (ret) {
		ERROR("Unable to get address from control module");
		return -1;
	}

	memset(&nfj, 0, sizeof(nfj));
	strncpy(nfj.target, "MIP6D_NG_FB", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
			&nfj, sizeof(nfj), NULL, 0);
	if (ret) {
		ERROR("Unable to delete ip6tables rule: to MIP6D_NG_FB");
		return -1;
	}

	memset(&nfj, 0, sizeof(nfj));
	memcpy(&nfj.src, &addr, sizeof(struct in6_addr));
	nfj.src_plen = 128;
	nfj.proto = IPPROTO_MH;
	strncpy(nfj.target, "ACCEPT", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
			&nfj, sizeof(nfj), NULL, 0);
	if (ret) {
		ERROR("Unable to delete ip6tables rule: to ACCEPT MH");
		return -1;
	}

	memset(&nfc, 0, sizeof(nfc));
	strncpy(nfc.name, "MIP6D_NG_FB", sizeof(nfc.name));
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_CHAIN_DEL,
			&nfc, sizeof(nfc), NULL, 0);
	if (ret) {
		ERROR("Unable to delete ip6tables chain MIP6D_NG_FB");
		return -1;
	}

	return 0;
}

/*!
 * \brief Argument of default_select_best_bce
 */
struct default_select_best_bce_arg {
	/*! Home Address */
	struct in6_addr hoa;
	/*! Current calculated best preference value */
	uint16_t best_preference;
	/*! BID value to the interface with bets preference */
	uint16_t best_bid;
};

/*!
 * \brief Helper function to find the best BCE for a given MN
 *
 * It gets the BID extension, and checks the preference value. 
 * The lowest preference value means the highest priority.
 * The interface with the lowest preference value will be selected.
 * The argument contains the current best values between the 
 * iterations.
 * \param bce BCE to check
 * \param arg struct default_select_best_bce_arg
 * \return Zero
 */
static int default_select_best_bce(struct bce * bce, void * arg)
{
	struct default_select_best_bce_arg * sela = (struct default_select_best_bce_arg *)arg;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	unsigned char preference = 0;

	if (sela == NULL)
		return -1;

	if (bce == NULL)
		return 0;

	bce_lock(bce);

	if (memcmp(&bce->hoa, &sela->hoa, sizeof(struct in6_addr)) != 0)
		goto out;

	list_for_each(pos, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
			preference = ext_bid->preference;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BCE: " IP6ADDR_FMT, IP6ADDR_TO_STR(&bce->hoa));
		goto out;
	}

	if (preference < sela->best_preference) {
		sela->best_bid = bid;
		sela->best_preference = preference;
	}

out:

	bce_unlock(bce);

	return 0;
}

/*!
 * \brief Update the default interface on the HA
 *
 * It gets the best interface by MSG_DATA_ITERATE_BC, with the 
 * default_select_best_bce selector function. Next it
 * updates the environment regarding to this.
 * It is a thread function to avoid blocking.
 * \param arg struct inet6_addr Home Address 
 * \return Zero if OK
 */
static void * __default_update_ha(void * arg)
{
	int ret;
	struct in6_addr * hoa = (struct in6_addr *)arg;
	struct msg_data_iterate_bc bci;
	struct default_select_best_bce_arg sela;
	struct list_head * pos;
	struct default_ha_bid * habid = NULL;
	struct msg_env_nf_jump nfj;

	if (hoa == NULL)
		return NULL;

	memcpy(&sela.hoa, hoa, sizeof(struct in6_addr));
	sela.best_preference = 255;
	sela.best_bid = 0;
	memset(&bci, 0, sizeof(bci));
	bci.fp = default_select_best_bce;
	bci.arg = &sela;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.data, MSG_DATA_ITERATE_BC,
			&bci, sizeof(bci), NULL, 0);
	if (ret < 0) {
		ERROR("Unable to find best BCE (itereate)");
		goto out;
	}

	//DEBUG("Best BCE for " IP6ADDR_FMT " BID %u", IP6ADDR_TO_STR(hoa), sela.best_bid);

	memset(&nfj, 0, sizeof(nfj));

	pthread_mutex_lock(&default_ha_bids_lock);

	list_for_each(pos, &current_default_ha_bids) {
		habid = list_entry(pos, struct default_ha_bid, list);

		if (memcmp(&habid->hoa, hoa, sizeof(struct in6_addr)) != 0) {
			habid = NULL;
			continue;
		} else
			break;
	}

	if (sela.best_bid == 0) {
		if (habid != NULL && habid->default_bid > 0) {
			snprintf(nfj.target, sizeof(nfj.target), "M6_NG_HOA%04x_BID%u", ntohs(habid->hoa.s6_addr16[7]), habid->default_bid);
			ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
					&nfj, sizeof(nfj), NULL, 0);
			if (ret == 0)
				habid->default_bid = 0;
		}
	} else {
		if (habid == NULL || (habid != NULL && habid->default_bid != sela.best_bid)) {
			if (habid != NULL && habid->default_bid > 0) {
				snprintf(nfj.target, sizeof(nfj.target), "M6_NG_HOA%04x_BID%u", ntohs(habid->hoa.s6_addr16[7]), habid->default_bid);
				ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
						&nfj, sizeof(nfj), NULL, 0);
				if (ret == 0)
					habid->default_bid = 0;
			}
			if (! cleanup_in_progress) {
				snprintf(nfj.target, sizeof(nfj.target), "M6_NG_HOA%04x_BID%u", ntohs(hoa->s6_addr16[7]), sela.best_bid);
				ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP,
						&nfj, sizeof(nfj), NULL, 0);
				if (ret == 0) {
					if (habid == NULL) {
						habid = malloc(sizeof(struct default_ha_bid));
						if (habid != NULL) {
							memcpy(&habid->hoa, hoa, sizeof(struct in6_addr));
							list_add(&habid->list, &current_default_ha_bids);
						}
					}
					if (habid != NULL) 
						habid->default_bid = sela.best_bid;
				}
			}
		}
	}

	if (ret == 0) 
		DEBUG("MCoA: New default interface with BID %u for HOA " IP6ADDR_FMT, sela.best_bid, IP6ADDR_TO_STR(hoa));

	pthread_mutex_unlock(&default_ha_bids_lock);

out:

	free(hoa);

	return NULL;
}

/*!
 * \brief Update the default interface on the HA
 *
 * Asynchronous function, for details please see __default_update_ha
 * \param hoa Home Address 
 * \return Zero if OK
 */
static int default_update_ha(struct in6_addr * hoa)
{
	struct in6_addr * hoa_p = NULL;
	pthread_t th;

	if (hoa == NULL)
		return -1;

	hoa_p = malloc(sizeof(struct in6_addr));
	if (hoa_p == NULL)
		return -1;

	memcpy(hoa_p, hoa, sizeof(struct in6_addr));

	th = threading_create(__default_update_ha, hoa_p);

	if (th != (pthread_t)0)
		return 0;
	else
		return -1;
}

/*!
 * \brief Update the default interfaces by a deletion of a BCE
 *
 * It deletes the created netfilter rules, and selects new default.
 * \param arg BCE
 * \return Zero if OK
 */	
static int default_update_ha_by_delete(void * arg)
{
	int ret;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct msg_env_nf_mark_and_acc nfm;
	struct msg_env_nf_to_mip6d_ng nf;
	struct default_ha_bid * habid = NULL;
	struct msg_env_nf_jump nfj;
	struct bce * bce = (struct bce *)arg;

	if (bce == NULL)
		return 0;

	bce_lock(bce);

	list_for_each(pos, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
			ext_bid = 0;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BCE: " IP6ADDR_FMT, IP6ADDR_TO_STR(&bce->hoa));
		goto out;
	}

	pthread_mutex_lock(&default_ha_bids_lock);

	list_for_each(pos, &current_default_ha_bids) {
		habid = list_entry(pos, struct default_ha_bid, list);

		if (memcmp(&habid->hoa, &bce->hoa, sizeof(struct in6_addr)) != 0) {
			habid = NULL;
			continue;
		} else
			break;
	}
	
	if (habid != NULL && habid->default_bid == bid) {
		memset(&nfj, 0, sizeof(nfj));
		snprintf(nfj.target, sizeof(nfj.target), "M6_NG_HOA%04x_BID%u", ntohs(habid->hoa.s6_addr16[7]), habid->default_bid);
		ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_JUMP_DEL,
				&nfj, sizeof(nfj), NULL, 0);
		if (ret == 0)
			habid->default_bid = 0;
	}

	pthread_mutex_unlock(&default_ha_bids_lock);

	default_update_ha(&bce->hoa);

	memset(&nfm, 0, sizeof(nfm));
	snprintf(nfm.name, sizeof(nfm.name), "HOA%04x_BID%u", ntohs(bce->hoa.s6_addr16[7]), bid);
	nfm.mark = bid;
	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_MARK_AND_ACC_DEL,
			&nfm, sizeof(nfm), NULL, 0);
	if (ret) {
		ERROR("Unable to delete ip6tables mark rule for BID %u: " IP6ADDR_FMT, bid, IP6ADDR_TO_STR(&bce->hoa));
		goto out;
	}

	if (bce->hoa_cnt <= 1) {
		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "OUTPUT", sizeof(nf.chain));
		memcpy(&nf.dst, &bce->hoa, sizeof(struct in6_addr));
		nf.dst_plen = 128;
		ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG_DEL,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to delete ip6tables rule from OUTPUT to mip6d-ng: " IP6ADDR_FMT, IP6ADDR_TO_STR(&bce->hoa));
		}

		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "PREROUTING", sizeof(nf.chain));
		memcpy(&nf.dst, &bce->hoa, sizeof(struct in6_addr));
		nf.dst_plen = 128;
		ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG_DEL,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to delete ip6tables rule from OUTPUT to mip6d-ng: " IP6ADDR_FMT, IP6ADDR_TO_STR(&bce->hoa));
		}
	}

	bce_unlock(bce);
	
	return 0;
		
out:

	bce_unlock(bce);

	return -1;
}

/*!
 * \brief Initialize new BCE
 *
 * It allocates and adds BCE BID extension, and creates
 * initial netfilter rules.
 * \param bce BCE
 * \return Zero if OK
 */
int default_init_bce(struct bce * bce)
{
	int ret;
	struct msg_env_nf_to_mip6d_ng nf;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct msg_env_nf_mark_and_acc nfm;

	hooks_add(bce->del_hook_id, 2, default_update_ha_by_delete);

	if (bce->hoa_cnt == 0) {
		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "OUTPUT", sizeof(nf.chain));
		memcpy(&nf.dst, &bce->hoa, sizeof(struct in6_addr));
		nf.dst_plen = 128;
		ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to create ip6tables rule from OUTPUT to mip6d-ng: " IP6ADDR_FMT, IP6ADDR_TO_STR(&bce->hoa));
			return -1;
		}

		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "PREROUTING", sizeof(nf.chain));
		memcpy(&nf.dst, &bce->hoa, sizeof(struct in6_addr));
		nf.dst_plen = 128;
		ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to create ip6tables rule from OUTPUT to mip6d-ng: " IP6ADDR_FMT, IP6ADDR_TO_STR(&bce->hoa));
			return -1;
		}
	}

	list_for_each(pos, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BCE: " IP6ADDR_FMT, IP6ADDR_TO_STR(&bce->hoa));
		return -1;
	}

	memset(&nfm, 0, sizeof(nfm));
	snprintf(nfm.name, sizeof(nfm.name), "HOA%04x_BID%u", ntohs(bce->hoa.s6_addr16[7]), bid);
	nfm.mark = bid;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_NF_MARK_AND_ACC,
			&nfm, sizeof(nfm), NULL, 0);
	if (ret) {
		ERROR("Unable to create ip6tables mark rule for BID %u: " IP6ADDR_FMT, bid, IP6ADDR_TO_STR(&bce->hoa));
		return -1;
	}

	default_update_ha(&bce->hoa);

	return 0;
}

/*! \} */
