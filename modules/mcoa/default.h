
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-mcoa
 * \{
 */

#ifndef _DEFAULT_H_
#define _DEFAULT_H_

#include <mip6d-ng/list.h>
#include <inttypes.h>

int default_init_mn();
int default_clean_mn();
int default_update_by_ba(struct evt_ccom_ba * ba);
int default_update_by_bule_change(struct bule * bule);
int default_update_by_bule_delete(void * arg);

int default_init_ha();
int default_clean_ha();
int default_init_bce(struct bce * bce);

/*!
 * \brief List of default BIDs per Mobile Node
 */
struct default_ha_bid {
	/*! Linked list management */
	struct list_head list;
	/*! Home Address, identify the Mobile Node */
	struct in6_addr hoa;
	/*! Current default BID */
	uint16_t default_bid;
};

#endif /* _DEFAULT_H_ */

/*! \} */

