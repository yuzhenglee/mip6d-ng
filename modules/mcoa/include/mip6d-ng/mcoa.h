
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-mcoa
 * \{
 */

#ifndef MIP6D_NG_MCOA_H
#define MIP6D_NG_MCOA_H

#include <inttypes.h>

/*! 
 * \brief The name of the internal messaging reference point
 */
#define MSG_MCOA		"mcoa"

/*!
 * \brief Module load order number of control module
 */
#define SEQ_MCOA		50

/*!
 * \brief Available mcoa API message commands
 */
enum mcoa_messages {
	/*!
	 * \brief Set interface preference
	 *
	 * Argument: struct msg_mcoa_set_preference
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_MCOA_SET_PREFERENCE,
	/*! 
	 * \brief Get the number of available interfaces
	 *
	 * Argument: none (it could to be NULL)
	 *
	 * Reply argument: none (it could to be NULL)
	 *
	 * Return value: the number of the interfaces 
	 */
	MSG_MCOA_GET_NUM,
	/*!
	 * \brief Get the current preference value of an interfaces
	 *
	 * Argument: uint16_t, the BID value
	 *
	 * Reply argument: none (it could to be NULL)
	 *
	 * Return value: the preference value
	 */
	MSG_MCOA_GET_PREFERENCE,
	/*!
	 * \brief Get the Binding Update list, used by API module
	 *
	 * Argument: struct imsg_arg_reply_len
	 *
	 * Reply argument: unsigned char *
	 */
	MSG_MCOA_API_GET_BUL,
	/*!
	 * \brief Get the Binding Cache, used by API module
	 *
	 * Argument: struct imsg_arg_reply_len
	 *
	 * Reply argument: unsigned char *
	 */
	MSG_MCOA_API_GET_BC,
	MSG_MCOA_NONE
};

/*!
 * \brief Set the preference value of a given interface
 */
struct msg_mcoa_set_preference {
	/*! Interface index */
	int ifindex;
	/*! New preference value */
	unsigned char preference;
};

/*!
 * \brief Available mcoa API commands by the api module 
 * \ingroup eapi-mcoa
 */
enum mcoa_api_commands {
	/*! Getting Binding Update List entries */
	API_MCOA_GET_BUL,
	/*! Getting Binding Cache entries */
	API_MCOA_GET_BC,
	API_MCOA_NONE
};

/*!
 * \brief API commands by the api module: get BUL
 * \ingroup eapi-mcoa
 */
struct mcoa_api_get_bul {
	/*! Destination (i.e. Home Agent address) */
	struct in6_addr dest;
	/*! Home Address */
	struct in6_addr hoa;
	/*! Cate-of Address */
	struct in6_addr coa;
	/*! Lifetime */
	uint16_t lifetime;
	/*! Remaining lifetime */
	uint16_t lifetime_rem;
	/*! Receiving time of the last BU */
	int32_t last_bu;
	/*! Flags */
	uint32_t flags;
	/*! Interface index */
	uint32_t ifindex;
	/*! Binding Identifier */
	uint16_t bid;
	/*! Priority */
	uint8_t prio;
} __attribute__((packed));

/*!
 * \brief API commands by the api module: get BC
 * \ingroup eapi-mcoa
 */
struct mcoa_api_get_bc {
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! Lifetime */
	uint16_t lifetime;
	/*! Flags */
	uint32_t flags;
	/*! Binding Identifier */
	uint16_t bid;
	/*! Priority */
	uint8_t prio;
} __attribute__((packed));

#endif /* MIP6D_NG_MCOA_H */

/*! \} */
