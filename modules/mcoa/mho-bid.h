
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-mcoa
 * \{
 */

#ifndef MIP6D_NG_MHO_BID_H_
#define MIP6D_NG_MHO_BID_H_

int mho_bid_create(struct iovec * iov, uint16_t bid, uint8_t status, uint8_t flags, struct in6_addr * coa);
int mho_bid_init();

#endif /* MIP6D_NG_MHO_BID_H_ */

/*! \} */
