
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2013 Commsignia Ltd. <support@commsignia.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-nemo
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>
#include <pthread.h>

#include <net/if.h>
#include <arpa/inet.h>
#include <linux/if_addr.h>
#include <netinet/icmp6.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/const.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/tasks.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/missing.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bce.h>
#include <mip6d-ng/nemo-bce.h>
#include <mip6d-ng/mho-mnp.h>

#include "main.h"
#include "ha.h"

/*!
 * \brief Creating environmental stuff for data packets
 *
 * It creates the outgoing and incoming tunnel's XFRM representation.
 * If do_local_stuff is non-zero, it creates NULL XFRM decapsulation rules.
 *
 * \param local Local address (home agent address)
 * \param remote Destination address (mobile network prefix)
 * \param remote_plen Destination prefix length
 * \param coa Care-of Address
 * \param mark MARK value if exists (otherwise zero)
 * \param do_local_stuff Creates local-only stuff
 * \return Zero if OK
 */
static int nemo_ha_data_env_create(struct in6_addr * local, struct in6_addr * remote, uint8_t remote_plen, struct in6_addr * coa, unsigned int mark, char do_local_stuff)
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;
	
	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.daddr, remote, sizeof(i6t.daddr));
	i6t.dplen = remote_plen;
	if (mark > 0)
		i6t.mark =mark;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, local, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, coa, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_OUT;
	i6t.skip_state = 1;
	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	if (do_local_stuff) {
		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
		i6t.splen = remote_plen;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_IN;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env,
				MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
		if (ret) {
			ERROR("Unable to create IP6IP6 XFRM tunnel");
			return -1;
		}

		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
		i6t.splen = remote_plen;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_FWD;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env,
				MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
		if (ret) {
			ERROR("Unable to create IP6IP6 XFRM tunnel");
			return -1;
		}
	}

	return 0;
}

/*!
 * \brief Deleting environmental stuff for data packets
 *
 * It deletes the outgoing and incoming tunnel's XFRM representation.
 * If do_local_stuff is non-zero, it deletes NULL XFRM decapsulation rules.
 *
 * \param local Local address (home agent address)
 * \param remote Destination address (home address)
 * \param remote_plen Destination prefix length
 * \param coa Care-of Address
 * \param mark MARK value if exists (otherwise zero)
 * \param do_local_stuff Creates local-only stuff
 * \return Zero if OK
 */
static int nemo_ha_data_env_clean(struct in6_addr * local, struct in6_addr * remote, uint8_t remote_plen, struct in6_addr * coa, unsigned int mark, char do_local_stuff)
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;
	
	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.daddr, remote, sizeof(i6t.daddr));
	i6t.dplen = remote_plen;
	if (mark > 0)
		i6t.mark = mark;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, local, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, coa, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_OUT;
	i6t.skip_state = 1;
	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to delete IP6IP6 XFRM tunnel");
		return -1;
	}

	if (do_local_stuff) {
		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
		i6t.splen = remote_plen;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_IN;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env,
				MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
		if (ret) {
			ERROR("Unable to create IP6IP6 XFRM tunnel");
			return -1;
		}

		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
		i6t.splen = remote_plen;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_FWD;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env,
				MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
		if (ret) {
			ERROR("Unable to create IP6IP6 XFRM tunnel");
			return -1;
		}
	}
	
	return 0;
}

/*!
 * \brief Handle Binding Cache deletion
 *
 * Deletes the environmental data stuff
 *
 * \param arg BCE
 * \return Zero if OK
 */
static int nemo_ha_bce_del(void * arg)
{
	int ret;
	struct bce * bce = (struct bce *)arg;
	struct in6_addr haa;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_mnp * ext_mnp = NULL;
	struct msg_env_nf_to_mip6d_ng nf;

	if (bce == NULL)
		return 0;

	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.ctrl, MSG_CTRL_GET_HAA,
			NULL, 0, &haa, sizeof(haa));
	if (ret) {
		ERROR("Unable to get HAA from control module");
		return -1;
	}

	bce_lock(bce);
	list_for_each(pos, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bce_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BCE");
		bce_unlock(bce);
		return -1;
	}

	if (bce->hoa_cnt == 0 && nemo_opts.msg_ids.mcoa > 0) {
		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "OUTPUT", sizeof(nf.chain));
		memcpy(&nf.dst, &ext_mnp->mnp, sizeof(struct in6_addr));
		nf.dst_plen = ext_mnp->plen;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG_DEL,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to delete ip6tables rule from OUTPUT to mip6d-ng: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ext_mnp->mnp));
			return -1;
		}

		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "PREROUTING", sizeof(nf.chain));
		memcpy(&nf.dst, &ext_mnp->mnp, sizeof(struct in6_addr));
		nf.dst_plen = ext_mnp->plen;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG_DEL,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to delete ip6tables rule from OUTPUT to mip6d-ng: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ext_mnp->mnp));
			return -1;
		}
	}

	nemo_ha_data_env_clean(&haa, &ext_mnp->mnp, ext_mnp->plen, &bce->coa, bce->mark, (bce->hoa_cnt <= 1) ? 1 : 0);

	bce_unlock(bce);

	return 0;
}

/*!
 * \brief Handle BCE initialization
 *
 * It creates the environmental data stuff, and registers the
 * nemo module's deletion handler function
 *
 * \param bce BCE
 * \return Zero if OK
 */
int nemo_ha_init_bce(struct bce * bce)
{
	int ret;
	struct in6_addr haa;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_mnp * ext_mnp = NULL;
	struct msg_env_nf_to_mip6d_ng nf;

	DEBUG("NEMO: Processing BCE init...");

	ret = hooks_add(bce->del_hook_id, 15, nemo_ha_bce_del);

	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.ctrl, MSG_CTRL_GET_HAA,
			NULL, 0, &haa, sizeof(haa));
	if (ret) {
		ERROR("Unable to get HAA from control module");
		return -1;
	}

	bce_lock(bce);
	list_for_each(pos, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bce_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BCE");
		bce_unlock(bce);
		return -1;
	}

	ret = nemo_ha_data_env_create(&haa, &ext_mnp->mnp, ext_mnp->plen, &bce->coa, bce->mark, (bce->hoa_cnt == 0) ? 1 : 0);
	if (ret) {
		bce_unlock(bce);
		return ret;
	}

	if (bce->hoa_cnt == 0 && nemo_opts.msg_ids.mcoa > 0) {
		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "OUTPUT", sizeof(nf.chain));
		memcpy(&nf.dst, &ext_mnp->mnp, sizeof(struct in6_addr));
		nf.dst_plen = ext_mnp->plen;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to create ip6tables rule from OUTPUT to mip6d-ng: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ext_mnp->mnp));
			bce_unlock(bce);
			return -1;
		}

		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "PREROUTING", sizeof(nf.chain));
		memcpy(&nf.dst, &ext_mnp->mnp, sizeof(struct in6_addr));
		nf.dst_plen = ext_mnp->plen;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to create ip6tables rule from OUTPUT to mip6d-ng: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ext_mnp->mnp));
			bce_unlock(bce);
			return -1;
		}
	}

	bce_unlock(bce);

	return 0;
}

/*!
 * \brief Handle BCE update
 *
 * It deletes the environmental data stuff, and
 * re-creates it with the new values
 *
 * \param bce BCE
 * \return Zero if OK
 */
int nemo_ha_update_bce(struct bce * bce)
{
	int ret;
	struct in6_addr dummy;
	struct in6_addr haa;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_mnp * ext_mnp = NULL;

	memset(&dummy, 0, sizeof(struct in6_addr));

	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.ctrl, MSG_CTRL_GET_HAA,
			NULL, 0, &haa, sizeof(haa));
	if (ret) {
		ERROR("Unable to get HAA from control module");
		return -1;
	}

	bce_lock(bce);
	list_for_each(pos, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bce_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BCE");
		bce_unlock(bce);
		return -1;
	}

	if (memcmp(&bce->prev_coa, &dummy, sizeof(struct in6_addr)) != 0 && memcmp(&bce->prev_coa, &bce->coa, sizeof(struct in6_addr)) != 0) {
		ret = nemo_ha_data_env_clean(&haa, &ext_mnp->mnp, ext_mnp->plen, &bce->prev_coa, bce->mark, (bce->hoa_cnt == 0) ? 1 : 0);
		if (ret) {
			ERROR("Unable to clean previous DATA environment of this Binding");
			bce_unlock(bce);
			return ret;
		}

		ret = nemo_ha_data_env_create(&haa, &ext_mnp->mnp, ext_mnp->plen, &bce->coa, bce->mark, (bce->hoa_cnt == 0) ? 1 : 0);
		if (ret) {
			bce_unlock(bce);
			return ret;
		}
	}

	bce_unlock(bce);

	return 0;
}

/*!
 * \brief Extend the Binding Acknowledge message with MNP option
 *
 * \param param struct send_hook_ba_param
 * \return Zero if OK
 */
int nemo_ha_extend_ba(void * param)
{
	struct send_hook_ba_param * hookp = (struct send_hook_ba_param *)param;
	struct ip6_mh_ba * ba;
	uint8_t flags;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_mnp * ext_mnp = NULL;
	
	if (hookp == NULL || hookp->iov == NULL || hookp->iov_count+1 >= hookp->iov_len || hookp->bce == NULL) 
		return -1;

	list_for_each(pos, &hookp->bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bce_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BCE");
		return -1;
	}

	DEBUG("Extend BA with MNP option: " IP6ADDR_FMT "/%u....", IP6ADDR_TO_STR(&ext_mnp->mnp), ext_mnp->plen);

	ba = (struct ip6_mh_ba *)((hookp->iov[0]).iov_base);
	flags = ba->ip6mhba_flags;
	flags |= MH_BA_F_R;
	ba->ip6mhba_flags = flags;

	return 0;
}

/*!
 * \brief Argument of __nemo_ha_bce_api_get
 */
struct nemo_ha_bce_api_get_arg {
	struct nemo_api_get_bc * bc;
	unsigned int bnum;
};

/*!
 * \brief Iterate BC to produce reply to API
 *
 * It is a callback function for the MSG_DATA_ITERATE_BC command.
 * It gets the BCE and the BID extension, and add all of them to the 
 * array in the argument.
 *
 * \param bce BCE
 * \param arg struct mcoa_bce_api_get_arg
 * \return Zero if OK
 */
int __nemo_ha_bce_api_get(struct bce * bce, void * arg)
{
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_mnp * ext_mnp = NULL;
	struct nemo_ha_bce_api_get_arg * apia = (struct nemo_ha_bce_api_get_arg *)arg;

	if (bce == NULL || apia == NULL)
		return 0;

	apia->bc = realloc(apia->bc, (apia->bnum + 1) * sizeof(struct nemo_api_get_bc));
	if (apia->bc == NULL) {
		ERROR("Out of memory: Unable to alloc get_bc command answer");
		return -1;
	}

	memcpy(&apia->bc[apia->bnum].hoa, &bce->hoa, sizeof(struct in6_addr));
	memcpy(&apia->bc[apia->bnum].coa, &bce->coa, sizeof(struct in6_addr));
	apia->bc[apia->bnum].lifetime = htons(bce->lifetime);
	apia->bc[apia->bnum].flags = htonl(bce->flags);

	list_for_each(pos, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bce_ext_mnp *)ext;
			memcpy(&apia->bc[apia->bnum].mnp, &ext_mnp->mnp, sizeof(struct in6_addr));
			apia->bc[apia->bnum].mnp_len = ext_mnp->plen;
			apia->bnum++;
			break;
		}
	}
	
	return 0;
}

/*!
 * \brief API function to get Binding Cache entries
 * \param reply_msg Reply message to construct
 * \param mlen Maximum length of reply_msg
 * \return Length of reply_message if OK, otherwise negative
 */
int nemo_ha_bce_api_get(unsigned char * reply_msg, unsigned int mlen)
{
	int ret;
	struct msg_data_iterate_bc bci;
	struct nemo_ha_bce_api_get_arg apia;

	memset(&apia, 0, sizeof(apia));
	memset(&bci, 0, sizeof(bci));
	bci.fp = __nemo_ha_bce_api_get;
	bci.arg = &apia;

	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.data, MSG_DATA_ITERATE_BC,
			&bci, sizeof(bci), NULL, 0);
	if (ret < 0) {
		ERROR("Unable to iterate BC");
		return ret;
	}

	if (apia.bnum == 0)
		return 0;

	if (reply_msg != NULL && mlen > 0) {
		if (mlen > apia.bnum * sizeof(struct nemo_api_get_bc))
			mlen = apia.bnum * sizeof(struct nemo_api_get_bc);
		memcpy(reply_msg, (unsigned char *)apia.bc, mlen);
		
		free(apia.bc);

		return apia.bnum * sizeof(struct nemo_api_get_bc);
	}

	if (apia.bc != NULL)
		free(apia.bc);
	
	return -1;
}

/*! \} */
