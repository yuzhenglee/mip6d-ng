
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2013 Commsignia Ltd. <support@commsignia.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-mcoa
 * \{
 */

#ifndef MIP6D_NG_NEMO_H
#define MIP6D_NG_NEMO_H

#include <inttypes.h>

/*! 
 * \brief The name of the internal messaging reference point
 */
#define MSG_NEMO		"nemo"

/*!
 * \brief Module load order number of control module
 */
#define SEQ_NEMO		51

/*!
 * \brief Available mcoa API message commands
 */
enum nemo_messages {
	/*!
	 * \brief Get the Binding Update list, used by API module
	 *
	 * Argument: struct imsg_arg_reply_len
	 *
	 * Reply argument: unsigned char *
	 */
	MSG_NEMO_API_GET_BUL,
	/*!
	 * \brief Get the Binding Cache, used by API module
	 *
	 * Argument: struct imsg_arg_reply_len
	 *
	 * Reply argument: unsigned char *
	 */
	MSG_NEMO_API_GET_BC,
	MSG_NEMO_NONE
};

/*!
 * \brief Available nemo API commands by the api module 
 * \ingroup eapi-nemo
 */
enum nemo_api_commands {
	/*! Getting Binding Update List entries */
	API_NEMO_GET_BUL,
	/*! Getting Binding Cache entries */
	API_NEMO_GET_BC,
	API_NEMO_NONE
};

/*!
 * \brief API commands by the api module: get BUL
 * \ingroup eapi-nemo
 */
struct nemo_api_get_bul {
	/*! Destination (i.e. Home Agent address) */
	struct in6_addr dest;
	/*! Home Address */
	struct in6_addr hoa;
	/*! Cate-of Address */
	struct in6_addr coa;
	/*! Lifetime */
	uint16_t lifetime;
	/*! Remaining lifetime */
	uint16_t lifetime_rem;
	/*! Receiving time of the last BU */
	int32_t last_bu;
	/*! Flags */
	uint32_t flags;
	/*! Interface index */
	uint32_t ifindex;
	/*! Mobile Network Prefix */
	struct in6_addr mnp;
	/*! Mobile Network Prefix Length */
	uint8_t mnp_len;
} __attribute__((packed));

/*!
 * \brief API commands by the api module: get BC
 * \ingroup eapi-nemo
 */
struct nemo_api_get_bc {
	/*! Home Address */
	struct in6_addr hoa;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! Lifetime */
	uint16_t lifetime;
	/*! Flags */
	uint32_t flags;
	/*! Mobile Network Prefix */
	struct in6_addr mnp;
	/*! Mobile Network Prefix Length */
	uint8_t mnp_len;
} __attribute__((packed));

#endif /* MIP6D_NG_NEMO_H */

/*! \} */
