
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2013 Commsignia Ltd. <support@commsignia.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-nemo
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/control.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/environment.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/api.h>
#include <mip6d-ng/mcoa.h>

#include "mho-mnp.h"
#include "mn.h"
#include "ha.h"
#include "main.h"

/*!
 * \brief NEMO configuration options
 *
 * See config sample for details!
 */

cfg_opt_t nemo_mnp_opts[] = {
	CFG_STR("mobile-network-prefix", "", CFGF_NONE),
	CFG_END()
};

/*!
 * \brief NEMO configuration options
 *
 * See config sample for details!
 */
cfg_opt_t nemo_main_opts[] = {
	CFG_SEC("NEMO", nemo_mnp_opts, CFGF_NODEFAULT),
	CFG_END()
};

struct nemo_opts nemo_opts;

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the nemo module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int nemo_proc_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	struct imsg_arg_reply_len * reply_len;
	unsigned char * msg;
	size_t mlen;
	int retval = -1;
	
	switch (message) {
	case MSG_NEMO_API_GET_BUL:
		reply_len = (struct imsg_arg_reply_len *)arg;
		if (reply_len == NULL)
			break;
		mlen = reply_len->ralen;
		msg = (unsigned char *)reply_arg;
		if (msg != NULL) {
			retval = nemo_mn_bule_api_get(msg, mlen);
		}
		break;
	case MSG_NEMO_API_GET_BC:
		reply_len = (struct imsg_arg_reply_len *)arg;
		if (reply_len == NULL)
			break;
		mlen = reply_len->ralen;
		msg = (unsigned char *)reply_arg;
		if (msg != NULL) {
			retval = nemo_ha_bce_api_get(msg, mlen);
		}
		break;
	}

	return retval;
}

/*!
 * \brief Event processing
 *
 * For details, please read the API documentation of the nemo module
 * \param sender Sender module ID
 * \param event Event ID
 * \param arg Event argument
 * \param need_to_free Non-zero if arg should to be freed
 */
static void nemo_proc_event(unsigned int sender, unsigned int event, void * arg, unsigned char need_to_free)
{
	struct bule * bule;
	struct bce * bce;
	struct evt_ccom_ba * ba;

	if (sender == nemo_opts.msg_ids.data) {
		switch (event) {
		case EVT_DATA_INIT_BULE:
			bule = (struct bule *)arg;
			if (bule == NULL)
				break;
			nemo_mn_init_bule(bule);
			refcnt_put(&bule->refcnt);
			break;
		case EVT_DATA_CHANGE_BULE:
			bule = (struct bule *)arg;
			if (bule == NULL)
				break;
			nemo_mn_update_bule(bule);
			refcnt_put(&bule->refcnt);
			break;
		case EVT_DATA_NEW_BCE:
			bce = (struct bce *)arg;
			if (bce == NULL)
				break;
			nemo_ha_init_bce(bce);
			refcnt_put(&bce->refcnt);
			break;
		case EVT_DATA_CHANGE_BCE:
			bce = (struct bce *)arg;
			if (bce == NULL)
				break;
			nemo_ha_update_bce(bce);
			refcnt_put(&bce->refcnt);
			break;
		}
	} else if (sender == nemo_opts.msg_ids.ccom) {
		switch (event) {
		case EVT_CCOM_BA:
			ba = (struct evt_ccom_ba *)arg;
			if (ba == NULL)
				break;
			if (ba->bule == NULL)
				break;
			nemo_mn_ba_recv(ba);
			refcnt_put(&ba->bule->refcnt);

		}
	}

	if (need_to_free)
		free(arg);
}

static struct imsg_opts imsg_opts = {
	.event_fp = nemo_proc_event,
	.message_fp = nemo_proc_message
};

/*!
 * \brief Cleanup
 *
 * \return Zero
 */
static int nemo_exit_handler(void * arg)
{
	int ret;
	enum ctrl_node_type node_type;
	
	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.ctrl,
			MSG_CTRL_GET_NODE_TYPE, NULL, 0, &node_type, sizeof(node_type));
	if (ret || node_type == CTRL_NODE_TYPE_NONE) {
		ERROR("Unknown node type");
		return 0;
	}

	switch (node_type) {
	case CTRL_NODE_TYPE_HA:

		break;
	case CTRL_NODE_TYPE_MN:
		nemo_mn_clean();
		break;
	case CTRL_NODE_TYPE_CN:
		break;
	case CTRL_NODE_TYPE_NONE:
		//Avoid warning: warning: enumeration value ‘CTRL_NODE_TYPE_NONE’ not handled in switch
		break;
	}

	return 0;
}

/*!
 * \brief Initializing nemo module
 *
 * Registers exit function to the 'core-exit' hook.
 * Register internal messaging.
 * It subscribes for the necessary events and hooks.
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int nemo_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	cfg_t * node;
	char * mnp;
	char * mnp_len;
	int len, i;
	char * end;
	unsigned int core_exit_id;
	unsigned int create_bu_id;
	unsigned int create_ba_id;
	enum ctrl_node_type node_type;
	struct msg_api_register_cmd api_rc;

	DEBUG("Initializing module: nemo");

	ret = imsg_register(MSG_NEMO, &imsg_opts);
    if (ret < 0)
		return ret;
	nemo_opts.msg_ids.me = ret;

	ret = imsg_get_id(MSG_CTRL);
	if (ret < 0) {
		ERROR("Unable to find module: control");
		return -1;
	}
	nemo_opts.msg_ids.ctrl = ret;
	
	ret = imsg_get_id(MSG_DATA);
	if (ret < 0) {
		ERROR("Unable to find module: data");
		return -1;
	}
	nemo_opts.msg_ids.data = ret;

	ret = imsg_get_id(MSG_ENV);
	if (ret < 0) {
		ERROR("Unable to find module: environment");
		return -1;
	}
	nemo_opts.msg_ids.env = ret;

	ret = imsg_get_id(MSG_CCOM);
	if (ret < 0) {
		ERROR("Unable to find module: core-comm");
		return -1;
	}
	nemo_opts.msg_ids.ccom = ret;	

	ret = imsg_get_id(MSG_API);
	if (ret < 0) {
		DEBUG("Unable to find module: API");
		nemo_opts.msg_ids.api = -1;
	} else {
		nemo_opts.msg_ids.api = ret;
	}

	ret = imsg_get_id(MSG_MCOA);
	if (ret < 0) {
		nemo_opts.msg_ids.mcoa = -1;
	} else {
		nemo_opts.msg_ids.mcoa = ret;
	}

	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 100-SEQ_NEMO, nemo_exit_handler);
	}


	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.ctrl,
			MSG_CTRL_GET_NODE_TYPE, NULL, 0, &node_type, sizeof(node_type));
	if (ret || node_type == CTRL_NODE_TYPE_NONE) {
		ERROR("Unknown node type");
		return -1;
	}

	switch (node_type) {
	case CTRL_NODE_TYPE_HA:
		ret = imsg_event_subscribe(nemo_opts.msg_ids.data, EVT_DATA_NEW_BCE, nemo_opts.msg_ids.me);
		ret |= imsg_event_subscribe(nemo_opts.msg_ids.data, EVT_DATA_CHANGE_BCE, nemo_opts.msg_ids.me);
		if (ret < 0) {
			ERROR("Unable to subscribe for BCE/BU events");
			return -1;
		}
		ret = hooks_get_id("create-mh-ba");
		if (ret >= 0) {
			create_ba_id = (unsigned int)ret;
			hooks_add(create_ba_id, 1, nemo_ha_extend_ba);
		} else  {
			ERROR("Unable to register for create-mh-ba hook");
			return -1;
		}

		if (nemo_opts.msg_ids.api >= 0) {
			memset(&api_rc, 0, sizeof(api_rc));
			api_rc.command = API_NEMO_GET_BC;
			snprintf(api_rc.command_name, sizeof(api_rc.command_name), "NEMO_GET_BC");
			api_rc.imsg_cmd = MSG_NEMO_API_GET_BC;
			ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
			if (ret) {
				ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
			}
		}
		break;
	case CTRL_NODE_TYPE_MN:
		node = cfg_getsec(cfg, "NEMO");
		if (node == NULL) {
			ERROR("Missing config option: NEMO");
			return -1;
		}
		mnp = cfg_getstr(node, "mobile-network-prefix");
		mnp_len = NULL;
		len = strlen(mnp);
		if (mnp == NULL || len == 0) {
			ERROR("Missing mobile-network-prefix option");
			return -1;
		}
		for (i = 0; i < len; i++) {
			if (mnp[i] == '/') {
				if (len > i+1)
					mnp_len = &mnp[i+1];
				mnp[i] = '\0';
			}
		}
		ret = inet_pton(AF_INET6, mnp, &nemo_opts.mnp);
		if (ret != 1) {
			ERROR("Invalid mobile-network-prefix option: '%s'", mnp);
			return -1;
		}
		nemo_opts.mnp_len = strtoul(mnp_len, &end, 10);
		if (nemo_opts.mnp_len < 1 || nemo_opts.mnp_len > 127 || mnp_len == end) {
			ERROR("Invalid mobile-network-prefix option: prefix length: '%s'", mnp_len);
			return -1;
		}
		DEBUG("Mobile Network Prefix: " IP6ADDR_FMT "/%u", IP6ADDR_TO_STR(&nemo_opts.mnp), nemo_opts.mnp_len);

		ret = imsg_event_subscribe(nemo_opts.msg_ids.data, EVT_DATA_INIT_BULE, nemo_opts.msg_ids.me);
		ret |= imsg_event_subscribe(nemo_opts.msg_ids.data, EVT_DATA_CHANGE_BULE, nemo_opts.msg_ids.me);
		ret |= imsg_event_subscribe(nemo_opts.msg_ids.ccom, EVT_CCOM_BA, nemo_opts.msg_ids.me);
		if (ret < 0) {
			ERROR("Unable to subscribe for BULE/BA events");
			return -1;
		}
		ret = hooks_get_id("create-mh-bu");
		if (ret >= 0) {
			create_bu_id = (unsigned int)ret;
			hooks_add(create_bu_id, 1, nemo_mn_extend_bu);
		} else  {
			ERROR("Unable to register for create-mh-bu hook");
			return -1;
		}

		nemo_mn_init();

		if (nemo_opts.msg_ids.api >= 0) {
			memset(&api_rc, 0, sizeof(api_rc));
			api_rc.command = API_NEMO_GET_BUL;
			snprintf(api_rc.command_name, sizeof(api_rc.command_name), "NEMO_GET_BUL");
			api_rc.imsg_cmd = MSG_NEMO_API_GET_BUL;
			ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
			if (ret) {
				ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
			}
		}
		break;
	case CTRL_NODE_TYPE_CN:
		break;
	case CTRL_NODE_TYPE_NONE:
		//Avoid warning: warning: enumeration value ‘CTRL_NODE_TYPE_NONE’ not handled in switch
		break;
	}

	mho_mnp_init();

	return 0;
}

MIP6_MODULE_INIT(MSG_NEMO, SEQ_NEMO, nemo_module_init, nemo_main_opts, nemo_mnp_opts);

/*! \} */

