
+-------------------------------------------------+
| Patching and compiling iproute2 on Ubuntu 10.10 |
+-------------------------------------------------+

1. Tools you'll need
--------------------

sudo apt-get install build-essential fakeroot dpkg-dev devscripts
sudo apt-get build-dep iproute

2. Getting the source
---------------------

git clone git://git.kernel.org/pub/scm/linux/kernel/git/shemminger/iproute2.git
git tag -l
git checkout <VERSION>

3. Patching
-----------

patch -p1 -d iproute2  -i <MIP6D-NG-SRC-TREE>patches/iproute2/<VERSION>/*

4. Building package
-------------------

cd iproute2
dpkg-buildpackage -rfakeroot -uc -b

5. Installation
----------------

sudo dpkg -i iproute-<VERSION>-*.deb

