
+-----------------------------------------+
| Patching and compiling kernel on Ubuntu |
+-----------------------------------------+

1. Tools you'll need
--------------------

sudo apt-get install fakeroot build-essential crash kexec-tools makedumpfile kernel-wedge kernel-package
sudo apt-get build-dep linux
sudo apt-get install git-core libncurses5 libncurses5-dev libelf-dev asciidoc binutils-dev

2. Getting the kernel source
-----------------------------

git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
git tag -l
git checkout <VERSION>

3. Patching
-----------

patch -p1 -d linux-stable -i <MIP6D-NG-SRC-TREE>/patches/kernel/<VERSION>/*

4. Configuration
----------------

cp /boot/config-`uname -r` .config
yes '' | make oldconfig
make menuconfig

Networking support / Networking options
	Transformation sub policy support ==>	y/m

Networking support / Networking options / The IPv6 protocol
	IPv6: Mobility ==>						y/m
	IPv6: XFRM raw IPv6 tunnel ==>			y/m

5. Compiling kernel
-------------------

cd linux-stable
make clean
make -j `getconf _NPROCESSORS_ONLN` deb-pkg LOCALVERSION=-mip6dng

6. Installing the kernel
------------------------

sudo dpkg -i linux-image-<VERSION>-*_i386.deb
sudo dpkg -i linux-headers-<VERSION>-*_i386.deb

