
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/cfg.h>
#include <module.h>
#include <main.h>
#include <cfg.h>

/*!
 * \brief Configuration options of core
 */
cfg_opt_t main_opts[] = {
	/*! Log level 	*/		CFG_INT("loglevel", 0, CFGF_NONE),
	/*! Log file 	*/		CFG_STR("logfile", "", CFGF_NONE),
	/*! Log file size*/		CFG_INT("logsize", 1024*1024*1024, CFGF_NONE)
};

/*!
 * \brief Size of configure options of core
 */
size_t main_opts_size = sizeof(main_opts);

/*!
 * \brief Parse main configure options
 *
 * This function parses the configure options of core
 * and stores them in the main_options structure
 * \param cfg Confuse handler
 * \param options Core options structure (output)
 */
void main_config_load(cfg_t * cfg, struct main_options * options)
{
	long int num;
	char * str;

	num = cfg_getint(cfg, "loglevel");
	if (num == LERR || num == LINF || num == LDBG)
		options->loglevel = (unsigned int)num;
	else
		options->loglevel = LERR;
	str = cfg_getstr(cfg, "logfile");
	if (str != NULL && strlen(str) > 0) {
		if (options->logfile != NULL)
			free(options->logfile);
		options->logfile = strdup(str);
	}
	num = cfg_getint(cfg, "logsize");
	if (num > 0)
		options->logsize = (unsigned int)num;
}

/*! \} */

