
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core
 * \{
 */

#ifndef MIP6D_NG_SRC_MAIN_H
#define MIP6D_NG_SRC_MAIN_H

/*!
 * \brief Main options
 */
struct main_options {
	/*! Config file path 	*/	char * config;
	/*! Modules directory 	*/	char * modules;
	/*! Log level 			*/	unsigned int loglevel;
	/*! Log file 			*/	char * logfile;
	/*! Log file size 		*/	unsigned int logsize;
	/*! Exit hook id 		*/	unsigned int exit_hook_id;
	/*! Inited hook id 		*/	unsigned int inited_hook_id;
	/*! Started hook id 	*/	unsigned int started_hook_id;
};

#endif  /* MIP6D_NG_SRC_MAIN_H */

/*! \} */

