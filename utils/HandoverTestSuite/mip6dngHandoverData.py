
import datetime
import os 

class Data:

	INPUT = 0.0
	OUTPUT = 0.2

	def __init__(self):
		now = datetime.datetime.today()
		ts = now.strftime('%s')

		try:
			self.startTs = int(ts)
		except ValueError:
			self.startTs = 0

		self.filename = '/tmp/mip6dngHandoverTest-' + ts + '.dat'

		self.fh = open(self.filename, 'w')

	def close(self):
		self.fh.close()
		self.fh = None
		os.system('cp %s mip6dngHandoverTest.dat' % self.filename)
		os.remove(self.filename)

	def add(self, ts, tmsec, iface_id, direction):
		if self.fh != None:
			self.fh.write('%d.%d\t%f\n' % (ts - self.startTs, tmsec, iface_id + direction))

	def getRel(self, ts):
		return ts - self.startTs


