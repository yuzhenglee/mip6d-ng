
import threading
import os
import subprocess
import re
import signal
import shlex

class PingThread(threading.Thread):

	def __init__(self, plot, bids, host):
		self.plot = plot
		self.bids = bids
		self.host = host
		self.bidsStr = ''
		if len(self.bids) > 0:
			self.bidsStr = ' -b %d' % self.bids[0]
		if len(self.bids) > 1:
				for i in range(1,len(self.bids)):
					self.bidsStr = self.bidsStr + ',%d' % self.bids[i]

		self.last_ts = 0
		self.last_tmsec = 0
		self.switch = False
		
		self.stop = False
		self.hh = False

		threading.Thread.__init__(self)

	def doStop(self):
		self.stop = True
		self.p6.terminate()

	def run(self):
		pcmd = 'pingm6 -D%s %s' % (self.bidsStr, self.host)
		self.p6 = subprocess.Popen(shlex.split(pcmd), bufsize=-1, stdout=subprocess.PIPE)
		rex = re.compile(r'^\[([0-9]+)\.([0-9]+)] [0-9]+ bytes from [a-zA-Z0-9\.\-]+: icmp_seq=([0-9]+) ttl=[0-9]+ time=([0-9]+)[\.]{0,1}[0-9]* ms$')
		while True:
			line = self.p6.stdout.readline()
			if not line:
				break
			rgroups = rex.findall(line.strip())
			if len(rgroups) < 1: continue
			if len(rgroups[0]) < 4: continue
			try:
				ts = int(rgroups[0][0])
				tmsec = int(rgroups[0][1])
				seq = int(rgroups[0][2])
				delay = int(rgroups[0][3])
			except ValueError: continue
			print '[%d.%d] seq %d time %d' % (ts, tmsec, seq, delay)
		
			if self.switch == True:
				try:
					tsf = float('%d.%d' % (ts, tmsec))
					ltsf = float('%d.%d' % (self.last_ts, self.last_tmsec))
					delta = tsf - ltsf
					print 'V Handover delta t: %f' % delta
					self.plot.addVHandover(self.last_ts, self.last_tmsec, delta)
				except ValueError: pass
				self.switch = False

			if self.hh == True:
				try:
					tsf = float('%d.%d' % (ts, tmsec))
					ltsf = float('%d.%d' % (self.last_ts, self.last_tmsec))
					delta = tsf - ltsf
					print 'H Handover delta t: %f' % delta
					self.plot.addHHandover(self.last_ts, self.last_tmsec, delta)
				except ValueError: pass
				self.hh = False

			self.last_ts = ts
			self.last_tmsec = tmsec

	def nextBid(self):
		self.p6.send_signal(signal.SIGUSR1)

		self.switch = True

	def calculateHH(self):
		self.hh = True
