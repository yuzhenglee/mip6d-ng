
import os
import datetime

class Plot:

	def __init__(self, data):
		self.data = data

		self.template = 'mip6dngHandoverGnuplotTemplate.gpl'
		
		self.fh = open('/tmp/handovers.gpl',  'w')
		self.fr = open('/tmp/rectangles.gpl', 'w')
		self.rnum = 0

		self.cifStartTs = [ 0, 0, 0, 0 ]

		self.rstyle = [ 'fc rgb "#ffcc66" fs solid 0.15 noborder', 
				   'fc rgb "#ff9966" fs solid 0.15 noborder',
				   'fc rgb "#66cc66" fs solid 0.15 noborder',
				   'fc rgb "#669966" fs solid 0.15 noborder' ]

	def addVHandover(self, ts, tmsec, delta):
		relTs = self.data.getRel(ts)
		relTmsec = int(tmsec / 10)

		handover = 'set label "V Handover\\n{/Symbol D}t = %4.2f s" at %d.%d,3.8 center font "Times,12"\nset arrow from %d.%d,3.1 to %d.%d,0.5 as 1\n' % (delta, relTs, relTmsec, relTs, relTmsec, relTs, relTmsec)

		self.fh.write(handover)

	def addHHandover(self, ts, tmsec, delta):
		relTs = self.data.getRel(ts)
		relTmsec = int(tmsec / 10)

		handover = 'set label "H Handover\\n{/Symbol D}t = %4.2f s" at %d.%d,3.8 center font "Times,12"\nset arrow from %d.%d,3.1 to %d.%d,0.5 as 1\n' % (delta, relTs, relTmsec, relTs, relTmsec, relTs, relTmsec)

		self.fh.write(handover)

	def startConnection(self, cid):
		now = datetime.datetime.today()
		ts = int(now.strftime('%s'))

		self.cifStartTs[cid] = ts
		
		print 'Start connection with ID %d at %d' % (cid, ts)

	def stopConnection(self, cid):
		now = datetime.datetime.today()
		ts = int(now.strftime('%s'))

		self.rnum = self.rnum + 1
		num = self.rnum
		relTs1 = self.data.getRel(self.cifStartTs[cid])
		relTs2 = self.data.getRel(ts)

		if cid % 2 == 1:
			y1 = 0.7
			y2 = 1.5
		else:
			y1 = 1.7
			y2 = 2.5

		rectangle = 'set obj %d rect from %d.0,%f to %d.0,%f %s\n' % (num, relTs1, y1, relTs2, y2, self.rstyle[cid])

		self.fr.write(rectangle)
		
		print 'Stopping connection with ID %d at %d (start at %d)' % (cid, ts, self.cifStartTs[cid])

	def finish(self):
		self.fh.close()
		self.fr.close()

		self.data.close()
		os.system('gnuplot ' + self.template)


