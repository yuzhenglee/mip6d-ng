
import SocketServer
import os
import re

class RaHandler(SocketServer.BaseRequestHandler):

	def manageRule(self, status):
		if status == 'disable':
			os.system('ip6tables -A OUTPUT -j DROP')
			print 'ip6tables -A OUTPUT -j DROP'
			os.system('ip6tables -A FORWARD -j DROP')
			print 'ip6tables -A FORWARD -j DROP'
		elif status == 'enable':
			p = os.popen('ip6tables -L OUTPUT | grep "^DROP[ \t]\+all[ \t]\+anywhere[ \t]\+anywhere[ \t]*$"','r')
			while 1:
				line = p.readline()
				if not line: break
				os.system('ip6tables -D OUTPUT -j DROP')
				print 'ip6tables -D OUTPUT -j DROP'
			p = os.popen('ip6tables -L FORWARD | grep "^DROP[ \t]\+all[ \t]\+anywhere[ \t]\+anywhere[ \t]*$"','r')
			while 1:
				line = p.readline()
				if not line: break
				os.system('ip6tables -D FORWARD -j DROP')
				print 'ip6tables -D FORWARD -j DROP'

		print '--- Result ---'
		os.system('ip6tables -L OUTPUT')
		os.system('ip6tables -L FORWARD')
		print '---   --   ---'

	def handle(self):
		while True:
			self.data = self.request.recv(1024).strip()
			if len(self.data) == 0:
				print "{} closed".format(self.client_address[0])
				break
			else:
				print "{} wrote: {}".format(self.client_address[0], self.data)
				self.manageRule(self.data.strip())

			self.request.sendall(self.data)

