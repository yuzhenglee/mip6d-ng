
import SocketServer

class RaServer(SocketServer.TCPServer):

	def setAcceptedClient(self, acceptedCli):
		self.acceptedCli = acceptedCli

	def verify_request(self, request, client_address):
		if hasattr(self, 'acceptedCli'):
			if self.acceptedCli != client_address[0]:
				print 'Rejected connection from %s' % client_address[0]
				return False
		return True

