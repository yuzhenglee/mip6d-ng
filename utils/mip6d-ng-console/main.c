/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <arpa/inet.h>

#include "libmip6.h"

static void usage(char * bin)
{
    fprintf(stderr, "Usage: %s [-a ADDRESS] [-p PORT] COMMAND OPTIONS\n", bin);
	fprintf(stderr, "    ADDRESS is the IPv6 address of mip6d-ng API\n");
	fprintf(stderr, "    PORT    is the IPv6 TCP port number of mip6d-ng API\n");
	fprintf(stderr, "    COMMAND is the mip6d-ng API command (see below)\n");
	fprintf(stderr, "    OPTIONS are options for the given COMMAND\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "COMMANDs:\n");
	fprintf(stderr, "    get-bul (no OPTIONS)\n");
	fprintf(stderr, "        Get the Binding Update List (on Mobile Node)\n");
	fprintf(stderr, "    get-bc  (no OPTIONS)\n");
	fprintf(stderr, "        Get the Binding Cache (on Home Agent)\n");
	fprintf(stderr, "    get-hoa  (no OPTIONS)\n");
	fprintf(stderr, "        Get the Home Address (on Mobile Node)\n");
	fprintf(stderr, "    get-haa  (no OPTIONS)\n");
	fprintf(stderr, "        Get Address of the Home Agent (on Mobile Node & Home Agent)\n");
	fprintf(stderr, "    flow-reg SRC_IPV6_ADDR SRC_PORT DST_IPV6_ADDR DST_PORT <tcp|udp|icmp6> BID\n");
	fprintf(stderr, "        Register flow into mip6d-ng\n");
	fprintf(stderr, "    flow-up  FLOW_ID BID\n");
	fprintf(stderr, "        Update previously registered flow in mip6d-ng\n");
	fprintf(stderr, "    flow-del FLOW_ID\n");
	fprintf(stderr, "        Delete previously registered flow from mip6d-ng\n");
	fprintf(stderr, "    log      MESSAGE\n");
	fprintf(stderr, "        Send MESSAGE message to mip6d-ng log\n");
	fprintf(stderr, "\n");
}

static char * libmip6_str_error[] = {
	[LIBMIP6_OK]			= "Success",
	[LIBMIP6_UNKNOWN_MOD]	= "Unknown module",
	[LIBMIP6_UNKNOWN_CMD]	= "Unknown command",
	[LIBMIP6_INVALID]		= "Invalid argument or parameter",
	[LIBMIP6_NOMEM]			= "Out of memory",
	[LIBMIP6_CONN]			= "Connection problem",
	[LIBMIP6_DISCONN]		= "Unable to disconnect",
	[LIBMIP6_SEND]			= "Send error",
	[LIBMIP6_RECV]			= "Receive error",
	[LIBMIP6_TOUT]			= "Timeout",
	[LIBMIP6_MESSAGE]		= "Invalid message format (Drop?)",
	[LIBMIP6_NOANSW]		= "No answer",
	[LIBMIP6_UNKNOWN]		= "Unknown error"
};

static void get_libmip6_error(int ret) 
{
	if (ret < 0) 
		fprintf(stderr, "ERROR: %s (%d)\n", libmip6_str_error[-ret], ret);
	else if (ret > 0)
		fprintf(stderr, "ERROR: %s (%d)\n", libmip6_str_error[ret], ret);
}

typedef int (* cmd_fn_t)(struct libmip6h *, int, char **);

static int get_bul(struct libmip6h * h, int r_argc, char * r_argv[])
{
	int ret;
	struct bule {
		struct libmip6_bule bule;
		uint16_t bid;
		uint8_t prio;
		struct in6_addr mnp;
		uint8_t mnp_len;
	} * bul = NULL;
	unsigned int bul_len = 0;
	struct libmip6_bule * bul_data = NULL;
	struct libmip6_bule_mcoa * bul_mcoa = NULL;
	struct libmip6_bule_nemo * bul_nemo = NULL;
	unsigned int bul_len_data = 0;
	unsigned int bul_len_mcoa = 0;
	unsigned int bul_len_nemo = 0;
	int i;
	char dest[INET6_ADDRSTRLEN + 1];
	char hoa[INET6_ADDRSTRLEN + 1];
	char coa[INET6_ADDRSTRLEN + 1];
	struct in6_addr empty;
	char mnp[INET6_ADDRSTRLEN + 1];

	memset(&empty, 0, sizeof(empty));

	ret = data_get_bul(h, &bul_data, &bul_len_data);
	if (ret) {
		get_libmip6_error(ret);
		return ret;
	}

	bul = malloc(bul_len_data * sizeof(struct bule));
	bul_len = bul_len_data;
	if (bul == NULL) {
		fprintf(stderr, "ERROR: Not enough memory\n");
	}
	memset(bul, 0, bul_len_data * sizeof(struct bule));

	for (i = 0; i < bul_len_data; i++) {
		memcpy(&bul[i].bule, &bul_data[i], sizeof(struct libmip6_bule));
	}

	ret = mcoa_get_bul(h, &bul_mcoa, &bul_len_mcoa);
	if (ret) {
		if (ret == -LIBMIP6_UNKNOWN_MOD) {
			bul_len_mcoa = 0;
		} else {
			get_libmip6_error(ret);
			return ret;
		}
	}

	for (i = 0; i < bul_len_mcoa; i++) {
		if (memcmp(&bul[i].bule, &bul_mcoa[i], sizeof(struct libmip6_bule)) == 0) {
			bul[i].bid = bul_mcoa[i].bid;
			bul[i].prio = bul_mcoa[i].prio;
		}
	}

	ret = nemo_get_bul(h, &bul_nemo, &bul_len_nemo);
	if (ret) {
		if (ret == -LIBMIP6_UNKNOWN_MOD) {
			bul_len_nemo = 0;
		} else {
			get_libmip6_error(ret);
			return ret;
		}
	}

	for (i = 0; i < bul_len_nemo; i++) {
		if (memcmp(&bul[i].bule, &bul_nemo[i], sizeof(struct libmip6_bule)) == 0) {
			memcpy(&bul[i].mnp, &bul_nemo[i].mnp, sizeof(struct in6_addr));
			bul[i].mnp_len = bul_nemo[i].mnp_len;
		}
	}

	for (i = 0; i < bul_len; i++) {
		inet_ntop(AF_INET6, &bul[i].bule.dest, dest, sizeof(dest));
		inet_ntop(AF_INET6, &bul[i].bule.hoa, hoa, sizeof(hoa));
		inet_ntop(AF_INET6, &bul[i].bule.coa, coa, sizeof(coa));
		printf("--- %d ---\n", i);
		printf("  dest %s\n", dest);
		printf("  hoa  %s\n", hoa);
		printf("  coa  %s\n", coa);
		if (bul[i].mnp_len > 0 && memcmp(&empty, &bul[i].mnp, sizeof(struct in6_addr)) != 0) {
			inet_ntop(AF_INET6, &bul[i].mnp, mnp, sizeof(mnp));
			printf("  mnp  %s/%u\n", mnp, bul[i].mnp_len);
		}
		printf("  lifetime %u remaining %u last BU %u\n", bul[i].bule.lifetime, bul[i].bule.lifetime_rem, (unsigned int)bul[i].bule.last_bu);
		printf("  flags %08X\n", bul[i].bule.flags);
		printf("  ifindex %d", bul[i].bule.ifindex);
		if (bul[i].bid > 0) {
			printf(" bid %u preference %u", bul[i].bid, bul[i].prio);
		}
		printf("\n");
	}

	if (bul_data != NULL)
		ret = data_free_bul(h, bul_data);
	if (bul_mcoa != NULL)
		ret = mcoa_free_bul(h, bul_mcoa);
	if (bul_nemo != NULL)
		ret = nemo_free_bul(h, bul_nemo);
	
	free(bul);

	return 0;
}

static int get_bc(struct libmip6h * h, int r_argc, char * r_argv[])
{
	int ret;
	struct bce {
		struct libmip6_bce bce;
		uint16_t bid;
		uint8_t prio;
		struct in6_addr mnp;
		uint8_t mnp_len;
	} * bc = NULL;
	unsigned int bc_len = 0;
	struct libmip6_bce * bc_data = NULL;
	struct libmip6_bce_mcoa * bc_mcoa = NULL;
	struct libmip6_bce_nemo * bc_nemo = NULL;
	unsigned int bc_len_data = 0;
	unsigned int bc_len_mcoa = 0;
	unsigned int bc_len_nemo = 0;
	int i;
	char hoa[INET6_ADDRSTRLEN + 1];
	char coa[INET6_ADDRSTRLEN + 1];
	struct in6_addr empty;
	char mnp[INET6_ADDRSTRLEN + 1];

	memset(&empty, 0, sizeof(empty));

	ret = data_get_bc(h, &bc_data, &bc_len_data);
	if (ret) {
		get_libmip6_error(ret);
		return ret;
	}

	bc = malloc(bc_len_data * sizeof(struct bce));
	bc_len = bc_len_data;
	if (bc == NULL) {
		fprintf(stderr, "ERROR: Not enough memory\n");
	}
	memset(bc, 0, bc_len_data * sizeof(struct bce));

	for (i = 0; i < bc_len_data; i++) {
		memcpy(&bc[i].bce, &bc_data[i], sizeof(struct libmip6_bce));
	}

	ret = mcoa_get_bc(h, &bc_mcoa, &bc_len_mcoa);
	if (ret) {
		if (ret == -LIBMIP6_UNKNOWN_MOD) {
			bc_len_mcoa = 0;
		} else {
			get_libmip6_error(ret);
			return ret;
		}
	}

	for (i = 0; i < bc_len_mcoa; i++) {
		if (memcmp(&bc[i].bce, &bc_mcoa[i], sizeof(struct libmip6_bce)) == 0) {
			bc[i].bid = bc_mcoa[i].bid;
			bc[i].prio = bc_mcoa[i].prio;
		}
	}

	ret = nemo_get_bc(h, &bc_nemo, &bc_len_nemo);
	if (ret) {
		if (ret == -LIBMIP6_UNKNOWN_MOD) {
			bc_len_nemo = 0;
		} else {
			get_libmip6_error(ret);
			return ret;
		}
	}

	for (i = 0; i < bc_len_nemo; i++) {
		if (memcmp(&bc[i].bce, &bc_nemo[i], sizeof(struct libmip6_bce)) == 0) {
			memcpy(&bc[i].mnp, &bc_nemo[i].mnp, sizeof(struct in6_addr));
			bc[i].mnp_len = bc_nemo[i].mnp_len;
		}
	}


	for (i = 0; i < bc_len; i++) {
		inet_ntop(AF_INET6, &bc[i].bce.hoa, hoa, sizeof(hoa));
		inet_ntop(AF_INET6, &bc[i].bce.coa, coa, sizeof(coa));
		printf("--- %d ---\n", i);
		printf("  hoa  %s\n", hoa);
		printf("  coa  %s\n", coa);
		if (bc[i].mnp_len > 0 && memcmp(&empty, &bc[i].mnp, sizeof(struct in6_addr)) != 0) {
			inet_ntop(AF_INET6, &bc[i].mnp, mnp, sizeof(mnp));
			printf("  mnp  %s/%u\n", mnp, bc[i].mnp_len);
		}
		printf("  lifetime %u flags %08X", bc[i].bce.lifetime, bc[i].bce.flags);
		if (bc[i].bid > 0) {
			printf(" bid %u preference %u", bc[i].bid, bc[i].prio);
		}
		printf("\n");
	}

	if (bc_data != NULL)
		ret = data_free_bc(h, bc_data);
	if (bc_mcoa != NULL)
		ret = mcoa_free_bc(h, bc_mcoa);
	if (bc_nemo != NULL)
		ret = nemo_free_bc(h, bc_nemo);
	
	free(bc);

	return 0;
}

static int get_hoa(struct libmip6h * h, int r_argc, char * r_argv[])
{
	int ret;
	struct in6_addr hoa_p;
	char hoa[INET6_ADDRSTRLEN + 1];

	ret = ctrl_get_hoa(h, &hoa_p);
	if (ret) {
		get_libmip6_error(ret);
		return ret;
	}
	
	inet_ntop(AF_INET6, &hoa_p, hoa, sizeof(hoa));
	printf("HoA %s\n", hoa);
	
	return 0;
}

static int get_haa(struct libmip6h * h, int r_argc, char * r_argv[])
{
	int ret;
	struct in6_addr haa_p;
	char haa[INET6_ADDRSTRLEN + 1];

	ret = ctrl_get_haa(h, &haa_p);
	if (ret) {
		get_libmip6_error(ret);
		return ret;
	}
	
	inet_ntop(AF_INET6, &haa_p, haa, sizeof(haa));
	printf("HA %s\n", haa);
	
	return 0;
}

static int flowb_reg(struct libmip6h * h, int r_argc, char * r_argv[])
{
	int ret;
	int i;
	unsigned long ul;
	char * ptr, * end;
	struct in6_addr src, dst;
	uint8_t splen, dplen;
	uint16_t sport, dport;
	uint32_t proto;
	uint16_t bid;

	if (r_argc < 6) {
		fprintf(stderr, "Missing options. See help for details.\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0, ptr = NULL; i < strlen(r_argv[0]); i++) {
		if (i == '/' && strlen(r_argv[0]) > i+1) {
			ptr = &r_argv[0][i+1];
			r_argv[0][i] = '\0';
		}
	}
	ret = inet_pton(AF_INET6, r_argv[0], &src);
	if (ret != 1) {
		fprintf(stderr, "Invalid option: SRC_IPV6_ADDR.\n");
		exit(EXIT_FAILURE);
	}
	if (ptr) {
		ul = strtoul(ptr, &end, 10);
		if (ptr == end) {
			fprintf(stderr, "Invalid option: SRC_IPV6_ADDR.\n");
			exit(EXIT_FAILURE);
		}
		splen = ul;
	} else {
		splen = 128;
	}

	ul = strtoul(r_argv[1], &end, 10);
	if (ptr == end) {
		fprintf(stderr, "Invalid option: SRC_PORT.\n");
		exit(EXIT_FAILURE);
	}
	sport = ul;

	for (i = 0, ptr = NULL; i < strlen(r_argv[2]); i++) {
		if (i == '/' && strlen(r_argv[2]) > i+1) {
			ptr = &r_argv[0][i+1];
			r_argv[0][i] = '\0';
		}
	}
	ret = inet_pton(AF_INET6, r_argv[2], &dst);
	if (ret != 1) {
		fprintf(stderr, "Invalid option: DST_IPV6_ADDR.\n");
		exit(EXIT_FAILURE);
	}
	if (ptr) {
		ul = strtoul(ptr, &end, 10);
		if (ptr == end) {
			fprintf(stderr, "Invalid option: DST_IPV6_ADDR.\n");
			exit(EXIT_FAILURE);
		}
		dplen = ul;
	} else {
		dplen = 128;
	}

	ul = strtoul(r_argv[3], &end, 10);
	if (ptr == end) {
		fprintf(stderr, "Invalid option: DST_PORT.\n");
		exit(EXIT_FAILURE);
	}
	dport = ul;

	if (strcmp(r_argv[4], "tcp") == 0) 
		proto = IPPROTO_TCP;
	else if (strcmp(r_argv[4], "udp") == 0) 
		proto = IPPROTO_UDP;
	else if (strcmp(r_argv[4], "icmp6") == 0) 
		proto = IPPROTO_ICMPV6;
	else {
		fprintf(stderr, "Invalid option: PROTO.\n");
		exit(EXIT_FAILURE);
	}

	ul = strtoul(r_argv[5], &end, 10);
	if (ptr == end) {
		fprintf(stderr, "Invalid option: BID.\n");
		exit(EXIT_FAILURE);
	}
	bid = ul;

	ret = flowb_register(h, &src, splen, sport, &dst, dplen, dport, proto, bid, 1, NULL);
	if (ret < 0) {
		get_libmip6_error(ret);
		return ret;
	}
	printf("MIP6D-NG FlOW ID: %d\n", ret);
	
	return 0;
}

static int flowb_up(struct libmip6h * h, int r_argc, char * r_argv[])
{
	int ret;
	unsigned long ul;
	char * end;
	unsigned int flow_id;
	uint16_t bid;

	if (r_argc < 2) {
		fprintf(stderr, "Missing options. See help for details.\n");
		exit(EXIT_FAILURE);
	}

	ul = strtoul(r_argv[0], &end, 10);
	if (end == r_argv[0]) {
		fprintf(stderr, "Invalid option: FLOW_ID.\n");
		exit(EXIT_FAILURE);
	}
	flow_id = ul;

	ul = strtoul(r_argv[1], &end, 10);
	if (end == r_argv[1]) {
		fprintf(stderr, "Invalid option: BID.\n");
		exit(EXIT_FAILURE);
	}
	bid = ul;

	ret = flowb_update(h, bid, flow_id);
	if (ret) {
		get_libmip6_error(ret);
		return ret;
	}
	
	return 0;
}

static int flowb_del(struct libmip6h * h, int r_argc, char * r_argv[])
{
	int ret;
	unsigned long ul;
	char * end;
	unsigned int flow_id;

	if (r_argc < 1) {
		fprintf(stderr, "Missing options. See help for details.\n");
		exit(EXIT_FAILURE);
	}

	ul = strtoul(r_argv[0], &end, 10);
	if (end == r_argv[0]) {
		fprintf(stderr, "Invalid option: FLOW_ID.\n");
		exit(EXIT_FAILURE);
	}
	flow_id = ul;

	ret = flowb_delete(h, flow_id);
	if (ret) {
		get_libmip6_error(ret);
		return ret;
	}

	return 0;
}

static int do_log(struct libmip6h * h, int r_argc, char * r_argv[])
{
	int ret;

	if (r_argc < 1) {
		fprintf(stderr, "Missing option: MESSAGE\n");
		exit(EXIT_FAILURE);
	}

	ret = mip6dng_log(h, r_argv[0]);
	if (ret) {
		get_libmip6_error(ret);
		return ret;
	}
	
	return 0;
}

int main(int argc, char * argv[])
{
    int ret;
    struct libmip6h * h;
    struct in6_addr addr;
    int port;
    char * end;
	cmd_fn_t cmd_fn = NULL;
        
	static const char * const short_opts = "a:p:h";
	static const struct option long_opts[] = {
		{ "address",	required_argument,	NULL,	'a' },
		{ "port",		required_argument,	NULL,	'p' },
		{ "help",		no_argument,		NULL,	'h' },
		{ NULL,			no_argument,		NULL,	0	}
	};
	int opt;

	inet_pton(AF_INET6, "::1", &addr);
	port = 7777;

	for (opt = getopt_long(argc, argv, short_opts, long_opts, NULL); opt != -1; 
			opt = getopt_long(argc, argv, short_opts, long_opts, NULL)) {

		switch (opt) {
		case 'a':
			ret = inet_pton(AF_INET6, optarg, &addr);
		    if (ret != 1) {
				fprintf(stderr, "Invalid ADDRESS\n");
		        usage(argv[0]);
				exit(EXIT_FAILURE);
			}
			break;
		case 'p':
			port = strtoul(optarg, &end, 10);
		    if (end == optarg) {
				fprintf(stderr, "Invalid PORT\n");
		        usage(argv[0]);
				exit(EXIT_FAILURE);
			}
			break;
		case 'h':
			usage(argv[0]);
			exit(EXIT_FAILURE);
		}
	}
    
	if (strcmp(argv[optind], "get-bul") == 0) {
		cmd_fn = get_bul;
	} else if (strcmp(argv[optind], "get-bc") == 0) {
		cmd_fn = get_bc;
	} else if (strcmp(argv[optind], "get-hoa") == 0) {
		cmd_fn = get_hoa;
	} else if (strcmp(argv[optind], "get-haa") == 0) {
		cmd_fn = get_haa;
	} else if (strcmp(argv[optind], "flowb-reg") == 0) {
		cmd_fn = flowb_reg;
	} else if (strcmp(argv[optind], "flowb-up") == 0) {
		cmd_fn = flowb_up;
	} else if (strcmp(argv[optind], "flowb-del") == 0) {
		cmd_fn = flowb_del;
	} else if (strcmp(argv[optind], "log") == 0) {
		cmd_fn = do_log;
	} else {
		fprintf(stderr, "Invalid COMMAND\n");
        usage(argv[0]);
		exit(EXIT_FAILURE);
	}
    
    h = libmip6_init(&addr, port);
	if (h == NULL) {
		fprintf(stderr, "Invalid ADDRESS or PORT. Unable to connect.\n");
        usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	ret = cmd_fn(h, argc - optind - 1, (argc >= optind) ? &argv[optind+1] : NULL);
	
	ret = libmip6_close(h);
	get_libmip6_error(ret);
    
	return ret;
}

